<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['login'] = 'Usform/login';
$route['logout'] = 'Usform/logoutU';
$route['proyectos'] = 'proyectos';
//$route['proyecto'] = 'proyectos/proyecto';
$route['p/(:num)']='proyectos/proyectoinfo/$1';
$route['proyFil/(:num)']='proyectos/proyectoFiltro/$1';
$route['misproyectos'] = 'proyectos/alta';
//$route['proyecto/(:num)'] = "proyectos/infoproye/$1";
$route['glosario'] = 'proyectos/glosario';
$route['comienza'] = 'proyectos/comienza';
$route['eventos'] = 'welcome/calendario';
$route['agradecimiento'] = 'proyectos/agradecimiento';
$route['en']='eng';
$route['snake'] = 'proyectos/snake';
$route['j'] = "proyectos/guardar_p";
$route['s'] = "proyectos/puntaje";
//$route['shop'] = "proyectos/shop";
$route['conoce'] = 'welcome/reward';
$route['home'] = "proyectos/home";
$route['politica'] = 'welcome/politica';

$route['aevento'] = "proyectos/altaeventos";

$route['instalacion'] = 'comienza/instalacion';
$route['primer-programa'] = 'comienza/comienzaprimerp';
$route['descargawin'] = 'comienza/descargawin';
$route['descargaios'] = 'comienza/descargaios';
$route['briko-led/(:any)'] = 'comienza/moduloled/$1';
$route['briko-distancia/(:any)'] = 'comienza/modulodistancia/$1';
$route['briko-knob/(:any)'] = 'comienza/moduloknob/$1';
$route['briko-display/(:any)'] = 'comienza/modulodisplay/$1';
$route['briko-temperatura/(:any)'] = 'comienza/modulotemperatura/$1';
$route['briko-luz/(:any)'] = 'comienza/moduloluz/$1';
$route['briko-boton/(:any)'] = 'comienza/moduloboton/$1';
$route['briko-motor/(:any)'] = 'comienza/modulomotor/$1';
$route['briko-bocina/(:any)'] = 'comienza/modulobocina/$1';
$route['briko-relay/(:any)'] = 'comienza/modulorelay/$1';
$route['briko-servo/(:any)'] = 'comienza/moduloservo/$1';
$route['briko-maestro/(:any)'] = 'comienza/modulomaestro/$1';
$route['briko-extra/(:any)'] = 'comienza/moduloextra/$1';
$route['brikos'] = 'proyectos/modulos';

$route['referencia'] = 'referencia';
$route['referencia/Link_var/(:any)'] = 'referencia/Link_var/$1';
$route['aprende'] = 'referencia/aprende';
//$route['aprendeTemp'] = 'referencia/aprendeTemp';

//$route['proyectopage1'] = 'proyectopage/page1';
//$route['proyectopage2'] = 'proyectopage/page2';
$route['nuevo-proyecto'] = 'proyectopage/page3';
$route['proyecto/(:num)'] = 'proyectopage/page4/$1';
$route['edu/(:num)'] = 'proyectopage/page6/$1';
$route['contenido-edu/(:num)'] = 'proyectopage/page5/$1';


$route['blockscode'] = 'coding/blockscode';
$route['blockscode2/(:any)'] = 'coding/blockscode2/$1';
$route['blockscode3'] = 'coding/blockscode3';
$route['codepost'] = "coding/codepost";
$route['codecompile/(:any)'] = "coding/savepostdata/$1";

$route['eliminarP/(:num)'] = 'proyectopage/eliminarP/$1';
$route['usform/existe_us'] = 'usform/existe_us';
$route['usform/existe_email'] = 'usform/existe_email';
$route['conoce'] = 'welcome/reward';
$route['calendario'] = 'welcome/calendario';
$route['cal'] = 'welcome/calinfo';
$route['lanzamiento'] = 'welcome/lanzamiento';

$route['add-new-entry'] = 'blog/add_new_entry';
$route['add-new-category'] = 'blog/add_new_category';
$route['category/(:any)'] = "blog/category/$1";
$route['post/(:num)'] = 'blog/post/$1';

$route['reset-password']='usform/resetpasswd';
$route['reset_password_form/(:any)/(:any)']='usform/reset_password_form/$1/$2';
$route['404_override'] = 'welcome/error';
$route['translate_uri_dashes'] = FALSE;

//$route['home'] = 'welcome';
//$route['modificar/(:num)'] = 'proyectopage/modProy/$1';
//$route['proyectopage6'] = 'proyectopage/page6';
//$route['proyectoteaching'] = 'proyectopage/proyectoteaching';
//$route['proyectopageingles'] = 'proyectopage/proyectopageingles';
//$route['proyectoteaching2'] = 'proyectopage/proyectoteaching2';
//$route['proyectopageingles2'] = 'proyectopage/proyectopageingles2';
//$route['carritobk'] = 'proyectopage/carritobk';
//$route['carritobkteach'] = 'proyectopage/carritobkteach';
//$route['mrbk'] = 'proyectopage/mrbk';
//$route['mrbkteach'] = 'proyectopage/mrbkteach';
//$route['piano'] = 'proyectopage/piano';
//$route['pianoteach'] = 'proyectopage/pianoteach';
//$route['carritobk2'] = 'proyectopage/carritobk2';
//$route['carritobk2teach'] = 'proyectopage/carritobk2teach';
//$route['gorra'] = 'proyectopage/gorra';
//$route['gorrateach'] = 'proyectopage/gorrateach';
