<link rel="stylesheet" href="<?php echo base_url();?>assets/css/unbound.css" type="text/css" />
<?php $this->load->view('blog/header');?>
<body>

	<!-- header top starts-->

	<?php $data['nombre']="no";$arr=array('usuario'=>$data);$this->load->view('header',$arr);?>
	<!-- header top ends here -->
	<script src="<?php echo base_url()?>js/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>js/ckeditor/sample.js"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>css/ckeditor/samples.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/ckeditor/neo.css">	
	<!-- content starts -->
	<div id="content-outer" class="clear"><div id="content-wrapper" class="row">
	
		<!-- column-one -->
		<div id="content" class="columns large-12">
				
			
			<?php if( $query ): foreach($query as $post): ?>
			
			<h1><img class="flechitasPost" src="<?php echo base_url();?>images/flechitas.png"><a href="<?php echo base_url().'post/'.$post->entry_id;?>"><?php echo ucwords($post->entry_name);?></a></h1>
			<p class="post-info">Publicado por <a href="#"><?php $author = $this->ion_auth->user($post->author_id)->row(); echo ucfirst($author->username);?></a> | Categoria <?php $item = $this->blog_model->get_related_categories($post->entry_id); foreach($item as $category): ?><a href="<?php echo base_url().$category['slug'];?>"><?php echo $category['category_name'];?></a> <?php endforeach;?></p>
			<div class="entrada">
            	<?php echo $post->entry_body;?>
        	</div>
			<p class="postmeta">		
			<a href="<?php echo base_url().'post/'.$post->entry_id;?>" class="comments">Comentarios (<?php echo $post->comment_count;?>)</a> |			
			Publicado el 
			<span class="date"><?php echo mdate('%n %M %Y %H:%i:%s',human_to_unix($post->entry_date));?></span>	
			</p>
			<?php endforeach; ?>
			<?php endif;?>
			
			<?php $this->load->view('blog/comment');?>
			
			<a id="comments"></a>
			
			<?php if(validation_errors()){echo validation_errors('<p class="error">','</p>');} ?>
            <?php if($this->session->flashdata('message')){echo '<p class="success">'.$this->session->flashdata('message').'</p>';}?>

			<?php echo form_open('post/'.$post_id.'#comments');?>			
			<p>
			
			<?php if( ! $this->ion_auth->logged_in() ): ?>
			<h3>¡Tu opinion nos interesa, resgitrate en nuestra comunidad para dejar!</h3>
			
			<?php else:?>
			
			<input type="hidden" name="commentor" value="<?php echo $user->username;?>" type="text" size="30" />
			
			<input type="hidden" name="email" value="<?php echo $user->email;?>" type="text" size="30" />
			
			<label>Tus Comentarios</label>
			<div id="editor">
				<p>Escribe aqu&iacute; tus comentarios.</p>
			</div>
			<br />	
			<input type="hidden" name="comment" id="comment11"/>
			<input type="hidden" name="post_id" value="<?php echo $post_id;?>" />
			<input type="hidden" name="user_id" value="<?php echo ( isset($user) && $user ) ? $user->id : '';?>" />
			
			<input class="button" type="submit" id="submitcomm" value="Comentar"/>
				
			</p>		
			</form>	
			<?php endif;?>
			
		</div>
		
		
	
	<!-- contents end here -->	
	</div></div>

	<!-- footer starts here -->	
	<?php $this->load->view('footer');?>
	<!-- footer ends here -->
	<script>
	initSample();
	$(document).ready(function() {
	$( "#submitcomm" ).click(function()
	{
		var comentarios= CKEDITOR.instances.editor.getData();
		$('#comment11').val(comentarios);
		//alert(comentarios);
		return true;
	});});
</script>
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
</body>
</html>