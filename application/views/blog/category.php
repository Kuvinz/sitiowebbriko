<?php $this->load->view('blog/header');?>
<body>


	<!-- header top starts-->
	<?php $data['nombre']="no";$arr=array('usuario'=>$data);$this->load->view('header',$arr);?>
	<!-- header top ends here -->

	<!-- content starts -->
		<div id="content-wrapper" class="row">
			<!-- column-one -->
			<div id="content" class="columns large-12">	
			
			<?php foreach($category as $row):?>
			<h2><a href="<?php echo base_url().'category/'.$row->slug;?>"><?php echo ucwords($row->category_name);?></a> (<?php echo count($query);?>)</h2>
			<?php endforeach;?>
			
			<?php if( isset($query) && $query ): ?>
			<ul>
			<?php foreach($query as $post):?>
				<li><a href="<?php echo base_url().'post/'.$post->entry_id;?>"><?php echo $post->entry_name?></a></li>
			<?php endforeach; ?>
			</ul>
				
			<?php else: ?>
			<h3>No post yet!</h3>
			<?php endif;?>
			
		
	
	<!-- contents end here -->	
	</div></div>

	<!-- footer starts here -->	
	<?php $this->load->view('footer');?>
	<!-- footer ends here -->
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
</body>
</html>