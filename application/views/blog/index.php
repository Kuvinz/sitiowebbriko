<!-- header top starts-->
	<?php $data['nombre']="no";$arr=array('usuario'=>$data);$this->load->view('header',$arr);?>
	<!-- header top ends here -->
<?php $this->load->view('blog/header');?>
<body>

<?php include 'HTMLCutter.php';$wanted_count = 400; ?>
	
	
	<!-- content starts -->
		<div id="content-wrapper" class="row">
			<!-- column-one -->
			<div id="content" class="columns large-12">	
					<?php if( $posts ): foreach($posts as $post): ?>
					<h1><img class="flechitasPost" src="<?php echo base_url();?>images/flechitas.png"><a href="<?php echo base_url().'post/'.$post->entry_id;?>"><?php echo ucwords($post->entry_name);?></a></h1>
					
					<p class="post-info">Publicado por <a href="#"><?php $author = $this->ion_auth->user($post->author_id)->row(); echo ucfirst($author->username);?></a> | Categoria <?php $item = $this->blog_model->get_related_categories($post->entry_id); foreach($item as $category): ?><?php echo $category['category_name'];?> <?php endforeach;?></p>
					<div class="entrada">	
		            	<div class="row">
		            		<div class="columns large-4">		
				            	<img src="<?php echo base_url();?>upload/blog/<?php echo $post->entry_image?>">
							</div>
							<div class="columns large-8">
								<?php 
					            	$data=$post->entry_description;
									//$cutstrObj = new HtmlCutter();
									//$new_string = $cutstrObj->cut($data,$wanted_count);
									$base=base_url();
									echo $data." <br> ... <a style='color:blue' href='".$base."post/".$post->entry_id."' class='readmore'>Leer m&aacute;s</a>";
								?>
							</div>
						</div>	
					</div>	
					<p class="postmeta">		
					<a href="<?php echo base_url().'post/'.$post->entry_id;?>" class="readmore">Leer m&aacute;s</a> |
					<a href="<?php echo base_url().'post/'.$post->entry_id.'#comments';?>" class="comments">Comentarios (<?php echo $post->comment_count;?>)</a> |				
					<span class="date"><?php echo mdate('%n %M %Y %H:%i:%s',human_to_unix($post->entry_date));?></span>	
					</p>
					<?php endforeach; else: ?>
					<h2>Aun no hay Art&iacute;culos!</h2>
					<?php endif;?>

			</div>
			
	<!-- contents end here -->	
		</div>

	<!-- footer starts here -->	
	<?php $this->load->view('footer');?>
	<!-- footer ends here -->
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
</body>
</html>