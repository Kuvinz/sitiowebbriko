<br /><br />
<br /><br />

<?php $this->load->helper('form');?>
<?php echo form_open_multipart('proyectos/alta_proyectos');?>
<div class="row principal">
    <div class="columns small-4">
        <label for="nombre">Nombre</label>
        <input type="text" id="nombre" name="nombre" size="20" />
    </div>
    <div class="columns small-8">
        <label for="des">Descripción</label>
        <input type="text" id="des" name="descripcion" size="50" />
    </div>
</div>
<div class="row principal">  
    <div class="columns small-2">
        <label for="tiempo">Tiempo</label>
        <select name="tiempo" id="tiempo">
            <option value="1">1- 10 minutos</option>
            <option value="2">2- 30 minutos</option>
            <option value="3">3- 60 minutos</option>
            <option value="4">4- +60 minutos</option>
        </select>
        
    </div>
    <div class="columns small-2">
        <label for="dificultad">Dificultad</label>
        <select name="dificultad" id="dificultad">
            <option value="1">1-Facil</option>
            <option value="2">2-Intermedio</option>
            <option value="3">3-Dificil</option>
            <option value="4">4-Muy Dificil</option>
        </select>
    </div>
    <div class="columns small-4">
        <label for="video">Video</label>
        <input type="file"  class = "form-control" name="uploadfile[]" size="20" /><br />
        <input type="text" id="video" name="video" size="50" />
    </div>
    <div class="columns small-4">
        <label for="imagen">Imagen</label>
        <input type="text" id="imagen" name="imagen" size="20" />
    </div>
</div>

<div class="row principal">  
    <div class="columns large-8">
        <div class="row principal">
            <div class="columns small-4">
                <?php 
                    $i=0;    
                foreach($mods as $value)
                {
                    $des = ucwords($value->Nombre);
                    $des = ucwords(strtolower($des));
                    $i++;
                    if($i>3)continue;
                    ?>
                <div class="row principal">  
                    <div class="columns small-8">
                        <input style="float:left" value="<?php echo $value->ID_mod?>" type='CHECKBOX' 
                    name="lista_modulos[<?php echo $value->ID_mod?>]" 
                    id="lista_modulos[<?php echo $value->ID_mod?>]">
                        <label for="lista_modulos[<?php echo $value->ID_mod?>]" > <?php echo $des; ?></label>
                    </div>
                    <div class="columns small-3 end">
                        <label for="lista_p[<?php echo $value->ID_mod?>]" > Puerto</label>
                        <input  type='text' 
                    name="lista_p[<?php echo $value->ID_mod?>]" 
                    id="lista_p[<?php echo $value->ID_mod?>]">
                    </div>
                </div>
                <?php if($i==5)break; }?>
            </div>
            <div class="columns small-4">
                <?php
                $i=0;
                foreach($mods as $value)
                {
                    $des = ucwords($value->Nombre);
                    $des = ucwords(strtolower($des));
                    $i++;
                    if($i<4)continue;
                    if($i>6)continue;
                    ?>
                <div class="row principal">  
                    <div class="columns small-8">
                        <input style="float:left" value="<?php echo $value->ID_mod?>" type='CHECKBOX' 
                    name="lista_modulos[<?php echo $value->ID_mod?>]" 
                    id="lista_modulos[<?php echo $value->ID_mod?>]">
                        <label for="lista_modulos[<?php echo $value->ID_mod?>]" > <?php echo $des; ?></label>
                    </div>
                    <div class="columns small-3 end">
                        <label for="lista_p[<?php echo $value->ID_mod?>]" > Puerto</label>
                        <input  type='text' 
                    name="lista_p[<?php echo $value->ID_mod?>]" 
                    id="lista_p[<?php echo $value->ID_mod?>]">
                    </div>
                </div>
                    
                <?php  }?>
            </div>
            <div class="columns small-4">
                <?php
                $i=0;
                foreach($mods as $value)
                {
                    $des = ucwords($value->Nombre);
                    $des = ucwords(strtolower($des));
                    $i++;
                    if($i<7)continue;
                    ?>
                <div class="row principal">  
                    <div class="columns small-8">
                        <input style="float:left" value="<?php echo $value->ID_mod?>" type='CHECKBOX' 
                    name="lista_modulos[<?php echo $value->ID_mod?>]" 
                    id="lista_modulos[<?php echo $value->ID_mod?>]">
                        <label for="lista_modulos[<?php echo $value->ID_mod?>]" > <?php echo $des; ?></label>
                    </div>
                    <div class="columns small-3 end">
                        <label for="lista_p[<?php echo $value->ID_mod?>]" > Puerto</label>
                        <input  type='text' 
                    name="lista_p[<?php echo $value->ID_mod?>]" 
                    id="lista_p[<?php echo $value->ID_mod?>]">
                    </div>
                </div>
                    
                <?php  }?>
            </div>
        </div>  
    </div>
    <div class="columns large-4">
        <label for="codigo">Codigo</label>
        <textarea  cols="50" rows="10" name="codigo">

        </textarea> 
    </div>
</div>


<div class="row principal">  
    <div class="columns small-6">
        <input type="submit" value="upload" />
        <button type="reset" value="Reset">Reset</button>
    </div>
    <?php
    $url = 'https://www.youtube.com/embed/3SJIq2NYCIY';
    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
    $id = $matches[1];
    $width = '800px';
    $height = '450px';
    ?>
    <iframe id="ytplayer" type="text/html" width="<?php echo $width ?>" height="<?php echo $height ?>"
        src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3"
        frameborder="0" allowfullscreen></iframe> 
</div>




</form>
<script>
onload = function() {
    x = 500;
    y = 500;

    var canvas = document.getElementById('canvas1');
    if (! canvas || ! canvas.getContext) { return false; }
    canvas.width = x;
    canvas.height = y;
    var ctx = canvas.getContext('2d');
    var img1 = new Image();
    var img2 = new Image();
    img1.src = "http://briko.cc/assets/brikoNew/images/1.png";
    img2.src = "http://briko.cc/images/modulosindividuales/Botones.png";
    img1.onload = function() {
        ctx.drawImage(img1, 0, 50, x, y);
    }
    img2.onload = function() {
        ctx.drawImage(img2, 150, 100, 150, 120);
    }
}
</script>