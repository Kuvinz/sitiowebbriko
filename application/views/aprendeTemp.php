<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/referencia.css">

<!-- Creamos el grid principal-->
<div class="row"  id="divp">
  <div class="large-12 columns">
    
    <!-- Titulo -->
    <div class="row">
        <div class="small-9 columns small-offset-3" style="text-align:center;">
            <h1 style="padding-left:60px" class = 'text-justify' > Aprende a programar:</h1>
        </div>
    </div>
      
    <!-- tabs para entrar a diferentes funciones -->
    <div class="row">
        <div class="small-12 columns">
        <div id="listContainer">
            <ul id="expListaprende">
                <li>5-Estructura If...Else
                    <ul>
                            <p>La estructura "If...Else" es muy similar a la estructura "if". Lo que agrega esta nueva estructura es que si la condición no se cumple corre otro código. Un ejemplo es el siguiente: Si/if presiono el botón, prendo los leds, sino/else apago los leds.</p><br/>
                            <p>La estructura "If...Else" se utiliza de la siguiente forma:</p>
                            <!-- Creamos el editor de codigo-->
                                <div class="row">
                                    <div class="small-12 columns" >
                                        <div id="editor51" class="editoraprende">
if(la condicion){
    // Corre este código si
    // la condición se cumple.
}
else {
    // Corre este código si la condición
    // no se cumple.
}
                                        </div>
                                    </div>  
                                </div> 
                        <li>5.1-Prender los leds con un botón
                          <ul>
                            <p>En el ejemplo 4 solo se prendían los leds cuando presionabas el botón, pero no hacia nada si no estaba presionado. Entonces al momento de que presionas el botón se quedaban prendidos los leds aunque soltaras el botón.</p><br/>
                            <p>Ahora a ese ejemplo vamos a agregarle la parte de apagar los leds cuando no está presionado el botón utilizando el "if...else"</p><br/>
                            <p><strong>Ejemplo</strong></p>
                            <!-- Creamos el editor de codigo-->
                                <div class="row">
                                    <div class="small-12 columns" >
                                        <div id="editor52" class="editoraprende">
ledsbk luces(PORT1);
buttonsbk botones(PORT2);

code(){
    if(botones.read() == 1){
        luces.color(BLUE);      // Si el boton esta presionado se prenden los leds
    }
    else{
        luces.color(BLACK);     // Si no esta  presionado se apagan los leds
    }
}
                                        </div>
                                    </div>  
                                </div>
                              <p><strong>Reto: Hacer que cuando el valor del botón presionado es mayor a 3 que se prendan los leds si no que estén apagados.</strong>Tip: ver la referencia de como utilizar el comparador mayor en la referencia.</p>
                              <!-- Creamos el editor de codigo-->
                                <div class="row">
                                    <div class="small-10 columns" >
                                        <div id="editor52r" class="editoraprende">
                                        </div>
                                    </div>
                                    <div class="small-2 end  columns" >
                                        <button id="editor52resp" class="botonrespuesta">Respuesta</button>
                                    </div>  
                                </div>
                          </ul>
                        </li>
                        <li>5.2-Bocina activada por distancia
                        <ul>
                          <p>En este ejemplo introduciremos dos módulos nuevos: el módulo de distancia y el módulo de bocina. El módulo de distancia es un sensor infrarrojo que puede medir la distancia a la que esta de un objeto y el módulo de bocina permite generar sonido. Lo que haremos es que cuando el sensor detecte un objeto cercano que prenda y apaga la bocina como una alerta.</p>
                          <p>Primero, los módulos se declaran de la siguiente forma: <span class="codep"><span class="funct">distancebk</span></span> y <span class="codep"><span class="funct">buzzerbk</span></span>. Conectaremos el módulo de distancia al puerto 2 y el de bocina al puerto 3 y les damos un nombre en el programa como a cualquier módulo que queremos utilizar.</p>
                          <div class="panel">
                            <p><span class="funct">distancebk</span> sensor(<span class="param">PORT2</span>);<br/>
                              <span class="funct">buzzerbk</span> bocina(<span class="param">PORT3</span>);
                            </p> 
                          </div>
                          
                          
                          
                          <p>Ahora dentro de <span class="codep"><span class="funct">code</span>(){ }</span> escribimos lo siguiente:</p>
                          <div class="row">
                            <div class="small-12 columns" >
                                <div id="editor54" class="editoraprende">
if(sensor.read() < 20){
    bocina.beep(500,1000);
}
else{
    bocina.set(OFF);
}
                                </div>
                              </div>  
                          </div>
                          <p>El módulo de distancia tiene solamente una función: <span class="codep"><span class="funct">read</span>(){ }</span>. Con esta función leemos la distancia a la que se encuentra un objeto. Si no hay un objeto la función regresa 80, la distancia máxima que puede medir el módulo.</p>
                          <p>El modulo de bocina cuenta con varias funciones. En este caso utilizamos dos de ellas. La primera <span class="codep"><span class="funct">beep</span>()</span> hace que la bocina se prenda por 500 milisegundos y se apague por 1000 milisegundos hasta que le mandemos un nuevo comando. La siguiente función <span class="codep"><span class="funct">set</span>()</span> se usa para prender y apagar la bocina, escribiendo entre los paréntesis <span class="codep"><span class="param">ON</span></span> para prender la bocina u <span class="codep"><span class="param">OFF</span></span> para apagarla.</p>
                          <br><p>Ejemplo:</p>
                          <div class="row">
                            <div class="small-12 columns" >
                                <div id="editor55" class="editoraprende">
// Aqui se declaran los modulos que vas a utilizar
distancebk sensor(PORT2);
buzzerbk bocina(PORT3);

code() {
    // Aqui se escribe tu programa
    if (sensor.read() < 20) {
        bocina.beep(500, 1000);
    }
    else {
        bocina.set(OFF);
    }
}
                                  </div>
                                </div>  
                            </div>
                            <p><strong>Reto: Hacer que cuando el valor de distancia sea mayor a 50 la bocina se encienda.</strong>Tip: ver la referencia de como utilizar el comparador mayor en la referencia.</p>
                              <!-- Creamos el editor de codigo-->
                              <div class="row">
                                  <div class="small-10 columns" >
                                      <div id="editor55r" class="editoraprende">
                                      </div>
                                  </div>
                                  <div class="small-2 end  columns" >
                                      <button id="editor55resp" class="botonrespuesta">Respuesta</button>
                                  </div>  
                              </div>
                          </ul>
                        </li>
                    </ul>
                </li>
                <li>6-Estructura If...Else if...Else
                  
                    <ul>
                      <p>La estructura "If...Elseif...Else" agrega una tercera parte el "else if". Cuando la condición en el "if" no se cumple checa la condición en el "else if" si la condición se cumple corre el código dentro del "else if" si no corre lo que esta adentro del "else". Nota: En esta estructura solo se corre un bloque de código. Si la condición en el "if" se cumple se salta lo que esta en el "else if" y "else" aunque la condición en el "else if" sea cierta.</p>
                        <li>Prender los leds de 2 colores con los botones
                            <ul>
                                <p>Como ejemplo para este tipo de estructura vamos a prender los leds azules cuando el botón 1 está presionado, si no vamos a prender los leds verdes si el botón 2 está presionado y si no los vamos a apagar.</p><br/>
                                <p>Primero revisamos si el boton 1 está presionado:</p>
                                <div class="panel">
                                  <p><span class="funct">if</span>(botones.<span class="funct">read</span>() == 1){<br/>
                                    luces.<span class="funct">color</span>(<span class="param">BLUE</span>);  <span class="comment">// Si el boton 1 esta presionado se prenden los leds azules</span> <br/>
                                    }
                                  </p> 
                                </div>
                                <p>Si está condición se cumple el código se salta el "else if" y el "else" si no revisa el "else if"</p>
                                <div class="panel">
                                  <p><span class="funct">else if</span>(botones.<span class="funct">read</span>() == 2){<br/>
                                    luces.<span class="funct">color</span>(<span class="param">GREEN</span>);  <span class="comment">// Si el boton 2 esta presionada se prenden los leds verdes</span> <br/>
                                    }
                                  </p> 
                                </div>
                                <p>Por último si ninguna de las dos condiciones se cumple el código corre lo que esta dentro del bloque del "else"</p>
                                <div class="panel">
                                  <p><span class="funct">else</span>{<br/>
                                    luces.<span class="funct">color</span>(<span class="param">BLACK</span>);  <span class="comment">// Si no esta  presionado se apagan los leds</span> <br/>
                                    }
                                  </p> 
                                </div>
                                <p>Ejemplo:</p>
                                <div class="row">
                                  <div class="small-12 columns" >
                                      <div id="editor61" class="editoraprende">
ledsbk luces(PORT1);
buttonsbk botones(PORT2);

code(){
    if(botones.read() == 1){
        luces.color(BLUE);      // Si el boton esta presionado se prenden los leds azules
    }
    else if(botones.read() == 2){
        luces.color(GREEN);     // Si el boton 2 esta presionado y el "if" no se cumplio
                                // se prenden los leds verdes
    }
    else{
        luces.color(BLACK);     // Si no esta  presionado se apagan los leds
    }
}
                                      </div>
                                    </div>  
                                  </div>
                                  <p><strong>Reto: Agregar al código que cuando se presiona el boton 3 los leds se prendan rojos.</strong></p>
                                    <!-- Creamos el editor de codigo-->
                                    <div class="row">
                                        <div class="small-10 columns" >
                                            <div id="editor61r" class="editoraprende">
                                            </div>
                                        </div>
                                        <div class="small-2 end  columns" >
                                            <button id="editor61resp" class="botonrespuesta">Respuesta</button>
                                        </div>  
                                    </div>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li>7-Variables
                  <ul>
                    <p>Las variables se utilizan para guardar información dentro de un código. Puedes guardar un valor en una variable y después utilizar ese valor el alguna otra parte del código.</p>
                    <p>Existen varios tipos de variables pero la más utilizada en briko es el tipo de variable "int". Una variable "int" guarda valores enteros, es decir un numero que no tiene decimales, e incluye negativos, como 1,5,-50,0, 300 etc. Los valores máximos y mínimos que puede guardar una variable tipo "int" es de -32,768 a 32,767.</p>
                    <br/><p>Una variable "int" se declara de la siguiente forma: <span class="codep"><span class="funct">int</span> mivariable;</span> .Primero se escribe la palabra <strong>int</strong> después el nombre que le quieres dar a la variable, en este caso <strong>mivariable</strong>.</p>
                    <li>7.1-Crear una variable
                      <ul>
                        <p>Las variables se suelen declarar antes de <span class="codep"><span class="funct">code</span>(){</span> y es posible asignarles un valor inicial. En este ejemplo se observan varias formas de crear variables con o sin valores.</p>
                         <p>Ejemplo:</p>
                            <div class="row">
                              <div class="small-12 columns" >
                                  <div id="editor71" class="editoraprende">
buttonsbk botones(PORT1);
int mivariable, botonpresionado; // Se crean 2 variables tipo int sin valor inicial
int temp = 0; // Se crea una variable con valor igual a 0
int luz; // Se crea una variable sin valor

code(){
   botonpresionado = botones.read(); // La variable boton presionado guardara el valor que regrese
                                     // la funcion botones.read() es decir el boton presionado 
}
                                  </div>
                                </div>  
                              </div>
                        <p>Como se observa es posible declarar distintas variables en el mismo renglón separadas por comas ó en distintos renglones de forma individual.</p>  
                        <p><strong>Reto: Asigna el valor que lee el sensor de distancia a una variable tipo int.</strong></p>
                        <!-- Creamos el editor de codigo-->
                        <div class="row">
                            <div class="small-10 columns" >
                                <div id="editor71r" class="editoraprende">
                                </div>
                            </div>
                            <div class="small-2 end  columns" >
                                <button id="editor71resp" class="botonrespuesta">Respuesta</button>
                            </div>  
                        </div>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>8-For
                  <ul>
                    <p>Esta instrucción permite que se repita un bloque de código cierto numero de veces. Al terminar de repetirse el ciclo el código continua normalmente con lo que haya después del for. Para usar esta instrucción es necesario hacer uso de operadores de comparación y variables. Su estructura es la siguiente:</p>
                        <div class="panel">
                          <p><span class="funct">for</span>(<span class="param">variable</span>,<span class="param">comparación</span>,<span class="param">incremento/decremento</span>){<br/>
                            <span class="comment">// bloque de código que se repite</span><br/>
                            }
                          </p> 
                        </div>
                        <p>La variable la usaremos para guardar cierto valor, el valor usa en la comparación para determinar cuantas veces se repite el código y finalmente se incrementa o decrementa la variable para en algún momento cumplir con la comparación.</p>
                    <li>8.1-Prender los leds del 1 al 4 uno por uno cada segundo
                      <ul>
                        <p>En este caso usaremos lo siguiente para la instrucción for:</p>
                        <div class="row">
                          <div class="small-8 columns small-offset-4" style="margin-bottom:0;padding-bottom:0;" >
                              <table style="border-style:hidden;margin-bottom:0;padding-bottom:0;" >
                                  <tbody>
                                      <tr>
                                          <td>
                                              <p class = 'text-center' style="font-size:1.5em;" > Variable </p>
                                          </td>
                                          <td style="padding-right: 0px;"> 
                                              <p class = 'text-center' style="font-size:1.5em;" > contador</p>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <p class = 'text-center' style="font-size:1.5em;" > Comparación </p>
                                          </td>
                                          <td style="padding-right: 0px;"> 
                                              <p class = 'text-center' style="font-size:1.5em;" > contador < 5</p>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <p class = 'text-center' style="font-size:1.5em;" > Incremento </p>
                                          </td>
                                          <td style="padding-right: 0px;"> 
                                              <p class = 'text-center' style="font-size:1.5em;" > contador++ (aumenta en 1 el valor de contador) </p>
                                          </td>
                                      </tr>
                                  </tbody>  
                              </table>
                          </div>   
                        </div>
                        <p>En el ejemplo se usan los valores que obtiene <strong>contador</strong>, los cuales van del 1 al 4, para controlar las veces que se repite nuestro ciclo FOR y para determinar que led se encenderá. Al terminar el FOR los leds se apagan.</p>
                         <p>Ejemplo:</p>
                          <div class="row">
                            <div class="small-12 columns" >
                              <div id="editor81" class="editoraprende">
ledsbk luces(PORT1);
int contador;
code(){
 for ( contador = 1, contador < 5, contador++){
   luces.color(contador,BLUE);
   delay(1000);
 }
 luces.color(BLACK);
}
                              </div>
                            </div>  
                          </div>
                          <p>Se podría decir que la parte de <span class="codep">contador = 1</span> lo realiza solo una vez al entrar al ciclo, la comparación de <span class="codep">contador < 5</span> cuando inicia el ciclo y cada vez que este se repite, por último, <span class="codep">contador++</span> solo lo hace al terminar cada ciclo. A continuación tenemos lo que nuestro BK7 interpreta al entrar al FOR:</p>
                          <div class="row">
                            <div class="small-12 columns" >
                              <div id="editor82" class="editoraprende">
ledsbk luces(PORT1);
int contador;
code(){
 for ( contador = 1, 1 < 5, contador++){
   luces.color(1,BLUE);                                                
   delay(1000); // despues de esperar 1 segundo vuelve a iniciar el for
 }
 luces.color(BLACK);
}
                              </div>
                            </div>  
                          </div>
                          <p>Al entrar por primera vez asigna el valor de "1" a <strong>contador</strong> y nuestra comparación lee <span class="codep">1 < 5</span> lo cual es verdadero por lo que se enciende el led 1 de color azul, se espera 1 segundo y después se activa el efecto de <span class="codep">contador++</span> haciendo que ahora contador sea igual a 2 y vuelve al inicio del FOR:</p>
                          <div class="row">
                            <div class="small-12 columns" >
                              <div id="editor83" class="editoraprende">
ledsbk luces(PORT1);
int contador;
code(){
 for ( contador = 2, 2 < 5, contador++){
   luces.color(2,BLUE);                                                
   delay(1000); // despues de esperar 1 segundo vuelve a iniciar el for
 }
 luces.color(BLACK);
}
                              </div>
                            </div>  
                          </div>
                          <p>Después de repetir el ciclo nuestro contador ya es igual a 2 por lo tanto nuestra comparación lee <span class="codep">2 < 5</span>  que al ser verdadero hace que se vuelva a hace el código dentro de las { }, es decir, se enciende ahora el led 2 azul, se espera 1 segundo y se vuelve a activar <span class="codep">contador++</span> haciendo contador = 3.</p>
                          <p>Supongamos que nuestro for ya se repitió para cumplir las condiciones <span class="codep">3 < 5</span>  y acabamos de terminar el ciclo para <span class="codep">4 < 5</span> . En este caso ya estaría encendido el led 3 y se acabaría de encender el led 4, habría pasado un segundo y finalmente <span class="codep">contador++</span> habrá hecho que contador sea igual a 5 y volviendo al inicio del FOR asi:</p>
                          <div class="row">
                            <div class="small-12 columns" >
                              <div id="editor84" class="editoraprende">
ledsbk luces(PORT1);
int contador;
code(){
 for ( contador = 5, 5 < 5, contador++){
   luces.color(5,BLUE);                                                
   delay(1000); // después de esperar 1 segundo vuelve a iniciar el for
 }
 luces.color(BLACK);
}
                              </div>
                            </div>  
                          </div>
                          <p>En esta parte nuestra comparación <span class="codep">5 < 5</span> es falsa por lo que no se realiza el código dentro de las { } y nuestro BK7 no enciende el led 5, no se espera un segundo y por lo tanto termina el ciclo. Al brincarse esa parte nuestros leds se apagaran por acción de la función <span class="codep">luces.<span class="funct">color</span>(<span class="param">BLACK</span>);</span> y nuestro código volverá a empezar haciendo que se repita el FOR, nuestro contador volverá a valer 1 y se repetirá el ciclo FOR.</p>
                          <br/><p><strong>Reto: Asigna el valor que lee el sensor de distancia a una variable tipo int.</strong></p>
                        <!-- Creamos el editor de codigo-->
                        <div class="row">
                            <div class="small-10 columns" >
                                <div id="editor84r" class="editoraprende">
                                </div>
                            </div>
                            <div class="small-2 end  columns" >
                                <button id="editor84resp" class="botonrespuesta">Respuesta</button>
                            </div>  
                        </div>
                        <p><strong>Reto: Usar 2 for para prender todos los leds uno por uno y luego apagarlos uno por uno</strong></p>
                        <!-- Creamos el editor de codigo-->
                        <div class="row">
                            <div class="small-10 columns" >
                                <div id="editor84r1" class="editoraprende">
                                </div>
                            </div>
                            <div class="small-2 end  columns" >
                                <button id="editor84resp1" class="botonrespuesta">Respuesta</button>
                            </div>  
                        </div>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>9-While
                  <ul>
                    <p>La estructura while permite que un bloque de código se repita infinitamente mientras se cumpla una condición, de ésta forma solo se hará lo que este dentro del while ignorando el resto del código, si la condición no se cumple o deja de cumplirse se ignora lo que este dentro de nuestro while.</p><br/>
                    <p>La estructura while se escribe de la siguiente forma:</p>
                    <div class="panel">
                      <p><span class="funct">while</span>(<span class="param">condición</span>){<br/>
                        <span class="comment">// Si la condición se cumple corre el código</span><br/>
                        <span class="comment">// adentro de los {} una y otra vez</span><br/>
                        }
                      </p> 
                    </div>
                    <li>9.1-Leds que parpadean rojo cambian a azul mientras se presiona un botón
                      <ul>
                        <p>En este ejemplo tendremos lo leds parpadeando en rojo cada segundo, pero mientras presionemos el botón 1 los leds parpadearan de color azul.</p>
                        <p><strong>Ejemplo:</strong></p>
                        <div class="row">
                            <div class="small-12 columns" >
                              <div id="editor91" class="editoraprende">
ledsbk luces(PORT1);
buttonsbk botones(PORT2);

code(){
  luces.color(RED);
  delay(1000);
  luces.color(BLACK);
  delay(1000);

  while(botones.read() == 1){
    luces.color(BLUE);
    delay(1000);
    luces.color(BLACK);
    delay(1000);
  }
}
                            </div>
                          </div>  
                        </div>
                        <dl>
                          <dt>¿Qué crees que pasaría si cambiáramos el while por un if ? ¡INTÉNTALO!</dt>
                          <dd>a) Funcionará igual que el while</dd>
                          <dd>b) Parpadeara únicamente ROJO sin importar si presionamos el botón</dd>
                          <dd>c) Parpadeara ROJO sin botón y parpadeara AZUL y ROJO al presionar el botón</dd>
                          <dd>d) Parpadeara únicamente ROJO sin importar si presionamos el botón</dd>
                        </dl>
                        <p>La respuesta correcta es "<strong>c</strong>", ya que al presionar el botón se cumplirá la condición del if sin embargo como ya vimos el if no evita que se siga corriendo el demás código mientras que el while si.</p>
                        <p><strong>Reto: Usar 2 for para prender todos los leds uno por uno y luego apagarlos uno por uno</strong></p>
                        <!-- Creamos el editor de codigo-->
                        <div class="row">
                            <div class="small-10 columns" >
                                <div id="editor91r" class="editoraprende">
                                </div>
                            </div>
                            <div class="small-2 end  columns" >
                                <button id="editor91resp" class="botonrespuesta">Respuesta</button>
                            </div>  
                        </div>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>10-Switch...Case                   
                  <ul>
                    <p>Esta estructura funciona de la misma manera en que lo harían muchos if...else if..else seguidos cuyas condiciones siempre usaran el comparador "==".</p>
                    <p>El switch...case analiza una variable y según su valor realiza cierta acción; su estructura es la siguiente:</p>
                    <div class="panel">
                      <p><span class="funct">switch</span>( variable ){<br/>
                        &#09;<span class="funct">case</span> valor1:<br/>
                        &#09;&#09;accion1;<br/>
                        &#09;<span class="funct">break;</span><br/>
                        &#09;<span class="funct">case</span> valor2:<br/>
                        &#09;&#09;accion2;<br/>
                        &#09;<span class="funct">break;</span><br/>
                        &#09;<span class="funct">case</span> valor3:<br/>
                        &#09;&#09;accion3;<br/>
                        &#09;<span class="funct">break;</span><br/>
                        ...<br/>
                        &#09;<span class="funct">case</span> valorN:<br/>
                        &#09;&#09;accionN;<br/>
                        &#09;<span class="funct">break;</span><br/>
                        &#09;<span class="funct">default:</span><br/>
                        &#09;&#09;accion_default;<br/>
                        }
                      </p> 
                    </div>
                    <p>Nuestro primer case seria equivalente a nuestro <strong>if</strong>, mientras que los siguientes case equivaldrían a un <strong>else if</strong> y por último nuestro default seria lo mismo que nuestro <strong>else</strong>.</p>
                    <li>10.1-Ejemplo: Prender leds de diferente color según el botón presionado
                      <ul>
                        <p>En este ejemplo encenderemos los leds azules si se presiona el botón 1, verdes si es el botón 2 y los apagaremos si es el botón 3.</p>
                        <p><strong>Ejemplo:</strong></p>
                        <div class="row">
                            <div class="small-12 columns" >
                              <div id="editor101" class="editoraprende">
ledsbk luces(PORT1);
buttonsbk botones(PORT2);

code(){
    switch ( botones.read() ){
      case 1:
        luces.color(BLUE);
        break;
      case 2:
        luces.color(GREEN);
        break;
      default:
        luces.color(BLACK);
    }
}
                            </div>
                          </div>  
                        </div>
                        <p>¿ Notaste que este ejemplo realiza la misma función que el ejemplo 6 ? Checalo y veras que hacen exactamente lo mismo.</p>
                        <p><strong>Reto: Usar cada botón para prender un color diferente en los leds</strong></p>
                        <!-- Creamos el editor de codigo-->
                        <div class="row">
                            <div class="small-10 columns" >
                                <div id="editor101r" class="editoraprende">
                                </div>
                            </div>
                            <div class="small-2 end  columns" >
                                <button id="editor101resp" class="botonrespuesta">Respuesta</button>
                            </div>  
                        </div>
                      </ul>
                    </li>
                  </ul>
                </li>
            </ul>
        </div>
        </div>
    </div>
      
     
    <!-- Cerramos grid principal-->
    </div>
    <br>
    <br>
</div>
    
<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 

<!--Se modifican las variables del editor de la pagina principal-->
<!-- Todos los editores de la seccion 5 aqui empiezan ///////////////////////////////////////////7-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor51 = ace.edit("editor51");  //liga el editor declaradoe en html
editor51.getSession().setMode( xml_mode ); //pone el modo
editor51.setTheme("ace/theme/brikode"); //pone el tema
editor51.getSession().setTabSize(2);
editor51.getSession().setUseWrapMode(true);
editor51.setReadOnly(true);  // false to make it editable
</script> 
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor52r = ace.edit("editor52r");  //liga el editor declaradoe en html
editor52r.getSession().setMode( xml_mode ); //pone el modo
editor52r.setTheme("ace/theme/brikode"); //pone el tema
editor52r.getSession().setTabSize(2);
editor52r.getSession().setUseWrapMode(true);
editor52r.setReadOnly(true);  // false to make it editable
</script> 

<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor52 = ace.edit("editor52");  //liga el editor declaradoe en html
editor52.getSession().setMode( xml_mode ); //pone el modo
editor52.setTheme("ace/theme/brikode"); //pone el tema
editor52.getSession().setTabSize(2);
editor52.getSession().setUseWrapMode(true);
editor52.setReadOnly(true);  // false to make it editable
</script> 

<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor54 = ace.edit("editor54");  //liga el editor declaradoe en html
editor54.getSession().setMode( xml_mode ); //pone el modo
editor54.setTheme("ace/theme/brikode"); //pone el tema
editor54.getSession().setTabSize(2);
editor54.getSession().setUseWrapMode(true);
editor54.setReadOnly(true);  // false to make it editable
</script> 

<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor55 = ace.edit("editor55");  //liga el editor declaradoe en html
editor55.getSession().setMode( xml_mode ); //pone el modo
editor55.setTheme("ace/theme/brikode"); //pone el tema
editor55.getSession().setTabSize(2);
editor55.getSession().setUseWrapMode(true);
editor55.setReadOnly(true);  // false to make it editable
</script> 

<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor55r = ace.edit("editor55r");  //liga el editor declaradoe en html
editor55r.getSession().setMode( xml_mode ); //pone el modo
editor55r.setTheme("ace/theme/brikode"); //pone el tema
editor55r.getSession().setTabSize(2);
editor55r.getSession().setUseWrapMode(true);
editor55r.setReadOnly(true);  // false to make it editable
</script>

<script>
$("#editor55resp").on("click",function(){  //abre pop
        editor55r.gotoLine(1);
        editor55r.setValue(
        "\n" + 
         "// Aqui se declaran los modulos que vas a utilizar\n" +
        "distancebk sensor(PORT2);\n" +
        "buzzerbk bocina(PORT3);\n" +
        "code(){\n  " + 
        "if (sensor.read() > 50) {\n" +
        "bocina.beep(500, 1000);\n" +
        "}\n"+
        "else{\n"+
        "bocina.set(OFF);\n"+
        "}\n"+
        "}\n");
    }); 

$("#editor52resp").on("click",function(){  //abre pop
        editor52r.gotoLine(1);
        editor52r.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "ledsbk luces(PORT1);\n" +
        "buttonsbk botones(PORT2);\n" +
        "code(){\n  " + 
        "if(botones.read() > 3){\n" +
        "luces.color(BLUE);      // Si el boton esta presionado se prenden los leds\n" +
        "}\n"+
        "else{\n"+
        "luces.color(BLACK); \n"+
        "}\n"+
        "}\n");
    }); 

</script> 
 <!-- Todos los editores de la seccion 5 aqui terminan //////////////////////////////////////////////////7-->

<!-- Todos los editores de la seccion 6 aqui empiezan ///////////////////////////////////////////7-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor61 = ace.edit("editor61");  //liga el editor declaradoe en html
editor61.getSession().setMode( xml_mode ); //pone el modo
editor61.setTheme("ace/theme/brikode"); //pone el tema
editor61.getSession().setTabSize(2);
editor61.getSession().setUseWrapMode(true);
editor61.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor61r = ace.edit("editor61r");  //liga el editor declaradoe en html
editor61r.getSession().setMode( xml_mode ); //pone el modo
editor61r.setTheme("ace/theme/brikode"); //pone el tema
editor61r.getSession().setTabSize(2);
editor61r.getSession().setUseWrapMode(true);
editor61r.setReadOnly(true);  // false to make it editable


$("#editor61resp").on("click",function(){  //abre pop
        editor61r.gotoLine(1);
        editor61r.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "ledsbk luces(PORT1);\n" +
        "buttonsbk botones(PORT2);\n" +
        "code(){\n  " + 
        "if(botones.read() == 1){\n" +
        "luces.color(BLUE);      // Si el boton esta presionado se prenden los leds azules\n" +
        "}\n"+
        "else if(botones.read() == 2){\n" +
        "luces.color(GREEN);      // Si el boton 2 esta presionada y el 'if' no se cumplio se prenden los leds verdes\n" +
        "}\n"+
        "else if(botones.read() == 3){\n" +
        "luces.color(RED);\n" +
        "}\n"+
        "else{\n"+
        "luces.color(BLACK);  // Si no esta  presionado se apagan los leds\n"+
        "}\n"+
        "}\n");
    }); 

</script> 
<!-- Todos los editores de la seccion 6 aqui terminan //////////////////////////////////////////////////7-->

<!-- Todos los editores de la seccion 7 aqui empiezan ///////////////////////////////////////////7-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor71 = ace.edit("editor71");  //liga el editor declaradoe en html
editor71.getSession().setMode( xml_mode ); //pone el modo
editor71.setTheme("ace/theme/brikode"); //pone el tema
editor71.getSession().setTabSize(2);
editor71.getSession().setUseWrapMode(true);
editor71.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor71r = ace.edit("editor71r");  //liga el editor declaradoe en html
editor71r.getSession().setMode( xml_mode ); //pone el modo
editor71r.setTheme("ace/theme/brikode"); //pone el tema
editor71r.getSession().setTabSize(2);
editor71r.getSession().setUseWrapMode(true);
editor71r.setReadOnly(true);  // false to make it editable


$("#editor71resp").on("click",function(){  //abre pop
        editor71r.gotoLine(1);
        editor71r.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "distancebk sensord(PORT1);\n" +
        "int distancia;\n" +
        "code(){\n  " + 
        "distancia=sonsord.read(); //Aqui se asigna el valor que se lee del sensor a la variable int llamada distancia\n" +
        "bk7print(variable); //para ver el valor imprimimos la variable en la consola\n" +
        "}\n");
    }); 

</script> 
<!-- Todos los editores de la seccion 7 aqui terminan //////////////////////////////////////////////////7-->

<!-- Todos los editores de la seccion 8 aqui empiezan ///////////////////////////////////////////7-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor81 = ace.edit("editor81");  //liga el editor declaradoe en html
editor81.getSession().setMode( xml_mode ); //pone el modo
editor81.setTheme("ace/theme/brikode"); //pone el tema
editor81.getSession().setTabSize(2);
editor81.getSession().setUseWrapMode(true);
editor81.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor82 = ace.edit("editor82");  //liga el editor declaradoe en html
editor82.getSession().setMode( xml_mode ); //pone el modo
editor82.setTheme("ace/theme/brikode"); //pone el tema
editor82.getSession().setTabSize(2);
editor82.getSession().setUseWrapMode(true);
editor82.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor83 = ace.edit("editor83");  //liga el editor declaradoe en html
editor83.getSession().setMode( xml_mode ); //pone el modo
editor83.setTheme("ace/theme/brikode"); //pone el tema
editor83.getSession().setTabSize(2);
editor83.getSession().setUseWrapMode(true);
editor83.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor84 = ace.edit("editor84");  //liga el editor declaradoe en html
editor84.getSession().setMode( xml_mode ); //pone el modo
editor84.setTheme("ace/theme/brikode"); //pone el tema
editor84.getSession().setTabSize(2);
editor84.getSession().setUseWrapMode(true);
editor84.setReadOnly(true);  // false to make it editable
</script>

<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor84r = ace.edit("editor84r");  //liga el editor declaradoe en html
editor84r.getSession().setMode( xml_mode ); //pone el modo
editor84r.setTheme("ace/theme/brikode"); //pone el tema
editor84r.getSession().setTabSize(2);
editor84r.getSession().setUseWrapMode(true);
editor84r.setReadOnly(true);  // false to make it editable


$("#editor84resp").on("click",function(){  //abre pop
        editor84r.gotoLine(1);
        editor84r.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "ledsbk luces(PORT1);\n" +
        "int mivariable;\n" +
        "code(){\n  " + 
        "for ( mivariable = 5, mivariable > 0, mivariable--){ // mivariable-- resta 1 a mivariable cada ciclo\n" +
        "luces.color(mivariable,BLUE);\n" +
        "delay(1000);\n" +
        "}\n"+
        "luces.color(BLACK);\n" +
        "}\n");
    }); 

</script> 

<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor84r1 = ace.edit("editor84r1");  //liga el editor declaradoe en html
editor84r1.getSession().setMode( xml_mode ); //pone el modo
editor84r1.setTheme("ace/theme/brikode"); //pone el tema
editor84r1.getSession().setTabSize(2);
editor84r1.getSession().setUseWrapMode(true);
editor84r1.setReadOnly(true);  // false to make it editable


$("#editor84resp1").on("click",function(){  //abre pop
        editor84r1.gotoLine(1);
        editor84r1.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "ledsbk luces(PORT1);\n" +
        "int mivariable;\n" +
        "code(){\n  " +
        "for ( mivariable = 1, mivariable < 6, mivariable++){\n" +
        "luces.color(mivariable,BLUE);\n" +
        "delay(1000);\n" +
        "}\n"+ 
        "for ( mivariable = 5, mivariable > 0, mivariable--){\n" +
        "luces.color(mivariable,BLACK);\n" +
        "delay(1000);\n" +
        "}\n"+
        "}\n");
    }); 

</script> 
<!-- Todos los editores de la seccion 8 aqui terminan //////////////////////////////////////////////////7-->

<!-- Todos los editores de la seccion 9 aqui empiezan ///////////////////////////////////////////7-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor91 = ace.edit("editor91");  //liga el editor declaradoe en html
editor91.getSession().setMode( xml_mode ); //pone el modo
editor91.setTheme("ace/theme/brikode"); //pone el tema
editor91.getSession().setTabSize(2);
editor91.getSession().setUseWrapMode(true);
editor91.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor91r = ace.edit("editor91r");  //liga el editor declaradoe en html
editor91r.getSession().setMode( xml_mode ); //pone el modo
editor91r.setTheme("ace/theme/brikode"); //pone el tema
editor91r.getSession().setTabSize(2);
editor91r.getSession().setUseWrapMode(true);
editor91r.setReadOnly(true);  // false to make it editable


$("#editor91resp").on("click",function(){  //abre pop
        editor91r.gotoLine(1);
        editor91r.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "ledsbk luces(PORT1);\n" +
        "buttonsbk botones(PORT2);\n" +
        "code(){\n  " + 
        "luces.color(WHITE);\n" +
        "delay(1000);\n" +
        "luces.color(BLACK);\n" +
        "delay(1000);\n" +
        "veces++\n" +
        "while(veces >= 5){\n" +
        "luces.color(BLUE);\n" +
        "delay(1000);\n" +
        "luces.color(BLACK);\n" +
        "delay(1000);\n" +
        "}\n"+
        "}\n");
    }); 

</script> 
<!-- Todos los editores de la seccion 9 aqui terminan //////////////////////////////////////////////////7-->

<!-- Todos los editores de la seccion 10 aqui empiezan ///////////////////////////////////////////7-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor101 = ace.edit("editor101");  //liga el editor declaradoe en html
editor101.getSession().setMode( xml_mode ); //pone el modo
editor101.setTheme("ace/theme/brikode"); //pone el tema
editor101.getSession().setTabSize(2);
editor101.getSession().setUseWrapMode(true);
editor101.setReadOnly(true);  // false to make it editable
</script>
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor101r = ace.edit("editor101r");  //liga el editor declaradoe en html
editor101r.getSession().setMode( xml_mode ); //pone el modo
editor101r.setTheme("ace/theme/brikode"); //pone el tema
editor101r.getSession().setTabSize(2);
editor101r.getSession().setUseWrapMode(true);
editor101r.setReadOnly(true);  // false to make it editable

$("#editor101resp").on("click",function(){  //abre pop
        editor101r.gotoLine(1);
        editor101r.setValue(
        "\n" + 
        "//Escribe tus codigos aqui\n" +
        "ledsbk luces(PORT1);\n" +
        "buttonsbk botones(PORT2);\n" +
        "code(){\n  " + 
        "switch( botones.read() ){\n" +
        "\tcase 1:\n" +
        "\t\tluces.color(BLUE);\n" +
        "\t\tbreak;\n" +
        "\tcase 2:\n" +
        "\t\tluces.color(GREEN);\n" +
        "\t\tbreak;\n" +
        "\tcase 3:\n" +
        "\t\tluces.color(RED);\n" +
        "\t\tbreak;\n" +
        "\tcase 4:\n" +
        "\t\tluces.color(AQUA);\n" +
        "\t\tbreak;\n" +
        "\tcase 5:\n" +
        "\t\tluces.color(PINK);\n" +
        "\t\tbreak;\n" +
        "\tdefault:\n" +
        "\t\tluces.color(BLACK);\n" +
        "}\n"+
        "}\n");
    }); 

</script> 
<!-- Todos los editores de la seccion 10 aqui terminan //////////////////////////////////////////////////7-->



<!-- listeners para mandar a los links -->
<script>

function prepareList() {
  $('#expListaprende').find('li:has(ul)')
  	.click( function(event) {
  		if (this == event.target) {
  			$(this).toggleClass('expanded');
  			$(this).children('ul').toggle('medium');
  		}
  		return false;
  	})
  	.addClass('collapsed')
  	.children('ul').hide();
  };
 
  $(document).ready( function() {
      prepareList();
  });
    
</script>
  
<!-- Agremas unas librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<script>
  $(document).foundation();

  var doc = document.documentElement;
  doc.setAttribute('first', navigator.userAgent);
</script>

  </body>
</html>