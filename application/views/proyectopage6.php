<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 
<!-- bordes de la pagina y fondo blanco del text area -->
<style>
.cointainer {
    border-width: 0 5px 0px 5px;
    border-style: solid;
    border-color: #006D91;
}
    
.cointainer2 {
    border-width: 0 5px 5px 5px;
    border-style: solid;
    border-color: #006D91;
}

.textareawhite{
background-color:#ffffff !important;
border-style: none !important ;
margin-bottom:0 !important;
box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0) !important;
}
/*los datos aqui tienen que ser iguales a los de la letra del textarea para que funcione */
.hiddendiv {
    display: none;
    min-height: 50px;
    padding: 5px;
    font-size: 1.7em;
    font-family: "MAXWELLREGULAR";
    white-space: pre-wrap;
    word-wrap: break-word;
}   
</style>
<style>
.cointainer {
    border-width: 0 5px 0px 5px;
    border-style: solid;
    border-color: #006D91;
}
    
.cointainer2 {
    border-width: 0 5px 5px 5px;
    border-style: solid;
    border-color: #006D91;
}
</style>

<!---------------------informacion de la base de datos----------------->
<!--*****basicos****---->
<?php $Images_gen_prin = "pizarron.png"; ?>
<?php $Nombre_gen_prin =[ "Objetivos","Antes de la clase","Durante la clase" ]?>

<?php 
$Desc_gen_basic = array();
?>
<?php if (isset($info['textos'])) {
$info_textos_pasos = array_keys($info['textos']);//regresa un arreglo con los indices
foreach( $info_textos_pasos as $value){
  //print_r($info['textos']);
if( $info['textos'][$value]['Paso'] == 1){
$ID_proyecto = $info['textos'][$value]['ID_Proyecto'];
$Nombre_gen_basic = $info['textos'][$value]['Texto'];
}else{
array_push($Desc_gen_basic,$info['textos'][$value]['Texto']);
}
}
} ?>

<?php 
$Nombre_gen_extra = array();
$Desc_gen_extra = array();
?>
<!--*****extras****---->
<?php if (isset($info['secciones'])) {
$info_textos_pasos = array_keys($info['secciones']);//regresa un arreglo con los indices
foreach( $info_textos_pasos as $value){
array_push($Nombre_gen_extra,$info['secciones'][$value]['Titulo']);
array_push($Desc_gen_extra,$info['secciones'][$value]['Descripcion']);
}
}  ?>

<?php 
$Img_gen_extra = array();
?>
<?php if (isset($info['imagenes'])) {
$info_textos_pasos = array_keys($info['imagenes']);//regresa un arreglo con los indices
foreach( $info_textos_pasos as $value){
array_push($Img_gen_extra,$info['imagenes'][$value]['Imagen']);
}

}  ?>

<?php 
$Codigo_gen_extra = array();
?>
<?php if (isset($info['codigo'])) {
$info_textos_pasos = array_keys($info['codigo']);//regresa un arreglo con los indices
foreach($info_textos_pasos as $value){
array_push($Codigo_gen_extra,$info['codigo'][$value]['Codigo']);
}
}  ?>

<?php 
$Nombre_gen_adjunto = array();
$File_gen_adjunto = array();
?>
<!--*****adjuntos****---->
<?php if (isset($info['adjuntos'])) {
$info_textos_pasos = array_keys($info['adjuntos']);//regresa un arreglo con los indices
foreach($info_textos_pasos as $value){
array_push($Nombre_gen_adjunto,$info['adjuntos'][$value]['Nombre']);
if (isset($info['adjuntos'])) {
array_push($File_gen_adjunto,$info['adjuntos'][$value]['Archivo']);
}else{
array_push($File_gen_adjunto,"");   
}
}
}  ?>

<!------------------------------------------------------------------------------>  

<div class="row"  id="div1">
  <div class="large-12  columns">
<!-- titulo principal -->
<div class="row cointainer">
    <div class="small-12 columns" style="background:#006D91; text-align:center;" >
    <h1 class="h1classbk7" style="margin-top: 20px;"><?php echo  $Nombre_gen_basic ?></h1>
    </div>
</div>

<style>
.image_teach_prin{
 background-image: url("<?php echo base_url(); ?>images/proyectopage/iconosextra/<?php echo $Images_gen_prin ?>");  
background-size:100% 100%;
background-repeat: no-repeat;
}
</style>      
      
<!-- tabs para entrar a diferentes funciones -->
    <div class="row cointainer">
        <div class="small-12 columns image_teach_prin ">
            <br><br><br><br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br>
            <ul class="tabs" id="myTabs" style="padding-left: 320px;padding-bottom: 40px;" data-tab>
                <li class="tab-title active"><a href="#panel1">General</a></li>
                <li class="tab-title"><a href="#panel2">Adjuntos</a></li>
            </ul>
        </div>
    </div>
      
<!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content" id="myTabscontainer">
<!------------------------------------------------------------------------------>
        
<!-- tab -->
<div class="content active" id="panel1" style="padding-top: 0px;">
<!--basicos-->  
<?php for($i = 0; $i< count($Nombre_gen_prin); $i++) { ?>
<div class="row cointainer">
    <div class="small-12 columns" style="background:#006D91;padding-bottom: 0px; !important" >
        <?php if($i ==0) { ?>
         <div class="small-2 columns" style ="padding-bottom:0px;padding-right:0px;">
    <center><img style ="width: 50%;" src="<?php echo base_url(); ?>images/proyectopage/iconosteach/objetivos.png"; ></center>
         </div>
        <?php } ?>
        <?php if($i ==1) { ?>
         <div class="small-2 columns" style ="padding-bottom:0px;padding-right:0px;">
    <center><img style ="width: 50%;" src="<?php echo base_url(); ?>images/proyectopage/iconosteach/preparacion.png"; ></center>
         </div>
        <?php } ?>
        <?php if($i ==2) { ?>
         <div class="small-2 columns" style ="padding-bottom:0px;padding-right:0px;">
    <center><img style ="width: 50%;" src="<?php echo base_url(); ?>images/proyectopage/iconosteach/durantelaclase.png"; ></center>
         </div>
        <?php } ?>
         <div class="small-10 end columns" style ="padding-bottom:0px;padding-left:0px;">
    <h1 class="h1classbk6" style="margin-top: 17px;"><?php echo $Nombre_gen_prin[$i] ?></h1>
        </div>
    </div>
</div>
           
<div class="row cointainer">
    <div class="small-12 columns">
    <textarea id ="basicstextarea<?php echo $i ?>" readonly style="height:100%;width:100%;overflow:hidden;resize:none;outline: none;text-align:justify;" class="pclassbk textareawhite"><?php echo $Desc_gen_basic[$i] ?></textarea>
    </div>
</div>
    
<script>
////agrega una div invisible que toma el texto y hace resize del text area
$(function() {
    var txt = $('#basicstextarea<?php echo $i ?>'),
        hiddenDiv = $(document.createElement('div')),
        content = '<br>';
    
    hiddenDiv.addClass('hiddendiv');

    $('body').append(hiddenDiv);
    content = content +  txt.val();
    content = content.replace(/\n/g, '<br>');
    content = content + '<br>';
    hiddenDiv.html(content + '<br class="lbr">');
    txt.css('height', hiddenDiv.height());
    $('body').remove(".hiddendiv");
});
</script> 
<?php } ?>
<!------------------------------------------------------------------------------>
<!-- arreglo para guardar la configuracion de los editores de codigo -->
<script>
var editor= new Array();
</script>
<!--Extras-->  
<?php for($i = 0; $i< count($Nombre_gen_extra); $i++) { ?>
<div class="row cointainer">
    <div class="small-12 columns" style="background:#006D91;padding-bottom: 0px; !important" >
         <div class="small-2 columns" style ="padding-bottom:0px;padding-right:0px;">
    <center><img  style ="width: 50%;" src="<?php echo base_url(); ?>images/proyectopage/iconosteach/extra.png"; ></center>
         </div>
         <div class="small-10 end columns" style ="padding-bottom:0px;padding-left:0px;">
    <h1 class="h1classbk6" style="margin-top: 10px;"><?php echo $Nombre_gen_extra[$i] ?></h1>
        </div>
    </div>
</div>
           
<div class="row cointainer">
    <div class="small-12 columns">
    <textarea id ="extratextarea<?php echo $i ?>" readonly style="height:100%;width:100%;overflow:hidden;resize:none;outline: none;text-align:justify;" class="pclassbk textareawhite"><?php echo $Desc_gen_extra[$i] ?></textarea>
    </div>
</div>
<script>
////agrega una div invisible que toma el texto y hace resize del text area
$(function() {
    var txt = $('#extratextarea<?php echo $i ?>'),
        hiddenDiv = $(document.createElement('div')),
        content = '<br>';
    
    hiddenDiv.addClass('hiddendiv');

    $('body').append(hiddenDiv);
    content = content +  txt.val();
    content = content.replace(/\n/g, '<br>');
    content = content + '<br>';
    hiddenDiv.html(content + '<br class="lbr">');
    txt.css('height', hiddenDiv.height());
    $('body').remove(".hiddendiv");
});
</script> 

<?php if(!empty($Img_gen_extra)){ ?>
<div class="row cointainer">
    <div class="small-12 columns">
     <center><img  style="height:50px" src="<?php echo base_url(); ?>uploads/<?php echo $ID_proyecto ?> /<?php echo $Img_gen_extra[$i] ?>" ></center>
    </div>
</div>     
<?php } ?> 
  
<?php if($Codigo_gen_extra[$i] != "") { ?>
    <!--ponemos un editor de texto-->
    <div class="row cointainer">
        <div class="large-12 columns">
            <div id="editorp<?php echo $i ?>">
<?php echo $Codigo_gen_extra[$i] ?>
            </div>
        </div>
    </div>
    
    <!-- Boton de copiar-->
    <div class="row cointainer">
        <div class="small-12 columns">
            <center><button id="botoncopy<?php echo $i ?>" class= "button round">Copiar el código</button></center>
        </div>
    </div>
    
    <!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var numx = parseInt("<?php echo $i ?>");
editor[numx] = ace.edit("editorp<?php echo $i ?>");  //liga el editor declaradoe en html
editor[numx].getSession().setMode( xml_mode ); //pone el modo
editor[numx].setTheme("ace/theme/brikode"); //pone el tema
editor[numx].getSession().setTabSize(2);
editor[numx].getSession().setUseWrapMode(true);
editor[numx].setReadOnly(true);  // false to make it editable
</script> 

<script>
//listener para copiar el codigo en el editor de texto
$("#botoncopy<?php echo $i ?>").on("click",function(){ 
var numx = parseInt("<?php echo $i ?>");
var Strind_editor  =editor[numx].getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
</script>
    
<style>
#editorp<?php echo $i ?> { 
  margin-left: 15px;
  margin-top: 15px;
  height: 400px;
 font-size: 25px;
}    
</style>
 <?php } ?>  
      
<?php } ?>
<!------------------------------------------------------------------------------>     
<!-- Botones-->
    <div class="row cointainer2">
        <div class="small-12 columns" >
            <center><button class= "next button round">Siguiente</button></center>
        </div>
    </div>
    
</div>   

<!------------------------------------------------------------------------------>  
<!-- tab 2 -->
<div class="content" id="panel2" style="padding-top: 0px;">
<?php for($i = 0; $i< count($Nombre_gen_adjunto); $i++) { ?> 
 <div class="row cointainer">
    <div class="small-8 columns">
    <p class="pclassbk" style="text-align:justify"><?php echo $Nombre_gen_adjunto[$i] ?></p>
    </div>
     <div class="small-4 end columns">
    <?php if($File_gen_adjunto[$i] != "") { ?> 
    <center><a href="<?php echo base_url(); ?>uploads/<?php echo $ID_proyecto ?>/<?php echo  $File_gen_adjunto[$i] ?>" download="<?php echo  $File_gen_adjunto[$i] ?>"><button class= "round">Descargar archivo</button></a></center>
    <?php }else{ ?>
         <center><p class="pclassbk" style="text-align:justify">No subio ningun archivo</p></center>   
    <?php } ?>
    </div>
</div>    
 <?php } ?>     
       
 <!-- Botones-->
<div class="row cointainer2">
    <div class="small-12 columns">
        <center><button class="back button round">Atras</button></center>
    </div>
</div>
    
</div>
  
<!------------------------------------------------------------------------------>  
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>
      
<script>

$('#myTabscontainer').on('click', '.next', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').next().addClass('active');
});
    
$('#myTabscontainer').on('click', '.back', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').prev().addClass('active');
});
    
</script>

<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>