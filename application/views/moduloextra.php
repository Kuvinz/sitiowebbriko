<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/comienzaahora.css">
<!--para hacer que la imagen gire al hacer hover -->
<style>
#f1_container {
  position: relative;
  margin: 10px auto;
  z-index: 1;
}
#f1_container {
  perspective: 1000;
}
#f1_card {
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transition: all 0.7s linear;
}
#f1_container:hover #f1_card {
  transform: rotateY(180deg);
}
.face {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}
.face.back {
  display: block;
  transform: rotateY(180deg);
  padding: 10px;
  text-align: center;
}
</style>

<!--Aqui es donde esta la informacion de toda la pagina -->

<?php $Rotate_text = "Funciones adicionales para  <br /> usar con tus brikos." ?>

<?php 
$Comentario1 = "Declara tus modulos aqui ";
$Comentario2 = "Escribe tu codigo aqui ";
?>

<!-- Arreglo para guardar los puertos-->
<?php $Ports_s = array("PORT1","PORT2","PORT3","PORT4","PORT5","PORT6","PORT7"); ?> 

<?php 
$Palabra_Numero = "numero";
$Palabra_Descripcion = "Descripción:";
$Palabra_Funciones = "Funciones:";
$Palabra_Generarcodigo = "Generar código";
$Palabra_Copiarcodigo = "Copiar código";
$Palabra_busca_color = "Busca tu color";
$Prin_titulo = "Funciones adicionales";
$Prin_desc = "-Estas funciones te permitaran hacer programas más complejos con tus brikos.";
$Label_1 = "Dale un nombre a tu modulo: (maximo 15 caracteres)";
$Label_2 = "Selecciona tu puerto:";
$Label_3 = "Funciones: (Seleccióna la funcion que deseas probar)";
$Placeholder_1= "Nombre de tu modulo";
$Funcion_name = array("map","random","randomSeed");
$Funcion_num = array(1,1,1);

?>

<!--Texto que va en los modales -->
<?php 
$Modal_list;
$Palabra_Ejemplo = "Ejemplo:";
$Modal_list[0]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> map</span>( variable, de_menor, de_mayor, a_menor, a_mayor );";
$Modal_list[0]["M_argg"] = array("","map");
$Modal_list[0]["M_argdefines"] = array("");
$Modal_list[0]["M_arg"] = array("variable","de_menor","de_mayor","a_menor", "a_mayor");
$Modal_list[0]["M_descg"] = array("","Función de briko.");
$Modal_list[0]["M_descdefines"] = array("");
$Modal_list[0]["M_desc"] = array("Variable a la que se le cambiara el rango.","Rango menor del rango en el que esta la variable.","Rango mayor del rango en el que esta la variable.","Rango menor del rango al que se cambiara la variable.", "Rango mayor del rango al que se cambiara la variable.");
$Modal_list[0]["M_descfun"] = "
Función que convierte una variable de un rango de valores a otro.
";
$Modal_list[0]["M_codigo"] = "
//Declara tus modulos aqui 
distancebk sensor(PORT1); //declaramos un modulo de distancia llamado sensor en el puerto 1

int dis; //declaramos una variable int

//Escribe tu codigo aqui 
code(){
  dis = sensor.read( ); //lee el sensor de distancia
  dis = map(dis,0,80,0,255); //cambia la variable dis del rango de 0-80 a 0-255
  bk7print(dis); //imprime en la pc la distancia
  delay(1000); //se espera 1000 milisegundos
}
";
$Modal_list[1]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> random</span>( min, max );";
$Modal_list[1]["M_argg"] = array("","random");
$Modal_list[1]["M_argdefines"] = array("");
$Modal_list[1]["M_arg"] = array("min","max");
$Modal_list[1]["M_descg"] = array("","Función de briko.");
$Modal_list[1]["M_descdefines"] = array("");
$Modal_list[1]["M_desc"] = array("El minimo numero que puede salir en el random.","El mayor numero que puede salir en el random, menos 1.");
$Modal_list[1]["M_descfun"] = "
Función que genera numeros al azar dentro del rango de numeros que se especifique. <br />
El numero mayor que se generara en el random es el que se especifique en la funcion 'MENOS 1'.
";
$Modal_list[1]["M_codigo"] = "
//Declara tus modulos aqui 
ledsbk luces(PORT1); //declaramos un modulo de leds llamado luces en el puerto 1

int num;  //declaramos una variable int

//Escribe tu codigo aqui 
code(){
  num = random(1,6);  //genera un numero al azar entre 1 y 5
  nombre.color(num,RED); //prende un led en rojo
  delay(1000); //se espera 1000 milisegundos
  nombre.color(BLACK); //apaga los leds
  delay(1000); //se espera 1000 milisegundos
}
";
$Modal_list[2]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> randomSeed</span>( numero );";
$Modal_list[2]["M_argg"] = array("","randomSeed");
$Modal_list[2]["M_argdefines"] = array("");
$Modal_list[2]["M_arg"] = array("numero");
$Modal_list[2]["M_descg"] = array("","Función de briko.");
$Modal_list[2]["M_descdefines"] = array("");
$Modal_list[2]["M_desc"] = array("Numero en el que se inicializa la semilla del random.");
$Modal_list[2]["M_descfun"] = "
Inicializa la semilla del random. <br />
Esta función se usa en conjunto con la función 'random'.
";
$Modal_list[2]["M_codigo"] = "
//Declara tus modulos aqui 
ledsbk luces(PORT1); //declaramos un modulo de leds llamado luces en el puerto 1

int num;  //declaramos una variable int
int flag=0; //declaramos una variable int y la inicializamos en 0

//Escribe tu codigo aqui 
code(){

  if(flag==0){  //para que solo haga esto una vez
  randomSeed(4);  //inicializa la semilla en 4
  flag=1;  //cambia la bandera a uno
  };
  
  num = random(1,6);  //genera un numero al azar entre 1 y 5
  nombre.color(num,RED); //prende un led en rojo
  delay(1000); //se espera 1000 milisegundos
  nombre.color(BLACK); //apaga los leds
  delay(1000); //se espera 1000 milisegundos
}
";
?>
<!---------------------------------------------------------->

<!-- Creamos el grid principal-->
<div class="row"  id="divp">
  <div class="large-12 columns">
    <br>
    <br>
    <!-- Primera imagen, descripcion y titulo -->
    <div class="row">
        <div class="small-4 columns">
            <div id="f1_container">
                <div id="f1_card" class="shadow">
                    <div class="front face">
                        <!-- front conten-->
                        <center><img alt="briko extra" style="margin-top: 10px;" src="<?php echo base_url(); ?>images/modulosindividuales/extra.png"; ></center>
                    </div>
                    <div class="back face center">
                        <!-- back content -->
                        <center><p class = 'pclassbk'><?php echo $Rotate_text ?></p></center>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-8 end columns">
            <h1 class = 'h1classbk2 text-justify' ><?php echo $Prin_titulo ?></h1>
            <span class='label desKY'><?php echo $Palabra_Descripcion ?></span>
            <br>
            <p class = ' pclassbk text-justify' ><?php echo $Prin_desc ?></p> 
        </div>
    </div>
      
      <!-- cintillo se separacion -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <img alt="briko cintillo" width= "200%" style="height:25px" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cintillo2.png"; >
        </div>
    </div>  
      
    <!---------------------------------------------------------->
      
    <!-- Segundo titulo-->
    <div class="row">
        <div class="large-12 columns"  style="padding-bottom: 0px;">
            <h1 class = ' h1classbk2 text-justify' ><?php echo $Palabra_Funciones ?></h1>
        </div>
    </div>
      
    <!-- Label del segundo titulo(los 2 labels) (con display:block para que es span respete la div)-->
    <div class="row">
        <div class="small-5 columns">
            <p class='brikospanp'><?php echo $Label_1 ?></p>
        </div>
        <div class="small-3 end columns small-offset-1">
            <p class='brikospanp'><?php echo $Label_2 ?></p>
        </div>
    </div>
    <!---------------------------------------------------------->
      
    <!-- Input para poner el nombre de los modulos y el selector de puertos-->
    <div class="row">
        <div class="small-5  columns" style="padding-left:20px">
            <input id="first" placeholder="<?php echo $Placeholder_1 ?>" type="text" maxlength="15" />  
        </div>
        <div class="small-3 end columns small-offset-1" style="padding-left:20px">
            <select id="objectPORT" style="color:#BB2462">
                <?php for($i=0;$i<count($Ports_s);$i++){ ?>
                <option>
                    <?php echo $Ports_s[$i]; ?>
                </option>
                <?php } ?> 
            </select>  
        </div>
    </div>
    <!---------------------------------------------------------->
      
    <!-- Label de las funciones-->
    <div class="row">
        <div class="small-5 columns">
            <p class='brikospanp'><?php echo $Label_3 ?></p> 
        </div>    
    </div>
     <!---------------------------------------------------------->
           
     <!-- Para modificar los anchos de los selectores y radio button en la tabla--> 
     <?php $box_width = 130 ?>
     <?php $radiob_width = 40 ?>
      
       <!-- Creamos las tablas-->
    <?php $counter = 0 ?>
    <?php for($x = 0; $x< count($Funcion_num); $x++) { ?>
    <?php for($j = 0; $j< $Funcion_num[$x]; $j++) { ?>
    <?php $counter++ ?>
    <div class="row">
        <div class="large-12 columns" style="margin-bottom:0;padding-bottom:0;" >
            <table style="border-style:hidden;margin-bottom:0;padding-bottom:0;" >
                <tbody>
                    <tr>
                        <?php if($counter==1){ //para poner la condicion de checked?>
                        <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio" value="<?php echo $counter ?>" style="margin-top:5px;" CHECKED />
                        </td>
                        <?php }else{ ?>
                         <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio" value="<?php echo $counter ?>" style="margin-top:5px;"/>
                        </td>
                        <?php } ?>
                        <td style="padding-right: 0px; padding-left:0px;"> 
                            <p class = 'text-justify' style="font-size:1.5em;color:#006D91" ><?php echo $Funcion_name[$x] ?></p>
                        </td>
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >(</p>
                        </td>
                        
                        <?php if($counter==1){ ?>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" > variable,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>1">
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>2">
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>3">
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>4">
                        </td>
                        <?php } ?>
                        
                        <?php if($counter==2){ ?>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>1">
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>2">
                        </td>
                        <?php } ?>
                        
                        <?php if($counter==3){ ?>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="<?php echo $Palabra_Numero ?>" min="-32768" max="32767" id="object<?php echo $counter ?>1">
                        </td>
                        <?php } ?>
                                                
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >);</p>
                        </td>
                         <td>
                            <img alt="briko ayuda modulo" id="but<?php echo $counter ?>1" src="<?php echo base_url(); ?>images/ayuda.png" width=30 height=30 style="cursor:pointer;margin-top:0px;padding-bottom: 19px;" />
                        </td>
                    </tr>
                </tbody>  
            </table>
        </div>   
    </div> 
      
    <!--listener de los botones -->
    <script>
    //listener del boton imagen para abrir la ayuda    
        $("#but<?php echo $counter ?>1").on("click",function(){  //abre pop
            $('#POP<?php echo $counter-1 ?>').foundation('reveal','open');
        });    
    </script>
      
    <?php } ?>
    <?php } ?>
    <!---------------------------------------------------------->
    
    
    <!-- Creamos el boton para generar el codigo y copiar el codigo-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="button round" id="buttongenerate" ><?php echo $Palabra_Generarcodigo ?></button>
        </div>
        <div class="small-3 columns end small-offset-2" >
            <button class="button round" id="buttoncopy" ><?php echo $Palabra_Copiarcodigo ?></button>
        </div>
    </div>
     <!---------------------------------------------------------->
     
    <!-- Creamos el editor de codigo-->
<div class="row">
    <div id="editor" >
  ////<?php echo $Comentario1 ?>
        
      
  //<?php echo $Comentario2 ?>
        
  code(){
        
  }

    </div>
</div>
 <!---------------------------------------------------------->
           
    <!-- Cerramos grid principal-->
    </div>
    <br>
    <br>
</div>
<!---------------------------------------------------------->

<!-- declaramso script para animaciones-->
<script src="<?php echo base_url(); ?>js/move/move.js"></script>
<script src="<?php echo base_url(); ?>js/move/movemodulos/extra.js"></script>

<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 


<!--para guardar las configuraciones de los editores de los pop -->
<script>
var editorpop= new Array();
</script>

<!-- declaramos el primer modal que saldra al presionar el boton de ayuda -->
<?php for($j = 0; $j< count($Modal_list); $j++) { ?>
<div id="POP<?php echo $j ?>" class="reveal-modal big" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="row">
        <div class="small-12 end columns"  >
            <p class="functcomp"><?php echo $Modal_list[$j]["M_func"] ?></p>
        </div>
    </div>  
    <div class="row">
        <div class="large-12 columns">
            <ul class="especF">
                <li><span class="funct"><?php echo $Modal_list[$j]["M_argg"][1] ?></span>: <?php echo $Modal_list[$j]["M_descg"][1] ?> </li>
                <?php for($i = 0; $i< count($Modal_list[$j]["M_arg"]); $i++) { ?>
                <?php if($Modal_list[$j]["M_arg"][$i] != ""){ ?>
                <li><?php echo $Modal_list[$j]["M_arg"][$i] ?>: <?php echo $Modal_list[$j]["M_desc"][$i] ?></li>
                <?php } } ?>
                <?php for($i = 0; $i< count($Modal_list[$j]["M_argdefines"]); $i++) { ?>
                <?php if($Modal_list[$j]["M_argdefines"][$i] != ""){ ?>
                <li><span class="param"><?php echo $Modal_list[$j]["M_argdefines"][$i] ?></span>: <?php echo $Modal_list[$j]["M_descdefines"][$i] ?></li>
                <?php } } ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <span class='label desKY'> <?php echo $Palabra_Descripcion ?> </span>
            <p class="lead"><?php echo $Modal_list[$j]["M_descfun"] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <span class='label desKY'> <?php echo $Palabra_Ejemplo ?> </span>
            <!-- Boton de copiar-->
            <center><button id="botoncopy<?php echo $j ?>" class= "button round" style="margin-bottom: 0px; padding-top: 5px; padding-left: 10px; padding-right: 10px;padding-bottom: 5px;">Copy code</button></center>
            <div id="editorpmotor<?php echo $j ?>">
                <?php echo $Modal_list[$j]["M_codigo"]?>
            </div>  
        </div>
    </div>
        
    <?php if($j == 0) { //animacion 1 ?>
    <div class="row">
        <div class="small-5 columns small-offset-3" style="text-align:center;margin-top:50px;" >
            <img alt="briko regla" src="<?php echo base_url(); ?>images/modulos_anim/extra/regla.png" 
            />
        </div>
    </div>
     <div class="row">
        <div class="small-3 columns small-offset-1" >
            <img alt="briko sensor distancia" src="<?php echo base_url(); ?>images/modulos_anim/extra/sensordis.png"
                />
        </div>  
        <div class="box4" style="text-align:center">
        <div class="small-3 columns end " >
                <img alt="briko robot plano" src="<?php echo base_url(); ?>images/modulos_anim/extra/robotplano.png"
                />
            </div>
        </div>
        <div class="box1" style="text-align:center">
            <div class="small-3 columns end small-offset-2" >
                <img class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/extra/compu48.png" alt="briko mostrando 48" />
                <img class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/extra/compu255.png" alt="briko mostrando 255" style="display:none"/>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 1) { //animacion 2 ?>
    <div class="row"> 
        <div class="box2" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="briko leds led 1 encendido" id="led_an11" src="<?php echo base_url(); ?>images/modulos_anim/extra/led1on.png"  />
                <img alt="briko leds led 2 encendido" id="led_an12" src="<?php echo base_url(); ?>images/modulos_anim/extra/led2on.png"  style="display:none"/>
                <img alt="briko leds led 3 encendido" id="led_an13" src="<?php echo base_url(); ?>images/modulos_anim/extra/led3on.png"  style="display:none"/>
                <img alt="briko leds led 4 encendido" id="led_an14" src="<?php echo base_url(); ?>images/modulos_anim/extra/led4on.png"  style="display:none"/>
                <img alt="briko leds led apagado" id="led_an15" src="<?php echo base_url(); ?>images/modulos_anim/extra/ledoff.png" style="display:none" />
                <img alt="briko leds led 5 encendido" id="led_an16" src="<?php echo base_url(); ?>images/modulos_anim/extra/led5on.png" style="display:none" />
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 2) { //animacion 3 ?>
    <div class="row"> 
        <div class="box3" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="briko leds led 1 encendido" id="led_an11" src="<?php echo base_url(); ?>images/modulos_anim/extra/led1on.png" style="display:none" />
                <img alt="briko leds led 2 encendido" id="led_an12" src="<?php echo base_url(); ?>images/modulos_anim/extra/led2on.png"  style="display:none"/>
                <img alt="briko leds led 3 encendido" id="led_an13" src="<?php echo base_url(); ?>images/modulos_anim/extra/led3on.png" />
                <img alt="briko leds led 4 encendido" id="led_an14" src="<?php echo base_url(); ?>images/modulos_anim/extra/led4on.png"  style="display:none"/>
                <img alt="briko leds led apagado" id="led_an15" src="<?php echo base_url(); ?>images/modulos_anim/extra/ledoff.png" style="display:none" />
                <img alt="briko leds led 5 encendido" id="led_an16" src="<?php echo base_url(); ?>images/modulos_anim/extra/led5on.png" style="display:none" />
            </div>
        </div>
    </div>
    <?php } ?>
    
    
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!---------------------------------------------------------->

<!--Se modifica el editor de los modales (estilo y links) -->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var numx = parseInt("<?php echo $j ?>");
editorpop[numx] = ace.edit("editorpmotor<?php echo $j ?>");  //liga el editor declaradoe en html
editorpop[numx].getSession().setMode( xml_mode ); //pone el modo
editorpop[numx].setTheme("ace/theme/brikode"); //pone el tema
editorpop[numx].getSession().setTabSize(2);
editorpop[numx].getSession().setUseWrapMode(true);
editorpop[numx].setReadOnly(true);  // false to make it editable
</script> 
<!---------------------------------------------------------->

<script>
//listener para copiar el codigo en el editor de texto
$("#botoncopy<?php echo $j ?>").on("click",function(){ 
var numx = parseInt("<?php echo $j ?>");
var Strind_editor  =editorpop[numx].getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
</script>
    
<style>
#editorpmotor<?php echo $j ?> { 
margin-left: 15px;
  margin-top: 15px;
  height: 300px;
 font-size: 14px;
}    
</style>

<?php } ?>
<!---------------------------------------------------------->


<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor = ace.edit("editor");  //liga el editor declaradoe en html
editor.getSession().setMode( xml_mode ); //pone el modo
editor.setTheme("ace/theme/brikode"); //pone el tema
editor.getSession().setTabSize(2);
editor.getSession().setUseWrapMode(true);
editor.setReadOnly(true);  // false to make it editable
</script> 


<!-- Script donde tenemos todos los listeners --> 
<script>
var secretmessage = Array(0,0,0,0,0,0,0,0); //donde se guardaran las letras
var flagm = 0; //variable para saber en que letra va
var flagm2 = 0; //variable para saber si se equivoco y hay que resetear
var flagm3 = 0;  //para resetear si presionan en otro lado que no sea en la nada
var $tempimg = $("<img>"); //para crear una imagen temporal en la pagina
$tempimg.attr("src","<?php echo base_url(); ?>images/primerprograma/paso15.png");//carga la imagen secreta
$tempimg.attr("style","margin-left: 200px;");//la centra
  
    
//listener para copiar el codigo en el editor de texto
$("#buttoncopy").on("click",function(){ 
var Strind_editor  =editor.getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
   
//listener para generar el codigo cuando presionen el boton
$("#buttongenerate").on("click",function(){  
    var value = $('input:radio[name=funcionesradio]:checked').val().toString(); 
    var Name= $('#first').val();
    if(Name == ""){ Name="nombre";}
    var Port_s= $('#objectPORT').val();
    if(Name == ""){ Name="PORT1";}
    
    
    if(value ==1){
    var num1 = $('#object11').val().toString();
    if(num1 == ""){num1="0";}
    var num2 = $('#object12').val().toString();
    if(num2 == ""){num2="80";}
    var num3 = $('#object13').val().toString();
    if(num3 == ""){num3="0";}
    var num4 = $('#object14').val().toString();
    if(num4 == ""){num4="255";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "distancebk "+Name+"("+Port_s+");\n\n" +
    "int dis;\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "dis = "+ Name + ".read( );\n  " +
    "dis = map(dis,"+num1+","+num2+","+num3+","+num4+");\n  "  +
    "bk7print(dis);\n  "  +   
    "delay(1000);\n"+
    "}\n");
    }
    
    if(value ==2){
    var num1 = $('#object21').val().toString();
    if(num1 == ""){num1="1";}
    var num2 = $('#object22').val().toString();
    if(num2 == ""){num2="6";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "ledsbk "+Name+"("+Port_s+");\n\n" +
    "int num;\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "num = random("+num1+","+num2+");\n  " +
    Name +".color(num,RED);\n  "  +
    "delay(1000);\n  "  +
    Name +".color(BLACK);\n  "  +  
    "delay(1000);\n"+
    "}\n");
    }
    
    if(value ==3){
    var num1 = $('#object31').val().toString();
    if(num1 == ""){num1="0";}
    editor.gotoLine(1);
    editor.setValue(
   "//<?php echo $Comentario1 ?>\n\n"+
    "ledsbk "+Name+"("+Port_s+");\n\n" +
    "int flag=0;\n" +
    "int num;\n\n  " +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "if(flag==0){\n  randomSeed("+num1+");\n  flag=1;\n  };\n  " +
    "num = random(1,6);\n  " +
    Name +".color(num,RED);\n  "  +
    "delay(1000);\n  "  +
    Name +".color(BLACK);\n  "  +  
    "delay(1000);\n"+
    "}\n");
    }

});    
  
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object11").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[0].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object11').val(); 
    if(value != ""){
        if(value >32767){
        $('#object11').val(5);  
        }
        if(value < -32768){
            $('#object11').val(1);  
        }
    }
});
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object12").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[0].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object12').val(); 
    if(value != ""){
        if(value >32767){
        $('#object12').val(5);  
        }
        if(value < -32768){
            $('#object12').val(1);  
        }
    }
});
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object13").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[0].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object13').val(); 
    if(value != ""){
        if(value >32767){
        $('#object13').val(5);  
        }
        if(value < -32768){
            $('#object13').val(1);  
        }
    }
});
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object14").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[0].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object14').val(); 
    if(value != ""){
        if(value >32767){
        $('#object14').val(5);  
        }
        if(value < -32768){
            $('#object14').val(1);  
        }
    }
});
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object21").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[1].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object21').val(); 
    if(value != ""){
        if(value >32767){
        $('#object21').val(5);  
        }
        if(value < -32768){
            $('#object21').val(1);  
        }
    }
});
    
    //listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object22").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[1].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object22').val(); 
    if(value != ""){
        if(value >32767){
        $('#object22').val(5);  
        }
        if(value < -32768){
            $('#object22').val(1);  
        }
    }
});
    
    //listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object31").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[2].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object31').val(); 
    if(value != ""){
        if(value >32767){
        $('#object31').val(5);  
        }
        if(value < -32768){
            $('#object31').val(1);  
        }
    }
});
    
//listener para detectar cuando el nombre de los modulos cambie y asi poder actualizar todas las fucniones con ese mismo nombre
$("#first").on("keypress keyup keydown",function(){  //redirige a otra pagina
    ///si detecta una tecla que no sea letra numero o guion bajo
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
  
    if (!((key <91 && key > 47) || (key <123 && key > 96) || key==189 || key==8 || key==46 || key==16 || (key <40 && key > 32 ) || key==12 || key==40 || key==95)) { //Space bar key code, dots, signs, etc
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    if (((key <58 && key > 47 ) || (key <40 && key > 32 ) || key==12 || key==40 || key==45) && ($('#first').val().length ==0) ) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    //cambia textos
    //console.log("entro");
    flagm3 = 1; //para recetear el mensaje secreto
    
});
    
//listener para detectar si presionas teclas en cualquier lugar de la pagino y si escriben el mensaje secreto salga la imagen secreta
document.addEventListener("keypress", function(){
    flagm2 = 0; 
    var letters = Array(98,114,105,107,111,98,111,116);  //brikobot
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    if(chCode==letters[0] && flagm==0 && flagm2==0){ secretmessage[0]=chCode;flagm=1;flagm2 = 1;} 
    if(chCode==letters[1] && flagm==1 && flagm2==0){ secretmessage[1]=chCode;flagm=2;flagm2 = 1;} 
    if(chCode==letters[2] && flagm==2 && flagm2==0){ secretmessage[2]=chCode;flagm=3;flagm2 = 1;} 
    if(chCode==letters[3] && flagm==3 && flagm2==0){ secretmessage[3]=chCode;flagm=4;flagm2 = 1;} 
    if(chCode==letters[4] && flagm==4 && flagm2==0){ secretmessage[4]=chCode;flagm=5;flagm2 = 1;} 
    if(chCode==letters[5] && flagm==5 && flagm2==0){ secretmessage[5]=chCode;flagm=6;flagm2 = 1;} 
    if(chCode==letters[6] && flagm==6 && flagm2==0){ secretmessage[6]=chCode;flagm=7;flagm2 = 1;} 
    if(chCode==letters[7] && flagm==7 && flagm2==0){ secretmessage[7]=chCode;flagm=0;flagm2 = 1;} 


    if(flagm2 ==0 || flagm3 ==1){
        flagm3 = 0;
        for(var i=0;i<secretmessage.length;i++){secretmessage[i]=0;flagm=0;}
        $tempimg.remove();
    }
    
    if(secretmessage[0]==letters[0] && secretmessage[1]==letters[1] && secretmessage[2]==letters[2] && secretmessage[3]==letters[3] && secretmessage[4]==letters[4] && secretmessage[5]==letters[5] && secretmessage[6]==letters[6] && secretmessage[7]==letters[7]){
        ////mensaje completado con exito
        $("body").append($tempimg); //agrega la imagen temporal a la pagina
        console.log("Secret message activated");
        for(var i=0;i<secretmessage.length;i++){secretmessage[i]=0;flagm=0;}
    }
});
    
</script>

<!--Para saber cuando la pagina es llamada desde un link y abra un pop-->
<script>
//cuando la pagina esta lista
$(document).ready(function() {   
<?php echo "var msg = '" .$refe_var . "'" ?>; //guarda la variable del php en javascript que se trajo como argumento al cargar la pagin a
  
console.log(msg);
if(msg == "extra1"){
  $('#POP0').foundation('reveal','open');   
}
                              
if(msg == "extra2"){
  $('#POP1').foundation('reveal','open');   
}
                              
if(msg == "extra3"){
  $('#POP2').foundation('reveal','open');   
}
                                                                                                              
});

</script>

<!-- Agremas unas librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<script>
  $(document).foundation();

  var doc = document.documentElement;
  doc.setAttribute('first', navigator.userAgent);
</script>

  </body>
</html>