<div class="row" id="reset_password_form">

	<?php echo validation_errors('<p class="error">');
		if(isset($error))
		{
			echo '<p class="error">'.$error.'</p>';
		} ?>
	<?php echo form_open('reset_password');?>
		<div class="row">
			<div class="colums large-12">
				<h2 class="text-center modalInicio">Restablecer Contrase&ntilde;a</h2>
			</div>
		</div>
		<div class="row">			
			<div class="columns large-12">
				<div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Correo</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="text" class="inputReg" name="email" placeholder="Bruno@correo.com">
			        </div>
			    </div>
			</div>
		</div>
		<div class="row collapse">
			<div class="colums large-6 large-offset-4">
				<button class="round comK" id="submitPass">Restablecer contrase&ntilde;a</button>
			</div>
		</div>
	</form>
</div>
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>