<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/comienzaahora.css">
<!--para hacer que la imagen gire al hacer hover -->
<style>
#f1_container {
  position: relative;
  margin: 10px auto;
  z-index: 1;
}
#f1_container {
  perspective: 1000;
}
#f1_card {
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transition: all 0.7s linear;
}
#f1_container:hover #f1_card {
  transform: rotateY(180deg);
}
.face {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}
.face.back {
  display: block;
  transform: rotateY(180deg);
  padding: 10px;
  text-align: center;
}
</style>

<!--Aqui es donde esta la informacion de toda la pagina -->

<?php $Rotate_text = "Me programas para <br /> controlar los brikos." ?>

<?php 
$Comentario1 = "Declara tus modulos aqui ";
$Comentario2 = "Escribe tu codigo aqui ";
?>

<!-- Arreglo para guardar los puertos-->

<!-- Arreglo para guardar las opciones del selector-->
<?php $opts_a = array("ON","OFF"); ?> 
<!-- Arreglo para guardar las opciones del selector-->
<?php $opts_b = array("KEY_LEFT_CTRL","KEY_LEFT_SHIFT","KEY_LEFT_ALT",        "KEY_LEFT_GUI","KEY_RIGHT_CTRL","KEY_RIGHT_SHIFT","KEY_RIGHT_ALT","KEY_RIGHT_GUI", "KEY_UP_ARROW","KEY_DOWN_ARROW","KEY_LEFT_ARROW", "KEY_RIGHT_ARROW", "KEY_BACKSPACE","KEY_TAB","KEY_RETURN","KEY_ESC","KEY_INSERT","KEY_DELETE", "KEY_PAGE_UP","KEY_PAGE_DOWN", "KEY_HOME" , "KEY_END" , "KEY_CAPS_LOCK","KEY_F1","KEY_F2", "KEY_F3", "KEY_F4", "KEY_F5", "KEY_F6", "KEY_F7",
"KEY_F8","KEY_F9","KEY_F10", "KEY_F11" ,"KEY_F12" ); ?> 
<?php $opts_c = array("1","2","3","4","5" ); ?> 
<!-- Arreglo para guardar las opciones del selector-->
<?php $opts_d = array("MOUSE_LEFT","MOUSE_RIGHT", "MOUSE_MIDDLE"); ?> 

<?php 
$Palabra_Descripcion = "Descripción:";
$Palabra_Funciones = "Funciones:";
$Palabra_Generarcodigo = "Generar código";
$Palabra_Copiarcodigo = "Copiar código";
$Prin_titulo = "Cerebro bk7";
$Prin_desc = "-Controlador principal de briko.";
$Label_1 = "Selecciona que tipo de función deseas ver:";
$Label_2 = "Funciones: (Seleccióna la funcion que deseas probar)";
$Tab_1 = "Basicas";
$Tab_2 = "Teclado";
$Tab_3 = "Mouse";
$Placeholder_1= "Mensaje";
$Placeholder_2= "Tiempo";
$Placeholder_3= "Posición x";
$Placeholder_4= "Posición y";
$Funcion_name_1 = array("bk7led","bk7write","bk7print","bk7read","bk7clean");
$Funcion_num_1 = array(1,1,1,2,1);
$Funcion_2_start = array_sum($Funcion_num_1);
$Funcion_name_2 = array("bk7keywrite","bk7keyprint","bk7keypress","bk7keyrelease","bk7keyreleaseall","bk7keybuttons","bk7keyend");
$Funcion_num_2 = array(1,1,2,1,1,2,1);
$Funcion_3_start = array_sum($Funcion_num_2) + $Funcion_2_start;
$Funcion_name_3 = array("bk7mouseclick","bk7mousemove","bk7mousepress","bk7mouserelease","bk7mouseend");
$Funcion_num_3 = array(1,1,1,1,1);

?>

<!--Texto que va en los modales -->
<?php 
$Modal_list;
$Palabra_Ejemplo = "Ejemplo:";
$Modal_list[0]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7led</span>(<span class='param'> ESTADO </span>);";
$Modal_list[0]["M_argg"] = array("", "bk7led" );
$Modal_list[0]["M_argdefines"] = array("ESTADO");
$Modal_list[0]["M_arg"] = array("");
$Modal_list[0]["M_descg"] = array("","Función de briko.");
$Modal_list[0]["M_descdefines"] = array("Estado del led.");
$Modal_list[0]["M_desc"] = array("");
$Modal_list[0]["M_descfun"] = "
Esta función prende o apaga el led de tu bk7. <br />
Las opciones a elegir pueden ser 'ON' y 'OFF'.
";
$Modal_list[0]["M_codigo"] = '
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {
    bk7led(ON);  //Prende el led de tu bk7.
    delay(1000); //Espera 1000 milisegundos.
    bk7led(ON);  //Apaga el led de tu bk7.
    delay(1000); //Espera 1000 milisegundos.
}
';
$Modal_list[1]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7write</span>( mensaje );";
$Modal_list[1]["M_argg"] = array("", "bk7write" );
$Modal_list[1]["M_argdefines"] = array("");
$Modal_list[1]["M_arg"] = array("mensaje");
$Modal_list[1]["M_descg"] = array("","Función de briko.");
$Modal_list[1]["M_descdefines"] = array("");
$Modal_list[1]["M_desc"] = array("Mensaje que se mostrara en la pantalla de tu pc.");
$Modal_list[1]["M_descfun"] = "
Esta función muestra un mensaje en la pantalla de tu pc. <br />
Esta función imprime los mensajes en la misma linea.
";
$Modal_list[1]["M_codigo"] = '
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {
    bk7write("hola");  //Imprime el mensaje "hola" en tu pc.
    delay(1000);       //Espera 1000 milisegundos.
}
';
$Modal_list[2]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7print</span>( mensaje );";
$Modal_list[2]["M_argg"] = array("", "bk7print" );
$Modal_list[2]["M_argdefines"] = array("");
$Modal_list[2]["M_arg"] = array("mensaje");
$Modal_list[2]["M_descg"] = array("","Función de briko.");
$Modal_list[2]["M_descdefines"] = array("");
$Modal_list[2]["M_desc"] = array("Mensaje que se mostrara en la pantalla de tu pc.");
$Modal_list[2]["M_descfun"] = "
Esta función muestra un mensaje en la pantalla de tu pc. <br />
Esta función imprime los mensajes en un nuevo renglon.
";
$Modal_list[2]["M_codigo"] = '
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {
    bk7print("hola");  //Imprime el mensaje "hola" en tu pc.
    delay(1000);       //Espera 1000 milisegundos.
}
';
$Modal_list[3]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7read</span>( );";
$Modal_list[3]["M_argg"] = array("", "bk7read" );
$Modal_list[3]["M_argdefines"] = array("");
$Modal_list[3]["M_arg"] = array("");
$Modal_list[3]["M_descg"] = array("","Función de briko.");
$Modal_list[3]["M_descdefines"] = array("");
$Modal_list[3]["M_desc"] = array("");
$Modal_list[3]["M_descfun"] = "
Esta función lee un mensaje en la pantalla de tu pc. <br />
Con esta función solo podemos leer una letra de la pc a la vez.
";
$Modal_list[3]["M_codigo"] = "
//Declara tus modulos aqui

char Var; //declara una variable de un caracter para guardar los mensajes que queremos leer

//Escribe tus codigos aqui
code() {
    Var = bk7read();  //Guarda en la variable 'Var' el mensaje leido de la pc.
    if (Var == 'a') { //Si el mensaje leido de la pc es igual a 'a'.
        bk7led(ON);       // Prende el led del BK7
        delay(1000);      // Espera 1000 milisegundos
        bk7led(OFF);      // Apaga el led del BK7
    }
    delay(10);            // Espera 10 milisegundos
}
";
$Modal_list[4]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7read</span>( &stringbk, número );";
$Modal_list[4]["M_argg"] = array("", "bk7read" );
$Modal_list[4]["M_argdefines"] = array("");
$Modal_list[4]["M_arg"] = array("&stringbk","número");
$Modal_list[4]["M_descg"] = array("","Función de briko.");
$Modal_list[4]["M_descdefines"] = array("");
$Modal_list[4]["M_desc"] = array("Objeto briko donde guardamos el mensaje.","Numero de letras que tiene el mensaje.");
$Modal_list[4]["M_descfun"] = "
Esta función lee un mensaje en la pantalla de tu pc. <br />
Con esta función podemos leer mensajes largos (máximo de 25 letras).
";
$Modal_list[4]["M_codigo"] = "
//Declara tus modulos aqui

stringbk Var; //declara una variable de un caracter para guardar los mensajes que queremos leer

//Escribe tus codigos aqui
code() {
    bk7read(&Var, 4);      //lee 4 bytes de la pc.
    if (Var.stringbuffer[0] == 'h' && Var.stringbuffer[1] == 'o' && Var.stringbuffer[2] == 'l' && Var.stringbuffer[3] == 'a' ) { //si en var leemos 'hola' que leimos de la pc.
        bk7led(ON);       //Prende el led del BK7
        delay(1000);      //Espera 1000 milisegundos
        bk7led(OFF);      //Apaga el led del BK7
    }
    delay(10);            //Espera 10 milisegundos
}
";
$Modal_list[5]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7clean</span>( );";
$Modal_list[5]["M_argg"] = array("", "bk7clean" );
$Modal_list[5]["M_argdefines"] = array("");
$Modal_list[5]["M_arg"] = array("");
$Modal_list[5]["M_descg"] = array("","Función de briko.");
$Modal_list[5]["M_descdefines"] = array("");
$Modal_list[5]["M_desc"] = array("");
$Modal_list[5]["M_descfun"] = "
Esta función borra el buffer de entrada de la pc. <br />
Esta función borra los mensajes no leidos por la función 'bk7read()'.
";
$Modal_list[5]["M_codigo"] = "
//Declara tus modulos aqui

char Var; //declara una variable de un caracter para guardar los mensajes que queremos leer

//Escribe tus codigos aqui
code() {
    Var = bk7read();  //Guarda en la variable 'Var' el mensaje leido de la pc.
    if (Var == 'a') { //Si el mensaje leido de la pc es igual a 'a'.
        bk7led(ON);       // Prende el led del BK7
        delay(1000);      // Espera 1000 milisegundos
        bk7led(OFF);      // Apaga el led del BK7
        bk7clean();       //Borra lo que quedo del mensaje que no se leyo por el 'bk7read'.
    }
    delay(10);            // Espera 10 milisegundos
}
";
$Modal_list[6]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keywrite</span>( mensaje );";
$Modal_list[6]["M_argg"] = array("", "bk7keywrite" );
$Modal_list[6]["M_argdefines"] = array("");
$Modal_list[6]["M_arg"] = array("mensaje");
$Modal_list[6]["M_descg"] = array("","Función de briko.");
$Modal_list[6]["M_descdefines"] = array("");
$Modal_list[6]["M_desc"] = array("Mensaje que se escribira en tu pc.");
$Modal_list[6]["M_descfun"] = "
Esta función escribe mensajes en tu pc como si fuera el teclado. <br />
Esta función imprime los mensajes en la misma linea.
";
$Modal_list[6]["M_codigo"] = '
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keywrite("hola");  //Escribe el mensaje "hola" en tu pc.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();           //libera los recursos del teclado

}
';
$Modal_list[7]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keyprint</span>( mensaje );";
$Modal_list[7]["M_argg"] = array("", "bk7keyprint" );
$Modal_list[7]["M_argdefines"] = array("");
$Modal_list[7]["M_arg"] = array("mensaje");
$Modal_list[7]["M_descg"] = array("","Función de briko.");
$Modal_list[7]["M_descdefines"] = array("");
$Modal_list[7]["M_desc"] = array("Mensaje que se escribira en tu pc.");
$Modal_list[7]["M_descfun"] = "
Esta función escribe mensajes en tu pc como si fuera el teclado. <br />
Esta función imprime los mensajes en un nuevo renglon.
";
$Modal_list[7]["M_codigo"] = '
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keyprint("hola");  //Escribe el mensaje "hola" en tu pc.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();           //libera los recursos del teclado

}
';
$Modal_list[8]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keypress</span>(<span class='param'> TECLA</span> ,tiempo );";
$Modal_list[8]["M_argg"] = array("", "bk7keypress" );
$Modal_list[8]["M_argdefines"] = array("TECLA");
$Modal_list[8]["M_arg"] = array("tiempo");
$Modal_list[8]["M_descg"] = array("","Función de briko.");
$Modal_list[8]["M_descdefines"] = array("Tecla que se presionara.");
$Modal_list[8]["M_desc"] = array("Tiempo que estara presionada la tecla.");
$Modal_list[8]["M_descfun"] = "
Esta función simula un teclado y presiona una tecla por un determinado tiempo.
";
$Modal_list[8]["M_codigo"] = "
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keypress(KEY_UP_ARROW, 800); //Presiona la tecla de la flecha hacia arriba por 800 milisegundos.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();           //libera los recursos del teclado

}
";
$Modal_list[9]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keypress</span>(<span class='param'> TECLA </span>);";
$Modal_list[9]["M_argg"] = array("", "bk7keypress" );
$Modal_list[9]["M_argdefines"] = array("TECLA");
$Modal_list[9]["M_arg"] = array("");
$Modal_list[9]["M_descg"] = array("","Función de briko.");
$Modal_list[9]["M_descdefines"] = array("Tecla que se presionara.");
$Modal_list[9]["M_desc"] = array("");
$Modal_list[9]["M_descfun"] = "
Esta función simula un teclado y presiona una tecla hasta que sea liberada por la funcion 'bk7keyrelease(KEY)'.
";
$Modal_list[9]["M_codigo"] = "
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keypress(KEY_UP_ARROW);  //Presiona la tecla de la flecha hacia arriba.
        delay(1000);       //Espera 1000 milisegundos.
        bk7keyrelease(KEY_UP_ARROW);  //Libera la tecla de la flecha hacia arriba p.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();           //cierra los recursos del teclado

}
";
$Modal_list[10]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keyrelease</span>(<span class='param'> TECLA </span>);";
$Modal_list[10]["M_argg"] = array("", "bk7keyrelease" );
$Modal_list[10]["M_argdefines"] = array("TECLA");
$Modal_list[10]["M_arg"] = array("");
$Modal_list[10]["M_descg"] = array("","Función de briko.");
$Modal_list[10]["M_descdefines"] = array("Tecla que se presionara.");
$Modal_list[10]["M_desc"] = array("");
$Modal_list[10]["M_descfun"] = "
Esta función simula un teclado y libera una 'TECLA' que fue presionada por la funcion 'bk7keypress(KEY)'.
";
$Modal_list[10]["M_codigo"] = "
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keypress(KEY_UP_ARROW);  //Presiona la tecla de la flecha hacia arriba.
        delay(1000);       //Espera 1000 milisegundos.
        bk7keyrelease(KEY_UP_ARROW);  //Libera la tecla de la flecha hacia arriba p.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();           //cierra los recursos del teclado

}
";
$Modal_list[11]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keyreleaseall</span>( );";
$Modal_list[11]["M_argg"] = array("", "bk7keyreleaseall" );
$Modal_list[11]["M_argdefines"] = array("");
$Modal_list[11]["M_arg"] = array("");
$Modal_list[11]["M_descg"] = array("","Función de briko.");
$Modal_list[11]["M_descdefines"] = array("");
$Modal_list[11]["M_desc"] = array("");
$Modal_list[11]["M_descfun"] = "
Esta función simula un teclado y libera 'todas' las teclas que esten presionadas por la funcion 'bk7keypress( KEY )'.
";
$Modal_list[11]["M_codigo"] = "
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keypress(KEY_UP_ARROW);  //Presiona la tecla de la flecha hacia arriba.
        delay(1000);       //Espera 1000 milisegundos.
        bk7keyreleaseall();  //Libera la tecla de la flecha hacia arriba p.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();            //cierra los recursos del teclado

}
";
$Modal_list[12]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keybuttons</span>(número, mensaje1, mensaje2, mensaje3, mensaje4, mensaje5);";
$Modal_list[12]["M_argg"] = array("", "bk7keybuttons" );
$Modal_list[12]["M_argdefines"] = array("");
$Modal_list[12]["M_arg"] = array("número","mensaje1","mensaje2","mensaje3","mensaje4","mensaje5");
$Modal_list[12]["M_descg"] = array("","Función de briko.");
$Modal_list[12]["M_descdefines"] = array("");
$Modal_list[12]["M_desc"] = array("Número que selecciona cual de los mensajes se escribira.","Mensaje que se ecribira en tu pc simulando el teclado.",
                                  "Mensaje que se ecribira en tu pc simulando el teclado.","Mensaje que se ecribira en tu pc simulando el teclado.",
                                  "Mensaje que se ecribira en tu pc simulando el teclado.","Mensaje que se ecribira en tu pc simulando el teclado.");
$Modal_list[12]["M_descfun"] = "
Esta función simula un teclado escribe un mensaje diferente en la pc dependiendo de que numero se ponga en la variable 'numero'. <br /> 
Esta función esta hecha para trabajar con el modulo de botones.
";
$Modal_list[12]["M_codigo"] = '
//Declara tus modulos aqui
buttonsbk Buttons(PORT1);    // Declara el modulo de botones en el puerto 1

int boton;                   // Variable para guardar el boton que presionas

//Escribe tus codigos aqui
code(){
    boton = Buttons.readbits();                    //Lee que boton presionas
    bk7keybuttons(boton,"hola","BRIKO","mundo","APRENDE","PROGRAMANDO");  //escribe un mensaje diferente dependiendo de que boton presiones
}
';
$Modal_list[13]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keybuttons</span>( número, <span class='param'>TECLA1</span>, <span class='param'>TECLA2</span>, <span class='param'>TECLA3</span>, <span class='param'>TECLA4</span>, <span class='param'>TECLA4</span>);";
$Modal_list[13]["M_argg"] = array("", "bk7keybuttons" );
$Modal_list[13]["M_argdefines"] = array("TECLA1","TECLA2","TECLA3","TECLA4","TECLA5");
$Modal_list[13]["M_arg"] = array("número");
$Modal_list[13]["M_descg"] = array("","Función de briko.");
$Modal_list[13]["M_descdefines"] = array("Mensaje que se ecribira en tu pc simulando el teclado.","Mensaje que se ecribira en tu pc simulando el teclado."
                                         ,"Mensaje que se ecribira en tu pc simulando el teclado.","Mensaje que se ecribira en tu pc simulando el teclado.","Mensaje que se ecribira en tu pc simulando el teclado.");
$Modal_list[13]["M_desc"] = array("Número que selecciona cual de los mensajes se escribira.");
$Modal_list[13]["M_descfun"] = "
Esta función simula un teclado y presiona una tecla diferente dependiendo de que numero se ponga en la variable 'numero'. <br /> 
Esta función esta hecha para trabajar con el modulo de botones.
";
$Modal_list[13]["M_codigo"] = "
//Declara tus modulos aqui
buttonsbk Buttons(PORT1);    // Declara el modulo de botones en el puerto 1

int boton;                   // Variable para guardar el boton que presionas

//Escribe tus codigos aqui
code(){
    boton = Buttons.readbits();                    //Lee que boton presionas
    bk7keybuttons(boton,KEY_RIGHT_ARROW,KEY_DOWN_ARROW,KEY_LEFT_ARROW,KEY_RETURN,KEY_UP_ARROW);  //Presiona una tecla diferente dependiendo de que boton presiones
}
";
$Modal_list[14]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7keyend</span>( );";
$Modal_list[14]["M_argg"] = array("", "bk7keyend" );
$Modal_list[14]["M_argdefines"] = array("");
$Modal_list[14]["M_arg"] = array("");
$Modal_list[14]["M_descg"] = array("","Función de briko.");
$Modal_list[14]["M_descdefines"] = array("");
$Modal_list[14]["M_desc"] = array("");
$Modal_list[14]["M_descfun"] = "
Esta función libera los recursos del teclado.
";
$Modal_list[14]["M_codigo"] = "
//Declara tus modulos aqui

//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7keypress(KEY_UP_ARROW);  //Presiona la tecla de la flecha hacia arriba.
        delay(1000);       //Espera 1000 milisegundos.
        bk7keyrelease(KEY_UP_ARROW);  //Libera la tecla de la flecha hacia arriba p.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7keyend();           //libera los recursos del teclado

}
";
$Modal_list[15]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7mouseclick</span>(<span class='param'> KEY </span> );";
$Modal_list[15]["M_argg"] = array("", "bk7mouseclick" );
$Modal_list[15]["M_argdefines"] = array("KEY");
$Modal_list[15]["M_arg"] = array("");
$Modal_list[15]["M_descg"] = array("","Función de briko.");
$Modal_list[15]["M_descdefines"] = array("Boton del mouse que se presiona.");
$Modal_list[15]["M_desc"] = array("");
$Modal_list[15]["M_descfun"] = "
Esta función simula un mouse y hace un click del mouse.
";
$Modal_list[15]["M_codigo"] = "
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7mouseclick(MOUSE_RIGHT);  //Hace un click derecho del mouse.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7mouseend();         //libera los recursos del mouse

}
";
$Modal_list[16]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7mousemove</span>( posición_x, posición_y );";
$Modal_list[16]["M_argg"] = array("", "bk7mousemove" );
$Modal_list[16]["M_argdefines"] = array("");
$Modal_list[16]["M_arg"] = array("posición_x","posición_y");
$Modal_list[16]["M_descg"] = array("","Función de briko.");
$Modal_list[16]["M_descdefines"] = array("");
$Modal_list[16]["M_desc"] = array("Posición en x en la pantalla donde se pondra el cursor.","Posición en y en la pantalla donde se pondra el cursor.");
$Modal_list[16]["M_descfun"] = "
Esta función simula un mouse y mueve el cursor a una posición 'x' , 'y'.
";
$Modal_list[16]["M_codigo"] = "
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7mousemove((10 * i), (10 * i)); //mueve el cursor del mouse.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7mouseend();         //libera los recursos del mouse

}
";
$Modal_list[17]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7mousepress</span>(<span class='param'> KEY </span> );";
$Modal_list[17]["M_argg"] = array("", "bk7mousepress" );
$Modal_list[17]["M_argdefines"] = array("KEY");
$Modal_list[17]["M_arg"] = array("");
$Modal_list[17]["M_descg"] = array("","Función de briko.");
$Modal_list[17]["M_descdefines"] = array("Boton del mouse que se presiona.");
$Modal_list[17]["M_desc"] = array("");
$Modal_list[17]["M_descfun"] = "
Esta función simula un mouse y deja presionado un boton del mouse.
";
$Modal_list[17]["M_codigo"] = "
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7mousepress(MOUSE_LEFT);  //Hace un click izquierdo del mouse y lo deja presionado.
        delay(1000);       //Espera 1000 milisegundos.
        bk7mouserelease(MOUSE_LEFT);  //libera el boton izquierdo del mouse.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7mouseend();         //libera los recursos del mouse

}
";
$Modal_list[18]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7mouserelease</span>(<span class='param'> KEY </span> );";
$Modal_list[18]["M_argg"] = array("", "bk7mouserelease" );
$Modal_list[18]["M_argdefines"] = array("KEY");
$Modal_list[18]["M_arg"] = array("");
$Modal_list[18]["M_descg"] = array("","Función de briko.");
$Modal_list[18]["M_descdefines"] = array("Boton del mouse que se libera.");
$Modal_list[18]["M_desc"] = array("");
$Modal_list[18]["M_descfun"] = "
Esta función simula un mouse y libera un boton del mouse que haya sido presionado por 'bk7mousepress(KEY)'.
";
$Modal_list[18]["M_codigo"] = "
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7mousepress(MOUSE_MIDDLE);  //Hace un click de enmedio del mouse y lo deja presionado.
        delay(1000);       //Espera 1000 milisegundos.
        bk7mouserelease(MOUSE_MIDDLE);  //libera el boton de enmedio del mouse.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7mouseend();         //libera los recursos del mouse

}
";
$Modal_list[19]["M_func"] = "<span class='label desKY' > Función: </span><span class='funct'> bk7mouseend</span>( );";
$Modal_list[19]["M_argg"] = array("", "bk7mouseend" );
$Modal_list[19]["M_argdefines"] = array("");
$Modal_list[19]["M_arg"] = array("");
$Modal_list[19]["M_descg"] = array("","Función de briko.");
$Modal_list[19]["M_descdefines"] = array("");
$Modal_list[19]["M_desc"] = array("");
$Modal_list[19]["M_descfun"] = "
Esta función libera los recursos del mouse.
";
$Modal_list[19]["M_codigo"] = "
//Declara tus modulos aqui


//Escribe tus codigos aqui
code() {

    for (int i = 0; i < 5; i++) { //para que haga 5 veces el mensaje
        bk7mouseclick(MOUSE_RIGHT);  //Hace un click derecho del mouse.
        delay(1000);       //Espera 1000 milisegundos.
    }

    bk7mouseend();         //libera los recursos del mouse

}
";
?>
<!---------------------------------------------------------->

<!-- Creamos el grid principal-->
<div class="row"  id="divp">
  <div class="large-12 columns">
    <br>
    <br>
    <!-- Primera imagen, descripcion y titulo -->
    <div class="row">
        <div class="small-4 columns">
            <div id="f1_container">
                <div id="f1_card" class="shadow">
                    <div class="front face">
                        <!-- front conten-->
                        <center><img alt="briko maestro b k 7"style="margin-top: 0px;" src="<?php echo base_url(); ?>images/modulosindividuales/bk7.png"; ></center>
                    </div>
                    <div class="back face center">
                        <!-- back content -->
                        <center><p style="margin-top: -10px;" class = 'pclassbk'><?php echo $Rotate_text ?></p></center>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-8 end columns">
            <h1 class = 'h1classbk2 text-justify' ><?php echo $Prin_titulo ?></h1>
            <span class='label desKY'><?php echo $Palabra_Descripcion ?></span>
            <br>
            <p class = ' pclassbk text-justify' ><?php echo $Prin_desc ?></p>
        </div>
    </div>
      
      <!-- cintillo se separacion -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <img alt="briko cintillo" width= "200%" style="height:25px" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cintillo2.png"; >
        </div>
    </div>  
      
    <!---------------------------------------------------------->
      
    <!-- Segundo titulo-->
    <div class="row">
        <div class="large-12 columns"  style="padding-bottom: 0px;">
            <h1 class = ' h1classbk2 text-justify' ><?php echo $Palabra_Funciones ?></h1>
        </div>
    </div>
      
    <!-- Label del segundo titulo(los 2 labels)-->
    <div class="row">
        <div class="small-4 columns">
            <p class='brikospanp'><?php echo $Label_1 ?></p>
        </div>
    </div>
        
    <!-- tabs para entrar a diferentes funciones --> 
    <div class="row">
        <div class="small-12 columns">
            <ul class="tabs" id="myTabs" data-tab>
                <li class="tab-title active"><a href="#panel1"><?php echo $Tab_1 ?></a></li>
                <li class="tab-title"><a href="#panel2"><?php echo $Tab_2 ?></a></li>
                <li class="tab-title"><a href="#panel3"><?php echo $Tab_3 ?></a></li>
            </ul>
        </div>
    </div>
    <!---------------------------------------------------------->
      
      
      
   <!-----------------comienzan los tabs------------------->
   <!-----------------tab 1------------------->
    <!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content">
        <!-- Primera tab -->
        <div class="content active" id="panel1">
        <!-- Label de las funciones-->
        <div class="row">
            <div class="small-5 columns">
                <p class='brikospanp'><?php echo $Label_2 ?></p>
            </div>   
        </div>
         
    <!---------------------------------------------------------->        
            
        <!-- Para modificar los anchos de los selectores y radio button en la tabla--> 
     <?php $box_width = 130 ?>
     <?php $radiob_width = 40 ?>
      
       <!-- Creamos las tablas-->
    <?php $counter = 0 ?>
    <?php for($x = 0; $x< count($Funcion_num_1); $x++) { ?>
    <?php for($j = 0; $j< $Funcion_num_1[$x]; $j++) { ?>
    <?php $counter++ ?>
    <div class="row">
        <div class="large-12 columns" style="margin-bottom:0;padding-bottom:0;" >
            <table style="border-style:hidden;margin-bottom:0;padding-bottom:0;" >
                <tbody>
                    <tr>
                        <?php if($counter==1){ //para poner la condicion de checked?>
                        <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio" value="<?php echo $counter ?>" style="margin-top:5px;" CHECKED />
                        </td>
                        <?php }else{ ?>
                         <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio" value="<?php echo $counter ?>" style="margin-top:5px;"/>
                        </td>
                        <?php } ?>
                        <td style="padding-right: 0px; padding-left:0px;"> 
                            <p class = 'text-justify' style="font-size:1.5em;color:#006D91" ><?php echo $Funcion_name_1[$x] ?></p>
                        </td>
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >(</p>
                        </td>
                        
                        <?php if($counter==1){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_a);$i++){ ?>
                                    <option>
                                        <?php echo $opts_a[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==2){ ?>
                        <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter ?>1">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==3){ ?>
                        <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter ?>1">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==4){ ?>
                        <!--nada-->
                        <?php } ?>
                        
                        <?php if($counter==5){ ?>
                        <td style="padding-right: 0px;padding-left:0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" > &Var,
                                </p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="number" placeholder="0-25" min="0" max="25" id="object<?php echo $counter ?>1">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==6){ ?>
                        <!--nada-->
                        <?php } ?>
                        
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >);</p>
                        </td>
                         <td>
                            <img alt="briko ayuda "id="but<?php echo $counter ?>1" src="<?php echo base_url(); ?>images/ayuda.png" width=30 height=30 style="cursor:pointer;margin-top:0px;padding-bottom: 19px;" />
                        </td>
                    </tr>
                </tbody>  
            </table>
        </div>   
    </div> 
      
    <!--listener de los botones -->
    <script>
    //listener del boton imagen para abrir la ayuda    
        $("#but<?php echo $counter ?>1").on("click",function(){  //abre pop
            $('#POP<?php echo $counter-1 ?>').foundation('reveal','open');
        });    
    </script>
      
    <?php } ?>
    <?php } ?>
    <!---------------------------------------------------------->
    </div>        
        
        
        <!-----------------tab 2------------------->
        <div class="content" id="panel2">
        <!-- Label de las funciones-->
        <div class="row">
            <div class="small-5 columns">
                <p class='brikospanp'><?php echo $Label_2 ?></p>
            </div>   
        </div>
         
    <!---------------------------------------------------------->        
            
     <!-- Para modificar los anchos de los selectores y radio button en la tabla--> 
     <?php $box_width = 130 ?>
     <?php $radiob_width = 40 ?>
      
       <!-- Creamos las tablas-->
    <?php $counter = 0 ?>
    <?php for($x = 0; $x< count($Funcion_num_2); $x++) { ?>
    <?php for($j = 0; $j< $Funcion_num_2[$x]; $j++) { ?>
    <?php $counter++ ?>
    <div class="row">
        <div class="large-12 columns" style="margin-bottom:0;padding-bottom:0;" >
            <table style="border-style:hidden;margin-bottom:0;padding-bottom:0;" >
                <tbody>
                    <tr>
                        <?php if($counter==1){ //para poner la condicion de checked?>
                        <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio2" value="<?php echo $counter+$Funcion_2_start ?>" style="margin-top:5px;" CHECKED />
                        </td>
                        <?php }else{ ?>
                         <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio2" value="<?php echo $counter+$Funcion_2_start ?>" style="margin-top:5px;"/>
                        </td>
                        <?php } ?>
                        <td style="padding-right: 0px; padding-left:0px;"> 
                            <p class = 'text-justify' style="font-size:1.5em;color:#006D91" ><?php echo $Funcion_name_2[$x] ?></p>
                        </td>
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >(</p>
                        </td>
                        
                        <?php if($counter==1){ ?>
                        <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>1">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==2){ ?>
                        <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>1">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==3){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="number" placeholder="<?php echo $Placeholder_2 ?>" min="0" max="30000" id="object<?php echo $counter+$Funcion_2_start ?>2">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==4){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==5){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==6){ ?>
                        <!--nada-->
                        <?php } ?>
                        
                        <?php if($counter==7){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>1">
                                    <?php for($i=0;$i<count($opts_c);$i++){ ?>
                                    <option>
                                        <?php echo $opts_c[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>2">   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>3">   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                        </tr>
                        <tr>
                            <td width=<?php echo $radiob_width ?>>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>4">   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>5">   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <input type="text" placeholder="<?php echo $Placeholder_1 ?>" maxlength="15" id="object<?php echo $counter+$Funcion_2_start ?>6">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==8){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>1">
                                    <?php for($i=0;$i<count($opts_c);$i++){ ?>
                                    <option>
                                        <?php echo $opts_c[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>2" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>3" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                        </tr>
                        <tr>
                            <td width=<?php echo $radiob_width ?>>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>4" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>5" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                            <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_2_start ?>6" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_b);$i++){ ?>
                                    <option>
                                        <?php echo $opts_b[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==9){ ?>
                        <!--nada-->
                        <?php } ?>
                        
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >);</p>
                        </td>
                         <td>
                            <img alt="briko ayuda modulo" id="but<?php echo $counter+$Funcion_2_start ?>1" src="<?php echo base_url(); ?>images/ayuda.png" width=30 height=30 style="cursor:pointer;margin-top:0px;padding-bottom: 19px;" />
                        </td>
                    </tr>
                </tbody>  
            </table>
        </div>   
    </div> 
      
    <!--listener de los botones -->
    <script>
    //listener del boton imagen para abrir la ayuda    
        $("#but<?php echo $counter+$Funcion_2_start  ?>1").on("click",function(){  //abre pop
            $('#POP<?php echo ($counter-1)+$Funcion_2_start  ?>').foundation('reveal','open');
        });    
    </script>
      
    <?php } ?>
    <?php } ?>
    <!---------------------------------------------------------->
    </div>      
        
        
    <!-----------------tab 3------------------->
        <div class="content" id="panel3">
        <!-- Label de las funciones-->
        <div class="row">
            <div class="small-5 columns">
                <p class='brikospanp'><?php echo $Label_2 ?></p>
            </div>   
        </div>
         
    <!---------------------------------------------------------->        
            
     <!-- Para modificar los anchos de los selectores y radio button en la tabla--> 
     <?php $box_width = 130 ?>
     <?php $radiob_width = 40 ?>
      
       <!-- Creamos las tablas-->
    <?php $counter = 0 ?>
    <?php for($x = 0; $x< count($Funcion_num_3); $x++) { ?>
    <?php for($j = 0; $j< $Funcion_num_3[$x]; $j++) { ?>
    <?php $counter++ ?>
    <div class="row">
        <div class="large-12 columns" style="margin-bottom:0;padding-bottom:0;" >
            <table style="border-style:hidden;margin-bottom:0;padding-bottom:0;" >
                <tbody>
                    <tr>
                        <?php if($counter==1){ //para poner la condicion de checked?>
                        <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio3" value="<?php echo $counter+$Funcion_3_start ?>" style="margin-top:5px;" CHECKED />
                        </td>
                        <?php }else{ ?>
                         <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio3" value="<?php echo $counter+$Funcion_3_start ?>" style="margin-top:5px;"/>
                        </td>
                        <?php } ?>
                        <td style="padding-right: 0px; padding-left:0px;"> 
                            <p class = 'text-justify' style="font-size:1.5em;color:#006D91" ><?php echo $Funcion_name_3[$x] ?></p>
                        </td>
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >(</p>
                        </td>
                        
                        <?php if($counter==1){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_3_start ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_d);$i++){ ?>
                                    <option>
                                        <?php echo $opts_d[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==2){ ?>
                        <td width=<?php echo $box_width ?>>
                                <input type="number" placeholder="<?php echo $Placeholder_3 ?>" min="0" max="30000" id="object<?php echo $counter+$Funcion_3_start ?>1">   
                            </td>
                            <td style="padding-left:0px;padding-right: 0px;"> 
                                <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                            </td>
                             <td width=<?php echo $box_width ?>>
                                <input type="number" placeholder="<?php echo $Placeholder_4 ?>" min="0" max="30000" id="object<?php echo $counter+$Funcion_3_start ?>2">   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==3){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_3_start ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_d);$i++){ ?>
                                    <option>
                                        <?php echo $opts_d[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==4){ ?>
                        <td width=<?php echo $box_width ?>>
                                <select id="object<?php echo $counter+$Funcion_3_start ?>1" style="color:#BB2462">
                                    <?php for($i=0;$i<count($opts_d);$i++){ ?>
                                    <option>
                                        <?php echo $opts_d[$i]; ?>
                                    </option>
                                    <?php } ?> 
                                </select>   
                            </td>
                        <?php } ?>
                        
                        <?php if($counter==5){ ?>
                        <!--nada-->
                        <?php } ?>
                        
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >);</p>
                        </td>
                         <td>
                            <img alt="briko ayuda" id="but<?php echo $counter+$Funcion_3_start ?>1" src="<?php echo base_url(); ?>images/ayuda.png" width=30 height=30 style="cursor:pointer;margin-top:0px;padding-bottom: 19px;" />
                        </td>
                    </tr>
                </tbody>  
            </table>
        </div>   
    </div> 
      
    <!--listener de los botones -->
    <script>
    //listener del boton imagen para abrir la ayuda    
        $("#but<?php echo $counter+$Funcion_3_start ?>1").on("click",function(){  //abre pop
            $('#POP<?php echo ($counter-1)+$Funcion_3_start ?>').foundation('reveal','open');
        });    
    </script>
      
    <?php } ?>
    <?php } ?>
    <!---------------------------------------------------------->
    </div>     
            
</div>
 <!-----------------terminan los tabs------------------->       

      
    <!-- Creamos el boton para generar el codigo y copiar el codigo-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="button round" id="buttongenerate" ><?php echo $Palabra_Generarcodigo ?></button>
        </div>
        <div class="small-3 columns end small-offset-2" >
            <button class="button round" id="buttoncopy" ><?php echo $Palabra_Copiarcodigo ?></button>
        </div>
    </div>
     <!---------------------------------------------------------->
     
    <!-- Creamos el editor de codigo-->
<div class="row">
    <div id="editor" >
  ////<?php echo $Comentario1 ?>
        
      
  //<?php echo $Comentario2 ?>
        
  code(){
        
  }

    </div>
</div>
 <!---------------------------------------------------------->
           
    <!-- Cerramos grid principal-->
    </div>
    <br>
    <br>
</div>
<!---------------------------------------------------------->

<!-- declaramso script para animaciones-->
<script src="<?php echo base_url(); ?>js/move/move.js"></script>
<script src="<?php echo base_url(); ?>js/move/movemodulos/maestro.js"></script>

<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 

<!--para guardar las configuraciones de los editores de los pop -->
<script>
var editorpop= new Array();
</script>

<!-- declaramos el primer modal que saldra al presionar el boton de ayuda -->
<?php for($j = 0; $j< count($Modal_list); $j++) { ?>
<div id="POP<?php echo $j ?>" class="reveal-modal big" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="row">
        <div class="small-12 end columns"  >
            <p class="functcomp"><?php echo $Modal_list[$j]["M_func"] ?></p>
        </div>
    </div>  
    <div class="row">
        <div class="large-12 columns">
            <ul class="especF">
                <li><span class="funct"><?php echo $Modal_list[$j]["M_argg"][1] ?></span>: <?php echo $Modal_list[$j]["M_descg"][1] ?> </li>
                <?php for($i = 0; $i< count($Modal_list[$j]["M_arg"]); $i++) { ?>
                <?php if($Modal_list[$j]["M_arg"][$i] != ""){ ?>
                <li><?php echo $Modal_list[$j]["M_arg"][$i] ?>: <?php echo $Modal_list[$j]["M_desc"][$i] ?></li>
                <?php } } ?>
                <?php for($i = 0; $i< count($Modal_list[$j]["M_argdefines"]); $i++) { ?>
                <?php if($Modal_list[$j]["M_argdefines"][$i] != ""){ ?>
                <li><span class="param"><?php echo $Modal_list[$j]["M_argdefines"][$i] ?></span>: <?php echo $Modal_list[$j]["M_descdefines"][$i] ?></li>
                <?php } } ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <span class='label desKY'> <?php echo $Palabra_Descripcion ?> </span>
            <p class="lead"><?php echo $Modal_list[$j]["M_descfun"] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <span class='label desKY'> <?php echo $Palabra_Ejemplo ?> </span>
            <!-- Boton de copiar-->
            <center><button id="botoncopy<?php echo $j ?>" class= "button round" style="margin-bottom: 0px; padding-top: 5px; padding-left: 10px; padding-right: 10px;padding-bottom: 5px;">Copy code</button></center>
            <div id="editorpmotor<?php echo $j ?>">
                <?php echo $Modal_list[$j]["M_codigo"]?>
            </div>  
        </div>
    </div>
        
    <?php if($j == 0) { //animacion 1 ?>
    <div class="row">
        <div class="box1"  style="text-align:center" >
            <img alt="briko maestro led prendido" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestroled.png" />
            <img alt="briko maestro led" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestro.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 1) { //animacion 2 ?>
    <div class="row">
        <div class="box2"  style="text-align:center" >
            <img alt="briko imagen compu apagada" id="c1" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png"  />
            <img alt="briko imagen compu imprimiendo hola" id="c2" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuhola.png"  style="display:none"/>
            <img alt="briko imagen compu imprimiendo hola hola" id="c3" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuholahola.png" style="display:none" />
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 2) { //animacion 3 ?>
    <div class="row">
        <div class="box3"  style="text-align:center" >
            <img alt="briko imagen compu apagada" id="c11" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png"  />
            <img alt="briko imagen compu imprimiendo hola" id="c22" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuhola.png"  style="display:none"/>
            <img alt="briko imagen compu imprimiendo hola" id="c33" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuholahola2.png" style="display:none" />
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 3) { //animacion 4 ?>
    <div class="row">
        <div class="box4"  style="text-align:center" >
            <img alt="briko maestro led" class="Change_Image2" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestro.png" />
            <img alt="briko maestro led encendido" class="Change_Image2" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestroled.png" style="display:none"/>
            <img alt="briko computadora" class="Change_Image3" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko computadora" class="Change_Image3" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compua.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 4) { //animacion 5 ?>
    <div class="row">
        <div class="box20"  style="text-align:center" >
            <img alt="briko maestro led" class="Change_Image4" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestro.png" />
            <img alt="briko maestro led encendido" class="Change_Image4" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestroled.png" style="display:none"/>
            <img alt="briko computadora" class="Change_Image5" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko computadora hola" class="Change_Image5" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuhola.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 5) { //animacion 6 ?>
    <div class="row">
        <div class="box5"  style="text-align:center" >
            <img alt="briko maestro led" class="Change_Image6" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestro.png" />
            <img alt="briko maestro led encendido" class="Change_Image6" src="<?php echo base_url(); ?>images/modulos_anim/maestro/maestroled.png" style="display:none"/>
            <img alt="briko computadora" class="Change_Image7" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko computadora" class="Change_Image7" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compua.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 6) { //animacion 7 ?>
    <div class="row">
        <div class="box6"  style="text-align:center" >
            <img alt="briko imagen compu apagada" id="c16" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png"  />
            <img alt="briko imagen compu imprimiendo hola" id="c26" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuhola.png"  style="display:none"/>
            <img alt="briko imagen compu imprimiendo hola hola" id="c36" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuholahola.png" style="display:none" />
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 7) { //animacion 8 ?>
    <div class="row">
        <div class="box7"  style="text-align:center" >
            <img alt="briko imagen compu apagada" id="c17" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png"  />
            <img alt="briko imagen compu" id="c27" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuhola.png"  style="display:none"/>
            <img alt="briko imagen compu" id="c37" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuholahola2.png" style="display:none" />
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 8) { //animacion 9 ?>
    <div class="row">
        <div class="box8"  style="text-align:center" >
            <img alt="briko imagen compu" class="Change_Image8" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko imagen teclado" class="Change_Image8" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla3.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 9) { //animacion 10 ?>
    <div class="row">
        <div class="box9"  style="text-align:center" >
            <img alt="briko imagen compu" class="Change_Image9" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko imagen teclado" class="Change_Image9" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla3.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 10) { //animacion 11 ?>
    <div class="row">
        <div class="box10"  style="text-align:center" >
            <img alt="briko imagen compu" class="Change_Image10" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko imagen teclado" class="Change_Image10" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla3.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 11) { //animacion 12 ?>
    <div class="row">
        <div class="box11"  style="text-align:center" >
            <img alt="briko imagen compu" class="Change_Image11" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img alt="briko imagen teclado" class="Change_Image11" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla3.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 12) { //animacion 13 ?>
    <div class="row">
        <div class="box12"  style="text-align:center" >
            <img class="Change_Image12" src="<?php echo base_url(); ?>images/modulos_anim/maestro/botonp.png" />
            <img class="Change_Image12" src="<?php echo base_url(); ?>images/modulos_anim/maestro/botonp3.png" style="display:none"/>
            <img class="Change_Image13" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuhola.png" />
            <img class="Change_Image13" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compumundo.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 13) { //animacion 14 ?>
    <div class="row">
        <div class="box13"  style="text-align:center" >
            <img class="Change_Image14" src="<?php echo base_url(); ?>images/modulos_anim/maestro/botonp.png" />
            <img class="Change_Image14" src="<?php echo base_url(); ?>images/modulos_anim/maestro/botonp3.png" style="display:none"/>
            <img class="Change_Image15" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla1.png" />
            <img class="Change_Image15" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla2.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 14) { //animacion 15 ?>
    <div class="row">
        <div class="box14"  style="text-align:center" >
            <img class="Change_Image16" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compu.png" />
            <img class="Change_Image16" src="<?php echo base_url(); ?>images/modulos_anim/maestro/computecla3.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 15) { //animacion 16 ?>
    <div class="row">
        <div class="box15"  style="text-align:center" >
            <img class="Change_Image17" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouse.png" />
            <img class="Change_Image17" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouseright.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 16) { //animacion 17 ?>
    <div class="row">
        <div class="box16"  style="text-align:center" >
            <img class="Change_Image18" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuflec1.png" />
            <img class="Change_Image18" src="<?php echo base_url(); ?>images/modulos_anim/maestro/compuflec2.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 17) { //animacion 18 ?>
    <div class="row">
        <div class="box17"  style="text-align:center" >
            <img class="Change_Image19" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouse.png" />
            <img class="Change_Image19" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouseleft.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 18) { //animacion 19 ?>
    <div class="row">
        <div class="box18"  style="text-align:center" >
            <img class="Change_Image20" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouse.png" />
            <img class="Change_Image20" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mousemiddle.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 19) { //animacion 20 ?>
    <div class="row">
        <div class="box19"  style="text-align:center" >
            <img class="Change_Image21" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouse.png" />
            <img class="Change_Image21" src="<?php echo base_url(); ?>images/modulos_anim/maestro/mouseright.png" style="display:none"/>
        </div>
    </div>
    <?php } ?>
    
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!---------------------------------------------------------->

<!--Se modifica el editor de los modales (estilo y links) -->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var numx = parseInt("<?php echo $j ?>");
editorpop[numx] = ace.edit("editorpmotor<?php echo $j ?>");  //liga el editor declaradoe en html
editorpop[numx].getSession().setMode( xml_mode ); //pone el modo
editorpop[numx].setTheme("ace/theme/brikode"); //pone el tema
editorpop[numx].getSession().setTabSize(2);
editorpop[numx].getSession().setUseWrapMode(true);
editorpop[numx].setReadOnly(true);  // false to make it editable
</script> 
<!---------------------------------------------------------->

<script>
//listener para copiar el codigo en el editor de texto
$("#botoncopy<?php echo $j ?>").on("click",function(){ 
var numx = parseInt("<?php echo $j ?>");
var Strind_editor  =editorpop[numx].getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
</script>
    
<style>
#editorpmotor<?php echo $j ?> { 
margin-left: 15px;
  margin-top: 15px;
  height: 300px;
 font-size: 14px;
}    
</style>

<?php } ?>
<!---------------------------------------------------------->

<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor = ace.edit("editor");  //liga el editor declaradoe en html
editor.getSession().setMode( xml_mode ); //pone el modo
editor.setTheme("ace/theme/brikode"); //pone el tema
editor.getSession().setTabSize(2);
editor.getSession().setUseWrapMode(true);
editor.setReadOnly(true);  // false to make it editable
</script> 
<!---------------------------------------------------------->

<!-- Script donde tenemos todos los listeners --> 
<script>
var secretmessage = Array(0,0,0,0,0,0,0,0); //donde se guardaran las letras
var flagm = 0; //variable para saber en que letra va
var flagm2 = 0; //variable para saber si se equivoco y hay que resetear
var flagm3 = 0;  //para resetear si presionan en otro lado que no sea en la nada
var $tempimg = $("<img>"); //para crear una imagen temporal en la pagina
$tempimg.attr("src","<?php echo base_url(); ?>images/primerprograma/paso15.png");//carga la imagen secreta
$tempimg.attr("style","margin-left: 200px;");//la centra
    
var tab_selector  = "panel1";  //por default esta en la 1
//listener para saber si cambias de tab
  $('#myTabs').on('toggled', function (event, tab) {
    //guarda el nombre del tab donde estas
    tab_selector = tab.context.href;
  });

//listener para copiar el codigo en el editor de texto
$("#buttoncopy").on("click",function(){ 
var Strind_editor  =editor.getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
   
//listener para generar el codigo cuando presionen el boton
$("#buttongenerate").on("click",function(){  
    if( tab_selector.indexOf("panel1")>-1){
    var value = $('input:radio[name=funcionesradio]:checked').val().toString(); 
    }
    
    if( tab_selector.indexOf("panel2")>-1){
    var value = $('input:radio[name=funcionesradio2]:checked').val().toString(); 
    }
    
    if( tab_selector.indexOf("panel3")>-1){
    var value = $('input:radio[name=funcionesradio3]:checked').val().toString(); 
    }
    
    if(value ==1){
    var num1 = $('#object11').val().toString();
    if(num1 == "ON"){num2="OFF";}
    if(num1 == "OFF"){num2="ON";}
    editor.gotoLine(1);
    editor.setValue(
    "\n" + 
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "bk7led("+num1+");\n  " +
    "delay(1000);\n  " +
    "bk7led("+num2+");\n  " +
    "delay(1000);\n" +
    "}\n");
    }
    
    if(value ==2){
    var num1 = $('#object21').val().toString();
    if(num1 == ""){num1="hola";}
    editor.gotoLine(1);
    editor.setValue(
    "\n" + 
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "bk7write(\""+num1+"\");\n  " +
    "delay(1000);\n" +
    "}\n");
    }
    
    if(value ==3){
    var num1 = $('#object31').val().toString();
    if(num1 == ""){num1="hola";}
    editor.gotoLine(1);
    editor.setValue(
    "\n" + 
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "bk7print(\""+num1+"\");\n  " +
    "delay(1000);\n" +
    "}\n");
    }
    
    if(value ==4){
    editor.gotoLine(1);
    editor.setValue(
    "char Var;\n" + 
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "Var= bk7read();\n  " +
    "if(Var=='a'){\n    " +
    "bk7led(ON);\n    " +
    "delay(1000);\n    " +
    "bk7led(OFF);\n    " +
    "}\n    " +
    "delay(10);\n" +
    "}\n");
    }
    
    if(value ==5){
    var num1 = $('#object51').val().toString();
    if(num1 == ""){num1="4";}
    var large_string = "";
    for( var j = 0; j<parseInt(num1)-1; j++){
        large_string = large_string +"Var.stringbuffer["+j.toString()+"]=='a' && ";     
    }
    large_string = large_string + "Var.stringbuffer["+num1+"]=='a'";
    editor.gotoLine(1);
    editor.setValue(
    "stringbk Var;\n" + 
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "bk7read(&Var,"+num1+");\n  " +
    "if("+ large_string +
    "){\n    " +
    "bk7led(ON);\n    " +
    "delay(1000);\n    " +
    "bk7led(OFF);\n    " +
    "}\n    " +
    "delay(10);\n" +
    "}\n");
    }
    
    if(value ==6){
    editor.gotoLine(1);
    editor.setValue(
    "char Var;\n" + 
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "Var= bk7read();\n  " +
    "if(Var=='a'){\n    " +
    "bk7led(ON);\n    " +
    "delay(1000);\n    " +
    "bk7led(OFF);\n    " +
    "bk7clean();\n    " +
    "}\n    " +
    "delay(10);\n" +
    "}\n");
    }

    if(value ==7){
    var num1 = $('#object71').val().toString();
    if(num1 == ""){num1="hola";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keywrite(\""+num1+"\");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==8){
    var num1 = $('#object81').val().toString();
    if(num1 == ""){num1="hola";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keyprint(\""+num1+"\");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==9){
    var num1 = $('#object91').val().toString();
    var num2 = $('#object92').val().toString();
    if(num2 == ""){num2="800";}
    var delay_t = (parseInt(num2)+200).toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keypress("+num1+","+num2+");\n      "  +
    "delay("+delay_t+");\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==10){
    var num1 = $('#object101').val().toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keypress("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "bk7keyrelease("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==11){
    var num1 = $('#object111').val().toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keypress("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "bk7keyrelease("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==12){
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keypress(KEY_UP_ARROW);\n      "  +
    "delay(1000);\n      "  +
    "bk7keyreleaseall();\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==13){
    var num1 = $('#object131').val().toString();
    var num2 = $('#object132').val().toString();
    if(num2 == ""){num2="hola";}
    var num3 = $('#object133').val().toString();
    if(num3 == ""){num3="mundo";}
    var num4 = $('#object134').val().toString();
    if(num4 == ""){num4="briko";}
    var num5 = $('#object135').val().toString();
    if(num5 == ""){num5="programa";}
    var num6 = $('#object136').val().toString();
    if(num6 == ""){num6="aprende";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "bk7keybuttons(\""+num1+"\",\""+num2+"\",\""+num3+"\",\""+num4+"\",\""+num5+"\",\""+num6+"\");\n  " +
    "delay(1000);\n  " +
    "}\n");
    }
    
    if(value ==14){
    var num1 = $('#object141').val().toString();
    var num2 = $('#object142').val().toString();
    var num3 = $('#object143').val().toString();
    var num4 = $('#object144').val().toString();
    var num5 = $('#object145').val().toString();
    var num6 = $('#object146').val().toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "bk7keybuttons("+num1+","+num2+","+num3+","+num4+","+num5+","+num6+");\n  " +
    "delay(1000);\n  " +
    "}\n");
    }
    
    if(value ==15){
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7keypress(KEY_UP_ARROW);\n      "  +
    "delay(1000);\n      "  +
    "bk7keyrelease(KEY_UP_ARROW);\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7keyend();\n  " + 
    "}\n");
    }
    
    if(value ==16){
    var num1 = $('#object161').val().toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7mouseclick("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7mouseend();\n  " + 
    "}\n");
    }
    
    if(value ==17){
    var num1 = $('#object171').val().toString();
    if(num1 == ""){num1="10";}
    var num2 = $('#object172').val().toString();
    if(num2 == ""){num2="10";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7mousemove(("+num1+"*i),("+num2+"*i));\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7mouseend();\n  " + 
    "}\n");
    }
    
    if(value ==18){
    var num1 = $('#object181').val().toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7mousepress("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "bk7mouserelease("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7mouseend();\n  " + 
    "}\n");
    }
    
    if(value ==19){
    var num1 = $('#object191').val().toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7mousepress("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "bk7mouserelease("+num1+");\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7mouseend();\n  " + 
    "}\n");
    }
    
    if(value ==20){
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    "for(int i=0; i<5; i++){\n      " +
    "bk7mouseclick(MOUSE_LEFT);\n      "  +
    "delay(1000);\n      "  +
    "}\n      " + 
    "bk7mouseend();\n  " + 
    "}\n");
    }
    
});    
  
//listener para mover el radio button
$("#object11").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[0].checked  = true; 
});  
    
//listener para mover el radio button
$("#object21").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[1].checked  = true; 
});  
    
//listener para mover el radio button
$("#object31").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[2].checked  = true; 
});  
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object51").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[4].checked  = true;
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object51').val(); 
    if(value != ""){
        if(value >25){
            $('#object51').val(25);  
        }
        if(value <0){
            $('#object51').val(0);  
        }
    }
});
 
//listener para mover el radio button
$("#object71").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[0].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object81").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[1].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object91").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[2].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object92").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[2].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object92').val(); 
    if(value != ""){
        if(value >30000){
            $('#object92').val(30000);  
        }
        if(value <0){
            $('#object92').val(0);  
        }
    }
}); 
    
//listener para mover el radio button
$("#object101").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[3].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object111").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[4].checked  = true; 
}); 

//listener para mover el radio button
$("#object131").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[6].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object132").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[6].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object133").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[6].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object134").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[6].checked  = true; 
}); 

//listener para mover el radio button
$("#object135").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[6].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object136").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[6].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object141").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[7].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object142").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[7].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object143").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[7].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object144").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[7].checked  = true; 
}); 

//listener para mover el radio button
$("#object145").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[7].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object146").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio2]');
    bbradio[7].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object161").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio3]');
    bbradio[0].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object171").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio3]');
    bbradio[1].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object171').val(); 
    if(value != ""){
        if(value >30000){
            $('#object171').val(30000);  
        }
        if(value <0){
            $('#object171').val(0);  
        }
    }
}); 
    
//listener para mover el radio button
$("#object172").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio3]');
    bbradio[1].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object172').val(); 
    if(value != ""){
        if(value >30000){
            $('#object172').val(30000);  
        }
        if(value <0){
            $('#object172').val(0);  
        }
    }
}); 
    
//listener para mover el radio button
$("#object181").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio3]');
    bbradio[2].checked  = true; 
}); 
    
//listener para mover el radio button
$("#object191").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio3]');
    bbradio[3].checked  = true; 
}); 
     
//listener para detectar si presionas teclas en cualquier lugar de la pagino y si escriben el mensaje secreto salga la imagen secreta
document.addEventListener("keypress", function(){
    flagm2 = 0; 
    var letters = Array(98,114,105,107,111,98,111,116);  //brikobot
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    if(chCode==letters[0] && flagm==0 && flagm2==0){ secretmessage[0]=chCode;flagm=1;flagm2 = 1;} 
    if(chCode==letters[1] && flagm==1 && flagm2==0){ secretmessage[1]=chCode;flagm=2;flagm2 = 1;} 
    if(chCode==letters[2] && flagm==2 && flagm2==0){ secretmessage[2]=chCode;flagm=3;flagm2 = 1;} 
    if(chCode==letters[3] && flagm==3 && flagm2==0){ secretmessage[3]=chCode;flagm=4;flagm2 = 1;} 
    if(chCode==letters[4] && flagm==4 && flagm2==0){ secretmessage[4]=chCode;flagm=5;flagm2 = 1;} 
    if(chCode==letters[5] && flagm==5 && flagm2==0){ secretmessage[5]=chCode;flagm=6;flagm2 = 1;} 
    if(chCode==letters[6] && flagm==6 && flagm2==0){ secretmessage[6]=chCode;flagm=7;flagm2 = 1;} 
    if(chCode==letters[7] && flagm==7 && flagm2==0){ secretmessage[7]=chCode;flagm=0;flagm2 = 1;} 


    if(flagm2 ==0 || flagm3 ==1){
        flagm3 = 0;
        for(var i=0;i<secretmessage.length;i++){secretmessage[i]=0;flagm=0;}
        $tempimg.remove();
    }
    
    if(secretmessage[0]==letters[0] && secretmessage[1]==letters[1] && secretmessage[2]==letters[2] && secretmessage[3]==letters[3] && secretmessage[4]==letters[4] && secretmessage[5]==letters[5] && secretmessage[6]==letters[6] && secretmessage[7]==letters[7]){
        ////mensaje completado con exito
        $("body").append($tempimg); //agrega la imagen temporal a la pagina
        console.log("Secret message activated");
        for(var i=0;i<secretmessage.length;i++){secretmessage[i]=0;flagm=0;}
    }
});
    
</script>


<!--Para saber cuando la pagina es llamada desde un link y abra un pop-->
<script>
//cuando la pagina esta lista
$(document).ready(function() {   
<?php echo "var msg = '" .$refe_var . "'" ?>; //guarda la variable del php en javascript que se trajo como argumento al cargar la pagin a
  
if(msg == "maestro11"){
  $('#POP0').foundation('reveal','open');   
}
                              
if(msg == "maestro12"){
  $('#POP1').foundation('reveal','open');   
}
                              
if(msg == "maestro13"){
  $('#POP2').foundation('reveal','open');   
}
                              
if(msg == "maestro14"){
  $('#POP3').foundation('reveal','open');   
}
                              
if(msg == "maestro15"){
  $('#POP4').foundation('reveal','open');   
}
                              
if(msg == "maestro16"){
  $('#POP5').foundation('reveal','open');   
}
                              
if(msg == "maestro21"){
  $('#POP6').foundation('reveal','open');   
}
                              
if(msg == "maestro22"){
  $('#POP7').foundation('reveal','open');   
}
                              
if(msg == "maestro23"){
  $('#POP8').foundation('reveal','open');   
}
                              
if(msg == "maestro24"){
  $('#POP9').foundation('reveal','open');   
}
                              
if(msg == "maestro25"){
  $('#POP10').foundation('reveal','open');   
}
                              
if(msg == "maestro26"){
  $('#POP11').foundation('reveal','open');   
}
                              
if(msg == "maestro27"){
  $('#POP12').foundation('reveal','open');   
}
                              
if(msg == "maestro28"){
  $('#POP13').foundation('reveal','open');   
}
                              
if(msg == "maestro29"){
  $('#POP14').foundation('reveal','open');   
}
                              
if(msg == "maestro31"){
  $('#POP15').foundation('reveal','open');   
}
                              
if(msg == "maestro32"){
  $('#POP16').foundation('reveal','open');   
}
                              
if(msg == "maestro33"){
  $('#POP17').foundation('reveal','open');   
}
                              
if(msg == "maestro34"){
  $('#POP18').foundation('reveal','open');   
}
                              
if(msg == "maestro35"){
  $('#POP19').foundation('reveal','open');   
}

});

</script>

<!-- Agremas unas librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<script>
  $(document).foundation();

  var doc = document.documentElement;
  doc.setAttribute('first', navigator.userAgent);
</script>

  </body>
</html>