<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>	

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/transitions.css" media="all" />
<script src="<?php echo base_url(); ?>js/collagePlus/jquery.collagePlus.js"></script>
<script src="<?php echo base_url(); ?>js/collagePlus/jquery.removeWhitespace.js"></script>
<script src="<?php echo base_url(); ?>js/collagePlus/jquery.collageCaption.js"></script>

<div class="row cover" >
    <div class="large-8 large-offset-2  columns">
    	<div class="row">
    		<h1 id="tmail">Makers for Good</h1>
	    	<div class="orbit-container">
				<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
				
					<li> 
					    <img alt="briko Makers for Good 1" src="<?php echo base_url(); ?>images/eventos/PosterBrikoMFG1.jpg" alt="slide1"  />
					</li>  
				
				   	<li> 
					    <img alt="briko Makers for Good 2" src="<?php echo base_url(); ?>images/eventos/PosterBrikoMFG2.jpg" alt="slide1"  />
				  	</li> 

				  	<li class="active"> 
					    <img alt="briko Makers for Good 3" src="<?php echo base_url(); ?>images/eventos/PosterBrikoMFG3.jpg" alt="slide1"  />
				  	</li>  
			
				</ul>

				
			</div>
		</div>

		<div class="row">
			<h1 id="tmail">Campus Party 2015!</h1>
	    	<div class="orbit-container">
				<ul id = "slideim2" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
				
					<li> 
					    <img alt="Briko campus party startup camp" src="<?php echo base_url(); ?>images/eventos/brikoo2.png" alt="slide1"  />
					</li>  
				
				   	<li> 
					    <img alt="briko campus party stand posible" src="<?php echo base_url(); ?>images/eventos/2b.png" alt="slide1"  />
				  	</li> 

				  	<li class="active"> 
					    <img alt="briko campus party stand labsol" src="<?php echo base_url(); ?>images/eventos/1a.png" alt="slide1"  />
				  	</li>  
			
				</ul>

				
			</div>
		</div> 
        
        <div class="row">
    		<h1 id="tmail">San Luis Potosi StartupWeek</h1>
	    	<div class="orbit-container">
				<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
				
					<li> 
					    <img alt="briko startup weekend evento 1" src="<?php echo base_url(); ?>images/eventos/startupweekend1.jpg" alt="slide1"  />
					</li>  
				
				   	<li> 
					    <img alt="briko startup weekend evento 2" src="<?php echo base_url(); ?>images/eventos/startupweekend2.jpg" alt="slide1"  />
				  	</li> 

				</ul>

				
			</div>
		</div>
        
    </div>    
</div>

		<script>
	      $(document).foundation();

	      var doc = document.documentElement;
	      doc.setAttribute('data-useragent', navigator.userAgent);
	    </script>
       <script type="text/javascript">

		// All images need to be loaded for this plugin to work so
		// we end up waiting for the whole window to load in this example
		$(window).load(function () {
		    $(document).ready(function(){
		        collage();
		        $('.Collage').collageCaption();
		    });
		});


		// Here we apply the actual CollagePlus plugin
		function collage() {
		    $('.Collage').removeWhitespace().collagePlus(
		        {
		            'fadeSpeed'     : 2000,
		            'targetHeight'  : 200,
		            'effect'        : 'effect-6',
		            'direction'     : 'vertical'
		        }
		    );
		};

		// This is just for the case that the browser window is resized
		var resizeTimer = null;
		$(window).bind('resize', function() {
		    // hide all the images until we resize them
		    $('.Collage .Image_Wrapper').css("opacity", 0);
		    // set a timer to re-apply the plugin
		    if (resizeTimer) clearTimeout(resizeTimer);
		    resizeTimer = setTimeout(collage, 200);
		});

		</script>

	</body>
</html>