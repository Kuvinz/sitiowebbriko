<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<?php 
$mBuz=[];
$mLuz=[];
$mTemp=[];
$mBot=[];
$mPot=[];
$mMot=[];
$mLed=[];
$mDis=[];
$mDisp=[];
foreach ($proyxmod['mBuz'] as $value) {
	array_push($mBuz, $value->ID_proyecto);
	
}
foreach ($proyxmod['mLuz'] as $value) {
	array_push($mLuz, $value->ID_proyecto);
	
}
foreach ($proyxmod['mTemp'] as $value) {
	array_push($mTemp, $value->ID_proyecto);
	
}
foreach ($proyxmod['mBot'] as $value) {
	array_push($mBot, $value->ID_proyecto);
	
}
foreach ($proyxmod['mPot'] as $value) {
	array_push($mPot, $value->ID_proyecto);
	
}
foreach ($proyxmod['mMot'] as $value) {
	array_push($mMot, $value->ID_proyecto);
	
}
foreach ($proyxmod['mLed'] as $value) {
	array_push($mLed, $value->ID_proyecto);
	
}
foreach ($proyxmod['mDis'] as $value) {
	array_push($mDis, $value->ID_proyecto);
	
}
foreach ($proyxmod['mDisp'] as $value) {
	array_push($mDisp, $value->ID_proyecto);
	
}
?>
	<div class="row collapse" style="margin-top:1rem;">
		<div class="large-3  columns" >
		 	<div id="myDropdown"></div>
		</div>
		<div class="large-9  columns" id="imagenesFiltros">
		 	
		</div>
	</div>
	<div class="row collapse">
		<div class="large-3  columns" >
		 	<div id="myDropdownCat"></div>
		</div>
		<div class="large-9  columns" id="imagenesFiltros">
		 	
		</div>
	</div>
	          <div class="row" style="margin-top:1rem;">
					    
				            <div class="large-12  columns" >
				              <div class="row" id="proyectos">
						     		
								<!-- Codigo para imprimir todos los proyectos-->
								<?php
					            $base= base_url();
					            $contadori=0;
					            $imagentemp;
					            //print_r($holap);
					            foreach ($holap as $value)
					            {
					            	foreach ($imagenesnew as $value1)
					            	{
					            		if($value1->ID_Proyecto==$value->ID_Proyecto)
					            		{
					            			
					            			$contadori++;
					            			$imagentemp=$value1->Imagen;
					            		}
					            	}
					            	if($contadori==0)
					            		$img='default1.png';
					            	else
					            		$img=$imagentemp;
					            echo "<div class='large-4 small-6 columns end' style='display:block;' id='P".$value->ID_Proyecto."'>";
					            if($img=="default1.png")
					            {
					            	echo "<img data-reveal-id='proyeP' class='imgP' onclick='info(".$value->ID_Proyecto.",\"".$base."\",\"".$img."\")'  src='".$base."images/proyectos/default1.png'>";
					            }
					            else
					            {
					            	echo "<img data-reveal-id='proyeP' class='imgP' onclick='info(".$value->ID_Proyecto.",\"".$base."\",\"".$img."\")'  src='".$base."uploads/".$value->ID_Proyecto."/".$img."'>";
					            }
				                
				     
				                echo "<div class='panel'>";
				                echo "<h5><a data-reveal-id='proyeP' onclick='info(".$value->ID_Proyecto.",\"".$base."\",\"".$img."\")' >".$value->Nombre."</a></h5>";
				                
				                echo "</div>";
				                echo "</div>";
				                $contadori=0;
					            }
					            ?>
				               
				              </div>
				     
				            </div>
				            
				            
	          </div>
	          

   
		<script src="<?php echo base_url();?>js/ddslick.js"></script>        
		<script id="source" language="javascript" type="text/javascript">
			var array = new Array();
			$('div','#proyectos').each(function(){
		    	array.push($(this).attr('id')); 
		    });
		    array=array.filter(Boolean);
		    //alert(array);
		    var ddBasic1 = [
			    { text: "Edu", value: 1, },
			    { text: "Robot", value: 2, },
			    { text: "Maker", value: 4, },
			    { text: "Dise&ntilde;o", value: 5, },
			    { text: "Luz", value: 6, },
			    { text: "Hack", value: 7, },
			    { text: "Musica", value: 8, },
			    { text: "Sonido", value: 9, },
			    { text: "Hogar", value: 10, },
			    { text: "Automatizacion", value: 11, },
			    { text: "Bluetooth", value: 12, },
			    { text: "Fun", value: 13, },
			    { text: "Juegos", value: 14, }
			];

			$('#myDropdownCat').ddslick({
			    data: ddBasic1,
			    selectText: "Selecciona una categoria"
			});
		</script>
      <script id="source" language="javascript" type="text/javascript">
      //Dropdown plugin data
      var contador=0;
      var contadorextra=0;
      var mBuz=<?php echo json_encode( $mBuz); ?>;
      var mLuz=<?php echo json_encode( $mLuz); ?>;
      var mTemp=<?php echo json_encode( $mTemp); ?>;
      var mBot=<?php echo json_encode( $mBot); ?>;
      var mPot=<?php echo json_encode( $mPot); ?>;
      var mMot=<?php echo json_encode( $mMot); ?>;
      var mLed=<?php echo json_encode( $mLed); ?>;
      var mDis=<?php echo json_encode( $mDis); ?>;
      var mDisp=<?php echo json_encode( $mDisp); ?>;
      //alert(mBuz);
      var proyectosActivos=[];
      var proyectosActivos1=[];
      var proyectosNuevos=[];
      var modulosSeleccionados=[];
      var temporal=[];
      var moduloanterior=0;
      var url="<?php echo base_url();?>";
		var ddData = [
		    {
		        
		        value: 1,
		        selected: false,		        
		        imageSrc: "http://briko.cc/images/modulos/1.png"
		    },
		    {
		        value: 2,
		        
		        selected: false,   
		        imageSrc: "http://briko.cc/images/modulos/2.png"
		    },
		    {
		    	
		        value: 3,
		        selected: false,
		        imageSrc: "http://briko.cc/images/modulos/3.png"
		    },
		    {
		        
		        value: 4,
		        selected: false,
		        
		        imageSrc: "http://briko.cc/images/modulos/4.png"
		    },
		    {
		    	
		        value: 5,
		        selected: false,
		        imageSrc: "http://briko.cc/images/modulos/5.png"
		    },
		    {
		    	
		        value: 6,
		        selected: false,
		        imageSrc: "http://briko.cc/images/modulos/6.png"
		    },
		    {
		    	
		        value: 7,
		        selected: false,
		        imageSrc: "http://briko.cc/images/modulos/7.png"
		    },
		    {
		    	
		        value: 8,
		        selected: false,   
		        imageSrc: "http://briko.cc/images/modulos/8.png"
		    },
		    {
		    	
		        value: 9,
		        selected: false,
		        imageSrc: "http://briko.cc/images/modulos/9.png"
		    }
		];
		$('#myDropdown').ddslick({
		    data:ddData,
		    width:300,
		    height:400,
		    selectText: "Selecciona un briko",

		    imagePosition:"left",
		    onSelected: function(data){
		    	if(modulosSeleccionados.length==0)
		    		{
		    			//console.log("primerbusqueda");
		    			moduloanterior=data.selectedData.value;
		    			if(contador==0)
			    		{
			    			$( "#imagenesFiltros" ).append( "<img  onClick='qFiltro("+data.selectedData.value+")' class='iconFil' id='I"+data.selectedData.value+"' src='"+url+"images/modulos/tachita/"+data.selectedData.value+".png'>" );
			             	$( "#proyectos" ).children().css( "display", "none" );
			              	//console.log(data.selectedData.value);
			              	$("#I"+data.selectedData.value).css("display", "inline-block");
			              	modulosSeleccionados.push(data.selectedData.value);
			              	proyectos(data.selectedData.value);

		              	}
		              	else
		              	{
		              		$( "#imagenesFiltros" ).append( "<img  onClick='qFiltro("+data.selectedData.value+")' class='iconFil' id='I"+data.selectedData.value+"' src='"+url+"images/modulos/notachita/"+data.selectedData.value+".png'>" );
		              		$("#I"+data.selectedData.value).css("display", "inline-block");
		              		modulosSeleccionados.push(data.selectedData.value);
			              	proyectos(data.selectedData.value);
		              	}

		    		}
		    	else if(modulosSeleccionados.length>=0&&modulosSeleccionados.indexOf(data.selectedData.value)>0)
		    		console.log("ya se selecciono ese valor");
		    	else if(modulosSeleccionados.length>=0&&modulosSeleccionados.indexOf(data.selectedData.value)<0)
		    	{
		    		
		    		
		    		
		    		if(contador==0)
			    		{
			    			$( "#imagenesFiltros" ).append( "<img  onClick='qFiltro("+data.selectedData.value+")' class='iconFil' id='I"+data.selectedData.value+"' src='"+url+"images/modulos/notachita/"+data.selectedData.value+".png'>" );
			    			$("#I"+moduloanterior).attr('src', '<?php echo base_url();?>images/modulos/notachita/'+moduloanterior+'.png');
		    				$("#I"+data.selectedData.value).attr('src', '<?php echo base_url();?>images/modulos/tachita/'+data.selectedData.value+'.png');
		    				moduloanterior=data.selectedData.value;
			             	$( "#proyectos" ).children().css( "display", "none" );
			              	//console.log(data.selectedData.value);
			              	$("#I"+data.selectedData.value).css("display", "inline-block");
			              	modulosSeleccionados.push(data.selectedData.value);
			              	proyectos(data.selectedData.value);

		              	}
		              	else
		              	{
		              		$( "#imagenesFiltros" ).append( "<img  onClick='qFiltro("+data.selectedData.value+")' class='iconFil' id='I"+data.selectedData.value+"' src='"+url+"images/modulos/notachita/"+data.selectedData.value+".png'>" );
		              		$("#I"+moduloanterior).attr('src', '<?php echo base_url();?>images/modulos/notachita/'+moduloanterior+'.png');
		    				$("#I"+data.selectedData.value).attr('src', '<?php echo base_url();?>images/modulos/tachita/'+data.selectedData.value+'.png');
		    				moduloanterior=data.selectedData.value;

		              		$("#I"+data.selectedData.value).css("display", "inline-block");
		              		modulosSeleccionados.push(data.selectedData.value);
			              	proyectos(data.selectedData.value);
		              	}
		    	}
		    		
	              	
		    }   
		});
		var moduloselec;
		var moduloselec1;
		function qFiltro(iModulo)
		{
			moduloselec=modulosSeleccionados.pop();
			//console.log("modulo ultimo en"+moduloselec);
			if(moduloselec!=iModulo)
			{
				modulosSeleccionados.push(moduloselec);
				return false;
			}
			else
			{

				modulosSeleccionados.push(moduloselec);
				contador--;
				$( "#I"+iModulo ).remove();
				//$("#I"+iModulo).css("display", "none");
				modulosSeleccionados.pop();

				moduloselec1=modulosSeleccionados.pop();
				$("#I"+moduloselec1).attr('src', '<?php echo base_url();?>images/modulos/tachita/'+moduloselec1+'.png');
				modulosSeleccionados.push(moduloselec1);
			}

			
			if(contador==0)
			{
				tProyectos();
				proyectosActivos=[];
			    proyectosActivos1=[];
			    proyectosNuevos=[];
			    modulosSeleccionados=[];
			}
			else
			{
				contadorextra--;
				$( "#proyectos" ).children().css( "display", "none" );
				function logArrayElements4(element, index, array)
		      	{

				    //console.log("quitando filtros y mostrando nuevos a[" + index + "] = " + array);
				    $("#P"+array[index]).css("display", "block");
				}
				function mostrarProyectosyAsi2(element,index,array)
				{
					
					if(contadorextra==0)
						console.log("vacio");
					else
					{
						temporal=getIntersect(arregloAUtilizar(array[index]),arregloAUtilizar(array[index+1]));
						contadorextra--;
					}
					//temporal=arregloAUtilizar(array[index]);

				}
				modulosSeleccionados.forEach(mostrarProyectosyAsi2);
				temporal.forEach(logArrayElements4);

			}
			
			
			console.log(contador);
		}
		function tProyectos()
		{
			$( "#proyectos" ).children().css( "display", "block" );
		}

		function arregloAUtilizar(modulo)
		{
			if(modulo==1)
		  		proyMostrar=mBot;
		  	else if(modulo==2)
		  		proyMostrar=mDis;
		  	else if(modulo==3)
		  		proyMostrar=mBuz;
		  	else if(modulo==4)
		  		proyMostrar=mLed;
		  	else if(modulo==5)
		  		proyMostrar=mPot;
		  	else if(modulo==6)
		  		proyMostrar=mTemp;
		  	else if(modulo==7)
		  		proyMostrar=mLuz;
		  	else if(modulo==8)
		  		proyMostrar=mDisp;
		  	else if(modulo==9)
		  		proyMostrar=mMot;

		  	return proyMostrar;
		}

		function proyectos(modulo)
		{
			var proyMostrar=[];
		  	proyMostrar=arregloAUtilizar(modulo);

	      	if(contador==0)
	      	{
		      	function logArrayElements(element, index, array)
		      	{
				    //console.log("mostrando proyectos primer click a[" + index + "] = " + array[index]);
				    $("#P"+array[index]).css("display", "block");
				    proyectosActivos.push(array[index]);
				}
				temporal=proyMostrar;
				proyMostrar.forEach(logArrayElements);
				proyMostrar=[];
				contador++;
			}
			else
			{
				contadorextra=contador;
				$( "#proyectos" ).children().css( "display", "none" );
				function logArrayElements3(element, index, array)
		      	{
				    //console.log("proyectos a mostrar a[" + index + "] = " + array[index]);
				    proyectosNuevos.push(array[index]);
				    //$("#P"+element.ID_proyecto).css("display", "block");
				}

				function logArrayElements2(element, index, array)
		      	{
				    //console.log("mostrando proyectos a[" + index + "] = " + array);
				    
				    $("#P"+array[index]).css("display", "block");
				}

				function mostrarProyectosyAsi(element,index,array)
				{
					
					if(contadorextra==0)
					{
						//console.log("vacio");
					}
					else
					{
						temporal=getIntersect(arregloAUtilizar(array[index]),temporal);
						
					}
					//temporal=arregloAUtilizar(array[index]);

				}
				contadorextra=contador;
				//console.log(contadorextra);
				modulosSeleccionados.forEach(mostrarProyectosyAsi);

				proyMostrar.forEach(logArrayElements3);
				proyectosActivos1=proyectosActivos;
				proyectosActivos=getIntersect(proyectosActivos,proyectosNuevos);
				
				temporal.forEach(logArrayElements2);
				//proyectosActivos.forEach(logArrayElements2);

				proyectosNuevos=[];
				contador++;

			}
			//alert(proyectosActivos);
			      
		}
		function getIntersect(arr1, arr2)
		{
		    var r = [], o = {}, l = arr2.length, i, v;
		    for (i = 0; i < l; i++) {
		        o[arr2[i]] = true;
		    }
		    l = arr1.length;
		    for (i = 0; i < l; i++) {
		        v = arr1[i];
		        if (v in o) {
		            r.push(v);
		        }
		    }
		    return r;
		}
      function info(valor,url,img)
      {
      	var val=valor;
      	var url1=url+"index.php/p/"+valor;
      	//alert(url1);
		  $(function () 
		  {
		    //-----------------------------------------------------------------------
		    // 2) Send a http request with AJAX http://api.jqueryom/jQuery.ajax/
		    //-----------------------------------------------------------------------
		    $.ajax({                                      
		      url: url1,                  //the script to call to get data          
		      data: "",                        //you can insert url argumnets here to pass to api.php
		                                       //for example "id=5&parent=6"
		      dataType: 'json',                //data format      
		      success: function(data)          //on recieve of reply
		      {
		      	var proyecto = data[0].ID_Proyecto; 
		        var nombre = data[0].Nombre;              //get id
		        var dificultad = data[0].Dificultad;           //get n;
		        var tiempo=data[0].Tiempo;
		        
		        var descripcion=data[0].Descripcion;
		        //alert(data);

		        document.getElementById('NProy').innerHTML=nombre;
		        document.getElementById('comtiC').src=url+"images/Level"+dificultad+"-120.png";
			    document.getElementById('comtiT').src=url+"images/Time"+tiempo+"-120.png";
			    document.getElementById('proyD').innerHTML=descripcion;
			    //alert(img.localeCompare("default1.png"));
			    if(img.localeCompare("default1.png")==0)
			    {
			    	document.getElementById('imgY').src=url+"images/proyectos/"+img;
			        
			        document.getElementById('imgY').style.display = "block";
			    }
			    else
			    {
			    	document.getElementById('imgY').src=url+"uploads/"+valor+"/"+img;
			        
			        document.getElementById('imgY').style.display = "block";
			    }
			    document.getElementById('buttonu1490-des').href=url+"proyecto/"+proyecto;
		        //--------------------------------------------------------------------
		        // 3) Update html content
		        //--------------------------------------------------------------------
		        //alert(id);
		        //$('#output').html("<b>id: </b>"+id+"<b> name: </b>"+vname); //Set output element html
		        //recommend reading up on jquery selectors they are awesome 
		        // http://api.jquery.com/category/selectors/
		      } 
		    });
		  });	 
		}
	  </script>
		<script>
		    $(document).foundation();
		 </script>
		
		
		<script>
	      $(document).foundation();

	      var doc = document.documentElement;
	      doc.setAttribute('data-useragent', navigator.userAgent);
	    </script>
	 	<div id="proyeP" class="reveal-modal big" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<div class="row">
				<div class="columns large-12">
					<div class="row collapse prefix-radius">
						<fieldset>
							<legend class="modalInicio" id="NProy">Proyecto</legend>
							<div class="large-5 columns">
					          	
					          	<img src="<?php echo base_url();?>images/logoImg.png" id="imgY" style="background-color: white; display:none; ">
					        </div>
					        <div class="large-7 columns">
					        	<div class="row" >
					        		<div class="columns large-6" style="padding-bottom: 0px;">
					        			<div class="row">
					        				<h2 class="text-center" id="CompP" >Complejidad</h2>	
					        			</div>
					        			<div class="row">
					        				<img src="<?php echo base_url();?>images/Level1-120.png" id="comtiC">
					        			</div>					        			
					        		</div>
					        		<div class="columns large-6" style="padding-bottom: 0px;">
					        			<div class="row">
					        				<h2 class="text-center" id="CompP" >Tiempo</h2>	
					        			</div>
					        			<div class="row">
					        				<img src="<?php echo base_url();?>images/Time1-120.png"  id="comtiT">
					        			</div>
					        		</div>
					        	</div>
					        	<div class="row" id="comentarios">
					        		<div class="columns large-12">
					        			<p id="proyD" >Informaciòn de proyecto</p>
					        		</div>
					        	</div>
					        	<div class="row" >
					        		<div class="columns large-8 large-offset-3">
					        			<a  class="round comK" href="http://briko.cc/proyecto/62"  id="buttonu1490-des" >&nbsp;&nbsp;&nbsp;&nbsp;Ver Proyecto&nbsp;&nbsp;&nbsp;&nbsp;</a>
					        		</div>
					        	</div>						          	
					        </div>
						</fieldset>
				        
				    </div>
				</div>
			</div>
		  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
		</div>
		
	</body>
</html>