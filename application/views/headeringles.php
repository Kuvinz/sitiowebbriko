
<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="shortcut icon" href="<?php echo base_url();?>images/brikon.ico?521017215"/>
		<title>briko</title>
		<link rel="stylesheet" href="<?php echo base_url();?>css/foundation.css"/>
		<script src="<?php echo base_url();?>js/vendor/modernizr.js"></script>
		<link href="<?php echo base_url();?>css/prettify.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/default.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/component.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/proyectoL.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/home.css" />
		
		<link rel="stylesheet" href="<?php echo base_url();?>css/foundation-datepicker.min.css">
		
		
		
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="<?php echo base_url();?>js/waypoints/lib/noframework.waypoints.min.js"></script>
		<script src="<?php echo base_url();?>js/vendor/jquery.js"></script>
		<script src="http://www.google-analytics.com/ga.js"></script>
		<script src="<?php echo base_url();?>js/foundation.min.js"></script>
		<script src="<?php echo base_url();?>js/foundation/foundation.equalizer.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>/scripts/prettify.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/scripts/prettify.js"></script>
        <link rel="stylesheet" href="<?php echo base_url();?>css/headerspecial.css">
        <script type="text/javascript" src="<?php echo base_url();?>/js/myheader/init.js"></script>
	</head>
	<body class="cbp-spmenu-push" onload="prettyPrint()">
		<?php $this->load->helper('form');?>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite",
  "name" : "briko - Robotica y programacion para todos",
  "alternateName" : "briko - Robotica y programacion para todos",
  "url" : "http://briko.cc"
}
</script>
         
<!-- Scripts para hacer el loading de la pagina -->
<script>
    paceOptions = {
    initialRate:0.7,
    minTime:1500,
    maxProgressPerFrame:5,
}
  </script>
<script src="<?php echo base_url(); ?>js/pace/pace.js"></script>
<link href="<?php echo base_url(); ?>js/pace/themes/purple/pace-theme-briko.css" rel="stylesheet" />
<script data-pace-options='{ "ajax": false }' src='<?php echo base_url(); ?>js/pace/pace.js'></script>
<link href="<?php echo base_url(); ?>css/pacehidepage.css" rel="stylesheet" />
<script>
    //para ignorar cierto urls que salga el de cargando.
    paceOptions = {
        ajax: {ignoreURLs: ['proyectos/proyectoinfo/$1']}
    }
</script>
        
        
		<div class="row headerprincipal">

	        <div class="large-12 columns" style="padding-bottom: 0px;">
	        	
	          <div class="row">
	            <div class="large-12 columns contain-to-grid fixed" id="u72">
	     
	              <nav class="top-bar important-class" data-topbar role="navigation">
	                <ul class="title-area wrap" >
	                   
	                  <li class="name brand">
	                    <a class="nonblock nontext" href="http://briko.cc"><img alt="Briko logo" src="<?php echo base_url();?>images/LogoBriko250.png" ><!-- simple frame --></a>
	                  </li>
                      <li class="toggle-topbar menu-icon">
                          <a id="menuc" href="#">Menu</a>
                        </li>
	                  
	                </ul>
	             
	                <section class="top-bar-section" style="float: right;">
	                   
	                  <ul class="left wrap2">
	                    <li class="brand has-dropdown">
	                    	<?php if($usuario['nombre']=="no"){ ?>
	                      <a href="#" class="asp2">Community</a>
	                      <?php }else{?>
	                      <a href="#" class="asp2"><?php echo $usuario['nombre']?></a>
	                      <?php }?>
	                      <ul class="dropdown">
	                      	<?php if($usuario['nombre']=="no"){ ?>
                            <li><a 	class="menud asp1" data-reveal-id="Entrar" href="#" style="padding-right: 105px!important;">Log in</a></li>
                            <li><a class="menud asp1" data-reveal-id="Registro" href="#">Register</a></li>
                            <?php }else { ?>
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>index.php/misproyectos" style="padding-right: 105px!important;">My projects</a></li>
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>index.php/logout" style="padding-right: 105px!important;">Log out</a></li>
                            <?php } ?>
                          </ul>	
	                    </li> 
                          <li class="divider"></li>
	                    <li class="brand">
	                      <a class="asp2" href="<?php echo base_url();?>eventos">Events</a>	                      
	                    </li>
                          <li class="divider"></li>
	                    <li class="brand" >
	                      <a class="asp2" href="<?php echo base_url();?>shop">Shop</a>
	                    </li>
                        <li class="divider"></li>
	                    <li class="brand">
	                    	<a class="asp2" href="<?php echo base_url();?>proyectos">Projects</a>
	                   	</li>
	                    <li class="divider"></li>
	                    <li  class="has-dropdown brand">
	                      <a class="asp2" href="#">References</a>
	                      <ul class="dropdown">
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>referencia" style="padding-right: 47px!important;">References</a></li>
                            <li><a class="menud asp1"  href="<?php echo base_url();?>modulos">Brikos</a></li>
                          </ul>	                   
	                    </li>
                        <li class="divider"></li>
	                    <li class="has-dropdown brand">
	                      <a class="asp2" href="#">Start now</a>
	                      <ul class="dropdown">
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>instalacion">Instalation</a></li>
                            <li><a class="menud asp1" href="<?php echo base_url();?>primer-programa">My first code</a></li>
                            <li><a class="menud asp1" href="<?php echo base_url();?>aprende" style="padding-right: 30px!important;">Learn</a></li>
                          </ul>	                      
	                    </li>
                        <li class="divider"></li>
	                  </ul>
	                </section>
	              </nav>
	            </div>
	          </div>  
          </div>
          </div>
       
        

<div id="Entrar" class="reveal-modal tiny" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<?php echo validation_errors(); ?>
	<?php echo form_open('usform/log');?>
		<div class="row">
			<div class="colums large-12">
				<h2 class="text-center modalInicio">Start session</h2>
			</div>
		</div>
		<div class="row">
			
			<div class="columns large-12">
				<div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">User</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="text" class="inputReg" name="usuE" placeholder="Bruno">
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Password</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="password" name="passE" class="inputReg" placeholder="***************">
			        </div>
			    </div>
			</div>
		</div>
		<div class="row collapse">
			<div class="colums large-6 large-offset-4">
				<button class="round comK">Enter</button>
			</div>
		</div>
	</form>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="Registro" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<?php echo validation_errors(); ?>
	<?php echo form_open('usform/registro');?>
		<div class="row">
			<div class="colums large-12">
				<h2 class=" text-center modalInicio">Log in</h2>
			</div>
		</div>
		<!--<div class="row">
			<div class="colums large-12">
				<h4 id="modalTitle">Registrate con tu cuenta de:<img style="cursor:pointer;" id="fbI" src="<?php //echo base_url();?>images/fb.png" >	</h4>
			</div>
		</div>-->
		<div class="row">
			<div class="columns large-6">
				<div class="row collapse prefix-radius">
			        <div class="small-3 columns">
			          	<span class="tagInputReg prefix">Name</span>
			        </div>
			        <div class="small-9 columns">
			          	<input type="text" class="inputReg" required name="nombre" placeholder="Bruno">
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-3 columns">
			          	<span class="tagInputReg prefix">Email</span>
			        </div>
			        <div class="small-9 columns">
			          	<input type="text" class="inputReg" required name="mail" id="mail" placeholder="bruno@correo.com">
			        </div>
			    </div>
			</div>
			<div class="columns large-6">
			    <div class="row collapse prefix-radius">
	      			<div class="small-4 columns">
			          	<span class="tagInputReg prefix">NickName</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="text" class="inputReg" required id="nick" name="nick" placeholder="brunoBot">
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-5 columns">
			          	<span class="tagInputReg prefix">Birthday</span>
			        </div>
			        <div class="small-7 columns">
			          	<input type="text" class="inputReg" name="nacimiento" class="span2" value="10-01-2000" id="dp1">
			        </div>
			        
			    </div>
			</div>
			
		</div>
		<div class="row">
			<div class="columns large-12">
			    <div class="row collapse prefix-radius">
			        <span class="label desKY">Gender</span><br/>
	      			<input type="radio" name="genero" value="H" id="Mas"><label for="Mas">Male;no</label>
	      			<input type="radio" name="genero" value="M" id="Fem"><label for="Fem">Female</label>
	      			<input type="radio" name="genero" value="O" id="Ot" checked><label for="Ot">Other</label>
			    </div>
			</div>
			
		</div>
		<div class="row">
			<div class="columns large-10">
			    <div class="row collapse prefix-radius">
	      			<div class="small-5 columns">
			          	<span class="tagInputReg prefix">Pasword</span>
			        </div>
			        <div class="small-7 columns">
			          	<input type="password" name="passwd" id="passwd" class="inputReg" placeholder="**************" required>
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-5 columns">
			          	<span class="tagInputReg prefix">Check pasword</span>
			        </div>
			        <div class="small-7 columns">
			          	<input type="password" name="passwd1" id="passwd1" class="inputReg" placeholder="**************">
			        </div>
			        
		    	</div>
			</div>
			<div class="columns large-2">
				<span class="round alert label" id="passwd_match"></span>
				<span class="round  label" id="able"></span>
				<span class="round  label" id="val_mail"></span>
				
	        	<span class="round alert label" id="strength_human">
        		<span class="round alert label"	 id="strength_score"></span></span>
			</div>
		</div>
		<div class="row collapse">
			<div class="colums large-6 large-offset-3">
				<input type="submit" class="round comK" id="registro" value="Registrame!" />
				
			</div>
		</div>
	</form>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
    
<script src="<?php echo base_url();?>js/foundation-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
    
    

    $('#nick').keyup(check_username);

    $('#mail').keyup(check_email);
    


    

});
function validateMail(mail) {
   
    var atpos = mail.indexOf("@");
    var dotpos = mail.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=mail.length) {
        return "E-mail no valido";}
        else
        {
            return "E-mail valido"
        }
    }

    function check_username(){
        var base="<?php echo base_url();?>";
        var username = $('#nick').val();
        jQuery.ajax({
            type: 'POST',
            url: base+'index.php/usform/existe_us',
            data: 'nombre_tienda='+ username,
            cache: false,
            success: function(response){
                if(response == 0){
                 $('#able').text('Disponible');
                 
             }
             else {
                $('#able').text('No disponible');
                
                
            }
        }
    });
    }

    function check_email(){
        var base="<?php echo base_url();?>";
        var usermail = $('#mail').val();
        jQuery.ajax({
            type: 'POST',
            url: base+'index.php/usform/existe_email',
            data: 'correo='+ usermail,
            cache: false,
            success: function(response){
                if(response == 0){
                 //document.getElementById('mai').value = '0';
                 $("#val_mail").text(validateMail(usermail));
             }
             else {
                //document.getElementById('mai').value = '1';
                $('#val_mail').text('Ya existe el correo');
                
            }
        }
    });
    }
</script>
<script>
	$(function () {
	
		$('#dp1').fdatepicker({
			format: 'mm-dd-yyyy',
			disableDblClickSelection: true
		});
	});
	$("#registro").on("click",function(){  //abre pop
		    var pass1 = document.getElementById("passwd").value;
		    var pass2 = document.getElementById("passwd1").value;
		    var ok = true;
		    if (pass1 != pass2) {
		        //alert("Passwords Do not match");
		        $("#passwd_match").text("No match");
		        document.getElementById("passwd").style.borderColor = "#E34234";
		        document.getElementById("passwd1").style.borderColor = "#E34234";
		        ok = false;
		    }
		    
		    return ok;
		}); 
</script>
<script type="text/javascript">


function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up pattern="[a-zA-Z0-9]+"
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80){
        $( "#strength_human" ).removeClass( "warning alert" ).addClass( "success" );
        $( "#strength_score" ).removeClass( "warning alert" ).addClass( "success" );
        return "Fuerte";
    }
    if (score > 60){
    	$( "#strength_human" ).removeClass( "alert success" ).addClass( "warning" );
    	$( "#strength_score" ).removeClass( "alert success" ).addClass( "warning" );
        return "Buena";
    }
    if (score >= 30){
    	$( "#strength_human" ).removeClass( "warning success" ).addClass( "alert" );
    	$( "#strength_score" ).removeClass( "warning success" ).addClass( "alert" );
        return "Débil";}

    return "Débil";
}

$(document).ready(function() {
    $("#passwd").on("keypress keyup keydown", function() {
        var pass = $(this).val();
        $("#strength_human").text(checkPassStrength(pass));
        $("#strength_score").text(scorePassword(pass));
    });
});
</script>
