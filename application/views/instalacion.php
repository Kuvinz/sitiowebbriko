<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/comienzaahora.css">

<div class="row"  id="div1">
  <div class="large-12  columns">

    <!-- titulo principal -->
    <div class="row">
        <div class="large-12 columns" style="text-align:center" >
            <h1 class = "h1classbk">Descarga brikode completo aquí:</h1>
        </div>
    </div>
      
    <!-- Botones de descarga -->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class= "button round comA" id="IOSb">Mac OS X</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "button round comA" id= "Windowsb">Windows</button>
        </div>
    </div>
      
      <!-- Botones de descarga + titulo -->
      <div class="row">
        <div class="large-12 columns" style="text-align:center" >
            <h1 class = "h1classbk">Descarga solo los drivers para windows aquí:</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns" >
            <center><button class= "button round comA" id="Wdriver">Drivers</button></center>
        </div>
    </div>

    <!-- Segundo titulo -->
    <div class="row">
        <div class="large-12 columns" style="text-align:center">
            <h1 class = "h1classbk">Sigue los pasos de instalación:</h1>
        </div>
    </div>
    
    <!-- tabs para entrar a diferentes funciones -->
    <div class="row">
        <div class="small-12 columns" id="tabinst">
            <ul class="tabs" id="myTabs" data-tab>
                <li class="tab-title active"><a href="#panel1">Windows</a></li>
                <li class="tab-title"><a href="#panel2">Mac OS X</a></li>
            </ul>
        </div>
    </div>

<!-- arreglo para guardar los mensajes del slider -->
<?php $pasos = array("Paso 1: Después de descargar el software, lo ejecutamos para comenzar el proceso de instalación.","Paso 2: Das click en siguiente.","Paso 3: Seleccionas el folder donde se instalará briko en tu pc y das click en siguiente.","Paso 4: Seleccionas el folder donde se instalara briko en el menu de inicio y das click en siguiente."," Paso 5: Seleccionas si deseas crear un icono en escritorio y das click en siguiente.","Paso 6: Das click en instalar y esperas a que termine.","Paso 7: Al salir la pestaña de Device Drivers das click en next.", "Paso 8: Esperas a que encuentre todos los drivers y después das click en finish. ", "Paso 9: Das click en finalizar instalación y corres brikode."); ?>
    
<!-- arreglo para guardar los mensajes del slider -->
<?php $pasos2 = array("Paso 1: Después de descargar el software, lo buscamos en nuestra computadora y lo descomprimimos.","Paso 2: Al ejecutar el programa si no tenemos configuradas las preferencias del sistema, nos aparecera este mensaje. ","Paso 3: Damos click a la manzanita en la parte superior izquierda de la pantalla y abrimos la opcion 'Preferencias del Sistema'","Paso 4: Brimos la opción Seguridad y privacidad.","Paso 5: Damos click en el candado que esta en la parte inferior de la ventana que nos salio, nos preguntara por nuestra contraseña.","Paso 6: Damos click al boton 'Abrir igualmente' para que nuestro equipo ejecute la applicacion Brikode.","Paso 7: Nos aparecere una ventana y seleccionamos la opcion abrir y Listo!."); ?>

    
    <!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content">
        <!-- Primera tab -->
        <div class="content active" id="panel1">
            <div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< count($pasos); $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img alt="imagen instalacion paso <?php echo $i+1; ?>" src="<?php echo base_url(); ?>images/instalacion/paso<?php echo $i+1; ?>.png" alt="slide<?php echo $i; ?>"  />
    <div class="orbit-caption">
    <?php echo $pasos[$i]; ?>
    </div>
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img alt="imagen instalacion paso <?php echo $i+1; ?>" src="<?php echo base_url(); ?>images/instalacion/paso<?php echo $i+1; ?>.png" alt="slide<?php echo $i; ?>"/>
    <div class="orbit-caption">
    <?php echo $pasos[$i];  ?>
    </div></center> 
  </li>   
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
</div>
        
    <!-- segundo tab -->
    <div class="content" id="panel2">
        <div class="row" >
            <div class="large-12 columns" >
<ul id = "slideim2" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
  <?php for($i = 0; $i< count($pasos2); $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img alt="imagen instalacion para mac paso <?php echo $i+1; ?>" src="<?php echo base_url(); ?>images/instalacion/pasomac<?php echo $i+1; ?>-1.png" alt="slide<?php echo $i; ?>"  />
    <div class="orbit-caption">
    <?php echo $pasos2[$i]; ?>
    </div>
  </li></center>   
<?php }else{ ?>  
   <center><li class="active"> 
    <img alt="imagen instalacion para mac paso <?php echo $i+1; ?>" src="<?php echo base_url(); ?>images/instalacion/pasomac<?php echo $i+1; ?>-1.png" alt="slide2<?php echo $i; ?>"/>
    <div class="orbit-caption">
    <?php echo $pasos2[$i];  ?>
    </div>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
</div>
        
</div>
 
    <!-- titulo de boton para cambiar a otra pagina -->
    <div class="row">
        <div class="large-12 columns" style="text-align:center" >
            <h1 class = "h1classbk">Ahora crea tu primer programa:</h1>
        </div>
    </div>

<!-- Boton para cambiar a otra pagina -->
<div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <button  class= "button round comA" id="Continuarb">Continuar</button>
        </div>
    </div>


</div>
 <br>
 <br>
</div>

<script>
$("#Continuarb").on("click",function(){  //redirige a otra pagina
    location.href = "<?php echo base_url(); ?>/primer-programa";
    window.open("<?php echo base_url(); ?>/primer-programa","_self");
});
$("#Windowsb").on("click",function(){  //redirige a otra pagina
        window.open("https://www.dropbox.com/s/k4vk3xk1lf39hbd/brikode_installer1_0_1.zip?dl=0");
/*
window.open("<?php echo base_url(); ?>index.php/descargawin");
*/
    
});
    
$("#IOSb").on("click",function(){  //redirige a otra pagina
window.open("https://www.dropbox.com/s/xnvh8m8de4r8f2h/Brikode-1.0.0-macosx.zip?dl=0");
/*
window.open("<?php echo base_url(); ?>index.php/descargaios");
*/   
});
    
$("#Wdriver").on("click",function(){  //redirige a otra pagina
window.open("https://www.dropbox.com/home/drivers?preview=drivers.zip");
/*
window.open("<?php echo base_url(); ?>index.php/descargaios");
*/   
});
      
//detecta cuando cambias de pestana
$('#myTabs').on('toggled', function (event, tab) {
    //guarda el nombre del tab donde estas
    //vuelve a cargar los sliders para que aparescan
    $(document).foundation('orbit', 'reflow');
  });
    
     
</script>

<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>