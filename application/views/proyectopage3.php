<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Aplica los estilos para el selector-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen/prism.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen/chosen.css">

<!-- Aplica el formato y librerias para el drag and drop-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery_ui.css">
<script src="<?php echo base_url(); ?>js/jquery_ui.js"></script>
<!-- Libreria para el mensaje que pide informacion -->
<script src="<?php echo base_url(); ?>js/impromptu/src/jquery-impromptu.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>js/impromptu/src/jquery-impromptu.css">
<!--sheepit script-->
<script src="<?php echo base_url(); ?>js/sheepit-jquery/jquery.sheepItPlugin.js"></script>


<div class="row"  id="div1">
  <div class="large-12  columns">
      
<!-- Hace un form para poder subir los archivos al servidor, esta en el controlador -->
<!-- la siguiente linea abre un <form> que liga a la fucnion del controlador do_upload que se cierra al final -->
<?php echo form_open_multipart('proyectopage/do_upload');?>      
      
<!------------------------------------------------------------------------------>
      
<!--Drag and drop para el paso de materiales -->
  <style>
  h1 { padding: .2em; margin: 0; }
  #products { float:left; margin-right: 2em; width: 50% ;}
  #cart { width: 40% ; float: right;}
  #olcart {height: 100%; }
  .scrollclass{width: 100%;
    height: 350px; overflow: scroll; }
  .scrollclass2{width: 100%;
    height: 500px; overflow: scroll; }
  /* style the list to maximize the droppable hitarea */
  #cart ol { margin: 0; padding: 1em 0 1em 3em; }
  </style>
  <script>
//creamos un json para ingresar la informacion del prompt
 var statesdemo = {
     ///cada estado es una ventana dentro del alert pero hay que poner en el submit if(v==-1) $.prompt.goToState('state1'); para cambiar entre ventanas
	state0: {
		title: 'Cantidad',
		html:'<label>Ingresa el número de brikos que usaste<input type="number" id ="textval" value="1" min="1" max="999" ></label>',
		buttons: {Done: 1 },
		focus: 1,
		submit:function(e,v,m,f){ 
			e.preventDefault();
			if(v==1){
                var numerop = parseInt($("#textval").val());
                //si es NaN la siguiente condicion regresa verdadero
                if( numerop != numerop){
                  numerop = 1;  
                }
                if(numerop ==0 || numerop == null || numerop < 0){
                    numerop = 1;
                }
                if(numerop >999){
                    numerop = 999;
                }
                 $("#olcart > " +"#"+global_last_id+"s").find("span.numberspan").text(numerop); 
                $.prompt.close();
            }
		}
	},
}; 
     
//para ir creciendo el area donde ponemos los objetos
 var initialheight  =600;
 var cont = 0;
 var global_last_id = "";
 var module_limiter=0;
 ///pARA MOVER OBJETOS DE LA LISTA A LA LISTA FINAL
  $(function() {
    $( "#catalog" ).accordion();
      //hace los objetos draggable
    $( "#catalog li" ).draggable({
      appendTo: "body",
      helper: "clone"
    });
      //al momento de soltar
    $( "#cart ol" ).droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            global_last_id = $(ui.draggable).attr("id");
            ///para limitar la entrada a 7 modulos
            var idspec=0;
            var idarrayspec = ["", "botonms", "disms", "buzms", "ledms", "potms", "tempms", "luzms", "dispms", "motorms"];
            for (var j=0; j<idarrayspec.length; j++) {
                if (idarrayspec[j].match(drag_id)){idspec=j};
            }
            module_limiter=0;  //recetea
            if(idspec>=1 && idspec<=9){
            var textnumf = 0;
            for (var conter = 0; conter < $('ol#olcart li.dropped').length; conter++) {
                textnumf = 0;
               var idf = $("ol#olcart li.dropped").get(conter).id;
               var idarrayspec = ["", "botonms", "disms", "buzms", "ledms", "potms", "tempms", "luzms", "dispms", "motorms"];
            for (var j=0; j<idarrayspec.length; j++) {
                if (idarrayspec[j].match(idf)){
                textnumf = parseInt($("#olcart > " +"#"+idf).find("span.numberspan" ).text());
                };
            }
            module_limiter=module_limiter+textnumf;
            }
            }
            if(module_limiter>=7){
             //no agrega nada  
             alert("No puedes usar mas de 7 modulos");
            ///******/////////
            //si ya existe el id en el carrito
            }else if ($("#olcart > " +"#"+drag_id+"s").length) {
                //guarda lo que hay en el span
                var textnum = parseInt($("#olcart > " +"#"+drag_id+"s").find("span.numberspan").text());
                //lo vuelve a poner pero sumando 1
                $("#olcart > " +"#"+drag_id+"s").find("span.numberspan").text((textnum+1).toString());
            } else{  //si no existe lo agrega
            if (ui.draggable.is('.dropped')) return false;
            $(this).find(".placeholder").remove();
            //pide el valor inicial en un promp(solo si no son modulos)
                if(idspec==0){
                $.prompt(statesdemo);
                }
            //pone por default 1 hasta que llegue la data del prompt
                var initval = 1;
            //lo agrega a la lista
            $("<li></li>").text(ui.draggable.text()).appendTo(this).draggable({
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped libk2').attr('id', drag_id+"s").append('<span class="numberspan spanbkspe2">'+initval+'</span>'); 
            cont++;
            if(cont>4){
            initialheight = initialheight+70;
            $("#olcart").css("height", initialheight);
            }
          //le pone la clase dropped
          //para quitar los elementos de la lista principal al pasar a la otra
          //$(ui.helper).remove(); //destroy clone
          //$(ui.draggable).remove(); //remove from list
      }
        }
        //para que se reacomoden los numeros si lo movemos ahi mismo
    }).sortable({
      items: "li:not(.placeholder)",
      sort: function() {
        // gets added unintentionally by droppable interacting with sortable
        // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
        $( this ).removeClass( "ui-state-default" );
      }
    });
   
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#cart ol").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
        var self = ui;
        if (ui.draggable.hasClass('dropped')){
        var drag_id2 = $(ui.draggable).attr("id");
        $("#"+drag_id2).remove();
        self.draggable.remove();  
        }
});       
      
  });   
         
  </script>
  
<!--Script para los sheep it -->
<script>

$(document).ready(function() {
    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItForm').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 1
    });
    
    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso3').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });

    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso4').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0,
    });

    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso5').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });

    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso6').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });

    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso1').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });

    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso11').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });
    
    
});

</script>
      
<!--style para el drag and drop de la parte de conectar-->
<style>
 .scrollclassconec{width: 100%;height: 350px; overflow: scroll; }
</style>

<!-- tabs para entrar a diferentes funciones como son verticales la division va contener todo -->
    <div class="row">
        <div class="small-2 columns" style="padding-top: 40px;">
            <ul class="tabs vertical" id="myTabs" data-tab>
                <li class="tab-title active"><a style="width:120px" href="#panel1">General</a></li>
                <li class="tab-title"><a style="width:120px" href="#panel2">Material</a></li>
                <li class="tab-title"><a style="width:120px" href="#panel3">Construir</a></li>
                <li class="tab-title"><a style="width:120px" href="#panel4">Conectar</a></li>
                <li class="tab-title"><a style="width:120px" href="#panel5">Programar</a></li>
                
            </ul>
        </div>
        <div class="small-10 end columns">
         <!--esta division contiene todo -->
 
<!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content" id="myTabscontainer">
      
<!------------------------------------------------------------------------------>
      
<!-- Paso 1 -->
<!-- tab -->
<div class="content active" id="panel1" style="padding-left: 30px;">
  <div class="row" >
    <div class="large-12 columns">
      <h1 class= "h1classbk">Informaci&oacute;n del Proyecto</h1>
    </div>
  </div> 
  <div class="row">
    <div class="columns large-7">
      <label class="h2classbk" for="nombreProyecto">Nombre del Proyecto</label>
      <input type="text" id="nombreProyecto" name="nombreProyecto"/>
      <label class="h2classbk" for="descripcionProyecto">Descripci&oacute;n del proyecto</label>
      <textarea style="resize:none;" rows="8" cols="50" id="descripcionProyecto" name="descripcionProyecto"></textarea>
    </div>
    <div class="columns large-4">
      <div class="row">
        <div class="columns large-12">
          <label class="h2classbk" for="categorias">Etiquetas</label>
          <select data-placeholder="Selecciona una etiqueta" class="chosen-select" id="categorias" name="categorias[]" multiple  tabindex="4">
                <option value=""></option>
                <?php foreach ($categoria as $value)
                {
                  echo "<option value='".$value->ID_Categoria."'>".$value->Nombre."</option>";
                }?>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="columns large-6">
          <label  class="h2classbk" for="tiempo">Tiempo</label>
          <select name="tiempo" id="tiempo">
              <option value="1">10 minutos</option>
              <option value="2">30 minutos</option>
              <option value="3">60 minutos</option>
              <option value="4">+60 minutos</option>
          </select>
        </div>
        <div class="columns large-6">
          <label  class="h2classbk" for="dificultad">Dificultad</label>
          <select name="dificultad" id="dificultad">
              <option value="1">Facil</option>
              <option value="2">Intermedio</option>
              <option value="3">Dificil</option>
              <option value="4">Muy Dificil</option>
          </select>
        </div> 
      </div>
    </div>

  </div>
    
  <div class="row">
    <div class="large-6 columns">
      <!-- sheepIt Form -->
      <div id="sheepItFormpaso1">
        <!-- Form template-->
        <div id="sheepItFormpaso1_template">
            <div class="row">
           <div class="small-10 columns" >
          <label class = "h2classbk"  for="Lista_img_paso1[#index#]">Imagen<span id="sheepItFormpaso1_label"></span></label>
          <input class="imgformpaso1" id="Lista_img_paso1[#index#]" name="userfile1[]" type="file" multiple="" />
            </div>
          <div class="small-2 end columns" >
          <center><a id="sheepItFormpaso1_remove_current">
            <img alt="borrar" style="margin-top:25px;cursor:pointer; width:35%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          
          </a></center>
         </div>
        </div>
        </div>
        </div>
        
    <div class="row">
    <div class="small-12 columns" >
        <!-- No forms template -->
        <div id="sheepItFormpaso1_noforms_template"><span class="spanbkspe">No hay imagenes</span></div>
         
        <!-- Controls -->
        <div id="sheepItFormpaso1_controls">
          <div id="sheepItFormpaso1_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Agregar imagen</span></li></ul></div>
          <div id="sheepItFormpaso1_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar imagen</span></li></ul></div>
        </div>
        <div id="sheepItFormpaso1_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar todas las imagenes</span></li></ul></div>
      </div>
    </div>
</div>

    
    <div class="large-6 end columns">
      <!-- sheepIt Form -->
      <div id="sheepItFormpaso11">
        <!-- Form template-->
        <div id="sheepItFormpaso11_template">
        <div class="row">
           <div class="small-10 columns" >
          <label class = "h2classbk" for="Lista_video_paso11[#index#]">Video <span id="sheepItFormpaso11_label"></span></label>
          <input  class="videoformpaso11" type="text" id="Lista_video_paso11[#index#]" name="Lista_video_paso11[#index#]"> </input>
            </div>
          <div class="small-2 end columns" >
          <center><a id="sheepItFormpaso11_remove_current">
            <img alt="borrar video" style="margin-top:30px;cursor:pointer; width:35%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          
          </a></center>
        </div>
        </div>
        </div>
        </div>
      
    <div class="row">
    <div class="small-12 columns" >
        <!-- No forms template -->
        <div id="sheepItFormpaso11_noforms_template"><span class="spanbkspe">No hay Videos</span></div>
         
        <!-- Controls -->
        <div id="sheepItFormpaso11_controls">
          <div id="sheepItFormpaso11_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Agregar Video</span></li></ul></div>
          <div id="sheepItFormpaso11_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar Video</span></li></ul></div>
        </div>
        <div id="sheepItFormpaso11_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar todos los videos</span></li></ul></div>
    </div>
    </div>
    </div>
    </div>
</div>
<!------------------------------------------------------------------------------>
      
<!-- Paso 2 -->
<!-- tab -->
<div class="content" id="panel2" style="padding-left: 30px;">
  
  <!-- titulo principal -->
  <div class="row" >
      <div class="large-12 columns">
          <h1 class= "h1classbk">¿Qué utilizaste de briko?</h1>
          <h1 class= "h2classbk">Arrastra lo que utilizaste de briko a la lista de piezas del proyecto</h1>
      </div>
  </div>     
      
  <!--Drag and drop de lista de piezas-->     
  <div class="row" >
    <div class="large-12 columns">
      <div id="products">
        <h1 class="ui-widget-header h1classbk3"><center>Familia briko</center></h1>
        <div id="catalog">
          <h2><a class="h2classbk" href="#"><center>Brikos</center></a></h2>
          <div class= "scrollclass">
            <ul style="list-style:none;">
              <li class="libk2" id="botonm"><img alt="briko botones" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png"; > Botones
              </li>
              <li class="libk2" id="dism"><img alt="briko sensor de distancia" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png"; > Sensor distancia
              </li>
              <li class="libk2" id="buzm"><img alt="briko bocina" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png"; > Bocina
              </li> 
              <li class="libk2" id="ledm"><img alt="briko leds" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; > Leds
              </li>
              <li class="libk2" id="potm"><img alt="briko perilla" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; > Perilla
              </li>
              <li class="libk2" id="tempm"><img alt="briko sensor de temperatura" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png"; > Sensor de temperatura
              </li>
              <li class="libk2" id="luzm"><img alt="briko sensor de luz" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; > Sensor de luz
              </li>
              <li class="libk2" id="dispm"><img alt="briko display" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; > Display
              </li>
              <li class="libk2" id="motorm"><img alt="briko motor" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png"; > Motor
              </li>
            </ul>
          </div>
          <h2><a href="#">Piezas mecanicas</a></h2>
          <div>
            <ul style="list-style:none;">
              <li class="libk2" id="l1m"><img alt="pieza de contruccion l1 viga" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/viga.png"; > l1
              </li>
              <li class="libk2" id="c1m"><img alt="pieza de contruccion c1" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza1.png"; > c1
              </li>
                <li class="libk2" id="o1m"><img alt="pieza de contruccion o1" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza2.png"; > o1
              </li>
                <li class="libk2" id="y1m"><img alt="pieza de contruccion y1" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/union3.png"; > y1
              </li>
                <li class="libk2" id="c2m"><img alt="pieza de contruccion c2" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/union2.png"; > c2
              </li>
                <li class="libk2" id="o2m"><img alt="pieza de contruccion o2" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/union1.png"; > o2
              </li>
                <li class="libk2" id="p1m"><img alt="pieza de contruccion p1" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza3.png"; > p1
              </li>
            </ul>
          </div>
          <h2><a href="#">Accesorios</a></h2>
          <div>
            <ul style="list-style:none;">
              <li class="libk2" id="llantam"><img alt="accesorio llanta" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/llanta.png"; > llanta
              </li>
                <li class="libk2" id="ruedalocam"><img alt="accesorio rueda loca" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/ruedaloca.png"; > ruedaloca
              </li>
                <li class="libk2" id="tor1m"><img alt="accesorio tornillo chico" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tornilloch.png"; > tornillo chico
              </li>
                <li class="libk2" id="tor2m"><img alt="accesorio tornillo grande" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tornillog.png"; > tornillo grande
              </li>
                <li class="libk2" id="cablem"><img alt="accesorio cable briko" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cablebriko.png"; > cable
              </li>
                <li class="libk2" id="tuercam"><img alt="accesorio tuerca" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tuerca.png"; > tuerca
              </li>
                <li class="libk2" id="remachem"><img alt="accesorio remache" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/remache.png"; > remache
              </li>
                <li class="libk2" id="cableum"><img alt="accesorio cable micro usb" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cablemicrousb.png"; > cable micro usb
              </li>
                <li class="libk2" id="bateriam"><img alt="accesorio bateria" width="15%" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pila.png"; > bateria
              </li>
            </ul>
          </div>
        </div>
      </div>
   
      <div id="cart">
        <h1 class="ui-widget-header h1classbk3"><center>Piezas del proyecto</center></h1>
        <div class="ui-widget-content scrollclass2">
          <ol id = "olcart">
            <li class="libk2" id="bk7m">Controlador bk7
            <span class="numberspan spanbkspe2">1</span>
              </li>
            <p class="placeholder spanbkspe">Add your items here</p>
          </ol>
        </div>
      </div>
    </div>
  </div>
      
  <!--u input hidden para poder guardar el json del drag and drop modulos -->
  <div class="row" >
      <div class="large-12 columns">
          <input style="display: none;" type="text"  id="Dragjson" name="Dragjson" />
      </div>
  </div>
    
  <!--u input hidden para poder guardar el json del drag and drop piezas mecanicas -->
  <div class="row" >
      <div class="large-12 columns">
          <input style="display: none;" type="text"  id="Dragjson2" name="Dragjson2" />
      </div>
  </div>
    
<!--u input hidden para poder guardar el json del drag and drop accesorios -->
  <div class="row" >
      <div class="large-12 columns">
          <input style="display: none;" type="text"  id="Dragjson3" name="Dragjson3" />
      </div>
  </div>
      
  <!-- segundo titulo -->
  <div class="row" >
      <div class="large-12 columns">
          <h1 class= "h1classbk">¿Utilizaste algo más?</h1>
      </div>
  </div>
      
  <!-- sheepit para agregar mas piezas -->
  <div class="row" >
    <div class="large-12 columns">
      <!-- sheepIt Form -->
      <div id="sheepItForm">
        <!-- Form template-->
        <div id="sheepItForm_template">
            <div class="row">
           <div class="small-10 columns" >
          <label class = "h2classbk" for="Lista_name_id[#index#]">Pieza <span id="sheepItForm_label"></span></label>
          <input class="nameform" type="text" id="Lista_name_id[#index#]" name="Lista_name[#index#]" />
          <label class = "h2classbk"  for="Lista_desc_id[#index#]">Descripción</label>
          <input class="descform" id="Lista_desc_id[#index#]" name="Lista_desc[#index#]" type="text" />
          <label class = "h2classbk"  for="Lista_img_id[#index#]">Archivos</label>
          <input class="imgform" id="Lista_img_id[#index#]" name="userfile2[]" type="file" multiple="" />
          </div>
          <div class="small-2 end columns" >
          <center><a id="sheepItForm_remove_current">
            <img alt="borrar pieza extra" style="margin-top:30px;cursor:pointer; width:25%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          </a></center>
        </div>
         </div>
        </div>
        </div>
        </div>
      </div>
    
    <div class="row ">
    <div class="small-12 columns" >
        <!-- No forms template -->
        <div id="sheepItForm_noforms_template"><span class="spanbkspe">No hay mas piezas</span></div> 
        <!-- Controls -->
        <div id="sheepItForm_controls">
          <div id="sheepItForm_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Agregar pieza</span></li></ul></div>
          <div id="sheepItForm_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar pieza</span></li></ul></div>
        </div>
        <div id="sheepItForm_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar todas las piezas</span></li></ul></div>
      </div>
    </div>
   
</div>
        
<!------------------------------------------------------------------------------>
      
<!-- Paso 3 -->
<!-- tab -->
<div class="content" id="panel3" style="padding-left: 30px;">
    
  <!-- titulo principal -->
  <div class="row" >
      <div class="large-12 columns">
          <h1 class= "h1classbk">Descripción general</h1>
      </div>
  </div> 
    
  <!-- input de la descripcion general -->
  <div class="row" >
      <div class="large-12 columns">
          <textarea style="resize:none;" rows="8" cols="50" id="paso3desc" name="paso3desc"></textarea>
      </div>
  </div>   
    
  <!-- segundo titulo -->
  <div class="row" >
      <div class="large-12 columns">
          <h1 class= "h1classbk">Agrega los pasos</h1>
      </div>
  </div> 
    
  <!-- sheepit para agregar mas pasos -->
  <div class="row" >
    <div class="large-12 columns">
      <!-- sheepIt Form -->
      <div id="sheepItFormpaso3">
        <!-- Form template-->
        <div id="sheepItFormpaso3_template">
        <div class="row">
           <div class="small-10 columns" >
          <label class = "h2classbk" for="Lista_desc_paso3[#index#]">Paso <span id="sheepItFormpaso3_label"></span></label>
          <textarea style="resize:none;" rows="5" cols="50" class="descformpaso3" type="text" id="Lista_desc_paso3[#index#]" name="Lista_desc_paso3[#index#]"></textarea>
          <label class = "h2classbk"  for="Lista_img_paso3[#index#]">Imagen</label>
          <input class="imgformpaso3" id="Lista_img_paso3[#index#]" name="userfile3[]" type="file" multiple="" />
          </div>
          <div class="small-2 end columns" >
          <center><a id="sheepItFormpaso3_remove_current">
            <img alt="borrar paso" style="margin-top:65px;cursor:pointer; width:25%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          
          </a></center>
        </div>
        </div>
        </div>
        </div>
        </div>
      </div>
   
    <div class="row">
    <div class="small-12 columns" >
        <!-- No forms template -->
        <div id="sheepItFormpaso3_noforms_template"><span class="spanbkspe">No hay pasos</span></div>
         
        <!-- Controls -->
        <div id="sheepItFormpaso3_controls">
          <div id="sheepItFormpaso3_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Agregar paso</span></li></ul></div>
          <div id="sheepItFormpaso3_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar paso</span></li></ul></div>
        </div>
        <div id="sheepItFormpaso3_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar todas los pasos</span></li></ul></div>
      </div>
    </div>
  
</div>
        
<!------------------------------------------------------------------------------>
      
<!-- Paso 4 -->
<!-- tab -->
<div class="content" id="panel4" style="padding-left: 30px;">
  <!-- titulo -->
  <div class="row" >
      <div class="large-12 columns">
          <h1 class= "h1classbk">Conecta tus brikos</h1>
          <h1 class= "h2classbk">Arrastra tus brikos al puerto donde los conectaste</h1>
      </div>
  </div> 
      
  <!-- Drag and drop -->
  <div class="row" >
      <div class="small-4 columns">
       <div id="modulosconect">
          <h2><a class="h1classbk" href="#"><center>Brikos</center></a></h2>
          <div class= "scrollclassconec">
            <ul style="list-style:none;" id="modulosconectul">    
            </ul>
          </div>
      </div> 
    </div>  
    <div class="small-2 end columns">
       <div id="dropport1">
            <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">1</span></label>
       </div>
       <div id="dropport2">
           <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">2</span></label>
       </div>
       <div id="dropport3">
            <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">3</span></label>
       </div>
       <div id="dropport4">
            <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">4</span></label>
       </div>
    </div>  
      <div class="small-4 end columns">
        <center><img alt="briko maestro numeros" style="margin-top: 100px;" width="100%" src="<?php echo base_url(); ?>images/proyectopage/iconosextra/bk7/bk7numeros700.png"; ></center>
    </div>  
      <div class="small-2 end columns">
       <div id="dropport5">
            <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">5</span></label>
       </div>
       <div id="dropport6">
            <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">6</span></label>
       </div>
        <div id="dropport7">
            <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3">7</span></label>
       </div>
  </div>  
</div>
</div>        
<!------------------------------------------------------------------------------>
      
<!-- Paso 5 -->
<!-- tab -->
<div class="content" id="panel5" style="padding-left: 30px;">
  <!-- titulo principal -->
  <div class="row" >
      <div class="large-12 columns">
          <h1 class= "h1classbk">Descripción general</h1>
      </div>
  </div> 
  <!-- input de la descripcion general -->
  <div class="row" >
      <div class="large-12 columns">
          <textarea style="resize:none;" rows="5" cols="50" id="paso5desc" name="paso5desc"></textarea>
      </div>
  </div>
  <!-- sheepit para agregar mas pasos -->
  <div class="row" >
      <div class="large-12 columns">
        <!-- sheepIt Form -->
        <div id="sheepItFormpaso5">
         
          <!-- Form template-->
          <div id="sheepItFormpaso5_template">
        <div class="row">
           <div class="small-10 columns" >
            <label class = "h2classbk" for="Lista_desc_paso5[#index#]">Paso <span id="sheepItFormpaso5_label"></span></label>
            <textarea style="resize:none;" rows="3" cols="50" class="descformpaso5" type="text" id="Lista_desc_paso5[#index#]" name="Lista_desc_paso5[#index#]"></textarea>
            <label class = "h2classbk" for="Lista_codigo_paso5[#index#]">C&oacute;digo </label>
            <textarea style="resize:none;" rows="8" cols="50" class="codigoformpaso5" type="text" id="Lista_codigo_paso5[#index#]" name="Lista_codigo_paso5[#index#]"></textarea>
            </div>
          <div class="small-2 end columns" >
            <center><a id="sheepItFormpaso5_remove_current">
              <img alt="borrar paso de codigo" style="margin-top:50px;cursor:pointer; width:25%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          
            </a></center>
          </div>
        </div>
        </div>
        </div>
        </div>
      </div>
           
<div class="row">
    <div class="small-12 columns" >    
          <!-- No forms template -->
          <div id="sheepItFormpaso5_noforms_template"><span class="spanbkspe">No hay pasos</span></div>
           
          <!-- Controls -->
          <div id="sheepItFormpaso5_controls">
              <div id="sheepItFormpaso5_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Agregar paso</span></li></ul></div>
            <div id="sheepItFormpaso5_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar paso</span></li></ul></div></div>
            <div id="sheepItFormpaso5_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar todas los pasos</span></li></ul></div>
        </div>
      </div>
 

  
</div>
        
<!------------------------------------------------------------------------------>
        
<!--cerramos divison de los tabs-->
</div> 
</div> 
</div> 


<!-- este es el que se forza para cargar las imagnes al servidor -->  
 <button id="uploadbutton" style="display: none;" name="submit" value="Upload" type="submit">Create Account</button>
<!-- cierra el form -->
</form>    
<!--boton para cargar todo lo del formulario -->
<center><button id="uploadspecial">Terminar formulario</button></center> 

    
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>

<!--para validar las imagenes y archivos de todos los pasos asi como espacios vacios -->
<script type="text/javascript">
    
///variable para saber si hubo algun error
var form_errorbk;
var conter=0;
var file_max_size = 10000000;  //10 megas
$(function () {
    $("#uploadspecial").on("click", function () {
        
        form_errorbk = false;  //resetea
        
        ////chequeo del paso de general//////
         if(document.getElementById("nombreProyecto").value == "" && form_errorbk ==false){
                form_errorbk = true;
                alert("Hay que agregar un nombre a tu proyecto en la parte de general");
           }
        if(document.getElementById("descripcionProyecto").value == "" && form_errorbk ==false ){
                form_errorbk = true;
                alert("Hay que agregar una descripción a tu proyecto en la parte de general");
           }
        if($(".chosen-choices li").size() <=1 && form_errorbk ==false ){
                form_errorbk = true;
                alert("Hay que agregar una etiqueta a tu proyecto en la parte de general");
           }
        
        if(form_errorbk == false){
        var Imgformgen = new Array();
        $(".imgformpaso1").each(function(){
        Imgformgen.push(this.id);
        });
        for (conter = 0; conter < Imgformgen.length; conter++) {
            if(form_errorbk == true){break;}
            form_errorbk = true; 
           //Get reference of FileUpload.
        var fileUpload = document.getElementById(Imgformgen[conter]);
       
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                var file = fileUpload.files[0];
        //console.log("File " + file.name + " is " + file.size + " bytes in size");
                if(file.size> file_max_size){  ///10 megas
                    alert("Archivo muy grande subido en material.");
                } else{
                    //todo estuvo bien
                    form_errorbk = false;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else { 
            //checa si esta vacio (lo cual no es malo ) o e un archico malo
            if (fileUpload.value == ""){form_errorbk = false;}else{
            alert("Porfavor selecciona un tipo de archivo valido subido en material.");
            }
        }
        }
        }
        if(form_errorbk == false){
        var urlform = new Array();
        $(".videoformpaso11").each(function(){
        urlform.push(this.id);
        });
        for (conter = 0; conter < urlform.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(urlform[conter]).value == ""){
                form_errorbk = true;
                alert("Hay un espacio de video vacio en general");
           }
        }
        }
        ///**** Acaba chequeo del paso general***////
        
        ////Chequeo del paso material//////
        if(form_errorbk == false){
        var Jsonmaterial2 = [];  //para hacer un objeto json
        var Jsonmaterial3 = [];  //para hacer un objeto json
        ///checa si hay algo en el de piezas
        if($('ol#olcart li.dropped').length >= 1){
            ///obtenemos los id del list
             for (conter = 0; conter < $('ol#olcart li.dropped').length; conter++) {
                 var id = $("ol#olcart li.dropped").get(conter).id;
                 
                 ///piezas mecanicas
                 var id2=0;  //para dar un numero a cada modulo
                 var idarray = ["", "l1ms", "c1ms", "o1ms", "y1ms", "c2ms", "o2ms", "p1ms"];
                 for (var j=0; j<idarray.length; j++) {
                    if (idarray[j].match(id)){id2=j};
                 }
                 if( id2 != 0){ 
                 var textnum = parseInt($("#olcart > " +"#"+id).find("span.numberspan").text());    
                 Jsonmaterial2.push({
                     id: id2,
                     cantidad: textnum
                    });
                 }
                ///accesorios
                 id2=0;    
                 idarray = ["", "llantams", "ruedalocams", "tor1ms", "tor2ms", "cablems","tuercams","remachems","cableums","bateriams"];
                 for (var j=0; j<idarray.length; j++) {
                    if (idarray[j].match(id)){id2=j};
                 }
                 if( id2 != 0){ 
                 textnum = parseInt($("#olcart > " +"#"+id).find("span.numberspan").text());    
                 Jsonmaterial3.push({
                     id: id2,
                     cantidad: textnum
                    });
                }
             }
            ///guarda el json en el input de texto
             $("#Dragjson2").val(JSON.stringify(Jsonmaterial2));  //guarad el json como string
            $("#Dragjson3").val(JSON.stringify(Jsonmaterial3));  //guarad el json como
        }else{
          form_errorbk = true; 
          alert("Necesitas agregar tus piezas briko que utilisaste en material");  
        }
        }
        
        if(form_errorbk == false){
        ///checa que todos los espacios esten llenos y los archivos bien
        var Namesform = new Array();
        $(".nameform").each(function(){
        Namesform.push(this.id);
        });
        for (conter = 0; conter < Namesform.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(Namesform[conter]).value == ""){
               form_errorbk = true; 
               alert("Hay un nombre vacio de una pieza en material");
           }
        }
        }
        
        if(form_errorbk == false){
        var Descform = new Array();
        $(".descform").each(function(){
        Descform.push(this.id);
        });
        for (conter = 0; conter < Descform.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(Descform[conter]).value == ""){
                form_errorbk = true;
                alert("Hay una descripcion vacia de una pieza en material");
           }
        }
        }
        
        if(form_errorbk == false){
        var Imgform = new Array();
        $(".imgform").each(function(){
        Imgform.push(this.id);
        });
        for (conter = 0; conter < Imgform.length; conter++) {
            if(form_errorbk == true){break;}
            form_errorbk = true; 
           //Get reference of FileUpload.
        var fileUpload = document.getElementById(Imgform[conter]);
       
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf|.txt|.stl|.dxf|.step|.ino|.briko|.jpg|.png|.gif|.zip)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                var file = fileUpload.files[0];
        //console.log("File " + file.name + " is " + file.size + " bytes in size");
                if(file.size> file_max_size){
                    alert("Archivo muy grande subido en material.");
                } else{
                    //todo estuvo bien
                    form_errorbk = false;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else { 
            //checa si esta vacio (lo cual no es malo ) o e un archico malo
            if (fileUpload.value == ""){form_errorbk = false;}else{
            alert("Porfavor selecciona un tipo de archivo valido subido en material.");
            }
        }
        }
        }
        ///**** Acaba chequeo del paso material***////
        
        ////Chequeo del paso armar//////
         if(document.getElementById("paso3desc").value == "" && form_errorbk ==false){
                form_errorbk = true;
                alert("Hay que agregar una descripcion en la parte de armar");
           }
        if(form_errorbk == false){
        ///checa que todos los espacios esten llenos y los archivos bien
        var paso3form = new Array();
        $(".descformpaso3").each(function(){
        paso3form.push(this.id);
        });
        for (conter = 0; conter < paso3form.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(paso3form[conter]).value == ""){
               form_errorbk = true; 
               alert("Hay un nombre vacio de un paso en armar");
           }
        }
        }
        
        if(form_errorbk == false){
        var Imgform4 = new Array();
        $(".imgformpaso3").each(function(){
        Imgform4.push(this.id);
        });
        for (conter = 0; conter < Imgform4.length; conter++) {
            if(form_errorbk == true){break;}
            form_errorbk = true; 
           //Get reference of FileUpload.
        var fileUpload = document.getElementById(Imgform4[conter]);
       
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                var file = fileUpload.files[0];
        //console.log("File " + file.name + " is " + file.size + " bytes in size");
                if(file.size> file_max_size){  ///10 megas
                    alert("Archivo muy grande subido en material.");
                } else{
                    //todo estuvo bien
                    form_errorbk = false;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else { 
            //checa si esta vacio (lo cual no es malo ) o e un archico malo
            if (fileUpload.value == ""){form_errorbk = false;}else{
            alert("Porfavor selecciona un tipo de archivo valido subido en material.");
            }
        }
        }
        }
        ///**** Acaba chequeo del paso armar***////
        
        ////Chequeo del paso conectar//////
        if(form_errorbk == false){
        if($('#modulosconectul li:visible').size() < 1){
        var port_mod = [0,0,0,0,0,0,0,0];  //para guardar los puertos
            
         if($('#dropport1 img').length >= 1){
            var id = $("#dropport1").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[1]=num_port_mod; //guarda que esta en el puerto 1
         }
        
         if($('#dropport2 img').length >= 1){
            var id = $("#dropport2").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[2]=num_port_mod; //guarda que esta en el puerto 2
         }
       
         if($('#dropport3 img').length >= 1){
            var id = $("#dropport3").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[3]=num_port_mod; //guarda que esta en el puerto 3
         }
        
         if($('#dropport4 img').length >= 1){
            var id = $("#dropport4").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[4]=num_port_mod; //guarda que esta en el puerto 4
         }
        
         if($('#dropport5 img').length >= 1){
            var id = $("#dropport5").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[5]=num_port_mod; //guarda que esta en el puerto 5
         }
        
         if($('#dropport6 img').length >= 1){
            var id = $("#dropport6").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[6]=num_port_mod; //guarda que esta en el puerto 6
         }
        
         if($('#dropport7 img').length >= 1){
            var id = $("#dropport7").find(".dropped").attr("id");
            var idarrayd = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
             var num_port_mod=0;
             for (var j=0; j<idarrayd.length; j++) {
                    if (idarrayd[j].match(id)){num_port_mod=j};
                 }
             port_mod[7]=num_port_mod; //guarda que esta en el puerto 7
         }
          
        ////aqui junta la informacion del paso de material en el mismo json
        ///obtenemos los id del list
            var Jsonmaterial = [];  //para hacer un objeto json
             for (conter = 0; conter < $('ol#olcart li.dropped').length; conter++) {
                 var id = $("ol#olcart li.dropped").get(conter).id;
                 var id2=0;  //para dar un numero a cada modulo
                 var idarray = ["", "botonms", "disms", "buzms", "ledms", "potms", "tempms", "luzms", "dispms", "motorms"];
                 for (var j=0; j<idarray.length; j++) {
                    if (idarray[j].match(id)){id2=j};
                 }
                 var port_num=0;  //para dar el puerto a cada modulo
                  for (var j=1; j<=7; j++) { //checa si 
                    if (port_mod[j] == id2){
                        port_num=j;
                        if( id2 != 0){  
                        Jsonmaterial.push({
                            id: id2,
                            puerto: port_num
                            });
                        }
                    }
                  }
             } 
            ///guarda el json en el input de texto
             $("#Dragjson").val(JSON.stringify(Jsonmaterial));  //guarad el json como string
        }else{
         form_errorbk = true; 
          alert("Hay que poner donde se conectaron todos los modulos en conectar");   
        }
        }
        
        ///**** Acaba chequeo del paso Programar***////
        
        ////Chequeo del paso armar//////
         if(document.getElementById("paso5desc").value == "" && form_errorbk ==false){
                form_errorbk = true;
                alert("Hay que agregar una descripcion en la parte de programar");
           }
        if(form_errorbk == false){
        ///checa que todos los espacios esten llenos y los archivos bien
        var paso3form = new Array();
        $(".descformpaso5").each(function(){
        paso3form.push(this.id);
        });
        for (conter = 0; conter < paso3form.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(paso3form[conter]).value == ""){
               form_errorbk = true; 
               alert("Hay un nombre vacio de un paso en armar");
           }
        }
        }
        
        if(form_errorbk == false){
        ///checa que todos los espacios esten llenos y los archivos bien
        var paso3form = new Array();
        $(".codigoformpaso5").each(function(){
        paso3form.push(this.id);
        });
        for (conter = 0; conter < paso3form.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(paso3form[conter]).value == ""){
               form_errorbk = true; 
               alert("Hay un codigo vacio en la parte de programar");
           }
        }
        }
        
        
        ///**** Acaba chequeo del paso armar***////
    
        
        ///si no hubo ningun error manda todo a la base de datos y sube las imagenes
        if(form_errorbk == false){
            ///falta aqui subir lo demas a la base de datos
            
            $( "#uploadbutton" ).trigger( "click" ); //activa la fucnion del controlador
        }
        
    
    });
});
</script>

<!--este sript es basicamente par la parte de conectar-->
<script>
var tab_selector  = "panel1";  //por default esta en la 1
//listener para saber si cambias de tab
  $('#myTabs').on('toggled', function (event, tab) {
    //guarda el nombre del tab donde estas
    tab_selector = tab.context.href;
    if(tab_selector.indexOf("panel4") >=0 ){
        ///checa si hay algo en el de piezas
        ///quitamos todos los li del ul de la lista y lo que haya en los puertos
        $("#modulosconectul li").remove();
        $("#dropport1 > .dropped").remove();
        $("#dropport2 > .dropped").remove();
        $("#dropport3 > .dropped").remove();
        $("#dropport4 > .dropped").remove();
        $("#dropport5 > .dropped").remove();
        $("#dropport6 > .dropped").remove();
        $("#dropport7 > .dropped").remove();
        if($('ol#olcart li.dropped').length >= 1){
            ///obtenemos los id del list
        for (var conter = 0; conter < $('ol#olcart li.dropped').length; conter++) {
               var id = $("ol#olcart li.dropped").get(conter).id;
               var id2=0;  //para dar un numero a cada modulo
               var idarray = ["", "botonms", "disms", "buzms", "ledms", "potms", "tempms", "luzms", "dispms", "motorms"];
                 for (var j=0; j<idarray.length; j++) {
                    if (idarray[j].match(id)){id2=j};
                 }
            var textnum = parseInt($("#olcart > " +"#"+id).find("span.numberspan" ).text());
                if(id2 == 1){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='botli'> <p class='h1classbk4' >Botones</p> <img class='"+conterr+"' id='botonp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png'; > </li>"); 
                    }
                }
            
                if(id2 == 2){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='disli'> <p class='h1classbk4'>Sensor distancia</p> <img  class='"+conterr+"' id='disp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png'; > </li>"); 
                    }
                }
            
                if(id2 == 3){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='buzli'> <p class='h1classbk4' >Bocina</p> <img  class='"+conterr+"' id='buzp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png'; >  </li>"); 
                    }
                }
            
                if(id2 == 4){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='ledli'> <p class='h1classbk4' >Leds</p>              <img  class='"+conterr+"' id='ledp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png'; > </li>"); 
                    }
                }
            
                if(id2 == 5){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='potli'> <p class='h1classbk4' >Perilla</p>              <img class='"+conterr+"' id='potp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png'; > </li> "); 
                    }
                }
            
                if(id2 == 6){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='templi'> <p class='h1classbk4' >Sensor de temperatura</p> <img  class='"+conterr+"' id='tempp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png'; > </li>"); 
                    }
                }
            
                if(id2 == 7){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='luzli'> <p class='h1classbk4' >Sensor de luz</p> <img  class='"+conterr+"' id='luzp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png'; > </li> "); 
                    }
                }
            
                if(id2 == 8){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='displi'> <p class='h1classbk4' >Display</p>               <img  class='"+conterr+"' id='dispp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png'; > </li>");
                    }
                }
            
                if(id2 == 9){
                    for(var conterr=0; conterr<textnum; conterr++){
                    $("#modulosconectul").append(
                    "<li class='libk2 "+conterr+"' id='motli'> <p class='h1classbk4' >Motor</p>              <img  class='"+conterr+"' id='motorp4' src='<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png'; > </li>"); 
                    }
                }
             }
            
            ///****aqui esta la parte de configuracion del drag and drop de la parte de conectar****////
             $(function() {
     $( "#modulosconect" ).accordion();
      //hace los objetos draggable
    $( "#modulosconect li img" ).draggable({
      appendTo: "body",
      helper: "clone"
    });
    
    ///*****el area drop del puerto 1****////
    var last_state1 = "";
    $("#dropport1").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport1 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }

                if(ids3 == 1){
                  $("#botli."+last_state1).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state1).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state1).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state1).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state1).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state1).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state1).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state1).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state1).show(); 
                }    
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                       }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport1").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport1 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            console.log("#"+idvar+"."+speclass);
            last_state1 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport1").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");    
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state1).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state1).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state1).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state1).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state1).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state1).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state1).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state1).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state1).show(); 
                }     
                 
                    $(document).trigger("mouseup");  //forza que suelten el elemento
                };
        });    
      
    ///*****el area drop del puerto 2****////
    var last_state2 = "";
    $("#dropport2").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport2 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }
                    
                if(ids3 == 1){
                  $("#botli."+last_state2).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state2).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state2).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state2).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state2).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state2).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state2).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state2).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state2).show(); 
                }       
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                       }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport2").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport2 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            last_state2 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport2").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state2).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state2).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state2).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state2).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state2).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state2).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state2).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state2).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state2).show(); 
                } 
                    
                $(document).trigger("mouseup");  //forza que suelten el elemento
                    
                };
        }); 
        
        
        ///*****el area drop del puerto 3****////
    var last_state3 = "";
    $("#dropport3").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport3 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }
                    
                if(ids3 == 1){
                  $("#botli."+last_state3).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state3).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state3).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state3).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state3).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state3).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state3).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state3).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state3).show(); 
                }     
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                       }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport3").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport3 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            last_state3 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport3").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state3).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state3).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state3).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state3).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state3).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state3).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state3).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state3).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state3).show(); 
                }     
                
                $(document).trigger("mouseup");  //forza que suelten el elemento    
                    
                };
        }); 
        
        
        ///*****el area drop del puerto 4****////
    var last_state4 = "";
    $("#dropport4").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport4 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }
                    
                if(ids3 == 1){
                  $("#botli."+last_state4).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state4).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state4).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state4).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state4).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state4).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state4).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state4).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state4).show(); 
                }     
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                       }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport4").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport4 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            last_state4 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport4").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state4).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state4).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state4).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state4).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state4).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state4).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state4).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state4).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state4).show(); 
                } 
                    
                $(document).trigger("mouseup");  //forza que suelten el elemento    
                    
                };
        }); 
        
        
        ///*****el area drop del puerto 1****////
    var last_state5 = "";
    $("#dropport5").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport5 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }
                    
                if(ids3 == 1){
                  $("#botli."+last_state5).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state5).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state5).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state5).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state5).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state5).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state5).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state5).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state5).show(); 
                }      
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                       }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport5").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport5 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            last_state5 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport5").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state5).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state5).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state5).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state5).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state5).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state5).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state5).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state5).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state5).show(); 
                }   
                    
                $(document).trigger("mouseup");  //forza que suelten el elemento    
                    
                };
        }); 
        
        
        ///*****el area drop del puerto 6****////
    var last_state6 = "";
    $("#dropport6").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport6 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }
                    
                if(ids3 == 1){
                  $("#botli."+last_state6).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state6).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state6).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state6).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state6).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state6).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state6).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state6).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state6).show(); 
                }    
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                     }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport6").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport6 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            last_state6 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport6").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state6).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state6).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state6).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state6).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state6).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state6).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state6).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state6).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state6).show(); 
                }  
                    
                $(document).trigger("mouseup");  //forza que suelten el elemento    
                    
                };
        }); 
        
        
        ///*****el area drop del puerto 7****////
    var last_state7 = "";
    $("#dropport7").droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ":not(.ui-sortable-helper)",
        drop: function (event, ui) {
            //guarda el id
            var drag_id = $(ui.draggable).attr("id");
            var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
            //si ya existe el id en el carrito
            if ($("#dropport7 > .dropped").length) {
              var drag_id3 = $(this).find(".dropped").attr("id");
              $(this).find(".dropped").remove(); 
              var ids3=0;  //para dar un numero a cada modulo
               var idarrays3 = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays3.length; j++) {
                    if (idarrays3[j].match(drag_id3)){ids3=j};
                 }
                    
                if(ids3 == 1){
                  $("#botli."+last_state7).show();
                }
            
                if(ids3 == 2){
                  $("#disli."+last_state7).show();
                }
            
                if(ids3 == 3){
                   $("#buzli."+last_state7).show(); 
                }
            
                if(ids3 == 4){
                    $("#ledli."+last_state7).show(); 
                }
            
                if(ids3 == 5){
                    $("#potli."+last_state7).show(); 
                }
            
                if(ids3 == 6){
                    $("#templi."+last_state7).show(); 
                }
            
                if(ids3 == 7){
                    $("#luzli."+last_state7).show(); 
                }
            
                if(ids3 == 8){
                    $("#displi."+last_state7).show(); 
                }
            
                if(ids3 == 9){
                    $("#motli."+last_state7).show(); 
                }     
                
            } 
            if (ui.draggable.is('.dropped'))return false;
            //lo agrega a la lista
             var imgvar = "";
             var idvar = "";
            if(drag_id == "botonp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/botones.png";
             idvar = "botli";                       }
            if(drag_id == "disp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png";
             idvar = "disli";                     }
            if(drag_id == "buzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/buzzer.png";
             idvar = "buzli";                     }
            if(drag_id == "ledp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; 
             idvar = "ledli";                     }
            if(drag_id == "potp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/pot.png"; 
              idvar = "potli";                    }
            if(drag_id == "tempp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/senstemp.png";
              idvar = "templi";                     }
            if(drag_id == "luzp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/sensluz.png"; 
              idvar = "luzli";                    }
            if(drag_id == "dispp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/display.png"; 
              idvar = "displi";                     }
            if(drag_id == "motorp4"){ imgvar = "<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png";
              idvar = "motli";                      }
            $("#dropport7").append("<div></div>");  //agrega una division
            ///agrega la imagen a la division
            $("#dropport7 div").append("<center><img style='margin-bottom: 10px;' src="+imgvar+" /></center>").draggable({
                axis: "x",
                appendTo: "body",
                helper: "clone"
            }).addClass('dropped '+speclass).attr('id', drag_id+"4"); 
            $("#"+idvar+"."+speclass).hide();
            last_state7 = speclass;
            }
    });  
 
///para detectar cuando agarras el objecto otra vez y lo tiras fuera                 
$("#dropport7").on("dropout", function(event, ui) {
        //lo borra solo si tiene la clase dropped
                var self = ui;
                if (ui.draggable.hasClass('dropped')){
                var drag_id2 = $(ui.draggable).attr("id");
                var drag_class = $(ui.draggable).attr("class"); 
            var speclass  = -1;
            ///checa que numero hay en la clase
            for(var num=0; num<7; num++){  //no puede haber mas de 7 modulos
              if(drag_class.search(num.toString())>=0){
                 speclass =num.toString(); 
              }
            }
                $("#"+drag_id2+"."+speclass).remove();
                self.draggable.remove();
                var ids=0;  //para dar un numero a cada modulo
               var idarrays = ["", "botonp44", "disp44", "buzp44", "ledp44", "potp44", "tempp44", "luzp44", "dispp44", "motorp44"];
                 for (var j=0; j<idarrays.length; j++) {
                    if (idarrays[j].match(drag_id2)){ids=j};
                 }
                    
                if(ids == 1){
                  $("#botli."+last_state7).show();
                }
            
                if(ids == 2){
                  $("#disli."+last_state7).show();
                }
            
                if(ids == 3){
                   $("#buzli."+last_state7).show(); 
                }
            
                if(ids == 4){
                    $("#ledli."+last_state7).show(); 
                }
            
                if(ids == 5){
                    $("#potli."+last_state7).show(); 
                }
            
                if(ids == 6){
                    $("#templi."+last_state7).show(); 
                }
            
                if(ids == 7){
                    $("#luzli."+last_state7).show(); 
                }
            
                if(ids == 8){
                    $("#displi."+last_state7).show(); 
                }
            
                if(ids == 9){
                    $("#motli."+last_state7).show(); 
                }   
                    
                $(document).trigger("mouseup");  //forza que suelten el elemento    
                    
                };
        }); 
        

 });  
///****aqui termina la parte de configuracion del drag and drop de la parte de conectar****////
        }else{
             alert("Porfavor agrega tus modulos que utilizaste en material antes de entrar a conectar.");
    }
    }
  });
    
</script>

<!-- librerias de foundation -->
  <script src="<?php echo base_url();?>js/chosen.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>js/prism.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>