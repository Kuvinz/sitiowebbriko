<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<!-- Aplica los estilos para el selector-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen/prism.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen/chosen.css">

<!-- Aplica el formato y librerias para el drag and drop-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery_ui.css">
<script src="<?php echo base_url(); ?>js/jquery_ui.js"></script>
<!-- Libreria para el mensaje que pide informacion -->
<script src="<?php echo base_url(); ?>js/impromptu/src/jquery-impromptu.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>js/impromptu/src/jquery-impromptu.css">
<!--sheepit script-->
<script src="<?php echo base_url(); ?>js/sheepit-jquery/jquery.sheepItPlugin.js"></script>


<div class="row"  id="div1">
  <div class="large-12  columns">
      
<!-- Hace un form para poder subir los archivos al servidor, esta en el controlador -->
<!-- la siguiente linea abre un <form> que liga a la fucnion del controlador do_upload que se cierra al final -->
<?php echo form_open_multipart('proyectopage/do_upload');?>      
      
<!------------------------------------------------------------------------------>
      
<!--Drag and drop para el paso de materiales -->
  <style>
  h1 { padding: .2em; margin: 0; }
  #products { float:left; margin-right: 2em; width: 50% ;}
  #cart { width: 40% ; float: right;}
  #olcart {height: 100%; }
  .scrollclass{width: 100%;
    height: 350px; overflow: scroll; }
  .scrollclass2{width: 100%;
    height: 500px; overflow: scroll; }
  /* style the list to maximize the droppable hitarea */
  #cart ol { margin: 0; padding: 1em 0 1em 3em; }
  </style>
  
<!--Script para los sheep it -->
<script>

$(document).ready(function() {
    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItFormpaso1').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });
});

</script>
      
<!--style para el drag and drop de la parte de conectar-->
<style>
 .scrollclassconec{width: 100%;height: 350px; overflow: scroll; }
</style>

<!-- tabs para entrar a diferentes funciones como son verticales la division va contener todo -->
  <div class="row">
    <div class="small-12 end columns">
      <div class="row" >
        <div class="large-12 columns">
          <h1 class= "h1classbk">Nuevo Evento</h1>
        </div>
      </div> 
      <div class="row">
        <div class="columns large-8">
          <label class="h2classbk" for="nombreProyecto">Nombre del Evento</label>
          <input type="text" id="nombreProyecto" name="nombreProyecto"/>
        </div>
        <div class="columns large-4">
          <label class = "h2classbk"  for="imgP">Imagen Principal</label>
          <input class="imgformpaso1" id="imgP" name="imgP" type="file" multiple="" />
        </div>
      </div>
      <div class="row">
        <div class="columns large-12">
          <label class="h2classbk" for="descripcionProyecto">Rese&ntilde;a del Evento</label>
          <textarea style="resize:none;" rows="8" cols="50" id="descripcionProyecto" name="descripcionProyecto"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <!-- sheepIt Form -->
          <div id="sheepItFormpaso1">
            <!-- Form template-->
            <div id="sheepItFormpaso1_template">
              <div class="row">
                <div class="small-10 columns" >
                  <label class = "h2classbk"  for="Lista_img_paso1[#index#]">Imagen<span id="sheepItFormpaso1_label"></span></label>
                  <input class="imgformpaso1" id="Lista_img_paso1[#index#]" name="userfile1[]" type="file" multiple="" />
                </div>
                <div class="small-2 end columns" >
                  <center>
                    <a id="sheepItFormpaso1_remove_current">
                    <img style="margin-top:25px;cursor:pointer; width:35%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
                    </a>
                  </center>
                </div>
              </div>
            </div>
          </div>
            
          <div class="row">
            <div class="small-12 columns" >
              <!-- No forms template -->
              <div id="sheepItFormpaso1_noforms_template"><span class="spanbkspe">No hay imagenes</span></div>
                 
              <!-- Controls -->
              <div id="sheepItFormpaso1_controls">
                <div id="sheepItFormpaso1_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Agregar imagen</span></li></ul></div>
                <div id="sheepItFormpaso1_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar imagen</span></li></ul></div>
              </div>
              <div id="sheepItFormpaso1_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3">Quitar todas las imagenes</span></li></ul></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 


<!-- este es el que se forza para cargar las imagnes al servidor -->  
 <button id="uploadbutton" style="display: none;" name="submit" value="Upload" type="submit">Create Account</button>
<!-- cierra el form -->
</form>    
<!--boton para cargar todo lo del formulario -->
<center><button id="uploadspecial">Terminar formulario</button></center> 

    
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>




<!-- librerias de foundation -->
  <script src="<?php echo base_url();?>js/chosen.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>js/prism.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>