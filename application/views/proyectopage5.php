<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 
<!--sheepit script-->
<script src="<?php echo base_url(); ?>js/sheepit-jquery/jquery.sheepItPlugin.js"></script>
<!--Script para los sheep it -->
<script>
$(document).ready(function() {
    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItForm').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });
    
    // para el sheepit su inicializacion 
    var sheepItForm = $('#sheepItForm2').sheepIt({
        separator: '',
        allowRemoveLast: true,
        allowRemoveCurrent: true,
        allowRemoveAll: true,
        allowAdd: true,
        maxFormsCount: 500,
        minFormsCount: 0,
        iniFormsCount: 0
    });
    
    });
</script> 

<style>
.cointainer {
    border-width: 0 5px 0px 5px;
    border-style: solid;
    border-color: #006D91;
}
    
.cointainer2 {
    border-width: 0 5px 5px 5px;
    border-style: solid;
    border-color: #006D91;
}
</style>

<!---------------------informacion de todos los textos----------------->
<!--*****Titulos****---->
<?php $Titulo_1 = "Información general"; ?>
<?php $Titulo_2 = "Nombre del proyecto"; ?>
<?php $Titulo_3 = "Objetivo"; ?>
<?php $Titulo_4 = "Antes de la clase"; ?>
<?php $Titulo_5 = "Durante la clase"; ?>
<?php $Titulo_6 =  "Titulo "; ?>
<?php $Titulo_sheep = [ "Descripción ","Imagen ","Código "]; ?>
<?php $Extras_sheep = [ "No hay mas secciones","Agregar sección","Quitar sección", "Quitar todas las secciones"]; ?>
<?php $Titulo_7 = "Nombre del archivo "; ?>
<?php $Titulo_8 = "Archivo "; ?>
<?php $Extras_sheep2 = [ "No hay archivos adjuntos","Agregar archivo","Quitar archivo", "Quitar todas los archivos"]; ?>
<!--*****Botones****---->
<?php $Boton_terminar =  "Terminar formulario"; ?>
<!------------------------------------------------------------------------------>  
<div class="row"  id="div1">
  <div class="large-12  columns">
<!-- Hace un form para poder subir los archivos al servidor, esta en el controlador -->
<!-- la siguiente linea abre un <form> que liga a la fucnion del controlador do_upload que se cierra al final -->
<?php echo form_open_multipart('proyectopage/do_upload2');?>       
 <!------------------------------------------------------------------------------>           
<!-- titulo principal -->
<input type='hidden' name='pID' value='<?php echo $proyecto;?>'>
<div class="row cointainer ">
    <div class="small-12 columns" style="background:#006D91; text-align:center;" >
    <h1 class="h1classbk5" style="margin-top: 20px;"><?php echo  $Titulo_1 ?></h1>
    </div>
</div>

 <!------------------------------------------------------------------------------>  
<div class="row cointainer ">
    <div class="small-12 columns" >
    <label class="h2classbk"><?php echo  $Titulo_2 ?></label>
      <input type="text" id="Teach_nombre_p" name="Teach_nombre_p"/>
    </div>
</div>
<!------------------------------------------------------------------------------>        
<div class="row cointainer ">
    <div class="small-12 columns" >
    <label class="h2classbk"><?php echo  $Titulo_3 ?></label>
      <textarea style="resize:none;" rows="8" cols="50" id="Teach_objetivo_p" name="Teach_objetivo_p"></textarea>
    </div>
</div>
 <!------------------------------------------------------------------------------>      
<div class="row cointainer ">
    <div class="small-12 columns" >
    <label class="h2classbk"><?php echo  $Titulo_4 ?></label>
       <textarea style="resize:none;" rows="8" cols="50" id="Teach_antes_p" name="Teach_antes_p"></textarea>
    </div>
</div>
<!------------------------------------------------------------------------------>        
<div class="row cointainer ">
    <div class="small-12 columns" >
    <label class="h2classbk"><?php echo  $Titulo_5 ?></label>
      <textarea style="resize:none;" rows="8" cols="50" id="Teach_durante_p" name="Teach_durante_p"></textarea>
    </div>
</div>
<!------------------------------------------------------------------------------>     
<div class="row cointainer ">
    <div class="small-12 columns" >
<!-- sheepIt Form -->
      <div id="sheepItForm">
        <!-- Form template-->
        <div id="sheepItForm_template">
        <div class="row">
           <div class="small-10 columns" > 
          <label class = "h2classbk" for="Lista_nombre_t[#index#]"><?php echo  $Titulo_6 ?><span id="sheepItForm_label"></span></label>
          <input class="Lista_nombre_t" type="text" id="Lista_nombre_t[#index#]" name="Lista_nombre_t[#index#]"></input>
            <?php for($i = 0; $i< count($Titulo_sheep); $i++) { ?>
            <label class = "h2classbk" for="Lista_obj_t<?php echo $i ?>[#index#]"><?php echo  $Titulo_sheep[$i] ?></label>
          <?php if($i != 1) { ?>
          <textarea class="Lista_obj_t<?php echo $i ?>" style="resize:none;" rows="8" cols="50" id="Lista_obj_t<?php echo $i ?>[#index#]" name="Lista_obj_t<?php echo $i ?>[#index#]"></textarea>
          <?php }else{ ?>
            <input class="Lista_obj_t<?php echo $i ?>" id="Lista_obj_t<?php echo $i ?>[#index#]" name="userfile1[]" type="file" multiple="" />
            <?php } ?>
            <?php } ?>
          </div>
          <div class="small-2 end columns" >
          <center><a id="sheepItForm_remove_current">
            <img alt="borrar " style="margin-top:30px;cursor:pointer; width:20%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          </a></center>
        </div>
        </div>
        </div>
        </div>
        </div>
      </div>
    
<div class="row cointainer">
    <div class="small-12 columns" >
        <!-- No forms template -->
        <div id="sheepItForm_noforms_template"><span class="spanbkspe"><?php echo  $Extras_sheep[0] ?></span></div>
         
        <!-- Controls -->
        <div id="sheepItForm_controls">
          <div id="sheepItForm_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3"><?php echo  $Extras_sheep[1] ?></span></li></ul></div>
          <div id="sheepItForm_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3"><?php echo  $Extras_sheep[2] ?></span></li></ul></div>
        </div>
        <div id="sheepItForm_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3"><?php echo  $Extras_sheep[3] ?></span></li></ul></div>
      </div>
    </div>
<!------------------------------------------------------------------------------>  
    
<div class="row cointainer ">
    <div class="small-12 columns" >
<!-- sheepIt Form -->
      <div id="sheepItForm2">
        <!-- Form template-->
        <div id="sheepItForm2_template">
        <div class="row">
           <div class="small-10 columns" >
          <label class = "h2classbk" for="Lista_filen_t[#index#]"><?php echo  $Titulo_7 ?><span id="sheepItForm_label"></span></label>
          <input class="Lista_filen_t" type="text" id="Lista_filen_t[#index#]" name="Lista_filen_t[#index#]"></input>
          <label class = "h2classbk" for="Lista_nombre_t[#index#]"><?php echo  $Titulo_8 ?></label>
         <input class="Lista_files_t" id="Lista_files_t[#index#]" name="userfile2[]" type="file" multiple="" />
          </div>
          <div class="small-2 end columns" >
          <center><a id="sheepItForm2_remove_current">
            <img alt="borrar" style="margin-top:30px;cursor:pointer; width:20%" class="delete" src="<?php echo base_url(); ?>images/trash_p.png" border="0">
          </a></center>
        </div>
        </div>
        </div>
        </div>
        </div>
      </div>
  
<div class="row cointainer">
    <div class="small-12 columns" >
        <!-- No forms template -->
        <div id="sheepItForm2_noforms_template"><span class="spanbkspe"><?php echo  $Extras_sheep2[0] ?></span></div>
         
        <!-- Controls -->
        <div id="sheepItForm2_controls">
          <div id="sheepItForm2_add"> <ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3"><?php echo  $Extras_sheep2[1] ?></span></li></ul></div>
          <div id="sheepItForm2_remove_last"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3"><?php echo  $Extras_sheep2[2] ?></span></li></ul></div>
        </div>
        <div id="sheepItForm2_remove_all"><ul class="especF"> <li> <span style="cursor:pointer;" class="h1classbk3"><?php echo  $Extras_sheep2[3] ?></span></li></ul></div>
      </div>
    </div>   
<!------------------------------------------------------------------------------>        

<!-- este es el que se forza para cargar las imagnes al servidor -->  
 <button id="uploadbutton" style="display: none;" name="submit" value="Upload" type="submit">Create Account</button>
<!-- cierra el form -->
</form>    
<div class="row cointainer2 ">
    <div class="small-12 columns" >
<!--boton para cargar todo lo del formulario -->
<center><button id="uploadspecial"><?php echo $Boton_terminar ?></button></center> 
    </div>
</div>
  
<!------------------------------------------------------------------------------>  
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>

<!--*****************para validar las imagenes y archivos de todos los pasos asi como espacios vacios************** --> 
<script>

///variable para saber si hubo algun error
var form_errorbk;
var conter=0;
var file_max_size = 10000000;  //10 megas
$(function () {
    $("#uploadspecial").on("click", function () {
        
        form_errorbk = false;  //resetea
        
        ////chequeo de los pasos basicos/////
         if(document.getElementById("Teach_nombre_p").value == "" && form_errorbk ==false){
                form_errorbk = true;
                alert("Hay que agregar un nombre del  proyecto");
           }
        if(document.getElementById("Teach_objetivo_p").value == "" && form_errorbk ==false ){
                form_errorbk = true;
                alert("Hay que agregar un objetivo del proyecto");
         }
        if(document.getElementById("Teach_antes_p").value == "" && form_errorbk ==false ){
                form_errorbk = true;
                alert("Hay que agregar lo que hay que planificar antes de la clase para realizar el proyecto");
         }
        if(document.getElementById("Teach_durante_p").value == "" && form_errorbk ==false ){
                form_errorbk = true;
                alert("Hay que agregar lo que se hara durante la clase para realizar el proyecto");
         }
        
        ///checa los elementos en el sheep it
        if(form_errorbk == false){
        var textform = new Array();
        $(".Lista_nombre_t").each(function(){
        textform.push(this.id);
        });
        for (conter = 0; conter < textform.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(textform[conter]).value == ""){
                form_errorbk = true;
                alert("Hay un titulo vacio en una seccion");
           }
        }
        }
        
 
        if(form_errorbk == false){
        var textform = new Array();
        $(".Lista_obj_t0").each(function(){
        textform.push(this.id);
        });
        for (conter = 0; conter < textform.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(textform[conter]).value == ""){
                form_errorbk = true;
                alert("Hay una descripción vacio en una seccion");
           }
        }
        }
        
        
        if(form_errorbk == false){
        var Imgformgen = new Array();
        $(".Lista_obj_t1").each(function(){
        Imgformgen.push(this.id);
        });
        for (conter = 0; conter < Imgformgen.length; conter++) {
            if(form_errorbk == true){break;}
            form_errorbk = true; 
           //Get reference of FileUpload.
        var fileUpload = document.getElementById(Imgformgen[conter]);
       
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                var file = fileUpload.files[0];
        //console.log("File " + file.name + " is " + file.size + " bytes in size");
                if(file.size> file_max_size){  ///10 megas
                    alert("Archivo muy grande subido en material.");
                } else{
                    //todo estuvo bien
                    form_errorbk = false;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else { 
            //checa si esta vacio (lo cual no es malo ) o e un archico malo
            if (fileUpload.value == ""){form_errorbk = false;}else{
            alert("Porfavor selecciona un tipo de archivo valido subido en material.");
            }
        }
        }
        }
        
         if(form_errorbk == false){
        var textform = new Array();
        $(".Lista_filen_t").each(function(){
        textform.push(this.id);
        });
        for (conter = 0; conter < textform.length; conter++) {
           if(form_errorbk == true){break;}
           if(document.getElementById(textform[conter]).value == ""){
                form_errorbk = true;
                alert("Hay una nombre vacio en un archivo");
           }
        }
        }
        
        if(form_errorbk == false){
        var Imgformgen = new Array();
        $(".Lista_files_t").each(function(){
        Imgformgen.push(this.id);
        });
        for (conter = 0; conter < Imgformgen.length; conter++) {
            if(form_errorbk == true){break;}
            form_errorbk = true; 
           //Get reference of FileUpload.
        var fileUpload = document.getElementById(Imgformgen[conter]);
       
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf|.txt|.stl|.dxf|.step|.ino|.briko|.jpg|.png|.gif|.zip)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                var file = fileUpload.files[0];
        //console.log("File " + file.name + " is " + file.size + " bytes in size");
                if(file.size> file_max_size){  ///10 megas
                    alert("Archivo muy grande subido en material.");
                } else{
                    //todo estuvo bien
                    form_errorbk = false;
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else { 
            //checa si esta vacio (lo cual no es malo ) o e un archico malo
            if (fileUpload.value == ""){form_errorbk = false;}else{
            alert("Porfavor selecciona un tipo de archivo valido subido en material.");
            }
        }
        }
        }
        
        ///si no hubo ningun error manda todo a la base de datos y sube las imagenes
        if(form_errorbk == false){
            ///falta aqui subir lo demas a la base de datos
            $( "#uploadbutton" ).trigger( "click" ); //activa la fucnion del controlador
        }
        
    });
});
        
</script>
      
<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>