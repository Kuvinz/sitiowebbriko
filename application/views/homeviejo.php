<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<script src="<?php echo base_url();?>js/jquery.bxslider.js"></script>

<script src="<?php echo base_url(); ?>js/move/move.js"></script>
<script src="<?php echo base_url(); ?>js/move/movemodulos/home.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/styleslider.css" />
<div class="row principal">
  <div class="large-12 columns">
    <div class="row principal"  data-equalizer>
    	<div class="orbit-container">
			<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:fade;
                                                            pause_on_hover:false;
                                                            timer: true;
                                                            slide_number: false; 
                                                        navigation_arrows:false;bullets: false;timer_speed: 5000;"  >
			
				<li> 
				    <img src="http://briko.cc/images/BannerN.jpg" alt="slide1"  />
				</li>  
			
			   	<li> 
				    <img src="<?php echo base_url(); ?>images/Banner.jpg" alt="slide1"  />
			  	</li> 

			  	<li class="active"> 
				    <img src="<?php echo base_url(); ?>images/muchosBrikos1.jpg" alt="slide1"  />
			  	</li>  
		
			</ul>

			
		</div>
		<div class="overlay-container">
    		<div class="large-8 large-offset-2 columns overlay">
		    
		        <div class="row principal" style="text-align:center;">
	    			<h1 id="tituloN">¡Construye tus ideas!</h1>
		    	</div>
		    	<div class="row principal" style="text-align:center;">
		    		<p id="textoN"><strong>briko</strong> una manera f&aacute;cil y divertida de aprender electr&oacute;nica, rob&oacute;tica y programaci&oacute;n mientras construyes proyectos.</p>
		    	</div>
		    	<div class="row principal" style="text-align:center;">
		    		<div class="small-6 columns">
		    			<button class="round comK" id="compra" onclick="_gaq.push(['_trackEvent', 'buttonCompra', 'clicked'])">Comprar</button>
		    		</div>
		    		<div class="small-6 columns">
		    			<button class="round comK" id="conoce" >Conoce más</button>
		    		</div>
		    	</div>
		    </div>
		</div>
    	
	    
    </div>
    <div class="row principal" >
	    <div class="small-12 columns" style="    padding-top: 3rem;    padding-bottom: 3rem;">
	    	<!-- slider -->
	    	<h1 id="vistoEn">Nos has visto y escuchado en:</h1>
			<div class="slider-container">
				<ul id="slider">
					<li id="logo1">  <a target="_blank" href="http://www.1070noticias.com.mx/"><img  src="<?php echo base_url();?>/images/logos/logo1.png"></a></li>
			        <li id="logo2">  <a target="_blank" href="http://www.campus-party.com.mx/"><img  src="<?php echo base_url();?>/images/logos/logo2.png"></a></li>
			        <li id="logo3">  <a target="_blank" href="https://collisionconf.com/"><img  src="<?php echo base_url();?>/images/logos/logo3.png"></a></li>
			        <li id="logo4">  <a target="_blank" href="http://www.imagen.com.mx/transmision-en-vivo"><img  src="<?php echo base_url();?>/images/logos/logo4.png"></a></li>
			        <li id="logo5">  <a target="_blank" href="https://posibleplus.mx/"><img  src="<?php echo base_url();?>/images/logos/logo5.png"></a></li>
			        <li id="logo7">  <a target="_blank" href="http://museolaberinto.com/"><img  src="<?php echo base_url();?>/images/logos/logo7.png"></a></li>
			        <li id="logo8">  <a target="_blank" href="http://let-emprendimientopublico.mx/mecate/"><img  src="<?php echo base_url();?>/images/logos/logo8.png"></a></li>
			        <li id="logo9">  <a target="_blank" href="http://mkrsfest.com/JaliscoMakerFaire/"><img  src="<?php echo base_url();?>/images/logos/logo9.png"></a></li>
			        <li id="logo10"> <a target="_blank" href="https://playbusiness.mx/"><img  src="<?php echo base_url();?>/images/logos/logo10.png"></a></li>
			        <li id="logo11"> <a target="_blank" href="http://www.geekpunto.com"><img  src="<?php echo base_url();?>/images/logos/logo11.png"></a></li>
			        <li id="logo13"> <a target="_blank" href="http://tunahack.mx/"><img  src="<?php echo base_url();?>/images/logos/logo13.png"></a></li>
			        <li id="logo14"> <a target="_blank" href="http://sanluispotosi.startupweek.co/?gclid=CjwKEAjwpPCuBRDris2Y7piU2QsSJAD1Qv7B00H93A6eeQBReEmAghrQmZ5ePr1EUjtuEVjan3vfuBoCBcrw_wcB"><img  src="<?php echo base_url();?>/images/logos/logo14.png"></a></li>
			        <li id="logo17"> <a target="_blank" href="http://jornadaemprendedor.com/"><img  src="<?php echo base_url();?>/images/logos/logo17.png"></a></li>
			        <li id="logo16"> <a target="_blank" href="http://www.up.co/communities/mexico/mexicocity/startup-weekend/5881"><img  src="<?php echo base_url();?>/images/logos/logo16.png"></a></li>
			        <li id="logo18"> <a target="_blank" href="http://disruptivo.tv/"><img  src="<?php echo base_url();?>/images/logos/logo18.png"></a></li>			     
				</ul>
			</div><!-- /slider -->
			
	    </div>
    </div>
    <div class="row principal" id="Anima1" >
	    <div class="small-9 small-offset-2 columns flex-video " style="padding-bottom:41%;" >
	    	<iframe id="bN" style="border-width: 0px;" src="<?php echo base_url();?>assets/brikoNew/brikoNew.html" width="960" height="650"></iframe>
	    </div>
    </div>

    <div class="row principal" id="Anima11">
	    <div class="small-4 small-offset-2 columns flex-video moverC" style="padding-bottom:27%;" >
	    	<iframe  style="border-width: 0px;" class="moverC" src="<?php echo base_url();?>assets/Programa/Programa.html" width="396" height="386"></iframe>
	    </div>
	    <div class="small-4 end columns moverCI">
	    	<img src="<?php echo base_url();?>images/numeros21.png">
	    </div>
    </div>
    <div class="row principal" id="Anima12" >
	    <div class="small-4 small-offset-2 columns flex-video moverD" style="padding-bottom:14%;" >
	    	<iframe  style="border-width: 0px;" src="<?php echo base_url();?>assets/Juega/Juega.html" width="430" height="180"></iframe>
	    </div>
	    <div class="small-4 end columns moverDI" >
	    	<img src="<?php echo base_url();?>images/CarritoNuevo.png">
	    </div>
    </div>
    <div class="row principal" id="Fami" >

	    <div class="small-12 columns small-centered">
	    	<img src="<?php echo base_url();?>images/FamiliaBrikoG.png">
	    </div>
    </div>
    <div class="row principal" >
	    <div class="small-12 columns" style="    padding-top: 3rem;    padding-bottom: 3rem;">
	    	<!-- slider -->
	    	<h1 id="vistoEn">Organizaciones que lo utilizan:</h1>
			<div class="slider-container">
				<ul id="slider2">
					
			        <li id="logo6">  <a target="_blank" href="https://www.facebook.com/art4371"><img  src="<?php echo base_url();?>/images/logos/logo6.png"></a></li>
			        
			        <li id="logo12"> <a target="_blank" href="https://www.facebook.com/Robohawk4262"><img  src="<?php echo base_url();?>/images/logos/logo12.png"></a></li>
			        
			        <li id="logo19"> <a target="_blank" href="http://www.clubesdeciencia.mx/"><img  src="<?php echo base_url();?>/images/logos/logo19.png"></a></li>

			        <li id="logo20">  <a target="_blank" href="http://www.itesm.mx/wps/portal?WCM_GLOBAL_CONTEXT="><img  src="<?php echo base_url();?>/images/logos/logo20.png"></a></li>
			        
			        <li id="logo21"> <a target="_blank" href="http://tecmilenio.mx/"><img  src="<?php echo base_url();?>/images/logos/logo21.png"></a></li>
			     
			        <li id="logo22"> <a target="_blank" href="https://www.facebook.com/TeamLamBot3478"><img  src="<?php echo base_url();?>/images/logos/logo22.png"></a></li>

			        <li id="logo23"> <a target="_blank" href="http://www.intelirobot.com.mx/"><img  src="<?php echo base_url();?>/images/logos/logo23.png"></a></li>
				</ul>
			</div><!-- /slider -->
	    </div>
    </div>
  </div>
</div>
 <br>
          <br>
   <script>



	var waypoint = new Waypoint({
	  element: document.getElementById('Anima11'),
	  handler: function(direction) {
	    //alert('I am 20px from the top of the window');
	     move('.moverC').x(0).duration('2s').end();
	     move('.moverCI').x(0).duration('2s').end();

	  },
	  offset: '90%'
	});
	var waypoint = new Waypoint({
	  element: document.getElementById('Anima11'),
	  handler: function(direction) {
	    //alert('I am 20px from the top of the window');
	     move('.moverC').x(0).duration('2s').end();
		 move('.moverCI').x(0).duration('2s').end();
	  },
	  offset: '10%'
	});
	
	var waypoint = new Waypoint({
	  element: document.getElementById('Anima11'),
	  handler: function(direction) {
	    move('.moverC').x(-820).duration('2s').end();
	    move('.moverCI').x(820).duration('2s').end();
	  },
	  offset: '5%'
	});
	/*animaciones para el carrito*/
	var waypoint = new Waypoint({
	  element: document.getElementById('Anima12'),
	  handler: function(direction) {
	    //alert('I am 20px from the top of the window');
	     move('.moverD').x(0).duration('2s').end();
	     move('.moverDI').x(0).duration('2s').end();
	  },
	  offset: '90%'
	});
	var waypoint = new Waypoint({
	  element: document.getElementById('Anima12'),
	  handler: function(direction) {
	    //alert('I am 20px from the top of the window');
	     move('.moverD').x(0).duration('2s').end();
	     move('.moverDI').x(0).duration('2s').end();

	  },
	  offset: '10%'
	});
	
	var waypoint = new Waypoint({
	  element: document.getElementById('Anima12'),
	  handler: function(direction) {
	    move('.moverD').x(-820).duration('2s').end();
	    move('.moverDI').x(820).duration('2s').end();
	  },
	  offset: '5%'
	});
	</script> 
    <script>
        $(document).foundation();
		        //listener del boton del principal para la compra 
		$("#compra").on("click",function(){  //abre pop
		    window.open('http://briko.cc/shop','_self');
		}); 
		$("#conoce").on("click",function(){  //abre pop
		    window.open('http://briko.cc/modulos','_self');
		}); 
      </script>
    
    
    <script type="text/javascript">
  		$(document).ready(function(){
     		$('#slider').bxSlider({
			  minSlides: 5,
			  maxSlides: 5,
			  //slideWidth: 170,
			  //slideMargin: 10,
			  ticker: true,
			  speed: 31000,
			  tickerHover: true,
			  autoHover:true
			});

     	$('#slider2').bxSlider({
    		minSlides: 5,
			  maxSlides: 5,
			  //slideWidth: 170,
			  //slideMargin: 10,
			  ticker: true,
			  speed: 15000,
			  tickerHover: true,
			  autoHover:true
  		});});
  		
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
  </body>
</html>