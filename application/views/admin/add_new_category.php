<?php $this->load->view('blog/header');?>
<body>

	<!-- header top starts-->
	<?php $data['nombre']="no";$arr=array('usuario'=>$data);$this->load->view('header',$arr);?>
	<!-- header top ends here -->
	
	<!-- content starts -->
		<div id="content-wrapper" class="row">
			<!-- column-one -->
			<div id="content" class="columns large-12">	
			
			<h2>Add New Category</h2>
			<?php echo form_open('add-new-category');?>
			
			<?php if(validation_errors()){echo validation_errors('<p class="error">','</p>');} ?>
            <?php if($this->session->flashdata('message')){echo '<p class="success">'.$this->session->flashdata('message').'</p>';}?>
			
			<p><label>Category Name</label>
			<input type="text" name="category_name" size="30" /></p>
			
			<p><label>Slug</label>
			<input type="text" name="category_slug" size="30" /></p>
			
			<br />	
			
			<input class="button" type="submit" value="Submit"/>
			<input class="button" type="reset" value="Reset"/>	
			
			</form>
	<!-- contents end here -->	
	</div></div>

	<!-- footer starts here -->	
	<?php $this->load->view('footer');?>
	<!-- footer ends here -->
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
</body>
</html>