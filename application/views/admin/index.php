<?php $this->load->view('blog/header');?>
<body>

	<!-- header top starts-->
	<?php $data['nombre']="no";$arr=array('usuario'=>$data);$this->load->view('header',$arr);?>
	<!-- header top ends here -->
	<!-- content starts -->
	<div class="row">
		<!-- column-one -->
		<div  class="columns large-5">	
			<h2>Dashboard</h2>
			<h3>My Profile</h3>
			
			<p>Username: <?php echo $user->username;?></p>
			<p>Email: <?php echo $user->email;?></p>
			<p>Last login: <?php echo unix_to_human($user->last_login);?></p>
		</div>
		<div class="columns large-2 end">	
			<!-- column-two -->
			<?php if ( ! $this->ion_auth->logged_in() ):?>
			<h3>Sidebar Menu</h3>
			<ul class="sidemenu">
				<li><a href="<?php echo base_url().'auth/login';?>">Login</a></li>
				<li><a href="<?php echo base_url().'about';?>">About</a></li>
				<li><a href="http://www.pisyek.com" rel="follow" target="_blank">Contact</a></li>
			<?php else: ?>
			<h3>Admin Menu</h3>
			<ul class="sidemenu">
				<li><a href="<?php echo base_url().'auth/';?>">Dashboard</a></li>
				<li><a href="<?php echo base_url().'add-new-entry';?>">Add new entry</a></li>
				<li><a href="<?php echo base_url().'add-new-category';?>">Add new category</a></li>
				<li><a href="<?php echo base_url().'auth/logout';?>">Logout</a></li>
			<?php endif; ?>
				
			</ul>

            <?php if( isset($categories) && $categories ):?>
			<h3>Category</h3>
			<ul class="sidemenu">
			<?php foreach( $categories as $category ):?>
				<li><a href="<?php echo base_url().'category/'.$category->slug;?>"><?php echo $category->category_name;?></a></li>
			<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</div>
	<!-- contents end here -->	
	</div>

	<!-- footer starts here -->	
	<?php $this->load->view('footer');?>
	<!-- footer ends here -->
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
</body>
</html>