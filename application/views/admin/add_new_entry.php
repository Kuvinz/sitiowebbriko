<?php $this->load->view('blog/header');?>
<body>

	<!-- header top starts-->
	<?php $data['nombre']="no";$arr=array('usuario'=>$data);$this->load->view('header',$arr);?>
	<script src="<?php echo base_url()?>js/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url()?>js/ckeditor/sample.js"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>css/ckeditor/samples.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/ckeditor/neo.css">
	<!-- header top ends here -->
	
	<!-- content starts -->
		<div id="content-wrapper" class="row">
			<!-- column-one -->
			<div id="content" class="columns large-12">	
				
				
					<h2>Add New Entry</h2>
					<?php echo form_open_multipart('add-new-entry');?>
					
					<?php if(validation_errors()){echo validation_errors('<p class="error">','</p>');} ?>
		            <?php if($this->session->flashdata('message')){echo '<p class="success">'.$this->session->flashdata('message').'</p>';}?>
					
					<p><label>Title</label>
					<input type="text" name="entry_name" size="30" /></p>

					<p><label id="count">Description</label>
					<textarea name="entry_description" id="entry_description" row="5" col="15"></textarea></p>
					<div id="" class="row">	
						<div class="columns large-6">
							<label class = "h2classbk" >Imagen Principal</label>
          					<input class="imgformpaso1" name="userfile1[]" type="file" multiple="" />
						</div>
						<div class="columns large-6">	
							<h3>Category</h3>
							<p><?php if( isset($categories) && $categories): foreach($categories as $category): ?>
								<label><input class="checkbox" type="checkbox" name="entry_category[]" value="<?php echo $category->category_id;?>"><?php echo $category->category_name;?></label>
								<?php endforeach; else:?>
								Please add your category first!
								<?php endif; ?>
							</p>
						</div>
					</div>
					<p><label>Your Entry: (in html)</label></p>
					<div id="editor">
						<p>Aqui pones el contenido del BLog</p>
					</div>
					
					<br />	
					<input type="hidden" name="entry_body" id="entry_body"/>
					
					<input class="button" type="submit" id="submitcomm" value="Submit"/>
					<input class="button" type="reset" value="Reset"/>	
					
					</form>
			

		
	<!-- contents end here -->	
			</div>
		</div>

	<!-- footer starts here -->	
	<?php $this->load->view('footer');?>
	<!-- footer ends here -->
	<script>
	initSample();
	var banderaDesc=false;
	$(document).ready(function() {
		$("#entry_description").keyup(function(){
		  	$("#count").text("Description -> Characters left: " + (200 - $(this).val().length));
		  	if((200 - $(this).val().length)==0||(200 - $(this).val().length)<0)
		  	{
		  		banderaDesc=true;
		   	}
		   	else
		   	{
		   		banderaDesc=false;
		   	}
		});
	$( "#submitcomm" ).click(function()
	{
		var comentarios= CKEDITOR.instances.editor.getData();
		$('#entry_body').val(comentarios);
		if(banderaDesc==true)
		{
			return false;
		}
		else if(banderaDesc==false)
		{
			return true;
		}
		//alert(comentarios);
		
	});});
</script>
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
</body>
</html>