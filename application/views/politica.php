<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/comienzaahora.css">

<div class="row"  id="div1">
  <div class="large-12  columns">

    <!-- titulo principal -->
    <div class="row">
        <div class="large-12 columns" style="text-align:center" >
            <h2 class = "h1classbk">Política de privacidad de briko:</h2>
        </div>
    </div>
      
      <!-- texto -->
      <div class="row">
        <div class="large-12 columns" style="text-align:justify" >
            <p style="color:rgb(10,10,10)">Briko se compromete a proteger la privacidad de la información personal que usted proporcione en este sitio Web a través de sus servicios en línea. La presente política de privacidad ha sido creada y está en vigencia desde el 01/12/2014. Las condiciones de esta política de privacidad son válidas para todos los usuarios del Sitio. Si usted no está de acuerdo con las condiciones de nuestra política de privacidad, deberá salir de este Sitio inmediatamente. Estas políticas de confidencialidad pueden tener cambios en el futuro, por lo que se recomienda revisarlas periódicamente.<br/><br/>
 
Briko recopila información personal que usted proporciona en forma voluntaria al completar los formularios en línea, entre los cuales se incluyen: solicitudes de contacto, y otras actividades en línea. La información personal ("Información personal") que se recopila en este Sitio puede incluir todos o algunos de los siguientes datos: nombre, dirección de correo electrónico, información demográfica y cualquier otra información que proporcione en forma voluntaria. Usted tendrá la posibilidad de elegir si revela o no dicha Información personal para las actividades antes mencionadas. Sin embargo, será más difícil o imposible utilizar algunas partes y servicios de nuestro Sitio si usted elige no revelar Información personal.<br/><br/>
 
Asimismo, nuestro Sitio utiliza tecnología denominada "cookies" para ofrecer a los visitantes información personalizada en cada visita. Las cookies son parte habitual de muchos sitios Web comerciales y permiten que éstas envíen pequeños archivos de texto, que el navegador de Internet los acepte y luego los coloque en el disco rígido como método para reconocer el Sitio cada vez que se lo visita. Cada vez que visite nuestro Sitio, nuestros servidores recopilan, a través de las cookies, los píxeles y/o los archivos GIF, información técnica básica como por ejemplo nombre de dominio, la dirección de la última URL que visitó antes de llegar al Sitio, tipo de navegador y sistema operativo. No es necesario que habilite el uso de cookies para visitar nuestro Sitio. Sin embargo, será más difícil o imposible utilizar algunas partes y servicios de nuestro Sitio si no está habilitado el uso de cookies. Enfatizamos que las cookies no están vinculadas a ningún tipo de Información personal mientras visita este Sitio.<br/><br/>
 
Si decide proporcionarnos su Información de manera voluntaria, guardaremos su Información personal en nuestra base de datos estrictamente segura y confidencial para los siguientes fines: (1) analizar el uso del Sitio y realizar mejoras; (2) responder a solicitudes específicas de los visitantes; (3) registrar a los visitantes para actividades en línea; (4) proteger la seguridad o integridad del Sitio si fuera necesario.<br/><br/>
 
En Briko la confidencialidad es nuestro valor más importante y es por esto que Briko no venderá, alquilará, prestará, transferirá ni revelará a terceros en ninguna otra forma la Información personal que los visitantes nos proporcionan de manera voluntaria.<br/><br/>
 
Briko no ofrece ningún tipo de premio, compensación, o cualquier tipo de beneficio a cambio de que usted proporcione sus datos.<br/><br/>
 
Briko toma las medidas de seguridad apropiadas para resguardar toda la Información personal recopilada en un lugar seguro, controlado y estrictamente confidencial.<br/><br/>
 
Briko atendiendo las solicitudes de información de los usuarios del sitio establece una comunicación vía email donde la persona contactada en caso de ya no estar interesada en nuestros servicios podrá solicitar que no se le vuelva a contactar y es así como serán eliminados sus datos de nuestra base de datos.<br/><br/>
 
Si tiene alguna pregunta adicional con respecto a la política de privacidad de Briko y el uso de su Información personal, por favor comuníquese con contacto@briko.cc<br/><br/>
</p>
        </div>
    </div>
    

</div>
 <br>
 <br>
</div>


<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>