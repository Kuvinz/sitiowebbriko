<div class="row" id="reset_password_form">

	<?php echo validation_errors('<p class="error">');
		if(isset($error))
		{
			echo '<p class="error">'.$error.'</p>';
		} ?>
	<?php echo form_open('usform/update_password');?>
		<div class="row">
			<div class="colums large-12">
				<h2 class="text-center modalInicio">Restablece Contrase&ntilde;a</h2>
			</div>
		</div>
		<div class="row">			
			<div class="columns large-12">
				<div class="row collapse prefix-radius">
					<?php if(isset($email_hash,$email_code)){?>
					<input type="hidden" value="<?php echo $email_hash;?>" name="email_hash"/>
					<input type="hidden" value="<?php echo $email_code;?>" name="email_code"/>
					<?php }?>
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Correo</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="text" name="email" class="inputReg" value="<?php echo (isset($email)?$email:'');?>">
			        </div>
			    </div>
				<div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Nueva Contrase&ntilde;a</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="password" name="pass" class="inputReg" placeholder="***************">
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Confirme nueva Contrase&ntilde;a</span>
			        </div>
			        <div class="small-8 columns">
			          	<input type="password" name="pass_conf" class="inputReg" placeholder="***************">
			        </div>
			    </div>
			</div>
		</div>
		<div class="row collapse">
			<div class="colums large-6 large-offset-4">
				<button class="round comK" id="submitPass">Restablecer contrase&ntilde;a</button>
			</div>
		</div>
	</form>
</div>
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>