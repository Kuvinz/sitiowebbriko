<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 

<div class="row"  id="div1">
  <div class="large-12  columns">

<!------------------------------------------------------------------------------>
  
<!--Slide show, titulo principal y descripcion-->     
<div class="row" >
        <div class="small-6 columns" id="tituloPrincipal">
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 2; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/prin<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li>  
<?php }else{ ?>  
   <li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/prin<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li>   
 <?php } ?>
 <?php } ?>
    <li data-orbit-slide="headline-1">
  <div class="flex-video">
        <iframe width="400" height="215" src="//www.youtube.com/embed/syXS6qOqDwc" frameborder="0" allowfullscreen></iframe>
  </div>
  </li>
</ul>
</div>
<div class="small-6 end columns" >
    <h1 class="h1classbk">Carrito briko:</h1>
    <span class='label desKY'> Descripci&oacute;n: </span>
    <ul class="especF">
        <li>
            <p class="pclassbk text-justify">Este proyecto consta de construir un carrito que evita obstaculos con las piezas mecanicas de briko.</p>
        </li>
    </ul>
</div>
</div>
      
<!-- cintillo se separacion -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <img alt="briko cintillo" width= "200%"src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cintillo2.png"; >
        </div>
    </div>
            
<!-- titulo "pasos" -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Pasos del proyecto:</h1>
        </div>
    </div>
      
<!-- tabs para entrar a diferentes funciones -->
    <div class="row">
        <div class="small-12 columns ">
            <ul class="tabs" id="myTabs" data-tab>
                <li class="tab-title active"><a href="#panel1">Paso 1</a></li>
                <li class="tab-title"><a href="#panel2">Paso 2</a></li>
                <li class="tab-title"><a href="#panel3">Paso 3</a></li>
                <li class="tab-title"><a href="#panel4">Paso 4</a></li>
                <li class="tab-title"><a href="#panel5">Paso 5</a></li>
                <li class="tab-title"><a href="#panel6">Paso 6</a></li>
                <li class="tab-title"><a href="#panel7">Paso 7</a></li>
            </ul>
        </div>
    </div>
      
<!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content" id="myTabscontainer">
      
<!------------------------------------------------------------------------------>
      
<!-- Paso 1 -->
<!-- tab -->
<div class="content active" id="panel1">
    <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Material:</h1>
        </div>
    </div>
    
<!-- titulode agreagar modulos a comprar -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <p class=" pclassbk">Agrega las piezas de briko utilizadas en este proyecto a tu carrito de compra:</p>
        </div>
    </div>
    
    <!-- Boton de compra-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="agregacompra" class= "button round">Agregar</button>
        </div>
    </div>
    
<!--piezas del proyecto -->
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> bk7</li>
                <p class=" pclassbk text-justify"><span class="param">Controlador principal de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko maestro" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/bk7.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> brikos de motor</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de motor de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko motor" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png"; >
    </div>
</div>

<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> briko de leds</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de leds de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko leds" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> briko de distancia</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de distancia de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko sensor distancia" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> llantas</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de distancia de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="llanta" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/llanta.png"; >
    </div>
</div>
    
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> rueda loca</li>
                <p class=" pclassbk text-justify"><span class="param">Una rueda loca de metal con sus tornillos para montarla.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="accesorio rueda loca" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/ruedaloca.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> piso</li>
                <p class=" pclassbk text-justify"><span class="param">La pieza mecanica que forma el piso de un carrito.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza3.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> vigas</li>
                <p class=" pclassbk text-justify"><span class="param">La pieza mecanica larga en forma de viga.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion viga" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/viga.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> uniones 90</li>
                <p class=" pclassbk text-justify"><span class="param">Las piezas chiquitas que sirven para unir mas piezas.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de contruccion union" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/union3.png"; >
    </div>
</div>  
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> pared</li>
                <p class=" pclassbk text-justify"><span class="param">La pieza cuadrada que sirve para montar módulos.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza1.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">6 </span> tornillos chicos</li>
                <p class=" pclassbk text-justify"><span class="param">Tornillos para el maestro y los modulos "menos" el de motor.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="tornillo chico" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tornilloch.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">4 </span> tornillos grandes</li>
                <p class=" pclassbk text-justify"><span class="param">Tornillos para los modulos de motor.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="tornillo grande" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tornillog.png"; >
    </div>
</div>
    
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">10 </span> tuercas</li>
                <p class=" pclassbk text-justify"><span class="param">Tuercas para sujetar los tornillos.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
       <img alt="tuerca" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tuerca.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">4 </span> cables</li>
                <p class=" pclassbk text-justify"><span class="param">Cables para conectar tus modulos al bk7.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="cable briko" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cablebriko.png"; >
    </div>
</div>

<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> pila</li>
                <p class=" pclassbk text-justify"><span class="param">Pila para alimentar tu carrito.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pila" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pila.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> cable micro usb</li>
                <p class=" pclassbk text-justify"><span class="param">Cable para programar tu briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="cable micro usb" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cablemicrousb.png"; >
    </div>
</div>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
      
<!-- arreglo para guardar los mensajes del slider -->
<?php $pasos = array("1- Montar el briko de distancia.","2-: Montar el briko de distancia.","3- Montar el briko de motor al piso.","4- Montar el briko de motor al piso.","5- Insertar las llantas en los motores."," 6-: Atornillar el bk7 al piso.","7- Montar la rueda loca al piso.","8- Colocar las 2 uniones 90 en la parte frontal del piso.","9- Unir el sensor de distancia a la parte frontal del piso.","10- Montar el briko de leds.","11- Ensamblar 2 vigas en la parte trasera del piso. ", "12- Montar los leds en las vigas puestas en la parte trasera del piso.","13- Vista final.","14- Vista final en carrito real."); ?>        
        
<!-- Paso 2 -->
<!-- tab -->
<div class="content" id="panel2">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Armemos el carrito:</h1>
        </div>
    </div>
           
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns">
            <ul class="especF">
                <li>
            <p id="textslider1" class="pclassbk text-justify">Sigue los pasos de las imagenes de abajo para armar el carrito.</p>
                </li>
             </ul>
        </div>
    </div>
    
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< count($pasos); $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso2<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
      <div class="orbit-caption">
    <?php echo $pasos[$i]; ?>
    </div>
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso2<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
       <div class="orbit-caption">
    <?php echo $pasos[$i]; ?>
    </div>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
<!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 3 -->
<!-- tab -->
<div class="content" id="panel3">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Conectar los módulos:</h1>
        </div>
    </div>
          
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Para poder programar nuestro carrito hay que conectar nuestro briko de leds en el puerto "5", el briko de motor izquierdo en el puerto "2", el briko de motor derecho en el puerto "6", y por último el briko de distancia en el puerto "4".</p>
                </li>
            </ul>
        </div>
    </div>
    
    <!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 3; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso3<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso3<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
        
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
               
<!-- Paso 4 -->
<!-- tab -->
<div class="content" id="panel4">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Probemos los leds:</h1>
        </div>
    </div>
          
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Lo primero que tenemos que hacer es abrir brikode. Una vez abierto declaramos nuestro briko de leds con el nombre "luces" en el puerto "5".</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Una vez declarado nuestro briko de leds, dentro de "code" ponemos la instrucción "color" que nos permitira prender nuestros leds del color que queramos, en el siguiente código los prenderemos rojos:</p>
                </li>
             </ul>
        </div>
    </div>
    
    <!-- Boton de compra-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="botoncopy" class= "button round">Copiar el código</button>
        </div>
    </div>
    
<!--ponemos un editor de texto-->
    <div class="row">
        <div class="large-12 columns">
            <div id="editor">
// Aqui se declaran los modulos que vas a utilizar
ledsbk luces(PORT5);

code(){
// Aqui se escribe tu programa
luces.color(RED);
}
            </div>
        </div>
    </div>
    <br>
    
    <!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 1; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso4<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso4<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 5 -->
<!-- tab -->
<div class="content" id="panel5">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Probemos el sensor de distancia:</h1>
        </div>
    </div>    
        
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Ahora declaramos nuestro briko de leds con el nombre "luces" en el puerto "5" y también declaramos nuestro briko de distancia con el nombre "sensor" en el puerto "4".</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Una vez declarado nuestros brikos, dentro de "code" ponemos la instrucción "read" que nos permitira medir la distancia con el sensor, también usaremos una estructura "if-else" para poder decirle al carrito que si la distancia medida es menor a 15 centimetros prenda los leds en azul, y si es mayor que los prenda en rojo, como se ve en el siguiente código:</p>
                </li>
             </ul>
        </div>
    </div>
    
    <!-- Boton de copiar-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="botoncopy2" class= "button round">Copiar el código</button>
        </div>
    </div>
    
<!--ponemos un editor de texto-->
    <div class="row">
        <div class="large-12 columns">
            <div id="editor2">
// Aqui se declaran los modulos que vas a utilizar
ledsbk luces(PORT5);
distancebk sensor(PORT4);

code() {
    // Aqui se escribe tu programa
    if (sensor.read() < 15) {
        luces.color(BLUE);
    } else {
        luces.color(RED);
    }

}
            </div>
        </div>
    </div>
    <br>
        
   <!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 1; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso5<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso5<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 6 -->
<!-- tab -->
<div class="content" id="panel6">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Probemos los motores:</h1>
        </div>
    </div>
      
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Ahora declaramos nuestros brikos de motores con los nombre "motorizquierdo" y "motorderecho" en el puerto "2" y "6" respectivamente.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Una vez declarado nuestros brikos de motor, dentro de "code" ponemos la instrucción "set" que nos permitira prender nuestros motores en la dirección y velocidad que queramos, en el siguiente código los pondremos a girar por 1000 milisegundos a la izquierda a una velocidad de 255 y luego un segundo a la derecha con la misma velocidad:</p>
                </li>
             </ul>
        </div>
    </div>
    
    <!-- Boton de compra-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="botoncopy3" class= "button round">Copiar el código</button>
        </div>
    </div>
    
<!--ponemos un editor de texto-->
    <div class="row">
        <div class="large-12 columns">
            <div id="editor3">
// Aqui se declaran los modulos que vas a utilizar
motorbk motorizquierda(PORT2);
motorbk motorderecha(PORT6);

code() {
    // Aqui se escribe tu programa
    motorizquierda.set(RIGHT,255);
    motorderecha.set(RIGHT,255);
    delay(1000);
    motorizquierda.set(LEFT,255);
    motorderecha.set(LEFT,255);
    delay(1000);

}
            </div>
        </div>
    </div>
    <br>
    
       <!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 1; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso6<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso6<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 7 -->
<!-- tab -->
<div class="content" id="panel7">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Unamos todo:</h1>
        </div>
    </div>
        
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Ahora si declaramos todos los modulos que hemos usado hasta ahora en los mismos puertos que antes.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Una vez declarado nuestro brikos, programaremos que si el carrito detecta un objeto a menos de 15 centimetros, que prenda los leds en rojo, gire a la derecha y se espere 250 milisegundos, y si no detecta nada que siga derecho y prenda los leds en azul, como se ve en el siguiente código:</p>
                </li>
             </ul>
        </div>
    </div>
    
    <!-- Boton de compra-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="botoncopy4" class= "button round">Copiar el código</button>
        </div>
    </div>
    
<!--ponemos un editor de texto-->
    <div class="row">
        <div class="large-12 columns">
            <div id="editor4">
// Aqui se declaran los modulos que vas a utilizar
ledsbk luces(PORT5);
distancebk sensor(PORT4);
motorbk motorizquierda(PORT2);
motorbk motorderecha(PORT6);

code() {
    // Aqui se escribe tu programa
    if (sensor.read() < 15) {
        luces.color(RED);
        motorizquierda.set(RIGHT, 255);
        motorderecha.set(RIGHT, 255);
        delay(250);
    } else {
        luces.color(BLUE);
        motorizquierda.set(RIGHT, 255);
        motorderecha.set(LEFT, 255);
    }
}
            </div>
        </div>
    </div>
    <br>
        
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 1; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso7<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carrito/paso7<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
        
    <!-- Botones-->
    <div class="row">
        <div class="small-7 small-offset-5 columns">
            <button class="back button round">Atras</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
 
</div>
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>
      
<!--modal para mandar un mensaje de error -->
<!--crea un mensaje de alerta tipo modal-->
<div id="first-modal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h2 id="modalTitle">Ups.</h2>
      <p class="lead">Hay un problema con este link.</p>
      <p>Este link no esta disponible por el momento pero nuestro equipo esta trabajando en ello, agradecemos tu comprensión.</p>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
        
<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor = ace.edit("editor");  //liga el editor declaradoe en html
editor.getSession().setMode( xml_mode ); //pone el modo
editor.setTheme("ace/theme/brikode"); //pone el tema
editor.getSession().setTabSize(2);
editor.getSession().setUseWrapMode(true);
editor.setReadOnly(true);  // false to make it editable
</script> 
<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor2 = ace.edit("editor2");  //liga el editor declaradoe en html
editor2.getSession().setMode( xml_mode ); //pone el modo
editor2.setTheme("ace/theme/brikode"); //pone el tema
editor2.getSession().setTabSize(2);
editor2.getSession().setUseWrapMode(true);
editor2.setReadOnly(true);  // false to make it editable
</script> 
<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor3 = ace.edit("editor3");  //liga el editor declaradoe en html
editor3.getSession().setMode( xml_mode ); //pone el modo
editor3.setTheme("ace/theme/brikode"); //pone el tema
editor3.getSession().setTabSize(2);
editor3.getSession().setUseWrapMode(true);
editor3.setReadOnly(true);  // false to make it editable
</script> 
<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor4 = ace.edit("editor4");  //liga el editor declaradoe en html
editor4.getSession().setMode( xml_mode ); //pone el modo
editor4.setTheme("ace/theme/brikode"); //pone el tema
editor4.getSession().setTabSize(2);
editor4.getSession().setUseWrapMode(true);
editor4.setReadOnly(true);  // false to make it editable
</script> 

<script>
    
//detecta cuando cambias de pestana
$('#myTabs').on('toggled', function (event, tab) {
    //guarda el nombre del tab donde estas
    //vuelve a cargar los sliders para que aparescan
    $(document).foundation('orbit', 'reflow');
  });
        
$('#myTabscontainer').on('click', '.next', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').next().addClass('active');
       $(document).foundation('orbit', 'reflow');
});
    
$('#myTabscontainer').on('click', '.back', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').prev().addClass('active');
       $(document).foundation('orbit', 'reflow');
});
    
    
//listener para copiar el codigo en el editor de texto
$("#botoncopy").on("click",function(){ 
var Strind_editor  =editor.getValue();
copyToClipboard(Strind_editor);
}); 
//listener para copiar el codigo en el editor de texto
$("#botoncopy2").on("click",function(){ 
var Strind_editor  =editor2.getValue();
copyToClipboard(Strind_editor);
}); 
//listener para copiar el codigo en el editor de texto
$("#botoncopy3").on("click",function(){ 
var Strind_editor  =editor3.getValue();
copyToClipboard(Strind_editor);
}); 
//listener para copiar el codigo en el editor de texto
$("#botoncopy4").on("click",function(){ 
var Strind_editor  =editor4.getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
    
$("#agregacompra").on("click",function(){  //redirige a otra pagina
    $('#first-modal').foundation('reveal','open');
});
 
</script>

<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>