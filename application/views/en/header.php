
<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="shortcut icon" href="<?php echo base_url();?>images/brikon.ico?521017215"/>
		<title>briko</title>
		<link rel="stylesheet" href="<?php echo base_url();?>css/foundation.css"/>
		<script src="<?php echo base_url();?>js/vendor/modernizr.js"></script>
		<link href="<?php echo base_url();?>css/prettify.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/default.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/component.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/proyectoL.css" />
		
		<script src="<?php echo base_url();?>js/vendor/jquery.js"></script>
		<script src="<?php echo base_url();?>js/foundation.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>/scripts/prettify.js"></script>
	</head>
	<body class="cbp-spmenu-push" onload="prettyPrint()">

		<div class="row">

	        <div class="large-12 columns">
	        	
	          <div class="row">
	            <div class="large-12 columns large-12 columns contain-to-grid fixed" id="u72">
	     
	              <nav class="top-bar" data-topbar role="navigation">
	                <ul class="title-area">
	                   
	                  <li class="name">
	                    <a class="nonblock nontext" href="http://briko.cc"><img src="<?php echo base_url();?>images/LogoBriko250.png" ><!-- simple frame --></a>
	                  </li>
	                  <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
	                </ul>
	             
	                <section class="top-bar-section" style="float: right;">
	                   
	                  <ul class="left">
	                    <li class="divider"></li>
	                    <li >
	                      <a href="<?php echo base_url();?>started">Get Started</a>	                      
	                    </li>
	                    <li class="divider"></li>
	                    <li>
	                    	<a href="<?php echo base_url();?>projects">Projects</a>
	                   	</li>
	                    <li class="divider"></li>
	                    <li >
	                      <a href="<?php echo base_url();?>reference">Reference</a>	                      
	                    </li>
	                    <li class="divider"></li>
	                    <li >
	                      <a href="<?php echo base_url();?>#kits">Buy</a>
	                    </li>
	                    <li class="divider"></li>
	                    <li >
	                      <a href="<?php echo base_url();?>eventos">Events</a>	                      
	                    </li>
	                  </ul>
	                </section>
	              </nav>
	               
	            </div>
	          </div>