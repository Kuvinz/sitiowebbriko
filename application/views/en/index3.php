<!DOCTYPE html>
<html class="html" lang="es-ES">
 <head>

  <script type="text/javascript">
   if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["jquery-1.8.3.min.js", "museutils.js", "jquery.watch.js", "webpro.js", "jquery.musepolyfill.bgsize.js", "musewpslideshow.js", "jquery.museoverlay.js", "touchswipe.js", "jquery.scrolleffects.js", "museredirect.js", "index.css"], "outOfDate":[]};
</script>

  

  <script src="<?php echo base_url();?>scripts/museredirect.js?17485671" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!--<script type="text/javascript">
   Muse.Redirect.redirect('desktop', 'tablet/index.html', 'phone/index.html', '');
</script>-->
  
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2014.2.1.284"/>
  <link rel="shortcut icon" href="<?php echo base_url();?>images/brikon.ico?521017215"/>
  <title>briko</title>
  <link media="only screen and (max-device-width: 370px)" rel="alternate" href="http://briko.cc/phone/index.html"/>
  <link media="only screen and (max-device-width: 960px)" rel="alternate" href="http://briko.cc/tablet/index.html"/>
  <meta http-equiv="X-UA-Compatible" content="chrome=IE8"/>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/site_global.css?3988879632"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/index.css?372246075" id="pagesheet"/>
  <!-- Other scripts -->
  <script type="text/javascript">
   document.documentElement.className += ' js';
</script>
    <!--custom head HTML-->
  <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-57402676-1', 'auto');
ga('require', 'displayfeatures');
ga('send', 'pageview');

</script>

<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700,700italic|Rokkitt:400,700|News+Cycle:400,700' rel='stylesheet' type='text/css'>

<style>


body{
    width: 100%;
    overflow-x: hidden;
}

#u1148, #u1149{
    
    border-style: solid;
    border-width: 7px 7px 0;
    width: 574px;
}

#u1075, #buttonu1202, #buttonu1239{
    width:1012px;
}

#u72, #u73, #u2407, #u595 { 
    z-index: 350; 
}

</style>
 </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <a class="anchor_item colelem" id="inicio"></a>
    <div class="clearfix colelem" id="pu72"><!-- group -->
     <div class="browser_width" id="u72-bw">
      <div id="u72"><!-- simple frame --></div>
     </div>
    <a class="nonblock nontext" id="u73" href="http://briko.cc"><!-- simple frame --></a>
     <div class="clearfix" id="u2407"><!-- group -->
      <a class="nonblock nontext Button anim_swing clearfix" id="buttonu2404" href="http://briko.cc/comienza"><!-- container box --><div class="position_content" id="buttonu2404_position_content"><div class="menu clearfix colelem" id="u2405-4"><!-- content --><p>Get Started</p></div></div></a>
      <a class="nonblock nontext Button anim_swing clearfix" id="buttonu150" href="http://briko.cc/proyectos"><!-- container box --><div class="position_content" id="buttonu150_position_content"><div class="menu clearfix colelem" id="u151-4"><!-- content --><p id="pact">Projects</p></div></div></a>
      <a class="nonblock nontext Button anim_swing clearfix" id="buttonu192" href="http://briko.cc/glosario"><!-- container box --><div class="position_content" id="buttonu192_position_content"><div class="menu clearfix colelem" id="u193-4"><!-- content --><p>Reference</p></div></div></a>
      <a class="nonblock nontext Button anim_swing clearfix" id="buttonu189" href="#kits"><!-- container box --><div class="position_content" id="buttonu189_position_content"><div class="menu clearfix colelem" id="u190-4"><!-- content --><p>Buy</p></div></div></a>

      <a class="nonblock nontext Button anim_swing clearfix" id="buttonu1890" href="http://briko.cc/eventos"><!-- container box --><div class="position_content" id="buttonu189_position_content"><div class="menu clearfix colelem" id="u1900-4"><!-- content --><p>Events</p></div></div></a>
      
     </div>
    </div>
    

    <div class="clearfix colelem" id="pu125"><!-- group -->
     <div class="browser_width grpelem" id="u125-bw">
      <div id="u125"><!-- column -->
       <div class="clearfix" id="u125_align_to_page">
        <div class="position_content" id="u125_position_content">
         <div class="titulo-ubuntu clearfix colelem" id="u138-6"><!-- content -->
          <h1>¡Build your</h1>
          <h1>ideas!</h1>
         </div>
         <div class="texto-rokkit clearfix colelem" id="u207-4"><!-- content -->

          <p><bold style="font-family: 'maxwellbold'; font-size: 45px;">briko</bold> A fun way to learn electronics, robotics and programming while creating proyects with a set of modules.</p>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="texto-rokkit clearfix grpelem" id="u195-4"><!-- content -->
      <p>Friendly Electronics</p>
     </div>
     <div class="texto-rokkit clearfix grpelem" id="u197-4"><!-- content -->
      <p>Simple Programming</p>
     </div>
     <div class="grpelem" id="u199"><!-- simple frame --></div>
    </div>
    
    <div class="clearfix colelem" id="pu397"><!-- group -->
     <div class="browser_width grpelem" id="u397-bw">
      <div class="museBGSize" id="u397"><!-- group -->
       <div class="clearfix" id="u397_align_to_page">
        <div class="grpelem" id="u398"><!-- simple frame --></div>
        <div class="texto-rokkit clearfix grpelem" id="u399-4"><!-- content -->
         <p>Everyone´s ideas can become reality with
          from building a robot that follows your hand, a fan that turns on at a certain temperature or even a house alarm.&quot;</p>
        </div>
       </div>
      </div>
     </div>
     <div class="subtitulos-ubuntu clearfix grpelem" id="u400-4"><!-- content -->
      <p></p><h1 style="position: relative; font-size: 65px; left: 3rem;">briko</h1>
     </div>
    </div>
    <div class="clearfix colelem" id="pu368"><!-- group -->
     <div class="browser_width grpelem" id="u368-bw">
      <div id="u368"><!-- column -->
       <div class="clearfix" id="u368_align_to_page">
        <div class="clearfix colelem" id="pu369-4"><!-- group -->
         <div class="texto-rokkit clearfix grpelem" id="u369-4"><!-- content -->
          <p>Plug</p>
         </div>
         <div class="texto-rokkit clearfix grpelem" id="u370-4"><!-- content -->
          <p>Code</p>
         </div>
        </div>
        <div class="clearfix colelem" id="pu374"><!-- group -->
         <img class="grpelem" id="u374" src="<?php echo base_url();?>images/Plug.png" width="235" height="235" alt="" data-mu-svgfallback="images/icon-conecta_poster_.png"/><!-- svg -->
         <img class="grpelem" id="u372" src="<?php echo base_url();?>images/Programa.png" width="235" height="235" alt="" data-mu-svgfallback="images/icon-programa_poster_.png"/><!-- svg -->
        </div>
        <div class="texto-rokkit clearfix colelem" id="u1409-4"><!-- content -->
         
        </div>
        <div class="colelem" id="u1410"><!-- simple frame --></div>
       </div>
      </div>
     </div>
     <div class="subtitulos-ubuntu clearfix grpelem" id="u378-4"><!-- content -->
      <h2>1</h2>
     </div>
     <div class="subtitulos-ubuntu clearfix grpelem" id="u379-4"><!-- content -->
      <h2>2</h2>
     </div>
     <div class="subtitulos-ubuntu clearfix grpelem" id="u380-4"><!-- content -->
      <h2>3</h2>
     </div>
     <div class="SlideShowWidget clearfix grpelem" id="slideshowu932"><!-- none box -->
      <div class="popup_anchor" id="u933popup">
       <div class="SlideShowContentPanel clearfix" id="u933"><!-- stack box -->
        <div class="SSSlide clip_frame grpelem" id="u934"><!-- image -->
         <img class="block ImageInclude" id="u934_img" data-src="<?php echo base_url();?>images/Aprende.png" src="<?php echo base_url();?>images/blank.gif" alt="" data-width="235" data-height="235"/>
        </div>
        <div class="SSSlide invi clip_frame grpelem" id="u968"><!-- image -->
         <img class="block ImageInclude" id="u968_img" data-src="<?php echo base_url();?>images/Crea.png" src="<?php echo base_url();?>images/blank.gif" alt="" data-width="235" data-height="235"/>
        </div>
        
        <div class="SSSlide invi clip_frame grpelem" id="u986"><!-- image -->
         <img class="block ImageInclude" id="u986_img" data-src="<?php echo base_url();?>images/Juega.png" src="<?php echo base_url();?>images/blank.gif" alt="" data-width="235" data-height="235"/>
        </div>
       </div>
      </div>
      <div class="popup_anchor" id="u941popup">
       <div class="SlideShowCaptionPanel clearfix" id="u941"><!-- stack box -->
        <div class="SSSlideCaption texto-rokkit clearfix grpelem" id="u942-4"><!-- content -->
         <p>Learn</p>
        </div>
        <div class="SSSlideCaption invi texto-rokkit clearfix grpelem" id="u974-4"><!-- content -->
         <p>Create</p>
        </div>
        <div class="SSSlideCaption invi texto-rokkit clearfix grpelem" id="u992-4"><!-- content -->
         <p>Play</p>
        </div>
       </div>
      </div>
     </div>
     <a class="anchor_item grpelem" id="definicion"></a>
    </div>
    <div class="clearfix colelem" id="pu1389"><!-- group -->
     <div class="grpelem" id="u1389"><!-- animation -->
      <iframe id="U2446_animation" src="<?php echo base_url();?>assets/brikoNew/brikoNew.html" scrolling="no" class="animationContainer an_invi"></iframe>
     </div>
     <img class="ose_pre_init grpelem" id="u1422" src="<?php echo base_url();?>images/usb.svg" width="328" height="716" alt="" data-mu-svgfallback="images/usb_poster_.png"/><!-- svg -->
    </div>
    <div class="clearfix colelem" id="pu1427"><!-- group -->
     <div class="mse_pre_init" id="u1427"><!-- simple frame --></div>
     <div class="clearfix mse_pre_init" id="u1445"><!-- group -->
      <div class="texto-rokkit clearfix grpelem" id="u1438-4"><!-- content -->
       <p>Code</p>
      </div>
      <div class="subtitulos-ubuntu clearfix grpelem" id="u1439-4"><!-- content -->
       <h2>2</h2>
      </div>
      <div class="texto-rokkit-anim clearfix grpelem" id="u1441-6"><!-- content -->
       <p>briko use it's own software.</p>
       
      </div>
      <div class="texto-ubuntu-anim clearfix grpelem" id="u1442-4"><!-- content -->
       <p>All you have to do is write a few lines of code and you are set!</p>
      </div>
     </div>
    </div>
    <div class="clearfix mse_pre_init" id="u1446"><!-- group -->
     <div class="subtitulos-ubuntu clearfix grpelem" id="u1450-4"><!-- content -->
      <h2>3</h2>
     </div>
     <div class="clearfix grpelem" id="pu1449-4"><!-- group -->
      <div class="texto-rokkit clearfix grpelem" id="u1449-4"><!-- content -->
       <p>Play</p>
      </div>
      <div class="texto-ubuntu-anim clearfix grpelem" id="u1448-4"><!-- content -->
       <p>Have fun and enjoy!</p>
      </div>
     </div>
    </div>
    <img class="mse_pre_init" id="u1456" src="<?php echo base_url();?>images/CarritoNuevo.png" width="560" height="335" alt="" data-mu-svgfallback="images/brikocar_poster_.png"/><!-- svg -->
    <div class="clearfix colelem" id="pu1395"><!-- group -->
     <div class="browser_width grpelem" id="u1395-bw">
      <div id="u1395"><!-- group -->
       <div class="clearfix" id="u1395_align_to_page">
        <div class="grpelem" id="u598"><!-- simple frame --></div>
       </div>
      </div>
     </div>
     
    </div>
    <div class="clearfix colelem" id="ppaprende"><!-- group -->
     <div class="clearfix grpelem" id="paprende"><!-- column -->
      <a class="anchor_item colelem" id="aprende"></a>
      <a class="anchor_item colelem" id="kits"></a>
      <a class="anchor_item colelem" id="contacto"></a>
     </div>
     <div class="browser_width grpelem" id="u480-bw">
      <div class="museBGSize" id="u480" style="height:13%!important"><!-- column -->
       <div class="clearfix" id="u480_align_to_page" style="left:-430px!important;width:486px!important;">
        <div class="clearfix colelem" id="pu481-8"style="left:7px!important"><!-- group -->
         <div class="titulo-ubuntu clearfix grpelem" id="u481-8" style="left: 20em; top: 1em;"><!-- content -->
          <h1>Premium</h1>
          <h1>Kit</h1>
          
         </div>
         <div class="titulo-ubuntu clearfix grpelem" id="u481-8" style="left: 11em; top: 1em;"><!-- content -->
          <h1>Robot</h1>
          <h1>Kit</h1>
          
         </div>
         <div class="titulo-ubuntu clearfix grpelem" id="u481-8" style="left: 2em; top: 1em;"><!-- content -->
          <h1>Starter</h1>
          <h1>Kit</h1>
          
         </div>
         
         <div class="texto-rokkit clearfix grpelem" id="u483-8" style="left:0px!important"><!-- content -->
          <img src="<?php echo base_url();?>images/Starter.png" style="position: relative; left: -1em;  top: 6.9em;">
          <img src="<?php echo base_url();?>images/Robot.png" style="position: relative; left: 22em; top: -16em;">
          <img src="<?php echo base_url();?>images/Premium1.png" style="position: relative; left: 44em; top: -39.5em;">
         </div>
        </div>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" id="formStarter">
          <input type="hidden" name="cmd" value="_s-xclick">
          <input type="hidden" name="hosted_button_id" value="EB6CHXWD5QHFC">
        <a  class="submitStarter"><!-- container box -->
        <img src="<?php echo base_url();?>images/SCartB-120.png" class="submitStarter" style="cursor: pointer; z-index:1000;margin-top: 39em; left: 13em ! important; position: relative; top: 4em;">
      </a>

        </form>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" id="formRobot">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="2FVZPN6ZHJ42N">
        <a  class="submitRobot" >
          <!-- container box -->
          <img src="<?php echo base_url();?>images/SCartB-120.png" class="submitRobot" style="cursor: pointer; left: 41.5em ! important; position: relative; top: -4.7em;">
        </a>
        </form>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" id="formPrem">
          <input type="hidden" name="cmd" value="_s-xclick">
          <input type="hidden" name="hosted_button_id" value="JVPTE9BBYQZHE">
        <a  class="submitPrem" ><!-- container box -->
          <img src="<?php echo base_url();?>images/SCartB-120.png"  class="submitPrem" style="cursor: pointer; left: 72.5em ! important; position: relative; top: -13.6em;">
        </a>
      </form>
       </div>
       
       
      </div>

     </div>
     <div class="browser_width grpelem" id="u421-bw">

      
        
      <div class="museBGSize" id="u421"><!-- group -->
       <div class="clearfix" id="u421_align_to_page">
        <div class="clearfix grpelem" id="u1075"><!-- column -->
         <div class="position_content" id="u1075_position_content">
          <div class="titulo-ubuntu clearfix colelem" id="u429-6"><!-- content -->
           <h1>¡Discover proyects!</h1>
          </div>
          <div class="clearfix colelem" id="pu434"><!-- group -->
           <a style="left: 310px ! important; margin-top: 180px ! important;" class="nonblock nontext Button anim_swing shadow rounded-corners clearfix colelem" id="buttonu1490" href="http://briko.cc/index.php/proyectos"><!-- container box --><div class="texto-rokkit clearfix grpelem" id="u1491-6"><!-- content --><p>Discover more</p><p>projects</p></div></a>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="browser_width grpelem" id="u1076-bw">
      <div id="u1076" style="left: -60px; margin-top: 6em;"><!-- column -->
        
       
      </div>
     </div>
     
    </div>
    <div class="clearfix colelem" id="pu1492"><!-- group -->
     <div class="browser_width grpelem" id="u1492-bw">
      <div id="u1492"><!-- group -->
       <div class="clearfix" id="u1492_align_to_page">
        <div class="subtitulos-ubuntu clearfix grpelem" id="u504-6"><!-- content -->
         <h2>Would you like to know more about BriKo?</h2>
         <h2>Send us your e-mail address and we will contact you :)</h2>
        </div>
        <div class="subtitulos-ubuntu clearfix grpelem" id="u2556-6"><!-- content -->
         <h2>Or write us:</h2>
         <h2>contacto@briko.cc</h2>
        </div>
       </div>
      </div>
     </div>
     <form class="form-grp clearfix grpelem" id="widgetu2536" method="post" enctype="multipart/form-data" action="scripts/form-u2536.php"><!-- none box -->
      <div class="fld-grp clearfix grpelem" id="widgetu2538" data-required="true" data-type="email"><!-- none box -->
       <span class="fld-input NoWrap actAsDiv rounded-corners subtitulos-ubuntu clearfix grpelem" id="u2539-4"><!-- content --><input class="wrapped-input" type="text" spellcheck="false" id="widgetu2538_input" name="Email" tabindex="1"/><label class="wrapped-input fld-prompt" id="widgetu2538_prompt" for="widgetu2538_input"><span class="actAsPara">E&#45;mail</span></label></span>
      </div>
      <div class="links clearfix grpelem" id="u2537-4"><!-- content -->
       <p>Sending mail...</p>
      </div>
      <div class="links clearfix grpelem" id="u2544-4"><!-- content -->
       <p>Server has detected an error.</p>
      </div>
      <div class="links clearfix grpelem" id="u2543-4"><!-- content -->
       <p>Mail recieved.</p>
      </div>
      <input class="submit-btn NoWrap grpelem" id="u2542-17" type="submit" value="" tabindex="2"/><!-- state-based BG images -->
     </form>
    </div>
    <div class="clearfix colelem" id="pu225"><!-- group -->
     <div class="browser_width grpelem" id="u225-bw" style="height: 110px!important;">
      <div id="u225"><!-- group -->
       <div class="clearfix" id="u225_align_to_page">
        <div class="texto-rokkit clearfix grpelem" id="u232-4"><!-- content -->
         <p>Follow us:</p>
        </div>
       </div>
      </div>
     </div>
     <a class="nonblock nontext grpelem" id="u233" href="https://www.facebook.com/BrikoEN" target="_blank"><!-- state-based BG images --></a>
     <a class="nonblock nontext grpelem" id="u240" href="http://twitter.com/BrikoEN" target="_blank"><!-- state-based BG images --></a>
     <a class="nonblock nontext grpelem" id="u241" href="https://www.youtube.com/channel/UC6oWg8Ehc9K1qmWTHcvoTIw" target="_blank"><!-- state-based BG images --></a>
    </div>
    <div class="browser_width colelem" id="u254-bw">
     <div id="u254"><!-- group -->
      <div class="clearfix" id="u254_align_to_page">
       <div class="links clearfix grpelem" id="u264-10"><!-- content -->
        <p><span><a class="nonblock" href="politica.html">
Privacy Policy</a></span> | Powered by <span><a class="nonblock">briko</a></span></p>
       </div>
       <div class="grpelem" id="u538"><!-- custom html -->
        <div style="text-align:right;">
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a>
</div>
</div>
      </div>
     </div>
    </div>
   </div>
  </div>
  <div class="preload_images">
   <img class="preload" src="<?php echo base_url();?>images/boton-venta-on.png" alt=""/>
   <img class="preload" src="<?php echo base_url();?>images/u2542-17-r.png" alt=""/>
   <img class="preload" src="<?php echo base_url();?>images/u2542-17-m.png" alt=""/>
   <img class="preload" src="<?php echo base_url();?>images/u2542-17-fs.png" alt=""/>
   <img class="preload" src="<?php echo base_url();?>images/icono-facebook-u233-r.png" alt=""/>
   <img class="preload" src="<?php echo base_url();?>images/icono-twitter-u240-r.png" alt=""/>
   <img class="preload" src="<?php echo base_url();?>images/icono-gplus-u241-r.png" alt=""/>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
    $(document).ready(function(){
    $(".submitStarter").click(function(){
       $("#formStarter").submit();
    });
    $(".submitRobot").click(function(){
       $("#formRobot").submit();
    });
    $(".submitPrem").click(function(){
       $("#formPrem").submit();
    });
  });
</script>

  <script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn2.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="<?php echo base_url();?>scripts/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script src="<?php echo base_url();?>scripts/museutils.js?3777594392" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/jquery.watch.js?4144919381" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/jquery.musepolyfill.bgsize.js?4259541792" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/webpro.js?474087315" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/musewpslideshow.js?274584885" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/jquery.museoverlay.js?170414611" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/touchswipe.js?100683906" type="text/javascript"></script>
  <script src="<?php echo base_url();?>scripts/jquery.scrolleffects.js?117544926" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
   $(document).ready(function() { try {
(function(){var a={},b=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),16);return 0};(function(){$('link[type="text/css"]').each(function(){var b=($(this).attr("href")||"").match(/\/?css\/([\w\-]+\.css)\?(\d+)/);b&&b[1]&&b[2]&&(a[b[1]]=b[2])})})();(function(){$("body").append('<div class="version" style="display:none; width:1px; height:1px;"></div>');
for(var c=$(".version"),d=0;d<Muse.assets.required.length;){var f=Muse.assets.required[d],g=f.match(/([\w\-\.]+)\.(\w+)$/),k=g&&g[1]?g[1]:null,g=g&&g[2]?g[2]:null;switch(g.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");c.addClass(k);var g=b(c.css("color")),h=b(c.css("background-color"));g!=0||h!=0?(Muse.assets.required.splice(d,1),"undefined"!=typeof a[f]&&(g!=a[f]>>>24||h!=(a[f]&16777215))&&Muse.assets.outOfDate.push(f)):d++;c.removeClass(k);break;case "js":k.match(/^jquery-[\d\.]+/gi)&&
typeof $!="undefined"?Muse.assets.required.splice(d,1):d++;break;default:throw Error("Unsupported file type: "+g);}}c.remove();if(Muse.assets.outOfDate.length||Muse.assets.required.length)c="Puede que determinados archivos falten en el servidor o sean incorrectos. Limpie la cache del navegador e inténtelo de nuevo. Si el problema persiste, póngase en contacto con el administrador del sitio web.",(d=location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi))&&Muse.assets.outOfDate.length&&(c+="\nOut of date: "+Muse.assets.outOfDate.join(",")),d&&Muse.assets.required.length&&(c+="\nMissing: "+Muse.assets.required.join(",")),alert(c)})()})();
/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight()/* resize height */
Muse.Utils.initWidget('#slideshowu932', function(elem) { $(elem).data('widget', new WebPro.Widget.ContentSlideShow(elem, {autoPlay:true,displayInterval:1000,slideLinkStopsSlideShow:false,transitionStyle:'vertical',lightboxEnabled_runtime:false,shuffle:false,transitionDuration:500,enableSwipe:true,elastic:'off',resumeAutoplay:false,resumeAutoplayInterval:3000,playOnce:false})); });/* #slideshowu932 */
Muse.Utils.initializeAnimations();/* animations */
$('#u1389').registerEdgeAnimateScrollEffect([{"play":false,"in":[-Infinity,949]},{"play":true,"in":[949,Infinity]}]);/* scroll effect */
$('#u1422').registerOpacityScrollEffect([{"fade":249,"in":[-Infinity,1663.6],"opacity":50},{"in":[1663.6,1663.6],"opacity":100},{"fade":65,"in":[1663.6,Infinity],"opacity":100}]);/* scroll effect */
$('#u1427').registerPositionScrollEffect([{"in":[-Infinity,1895.6],"speed":[1,1]},{"in":[1895.6,Infinity],"speed":[0,1]}]);/* scroll effect */
$('#u1445').registerPositionScrollEffect([{"in":[-Infinity,1896.6],"speed":[-1,1]},{"in":[1896.6,Infinity],"speed":[0,1]}]);/* scroll effect */
$('#u1446').registerPositionScrollEffect([{"in":[-Infinity,2431.25],"speed":[1,1]},{"in":[2431.25,Infinity],"speed":[0,1]}]);/* scroll effect */
$('#u1456').registerPositionScrollEffect([{"in":[-Infinity,2465.6],"speed":[-1,1]},{"in":[2465.6,Infinity],"speed":[0,1]}]);/* scroll effect */
Muse.Utils.initWidget('#pamphletu1143', function(elem) { new WebPro.Widget.ContentSlideShow(elem, {contentLayout_runtime:'loose',event:'click',deactivationEvent:'none',autoPlay:false,displayInterval:3000,transitionStyle:'fading',transitionDuration:500,hideAllContentsFirst:false,shuffle:false,enableSwipe:true,resumeAutoplay:true,resumeAutoplayInterval:3000,playOnce:false}); });/* #pamphletu1143 */
Muse.Utils.initWidget('#widgetu2536', function(elem) { new WebPro.Widget.Form(elem, {validationEvent:'submit',errorStateSensitivity:'high',fieldWrapperClass:'fld-grp',formSubmittedClass:'frm-sub-st',formErrorClass:'frm-subm-err-st',formDeliveredClass:'frm-subm-ok-st',notEmptyClass:'non-empty-st',focusClass:'focus-st',invalidClass:'fld-err-st',requiredClass:'fld-err-st',ajaxSubmit:true}); });/* #widgetu2536 */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
} catch(e) { if (e && 'function' == typeof e.notify) e.notify(); else Muse.Assert.fail('Error calling selector function:' + e); }});
</script>

   </body>
</html>