<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/comienzaahora.css">

<div class="row"  id="div1">
  <div class="large-12  columns">
      
    <!-- titulo principal -->
    <div class="row">
        <div class="large-12 columns" style="text-align:center">
            <h1 class = "h1classbk">Sigue los siguientes pasos y crea tu primer programa: </h1>
        </div>
    </div>
    <!-- Arreglo para mensajes del slideshow -->
    <?php $pasos = array("Paso 1: Conoce brikode.","Paso 2: Qué hace cada botón?","Paso 3: Conecta tu briko a la computadora.","Paso 4: Conecta tu módulo de leds en el puerto 1 de tu bk7.","Paso 5: Una vez abierto brikode nos vamos a Herramientas->Puerto y checamos que esté seleccionado el puerto correcto, este debe decir algo como COM12(Briko).","Paso 6: Ahí mismo en Herramientas->Placa verificamos que esté seleccionado Briko."," Paso 7: Hay que verificar que brikode esté funcionando, presionamos el botón  de subir, y esperamos a que termine de cargar, al finalizar debe salir el mensaje de Subido.","Paso 8: Ahora que todo esta funcionando comenzaremos con nuestro primer programa, lo primero que hay que hacer es declarar el módulo de leds.","Paso 9: Para declarar un módulo de leds nos vamos arriba de la palabra code() y escribimos:ledsbk nombre(PORT1); donde ledsbk es el tipo de módulo a utilizar, nombre es el nombre con el que vamos a estar usando nuestro modulo y PORT1 es el puerto donde conectamos nuestro módulo.", "Paso 10: Ahora que ya declaramos nuestro módulo, escribiremos lo que deseamos que haga nuestro módulo, esto va dentro de code(). ", "Paso 11: Para hacer que nuestro módulo de led prenda utilizaremos la función color de la siguiente manera: nombre.color(RED); con la cual nuestro modulo de leds prendera sus 5 leds de color rojo.","Paso 12: De la misma manera podemos apagar nuestro leds poniendolos de color negro: nombre.color(BLACK);","Paso 13: Para poder hacer que nuestros leds parpaden, hay que utilizar la función delay(milisegundos); con la cual detenemos nuestro programa por un cierto tiempo.","Paso 14: Ahora unimos todo de manera que haremos que prendan nuestros leds de color rojo, se esperen 1 segundo, y después se apaguen por otro segundo y así indefinidamente.","Paso 15: Felicidades has creado tu primer programa con briko! Ahora puedes pasar a nuestra sección de módulos para conocer más funciones de briko, así como a nuestra sección de piezas mecánicas para saber como armar más proyectos."); ?>
      
    <!-- slide show -->
    <div class="row" >
      <div class="large-12 columns" >
        <ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;pause_on_hover:true;timer: false;slide_number: false;navigation_arrows:true;"  >
          <?php for($i = 0; $i< count($pasos); $i++) { ?>    
          <?php if($i !=1){ ?>  
          <center><li> 
            <img alt="Paso <?php echo $i+1; ?> para la instalacion" src="<?php echo base_url(); ?>images/primerprograma/paso<?php echo $i+1; ?>.png" alt="slide<?php echo $i; ?>"  />
            <div class="orbit-caption">
              <?php echo $pasos[$i]; ?>
            </div>
          </li></center>  
          <?php }else{ ?>  
          <center><li class="active"> 
            <img alt="Paso <?php echo $i+1; ?> para la instalacion" src="<?php echo base_url(); ?>images/primerprograma/paso<?php echo $i+1; ?>.png" alt="slide<?php echo $i; ?>"/>
            <div class="orbit-caption">
              <?php echo $pasos[$i];  ?>
            </div>
          </li></center>  
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </div>
    </br>

    <!-- Segundo titulo -->
    <div class="row">
      <div class="large-12 columns" style="text-align:center">
         <h1 class = "h1classbk">Conoce más de como programar con briko: </h1>
      </div>
    </div>

    <!-- Botones para mandar a otras paginas -->
    <div class="row">
      <div class="small-12 columns" style="text-align:center">
        <button class= "button round comA" id="Aprendeb">Aprende a programar</button>
      </div>
    </div>

  <!-- cierra el div principal -->
  </div>
 <br> <!-- agrega un renglon de separacio -->
 <br>
</div>

<!--crea un mensaje de alerta tipo modal-->
<div id="first-modal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h2 id="modalTitle">Ups.</h2>
      <p class="lead">Hay un problema con este link.</p>
      <p>Este link no esta disponible por el momento pero nuestro equipo esta trabajando en ello, agradecemos tu comprensión.</p>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>

<!-- script para los listeners-->
<script>

var url_def = "<?php echo base_url(); ?>index.php/";
    
$("#Aprendeb").on("click",function(){  //redirige a otra pagina
    window.open(url_def+"aprende","_self");  //cambia la ventana
});

</script>

<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<script>
  $(document).foundation();

  var doc = document.documentElement;
  doc.setAttribute('data-useragent', navigator.userAgent);
</script>

  </body>
</html>