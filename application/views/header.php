
<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="es" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<meta name="author" content="briko" />
		<meta name="description" content="Kit para aprender electr&oacute;nica robot&iacute;ca y programaci&oacute;n de manera fac&iacute;l y divertida" />
		<meta name="keywords" content="Robotica, Electronica, Programacion" />
		<meta name="googlebot" content="noarchive" />

		<link href="https://plus.google.com/112509929810418783761/about" rel="publisher" />
		<link rel="shortcut icon" href="<?php echo base_url();?>images/brikon.ico?521017215"/>
		<title>briko: Robot&iacute;ca y programaci&oacute;n para todos</title>
		<link rel="stylesheet" href="<?php echo base_url();?>css/foundation.css"/>
		<script src="<?php echo base_url();?>js/vendor/modernizr.js"></script>
		<link href="<?php echo base_url();?>css/prettify.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/default.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/component.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/proyectoL.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/home.css" />
		<link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url();?>css/foundation-datepicker.min.css">
		
		
		
		<!--<script src="http://code.jquery.com/jquery-latest.js"></script>-->
		<script src="<?php echo base_url();?>js/waypoints/lib/noframework.waypoints.min.js"></script>
		<script src="<?php echo base_url();?>js/vendor/jquery.js"></script>
		<script src="http://www.google-analytics.com/ga.js"></script>
		<script src="<?php echo base_url();?>js/foundation.min.js"></script>
		<script src="<?php echo base_url();?>js/foundation/foundation.equalizer.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>/scripts/prettify.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/scripts/prettify.js"></script>
        <link rel="stylesheet" href="<?php echo base_url();?>css/headerspecial.css">
        <script type="text/javascript" src="<?php echo base_url();?>/js/myheader/init.js"></script>
	</head>
	<body class="cbp-spmenu-push" onload="prettyPrint()">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->

		<?php $this->load->helper('form');?>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite",
  "name" : "briko - Robotica y programacion para todos",
  "alternateName" : "briko - Robotica y programacion para todos",
  "url" : "http://briko.cc"
}
</script>
         
<!-- Scripts para hacer el loading de la pagina -->
<script>
    paceOptions = {
    initialRate:0.7,
    minTime:1500,
    maxProgressPerFrame:5,
}
  </script>
<script src="<?php echo base_url(); ?>js/pace/pace.js" async></script>
<link href="<?php echo base_url(); ?>js/pace/themes/purple/pace-theme-briko.css" rel="stylesheet" />
<script data-pace-options='{ "ajax": false }' src='<?php echo base_url(); ?>js/pace/pace.js' async></script>
<link href="<?php echo base_url(); ?>css/pacehidepage.css" rel="stylesheet" />
<script>
    //para ignorar cierto urls que salga el de cargando.
    paceOptions = {
        ajax: {ignoreURLs: ['proyectos/proyectoinfo/$1','index.php/eliminarP/$1']}
    }
</script>
        
        
		<div class="row headerprincipal">

	        <div class="large-12 columns" style="padding-bottom: 0px;">
	        	
	          <div class="row">
	            <div class="large-12 columns contain-to-grid fixed" id="u72">
	     
	              <nav class="top-bar important-class" data-topbar role="navigation">
	                <ul class="title-area wrap" >
	                   
	                  <li class="name brand">
	                    <a class="nonblock nontext" href="http://briko.cc"><img alt="Logo briko link pagina principal" src="<?php echo base_url();?>images/LogoBriko250.png" ><!-- simple frame --></a>
	                  </li>
                      <li class="toggle-topbar menu-icon">
                          <a id="menuc" href="#">Menu</a>
                        </li>
	                  
	                </ul>
	             
	                <section class="top-bar-section" style="float: right;">
	                   
	                  <ul class="left wrap2">
	                    <li class="brand has-dropdown">
	                    	<?php if (!$this->ion_auth->logged_in()){ ?>
	                      <a href="#" class="asp2">Comunidad</a>
	                      <?php }else{?>
	                      <a href="#" class="asp2"><?php echo $usuario['nombre']?></a>
	                      <?php }?>
	                      <ul class="dropdown">
	                      	<li><a class="menud asp1"  href="<?php echo base_url();?>blog">Blog</a></li>
	                      	<?php if (!$this->ion_auth->logged_in()){ ?>
                            <li><a 	class="menud asp1" data-reveal-id="Entrar" href="#" style="padding-right: 105px!important;">Entrar</a></li>
                            <li><a class="menud asp1" data-reveal-id="Registro" href="#">Registrarse</a></li>
                            <?php }else { ?>
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>index.php/misproyectos" style="padding-right: 105px!important;">Mis proyectos</a></li>
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>index.php/auth/logout" style="padding-right: 105px!important;">Salir</a></li>
                            <?php } ?>
                          </ul>	
	                    </li> 
                          <li class="divider"></li>
	                    <li class="brand">
	                      <a class="asp2" href="<?php echo base_url();?>eventos">Eventos</a>	                      
	                    </li>
                          <li class="divider"></li>
	                    <li class="brand" >
	                      <a class="asp2 comparBu" style="background-color: #3FB1E5;">Comprar</a>
	                    </li>
                        <li class="divider"></li>
	                    <li class="brand">
	                    	<a class="asp2" href="<?php echo base_url();?>proyectos">Proyectos</a>
	                   	</li>
	                    <li class="divider"></li>
	                    <li  class="has-dropdown brand">
	                      <a class="asp2" href="#">Referencias</a>
	                      <ul class="dropdown">
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>referencia" style="padding-right: 47px!important;">Referencia</a></li>
                            <li><a class="menud asp1"  href="<?php echo base_url();?>brikos">Brikos</a></li>
                          </ul>	                   
	                    </li>
                        <li class="divider"></li>
	                    <li class="has-dropdown brand">
	                      <a class="asp2" href="#">Comienza Ahora</a>
	                      <ul class="dropdown">
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>instalacion">Instalaci&oacute;n</a></li>
                            <li><a 	class="menud asp1" href="<?php echo base_url();?>blockscode3">Brikoblocks</a></li>
                            <li><a class="menud asp1" href="<?php echo base_url();?>primer-programa">Mi primer programa</a></li>
                            <li><a class="menud asp1" href="<?php echo base_url();?>aprende" style="padding-right: 30px!important;">Aprende</a></li>
                          </ul>	                      
	                    </li>
                        <li class="divider"></li>
	                  </ul>
	                </section>
	              </nav>
	            </div>
	          </div>  
          </div>
          </div>
       
        

<div id="Entrar" class="reveal-modal tiny" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<?php 
		$identity = array('name' => 'identity',
			'id'    => 'identity',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('identity'),
		);
		$password= array('name' => 'password',
			'id'   => 'password',
			'type' => 'password',
		);
		$name = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'value' => $this->form_validation->set_value('name'),
	    );
	    $username = array(
	        'name'  => 'username',
	        'id'    => 'username',
	        'type'  => 'text',
	        'pattern' => '^[a-zA-Z0-9]*$',
	        'placeholder' => 'brunoBot',
	        'value' => $this->form_validation->set_value('username'),
	    );
	    $identity = array(
	        'name'  => 'identity',
	        'id'    => 'identity',
	        'type'  => 'text',
	        'value' => $this->form_validation->set_value('identity'),
	    );
	    $email = array(
	        'name'  => 'email',
	        'id'    => 'email',
	        'type'  => 'text',
	        'value' => $this->form_validation->set_value('email'),
	    );
	    $company = array(
	        'name'  => 'company',
	        'id'    => 'company',
	        'type'  => 'text',
	        'value' => $this->form_validation->set_value('company'),
	    );
	    $phone = array(
	        'name'  => 'phone',
	        'id'    => 'phone',
	        'type'  => 'text',
	        'value' => $this->form_validation->set_value('phone'),
	    );
	    $passwordr = array(
	        'name'  => 'password',
	        'id'    => 'password',
	        'type'  => 'password',
	        'value' => $this->form_validation->set_value('password'),
	    );
	    $password_confirm = array(
	        'name'  => 'password_confirm',
	        'id'    => 'password_confirm',
	        'type'  => 'password',
	        'value' => $this->form_validation->set_value('password_confirm'),
	    );
	?>
	<?php echo validation_errors(); ?>
	<?php echo $usuario['message'];?>
	<?php echo form_open("auth/login");?>
		<div class="row">
			<div class="colums large-12">
				<h2 class="text-center modalInicio">Inicio de sesión</h2>
			</div>
		</div>
		<div class="row">
			
			<div class="columns large-12">
				<div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Usuario</span>
			        </div>
			        <div class="small-8 columns">
			          	<?php echo form_input($identity);?>
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<span class="tagInputReg prefix">Contrase&ntilde;a</span>
			        </div>
			        <div class="small-8 columns">
			          	<?php echo form_input($password);?>
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-4 columns">
			          	<p>
						    <?php echo lang('login_remember_label', 'remember');?>
						    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
						  </p>
			        </div>
			    </div>
			    
			</div>
		</div>
		<div class="row collapse">
			<div class="colums large-6 large-offset-4">
				<p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>
			</div>
		</div>
		<div class="row collapse prefix-radius">
	        <div class="small-12 columns" style="text-align:center;">
	          	<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
	        </div>
	    </div>
	<?php echo form_close();?>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="Registro" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<?php echo validation_errors(); 
	$attr = array('id' => 'formRegistro');?>
	<?php echo form_open("auth/create_user");?>
		<div class="row">
			<div class="colums large-12">
				<h2 class=" text-center modalInicio">Reg&iacute;strate</h2>
				<p style="align:center;">Para ser de la comunidad briko y compartir tus proyectos briko</p>
			</div>
		</div>
		<!--<div class="row">
			<div class="colums large-12">
				<h4 id="modalTitle">Registrate con tu cuenta de:<img style="cursor:pointer;" id="fbI" src="<?php //echo base_url();?>images/fb.png" >	</h4>
			</div>
		</div>-->
		<div class="row">
			<div class="columns large-12">
			    <div class="row collapse prefix-radius">
			        <span class="label desKY">G&eacute;nero</span><br/>
	      			<input type="radio" name="genero" value="H" id="Mas"><label for="Mas">Masculino</label>
	      			<input type="radio" name="genero" value="M" id="Fem"><label for="Fem">Femenino</label>
	      			<input type="radio" name="genero" value="O" id="Ot" checked><label for="Ot">Otro</label>
			    </div>
			</div>
		</div>
		<div class="row">
			<div class="columns large-6 small-6">
				<div class="row collapse prefix-radius">
			        <div class="small-3 columns">
			          	<span class="tagInputReg prefix">Nombre</span>
			        </div>
			        <div class="small-9 columns">
			          	<?php echo form_input($name);?>
			        </div>
			    </div>
			</div>
			
			<div class="columns large-6 small-6">
			    <div class="row collapse prefix-radius">
			        <div class="small-5 columns">
			          	<span class="tagInputReg prefix">Naciste el</span>
			        </div>
			        <div class="small-7 columns">
			          	<input type="text" class="inputReg" name="edad" class="span2" value="10-01-2000" id="dp1">
			        </div>
			        
			    </div>
			</div>
		</div>
		<div class="row">
			<div class="columns large-6 small-6">
			    <div class="row collapse prefix-radius">
			        <div class="small-3 columns">
			          	<span class="tagInputReg prefix">Correo</span>
			        </div>
			        <div class="small-9 columns">
			        	<?php echo form_input($email);?>
			          	
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-12 columns">
			        	<span class="round  label" style="display:none;" id="val_mail"></span>
			          	
			        </div>
			        
			    </div>
			</div>
			<div class="columns large-6 small-6">
			    <div class="row collapse prefix-radius">
	      			<div class="small-4 columns">
			          	<span class="tagInputReg prefix">NickName</span>
			        </div>
			        <div class="small-8 columns">
			        	<?php echo form_input($username);?>
			          	
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
	      			<div class="small-12 columns" align="center">
			          	<span class="round  label" style="display:none;" id="able"></span>
			        </div>    
			    </div>
			    
			</div>
			
			
		</div>
		
		<div class="row">
			<div class="columns large-12">
			    <div class="row collapse prefix-radius">
	      			<div class="small-5 columns">
			          	<span class="tagInputReg prefix">Contrase&ntilde;a</span>
			        </div>
			        <div class="small-7 columns">
			          	<?php echo form_input($passwordr);?>
			        </div>
			    </div>
			    <div class="row collapse prefix-radius">
			        <div class="small-5 columns">
			          	<span class="tagInputReg prefix">Confirmar contrase&ntilde;a</span>
			        </div>
			        <div class="small-7 columns">
			          	<?php echo form_input($password_confirm);?>
			        </div>
		    	</div>
		    	<div class="row collapse prefix-radius">
			        <div class="small-6 columns">
			          	<span style="display:none;"  class="round alert label" id="passwd_match"></span>
			        </div>
			        <div class="small-6 columns">
			          	<span style="display:none;"  class="round alert label" id="strength_human">
        				<span style="display:none;" class="round alert label"	 id="strength_score"></span></span>
			        </div>
		    	</div>
			</div>
		</div>
		<div class="row collapse">
			<div class="colums large-6 large-offset-3">
				<input type="submit" class="round comK button" id="registro" value="Reg&iacute;strame!" />
				
			</div>
		</div>
	</form>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<div id="ModalAgradece" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<h3 id="tituloCompra">Muchas gracias por tu interés en nuestro producto.</h3>
	<img alt="Logo briko" src="http://briko.cc/images/Logo-300x100.png">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<div id="ModalCorreo" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">	
	<div class='row'>
		<div class='columns large-12'>
			<p id='textoCompra'>Estas siendo redirigido a <span>play business</span> en donde estamos vendiendo los kits en una campaña de Crowfounding</p>
			<p>¡Cualquier duda estamos a tus &oacute;rdenes en contacto@briko.cc!</p>
			
		</div>
	</div>
</div>
<script type="text/javascript">
		$('.comparBu').on('click', function() {
			$('#ModalCorreo').foundation('reveal','open');
			window.setTimeout(function(){window.open('https://playbusiness.mx/proyectos/briko-robotics','_self');}, 7000);
			//window.open('https://playbusiness.mx/proyectos/briko-robotics','_self');
		});
	</script>
<script src="<?php echo base_url();?>js/foundation-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('#nick').on('keyup keydown keypress',check_username);
    $('#mail').on('keyup keydown keypress',check_email);
    
});
function validateMail(mail) {
   
	    var atpos = mail.indexOf("@");
	    var dotpos = mail.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=mail.length)
	    {
	    	$( "#val_mail" ).addClass( "alert" );
	        return "E-mail no valido";
	    }
	    else
	   	{
	   		$( "#val_mail" ).removeClass( "alert" );
            return "E-mail valido"
        }
    }

    function check_username(){
        var base="<?php echo base_url();?>";
        var username = $('#nick').val();
        jQuery.ajax({
            type: 'POST',
            url: base+'index.php/usform/existe_us',
            data: 'nombre_tienda='+ username,
            cache: false,
            success: function(response){
                if(response == 0){
                	document.getElementById("able").style.display = "block";
                	$( "#able" ).removeClass( "alert" );
                 $('#able').text('Disponible');
                 
             }
             else {
             	document.getElementById("able").style.display = "block";
             	$( "#able" ).addClass( "alert" );
                $('#able').text('No disponible');
                
                
            }
        }
    });
    }

    function check_email(){
        var base="<?php echo base_url();?>";
        var usermail = $('#mail').val();
        jQuery.ajax({
            type: 'POST',
            url: base+'index.php/usform/existe_email',
            data: 'correo='+ usermail,
            cache: false,
            success: function(response){
                if(response == 0){
                 //document.getElementById('mai').value = '0';
                 document.getElementById("val_mail").style.display = "block";
                 $("#val_mail").text(validateMail(usermail));
             }
             else {
                //document.getElementById('mai').value = '1';
                document.getElementById("val_mail").style.display = "block";
                $( "#val_mail" ).addClass( "alert" );
                $('#val_mail').text('Ya existe el correo');
                
            }
        }
    });
    }
</script>
<script>
	$(function () {
	
		$('#dp1').fdatepicker({
			format: 'mm-dd-yyyy',
			disableDblClickSelection: true
		});
	});
	$("#registro").on("click",function(){  //abre pop
		    var pass1 = document.getElementById("passwd").value;
		    var pass2 = document.getElementById("passwd1").value;
		    var ok = true;
		    var ok1=true;
		    var ok2=true;
		    var ok3=true;
		    var regMail= $('#val_mail').text();
    		var regUsu= $('#able').text();
    		var sps=$('#strength_human').text();
    		//alert(regMail);
    		//alert(regUsu);
		    if (pass1 != pass2) {
		        //alert("Passwords Do not match");
		        document.getElementById("passwd_match").style.display = "block";
		        $("#passwd_match").text("Contraseñas no coinciden");
		        document.getElementById("passwd").style.borderColor = "#E34234";
		        document.getElementById("passwd1").style.borderColor = "#E34234";
		        ok = false;
		    }
		    else
		    {
		    	document.getElementById("passwd_match").style.display = "none";
		    	document.getElementById("passwd").style.borderColor = "#cccccc";
		        document.getElementById("passwd1").style.borderColor = "#cccccc";
		    	ok = true;
		    }
		    
	        if(regMail.localeCompare('Ya existe el correo')==0||regMail.localeCompare('E-mail no valido')==0)
			{
				document.getElementById("mail").style.borderColor = "#E34234";
				ok1 = false;
			}
			else
			{
				document.getElementById("val_mail").style.display = "none";
				document.getElementById("mail").style.borderColor = "#cccccc";
				ok1=true;
			}

			if(regUsu.localeCompare('No disponible')==0)
			{
				document.getElementById("nick").style.borderColor = "#E34234";
				$( "#able" ).addClass( "alert" );
				ok2= false;
			}
			else
			{
				document.getElementById("able").style.display = "none";
				document.getElementById("nick").style.borderColor = "#cccccc";
				ok2=true;
			}

			if(sps.localeCompare('Débil')==0||sps.localeCompare('Contraseña muy débil')==0)
			{
				$('#strength_human').text('Contraseña muy débil');
				ok3= false;
			}
			else
			{
				ok3=true;
			}

			if(ok==true&&ok1==true&&ok2==true&&ok3==true)
			{
				//alert("paso");
				return true;
			}
			else
			{
				return false;
			}
			   // alert(ok);
			    
		}); 
</script>
<script type="text/javascript">


function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up pattern="[a-zA-Z0-9]+"
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80){
        $( "#strength_human" ).removeClass( "warning alert" ).addClass( "success" );
        $( "#strength_score" ).removeClass( "warning alert" ).addClass( "success" );
        return "Fuerte";
    }
    if (score > 60){
    	$( "#strength_human" ).removeClass( "alert success" ).addClass( "warning" );
    	$( "#strength_score" ).removeClass( "alert success" ).addClass( "warning" );
        return "Buena";
    }
    if (score >= 30){
    	$( "#strength_human" ).removeClass( "warning success" ).addClass( "alert" );
    	$( "#strength_score" ).removeClass( "warning success" ).addClass( "alert" );
        return "Débil";}
    document.getElementById("strength_human").style.display = "block";
    document.getElementById("strength_score").style.display = "block";
    return "Débil";
}

$(document).ready(function() {
    $("#passwd").on("keypress keyup keydown", function() {
        var pass = $(this).val();
        $("#strength_human").text(checkPassStrength(pass));
        $("#strength_score").text(scorePassword(pass));
    });
});
$(document).ready(function(){
	var regMail;
	var banderaReg=false;
	var regUsu;
    $("#formRegistro").submit(function(){

		
		

    });
});
</script>
<script type="text/javascript">
			$(document).ready(function() {
				var base="<?php echo base_url();?>";
		    	var mailCompra;
		    	var mensajeCompra;
		    	var banderaCompra=false;
		    	var banderaCompra2=false;
		    	var banderaCompraN=false;
		    	var perfil;
		    	$('#CompraEmail').on('keyup keydown',function(){
		    		mailCompra= $('#CompraEmail').val();
		    		mensajeCompra=validateMail(mailCompra);
		    		if(mensajeCompra.localeCompare('E-mail valido')==0)
		    		{
		    			banderaCompra=true;
		    			document.getElementById("compraVal").style.display = "none";
		    		}
		    		else
		    		{
		    			banderaCompra=false;
		    			document.getElementById("compraVal").style.display = "block";	
		    		}
		    		$("#compraVal").text(mensajeCompra);
		    	});

		    	$('#CompraNombre').on('keyup keydown',function(){
		    		nombreCompra= $('#CompraNombre').val();
		    		if(nombreCompra.localeCompare('')==0||nombreCompra.localeCompare(' ')==0)
		    		{
		    			banderaCompraN=false;
		    			document.getElementById("compraValN").style.display = "block";
		    			$("#compraValN").text("Ingresa un Nombre Vàlido");
		    		}
		    		else
		    		{
		    			banderaCompraN=true;
		    			document.getElementById("compraValN").style.display = "none";	
		    		}
		    		
		    	});

		    	$( "#submitCOmpra" ).click(function()
		    	{
		    		perfil=$('#perfil').val();
		    		if(perfil.localeCompare('-')==0)
		    		{
		    			document.getElementById("compraValP").style.display = "block";
		    			$("#compraValP").text("Selecciona un Perfil");	
		    			banderaCompra2=false;
		    			return false;
		    		}
		    		else
		    		{	
		    			document.getElementById("compraValP").style.display = "none";	
		    			banderaCompra2=true;
		    		}
		    		//console.log(perfil);
		    		if(banderaCompra==true&&banderaCompra2==true&&banderaCompraN==true)
		    		{

		    			var nombreCompra=$('#CompraNombre').val();
						document.getElementById("submitCOmpra").disabled = true;
		    			jQuery.ajax(
		    			{
				            type: 'POST',
				            url: base+'index.php/usform/mailCompra',
				            data: {Correo : nombreCompra ,Nombre :mailCompra,perfil:perfil },
				            cache: false,

				            success: function(response)
				            {
				            					         
				                if(response == true)
				                {

				                 	$('#ModalAgradece').foundation('reveal', 'open');
				                 	document.getElementById("submitCOmpra").disabled = false;
				             	}
					            else
					            {
					                $('#compraVal').text('Ya existe el correo');
					            }
					        }
					    });
		    		}
		    		else
		    		{
		    			return false;
		    		}
				  	
				});
		    
			});
		</script>