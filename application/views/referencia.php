<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/referencia.css">

<!--Aqui estan todas las variables y textos de la pagina-->
<?php 

$Prin_tit = "Referencias de briko:";

///columna de la izquierda
$Paginas_links = array("briko-led","briko-bocina","briko-knob","briko-distancia","briko-temperatura","briko-maestro","briko-extra");
$Titulos_L = array("Briko de Leds:","Briko de Bocina:","Briko de Perilla:","Brikode Distancia:","Briko de Temperatura:","Controlador bk7 teclado:","Funciones adicionales:");
$ID_L = array("led","bocina","perilla","distancia","temperatura","maestro2","extra");
$Fun_p_mod = array(2,4,1,1,1,7,3);  //numero de funciones diferentes por modulo
$Fun_gen= array(".color",".brightness",   //funciones
                ".set",".beep",".playtone",".play",
                ".read",
                ".read",
                ".read",
               "bk7keywrite","bk7keyprint","bk7keypress","bk7keyrelease","bk7keyreleaseall","bk7keybuttons","bk7keyend",
               "map","random","randomSeed");
$Num_f_gen= array(4,1,  //veces que se repite cada funcion
                  1,1,2,1,
                  1,
                  2,
                  1,
                  1,1,2,1,1,2,1,
                  1,1,1);
$Param_gen = array("(<span class='param'> COLOR </span>);","(led,<span class='param'> COLOR </span>);","(r, g, b);" , "(led, r, g, b);", "(intensidad);",
                   "(<span class='param'> ESTADO </span>);","(tiempo_prendido, tiempo_apagado );","(<span class='param'> NOTA </span>);" , "(<span class='param'> NOTA </span>,tiempo);", "(<span class='param'> NOTA </span>,tiempo_prendido, tiempo_apagado);",
                   "( );",
                   "( );","(<span class='param'> UNIDAD </span>);",
                   "( );",
                   "( mensaje );","( mensaje );","(<span class='param'> TECLA </span>,tiempo);","(<span class='param'> TECLA </span>);" , "(<span class='param'> TECLA </span>);","( );","( numero, mensaje1, mensaje2, mensaje3, mensaje4, mensaje5 );", "( numero,<span class='param'> TECLA1 </span>,<span class='param'> TECLA2 </span>,<span class='param'> TECLA3 </span>,<span class='param'> TECLA4 </span>,<span class='param'> TECLA5 </span> ","( );",
                   "( variable, de_menor, de_mayor, a_menor, a_mayor );", "( min, max );", "( numero );"
                  );

///columna de la derecha
$Paginas2_links = array("briko-display","briko-boton","briko-luz","briko-motor","briko-maestro","briko-maestro","briko-relay","briko-servo");
$Titulos2_L = array("Briko de display:","Briko de Botones:","Sensor de luz","Briko de motor:","Controlador bk7 basicas:","Controlador bk7 mouse:","Briko de relevador:","Briko de servo:");
$ID2_L = array("display","boton","luz","motor","maestro1","maestro3","relay","servo");
$Fun2_p_mod = array(4,2,1,1,5,5,1,1); //numero de funciones diferentes por modulo
$Fun2_gen= array(".print",".printindividual",".printcustom",".erase", //funciones
                ".read",".readbits",
                ".read",
                ".set",
               "bk7led","bk7write","bk7print","bk7read","bk7clean",
               "bk7mouseclick","bk7mousemove","bk7mousepress","bk7mouserelease","bk7mouseend",
               ".set",
               ".set");
$Num2_f_gen= array(1,1,1,2, //veces que se repite cada funcion
                  2,1,
                  1,
                  2,
                  1,1,1,2,1,
                  1,1,1,1,1,
                  1,
                  1);
$Param2_gen = array("( numero );","( digito, numero );","( digito, segmentos);" , "( );", "( digito );",
                    "( );","( numero );","( );","( );", "(<span class='param'> DIRECCION </span>,velocidad );", "(<span class='param'> DIRECCION </span>);",
                   "(<span class='param'> ESTADO </span>);","( mensaje );","( mensaje );","( );" , "( &stringbk, numero );", "( );",
                   "(<span class='param'> KEY </span>);","( posicion_x, posicion_y );" ,"(<span class='param'> KEY </span>);","(<span class='param'> KEY </span>);","( );",
                   "(<span class='param'> ESTADO </span>);",
                   "( grados );",
                  );


?>

<!---------------------------------------------------------->

<!-- Creamos el grid principal-->
<div class="row"  id="divp">
  <div class="large-12 columns">
    
    <!-- Titulo -->
    <div class="row">
        <div class="small-12 columns">
            <center><h1 class="h1classbk2" ><?php echo $Prin_tit ?></h1></center>
        </div>
    </div>
      
    
    <div class="row">
        <!-- hace la columna de la izquierda con los modulos -->
        <div class="small-5 columns">
        
        <?php $conter2 = 0; ?>  <!--para cambiar las funciones y cuantas son iguales -->
        <?php $conter3 = 0; ?>  <!--para incrementar los parametros-->
              <!-- Creamos las tablas-->
        <?php for($x = 0; $x< count($Titulos_L); $x++) { ?>
            
            <p class='brikospanp'><?php echo $Titulos_L[$x] ?></p>
            <ul class="especF">
                <?php $conter = 1; ?> <!--para poner los id-->
                <?php for($j = 0; $j< $Fun_p_mod[$x]; $j++) { ?>
                <?php for($i = 0; $i< $Num_f_gen[$j+$conter2]; $i++) { ?>
                <li id="<?php echo ($ID_L[$x].$conter) ?>" style="cursor:pointer;" ><p class="functcomp"><span class="funct"><?php echo $Fun_gen[$j+$conter2] ?></span><?php echo $Param_gen[$conter3] ?></p></li>
                <!-- listeners para mandar a los links -->
                <script>
                    var url_def = "<?php echo base_url(); ?>";
                    var type_P = "";
    
                    function Redirect_var(url_T,type_T){
                        window.open(url_def+"referencia/Link_var/"+type_T+"/"+url_T,"_self");  //cambia la ventana   
                    }
    
                    $("#<?php echo ($ID_L[$x].$conter) ?>").on("click",function(){ 
                        Redirect_var("<?php echo $Paginas_links[$x] ?>","<?php echo ($ID_L[$x].$conter) ?>");
                    }); 
                </script>
                <?php $conter++; ?>
                <?php $conter3++; ?>
                <?php } ?>
                <?php } ?>
                <?php $conter2 = $conter2 + $Fun_p_mod[$x] ; ?>
            </ul>
        <?php } ?>
            
        </div>
        
         <!-- hace la columna de la derecha con los modulos -->
        <div class="small-5 columns small-offset-2">
        
        <?php $conter2 = 0; ?>  <!--para cambiar las funciones y cuantas son iguales -->
        <?php $conter3 = 0; ?>  <!--para incrementar los parametros-->
              <!-- Creamos las tablas-->
        <?php for($x = 0; $x< count($Titulos2_L); $x++) { ?>
            
            <p class='brikospanp'><?php echo $Titulos2_L[$x] ?></p>
            <ul class="especF">
                <?php $conter = 1; ?> <!--para poner los id-->
                <?php for($j = 0; $j< $Fun2_p_mod[$x]; $j++) { ?>
                <?php for($i = 0; $i< $Num2_f_gen[$j+$conter2]; $i++) { ?>
                <li id="<?php echo ($ID2_L[$x].$conter) ?>" style="cursor:pointer;" ><p class="functcomp"><span class="funct"><?php echo $Fun2_gen[$j+$conter2] ?></span><?php echo $Param2_gen[$conter3] ?></p></li>
                <!-- listeners para mandar a los links -->
                <script>
                    var url_def = "<?php echo base_url(); ?>";
                    var type_P = "";
    
                    function Redirect_var(url_T,type_T){
                        window.open(url_def+"referencia/Link_var/"+type_T+"/"+url_T,"_self");  //cambia la ventana   
                    }
    
                    $("#<?php echo ($ID2_L[$x].$conter) ?>").on("click",function(){ 
                        Redirect_var("<?php echo $Paginas2_links[$x] ?>","<?php echo ($ID2_L[$x].$conter) ?>");
                    }); 
                </script>
                <?php $conter++; ?>
                <?php $conter3++; ?>
                <?php } ?>
                <?php } ?>
                <?php $conter2 = $conter2 + $Fun2_p_mod[$x] ; ?>
            </ul>
        <?php } ?>
            
        </div>
            
    </div>
       
    <!-- Cerramos grid principal-->
    </div>
    <br>
    <br>
</div>


  
<!-- Agremas unas librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<script>
  $(document).foundation();

  var doc = document.documentElement;
  doc.setAttribute('first', navigator.userAgent);
</script>

  </body>
</html>