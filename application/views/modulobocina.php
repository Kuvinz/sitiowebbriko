<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/comienzaahora.css">
<!--para hacer que la imagen gire al hacer hover -->
<style>
#f1_container {
  position: relative;
  margin: 10px auto;
  z-index: 1;
}
#f1_container {
  perspective: 1000;
}
#f1_card {
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transition: all 0.7s linear;
}
#f1_container:hover #f1_card {
  transform: rotateY(180deg);
}
.face {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}
.face.back {
  display: block;
  transform: rotateY(180deg);
  padding: 10px;
  text-align: center;
}
</style>

<!--Aqui es donde esta la informacion de toda la pagina -->

<?php $Rotate_text = "Me puedes utilizar con <br />la función: <br /><span style='color:#006D91'>buzzerbk</span> " ?>

<?php 
$Comentario1 = "Declara tus modulos aqui ";
$Comentario2 = "Escribe tu codigo aqui ";
?>

<!-- Arreglo para guardar los puertos-->
<?php $Ports_s = array("PORT1","PORT2","PORT3","PORT4","PORT5","PORT6","PORT7"); ?> 
<!-- Arreglo para guardar las notas-->
<?php $notas_a = array("NOTE_B1","NOTE_D2","NOTE_DS2","NOTE_E2","NOTE_F2","NOTE_FS2","NOTE_G2","NOTE_GS2","NOTE_A2","NOTE_AS2","NOTE_B2",
                        "NOTE_C3","NOTE_CS3","NOTE_D3","NOTE_DS3","NOTE_E3","NOTE_F3","NOTE_FS3","NOTE_G3","NOTE_GS3","NOTE_A3","NOTE_AS3",
                        "NOTE_B3","NOTE_C4","NOTE_CS4","NOTE_D4","NOTE_DS4","NOTE_E4","NOTE_F4","NOTE_FS4","NOTE_G4","NOTE_GS4","NOTE_A4",
                        "NOTE_AS4","NOTE_B4","NOTE_C5","NOTE_CS5","NOTE_D5","NOTE_DS5","NOTE_E5","NOTE_F5","NOTE_FS5","NOTE_G5","NOTE_GS5",
                        "NOTE_A5","NOTE_AS5","NOTE_B5","NOTE_C6","NOTE_CS6","NOTE_D6","NOTE_DS6","NOTE_E6","NOTE_F6","NOTE_FS6","NOTE_G6",
                        "NOTE_GS6","NOTE_A6","NOTE_AS6","NOTE_B6","NOTE_C7","NOTE_CS7","NOTE_D7","NOTE_DS7","NOTE_E7","NOTE_F7","NOTE_FS7",
                        "NOTE_G7","NOTE_GS7","NOTE_A7","NOTE_AS7","NOTE_B7","NOTE_C8","NOTE_CS8","NOTE_D8","NOTE_DS8","NOTE_DO","NOTE_RE",
                        "NOTE_MI","NOTE_FA","NOTE_SOL","NOTE_LA","NOTE_SI"); ?>
<!-- Arreglo para guardar los estados-->
<?php $estado_a = array("ON","OFF"); ?> 

<?php 
$Palabra_Descripcion = "Descripción:";
$Palabra_Funciones = "Funciones:";
$Palabra_Nombre = " nombre.";
$Palabra_Generarcodigo = "Generar código";
$Palabra_Copiarcodigo = "Copiar código";
$Prin_titulo = "Briko de bocina";
$Prin_desc = "-Reproductor de sonidos para utilizar en tus proyectos.";
$Label_1 = "Dale un nombre a tu modulo: (maximo 15 caracteres)";
$Label_2 = "Selecciona tu puerto:";
$Label_3 = "Funciones: (Seleccióna la funcion que deseas probar)";
$Placeholder_1= "Nombre de tu modulo";
$Funcion_name = array("set","beep","playtone","play");
$Funcion_num = array(1,1,2,1);

?>

<!--Texto que va en los modales -->
<?php 
$Modal_list;
$Palabra_Ejemplo = "Ejemplo:";
$Modal_list[0]["M_func"] = "<span class='label desKY' > Función: </span> nombre.<span class='funct'>set</span>(<span class='param'>ESTADO</span>);";
$Modal_list[0]["M_argg"] = array("nombre", "set" );
$Modal_list[0]["M_argdefines"] = array("ESTADO");
$Modal_list[0]["M_arg"] = array("");
$Modal_list[0]["M_descg"] = array("Nombre que se le da al módulo.","Función de briko.");
$Modal_list[0]["M_descdefines"] = array("Prende(ON) o apaga(OFF) la bocina.");
$Modal_list[0]["M_desc"] = array("");
$Modal_list[0]["M_descfun"] = "
Esta función prende o apaga la bocina.
";
$Modal_list[0]["M_codigo"] = "
//Declara tus modulos aqui
buzzerbk bocina(PORT1); ////Declaramos un modulo buzzer con el nombre bocina en el puerto 1

//Escribe tus codigos aqui
code() {
    bocina.set(ON); //En este caso nuestro modulo buzzer se llama bocina y lo prendemos.
    delay(1000); //Espera 1000 milisegundos.
    bocina.set(OFF); //Apaga la bocina.
    delay(1000); //Espera 1000 milisegundos.
}
";
$Modal_list[1]["M_func"] = "<span class='label desKY' > Función: </span> nombre.<span class='funct'>beep</span>( tiempo_prendido, tiempo_apagado );";
$Modal_list[1]["M_argg"] = array("nombre", "beep" );
$Modal_list[1]["M_argdefines"] = array("");
$Modal_list[1]["M_arg"] = array("tiempo_prendido","tiempo_apagado");
$Modal_list[1]["M_descg"] = array("Nombre que se le da al módulo.","Función de briko.");
$Modal_list[1]["M_descdefines"] = array("");
$Modal_list[1]["M_desc"] = array("Tiempo que esta prendida la bocina.","Tiempo que esta apagada la bocina.");
$Modal_list[1]["M_descfun"] = "
Prende la bocina por un cierto tiempo y la apaga por otro. <br />
Esta función no detiene el proceso del bk7, el proceso se hace en paralelo y el buzzer puede seguir sonando mientras se realizan otras cosas. <br />
Los tiempos son en milisegundos.
";
$Modal_list[1]["M_codigo"] = "
//Declara tus modulos aqui
buzzerbk bocina(PORT1);     ////Declaramos un modulo buzzer con el nombre bocina en el puerto 1

//Escribe tus codigos aqui
code() {
    bocina.beep(500, 1000);           //En este caso nuestro modulo buzzer se llama bocina y lo prendemos por 500 milisegundos y lo apagamos por 1000 milisegundos.
    delay(1200);                      //Espera 1200 milisegundos
}
";
$Modal_list[2]["M_func"] = "<span class='label desKY' > Función: </span> nombre.<span class='funct'>playtone</span>(<span class='param'>NOTA</span>);";
$Modal_list[2]["M_argg"] = array("nombre", "playtone" );
$Modal_list[2]["M_argdefines"] = array("NOTA");
$Modal_list[2]["M_arg"] = array("");
$Modal_list[2]["M_descg"] = array("Nombre que se le da al módulo.","Función de briko.");
$Modal_list[2]["M_descdefines"] = array("Nota musical que se especifique.");
$Modal_list[2]["M_desc"] = array("");
$Modal_list[2]["M_descfun"] = "
La bocina reproduce la nota musical que especifícaste.
";
$Modal_list[2]["M_codigo"] = "
//Declara tus modulos aqui
buzzerbk bocina(PORT1);     ////Declaramos un modulo buzzer con el nombre bocina en el puerto 1

//Escribe tus codigos aqui
code() {
    bocina.playtone(NOTE_D2);          //En este caso nuestro modulo buzzer se llama bocina y toca la nota 'D2'.
    delay(1000);                      //Espera 1000 milisegundos
    bocina.playtone(NOTE_A2);          //Toca la nota 'A2'.
    delay(1000);                      //Espera 1000 milisegundos
}
";
$Modal_list[3]["M_func"] = "<span class='label desKY' > Función: </span> nombre.<span class='funct'>playtone</span>(<span class='param'>NOTA</span> ,tiempo);";
$Modal_list[3]["M_argg"] = array("nombre", "playtone" );
$Modal_list[3]["M_argdefines"] = array("NOTA");
$Modal_list[3]["M_arg"] = array("tiempo");
$Modal_list[3]["M_descg"] = array("Nombre que se le da al módulo.","Función de briko.");
$Modal_list[3]["M_descdefines"] = array("Nota musical que se especifique.");
$Modal_list[3]["M_desc"] = array("Tiempo que tocara la nota.");
$Modal_list[3]["M_descfun"] = "
La bocina reproduce la nota musical que especifícaste por un tiempo determinado. <br />
Esta función no detiene el proceso del bk7, el proceso se hace en paralelo y el buzzer puede seguir sonando mientras se realizan otras cosas. <br />
El tiempo esta en milisegundos.
";
$Modal_list[3]["M_codigo"] = "
//Declara tus modulos aqui
buzzerbk bocina(PORT1);     ////Declaramos un modulo buzzer con el nombre bocina en el puerto 1

//Escribe tus codigos aqui
code() {
    bocina.playtone(NOTE_D2, 1500);         //En este caso nuestro modulo buzzer se llama bocina y toca la nota 'D2' por 1500 milisegundos.
    delay(2000);                      //Espera 2000 milisegundos
}
";
$Modal_list[4]["M_func"] = "<span class='label desKY' > Función: </span> nombre.<span class='funct'>play</span>(<span class='param'>NOTA</span> ,tiempo_prendido ,tiempo_apagado );";
$Modal_list[4]["M_argg"] = array("nombre", "play" );
$Modal_list[4]["M_argdefines"] = array("NOTA");
$Modal_list[4]["M_arg"] = array("tiempo_prendido" , "tiempo_apagado");
$Modal_list[4]["M_descg"] = array("Nombre que se le da al módulo.","Función de briko.");
$Modal_list[4]["M_descdefines"] = array("Nota musical que se especifique.");
$Modal_list[4]["M_desc"] = array("Tiempo que esta prendida la bocina.","Tiempo que esta apagada la bocina.");
$Modal_list[4]["M_descfun"] = "
La bocina reproduce la nota musical que especifícaste por un tiempo y la apaga por otro. <br />
Los tiempos esta en milisegundos.
";
$Modal_list[4]["M_codigo"] = "
//Declara tus modulos aqui
buzzerbk bocina(PORT1);     ////Declaramos un modulo buzzer con el nombre bocina en el puerto 1

//Escribe tus codigos aqui
code() {
    bocina.play(NOTE_D2, 500, 500);        //En este caso nuestro modulo buzzer se llama bocina y toca la nota 'D2' por 500 milisegundos y se apaga por 500 milisegundos.
}
";
?>
<!---------------------------------------------------------->

<!-- Creamos el grid principal-->
<div class="row"  id="divp">
  <div class="large-12 columns">
    <br>
    <br>
    <!-- Primera imagen, descripcion y titulo -->
    <div class="row">
        <div class="small-4 columns">
            <div id="f1_container">
                <div id="f1_card" class="shadow">
                    <div class="front face">
                        <!-- front conten-->
                        <center><img alt="briko bocina" src="<?php echo base_url(); ?>images/modulosindividuales/buzzer.png"; ></center>
                    </div>
                    <div class="back face center">
                        <!-- back content -->
                        <center><p style="margin-top: -10px;" class = 'pclassbk'><?php echo $Rotate_text ?></p></center>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-8 end columns">
            <h1 class = 'h1classbk2 text-justify' ><?php echo $Prin_titulo ?></h1>
            <span class='label desKY'><?php echo $Palabra_Descripcion ?></span>
            <br>
            <p class = ' pclassbk text-justify' ><?php echo $Prin_desc ?></p>
        </div>
    </div>
      
      <!-- cintillo se separacion -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <img alt="cintillo 2 briko" width= "200%" style="height:25px" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cintillo2.png"; >
        </div>
    </div>  
      
   <!---------------------------------------------------------->
      
    <!-- Segundo titulo-->
    <div class="row">
        <div class="large-12 columns"  style="padding-bottom: 0px;">
            <h1 class = ' h1classbk2 text-justify' ><?php echo $Palabra_Funciones ?></h1>
        </div>
    </div>
      
    <!-- Label del segundo titulo(los 2 labels) (con display:block para que es span respete la div)-->
    <div class="row">
        <div class="small-5 columns">
            <p class='brikospanp'><?php echo $Label_1 ?></p>
        </div>
        <div class="small-3 end columns small-offset-1">
            <p class='brikospanp'><?php echo $Label_2 ?></p>
        </div>
    </div>
    <!---------------------------------------------------------->
      
    <!-- Input para poner el nombre de los modulos y el selector de puertos-->
    <div class="row">
        <div class="small-5  columns" style="padding-left:20px">
            <input id="first" placeholder="<?php echo $Placeholder_1 ?>" type="text" maxlength="15" />  
        </div>
        <div class="small-3 end columns small-offset-1" style="padding-left:20px">
            <select id="objectPORT" style="color:#BB2462">
                <?php for($i=0;$i<count($Ports_s);$i++){ ?>
                <option>
                    <?php echo $Ports_s[$i]; ?>
                </option>
                <?php } ?> 
            </select>  
        </div>
    </div>
    <!---------------------------------------------------------->
      
    <!-- Label de las funciones-->
    <div class="row">
        <div class="small-5 columns">
            <p class='brikospanp'><?php echo $Label_3 ?></p> 
        </div>    
    </div>
     <!---------------------------------------------------------->
          
     <!-- Para modificar los anchos de los selectores y radio button en la tabla--> 
     <?php $box_width = 130 ?>
     <?php $radiob_width = 40 ?>
      
       <!-- Creamos las tablas-->
    <?php $counter = 0 ?>
    <?php for($x = 0; $x< count($Funcion_num); $x++) { ?>
    <?php for($j = 0; $j< $Funcion_num[$x]; $j++) { ?>
    <?php $counter++ ?>
    <div class="row">
        <div class="large-12 columns" style="margin-bottom:0;padding-bottom:0;" >
            <table style="border-style:hidden;margin-bottom:0;padding-bottom:0;" >
                <tbody>
                    <tr>
                        <?php if($counter==1){ //para poner la condicion de checked?>
                        <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio" value="<?php echo $counter ?>" style="margin-top:5px;" CHECKED />
                        </td>
                        <?php }else{ ?>
                         <td width=<?php echo $radiob_width ?>>
                            <input type="radio" id ="radiob" name="funcionesradio" value="<?php echo $counter ?>" style="margin-top:5px;"/>
                        </td>
                        <?php } ?>
                        <td style="padding-right: 0px;"> 
                            <p class = 'text-justify' id="fun<?php echo $counter ?>" style="font-size:1.5em;" ><?php echo $Palabra_Nombre ?></p>
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;"> 
                            <p class = 'text-justify' style="font-size:1.5em;color:#006D91" ><?php echo $Funcion_name[$x] ?></p>
                        </td>
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >(</p>
                        </td>
                        
                        <?php if($counter==1){ ?>
                        <td width=<?php echo $box_width ?>>
                            <select id="object<?php echo $counter ?>1" style="color:#BB2462">
                                <?php for($i=0;$i<count($estado_a);$i++){ ?>
                                <option>
                                    <?php echo $estado_a[$i]; ?>
                                </option>
                                <?php } ?> 
                            </select>   
                        </td>
                        <?php } ?>
                        
                        <?php if($counter==2){ ?>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="0-65000" min="0" max="65000" id="object<?php echo $counter ?>1">
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="0-65000" min="0" max="65000" id="object<?php echo $counter ?>2">
                        </td>
                        <?php } ?>
                        
                        <?php if($counter==3){ ?>
                        <td width=<?php echo $box_width ?>>
                            <select id="object<?php echo $counter ?>1" style="color:#BB2462">
                                <?php for($i=0;$i<count($notas_a);$i++){ ?>
                                <option>
                                    <?php echo $notas_a[$i]; ?>
                                </option>
                                <?php } ?> 
                            </select> 
                        </td>
                        <?php } ?>
                        
                        <?php if($counter==4){ ?>
                        <td width=<?php echo $box_width ?>>
                            <select id="object<?php echo $counter ?>1" style="color:#BB2462">
                                <?php for($i=0;$i<count($notas_a);$i++){ ?>
                                <option>
                                    <?php echo $notas_a[$i]; ?>
                                </option>
                                <?php } ?> 
                            </select>
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="0-65000" min="0" max="65000" id="object<?php echo $counter ?>2">
                        </td>
                        <?php } ?>
                        
                        <?php if($counter==5){ ?>
                        <td width=<?php echo $box_width ?>>
                            <select id="object<?php echo $counter ?>1" style="color:#BB2462">
                                <?php for($i=0;$i<count($notas_a);$i++){ ?>
                                <option>
                                    <?php echo $notas_a[$i]; ?>
                                </option>
                                <?php } ?> 
                            </select>
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="0-65000" min="0" max="65000" id="object<?php echo $counter ?>2">
                        </td>
                        <td style="padding-right: 0px; padding-left:0px;">
                            <p class = 'text-justify'style="font-size:1.5em;" >,</p>
                        </td>
                        <td width=<?php echo $box_width ?>>
                            <input type="number" placeholder="0-65000" min="0" max="65000" id="object<?php echo $counter ?>3">
                        </td>
                        <?php } ?>
                        
                        <td style="padding-left:0px;"> 
                            <p class = 'text-justify'style="font-size:1.5em;" >);</p>
                        </td>
                         <td>
                            <img alt="ayuda" id="but<?php echo $counter ?>1" src="<?php echo base_url(); ?>images/ayuda.png" width=30 height=30 style="cursor:pointer;margin-top:0px;padding-bottom: 19px;" />
                        </td>
                    </tr>
                </tbody>  
            </table>
        </div>   
    </div> 
      
    <!--listener de los botones -->
    <script>
    //listener del boton imagen para abrir la ayuda    
        $("#but<?php echo $counter ?>1").on("click",function(){  //abre pop
            $('#POP<?php echo $counter-1 ?>').foundation('reveal','open');
        });    
    </script>
      
    <?php } ?>
    <?php } ?>
    <!---------------------------------------------------------->
    
    
    <!-- Creamos el boton para generar el codigo y copiar el codigo-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="button round" id="buttongenerate" ><?php echo $Palabra_Generarcodigo ?></button>
        </div>
        <div class="small-3 columns end small-offset-2" >
            <button class="button round" id="buttoncopy" ><?php echo $Palabra_Copiarcodigo ?></button>
        </div>
    </div>
     <!---------------------------------------------------------->
     
    <!-- Creamos el editor de codigo-->
<div class="row">
    <div id="editor" >
  ////<?php echo $Comentario1 ?>
        
      
  //<?php echo $Comentario2 ?>
        
  code(){
        
  }

    </div>
</div>
 <!---------------------------------------------------------->
           
    <!-- Cerramos grid principal-->
    </div>
    <br>
    <br>
</div>
 <!---------------------------------------------------------->

<!-- declaramso script para animaciones-->
<script src="<?php echo base_url(); ?>js/move/move.js"></script>
<script src="<?php echo base_url(); ?>js/move/movemodulos/buzzer.js"></script>

<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 
    

<!--para guardar las configuraciones de los editores de los pop -->
<script>
var editorpop= new Array();
</script>

<!-- declaramos el primer modal que saldra al presionar el boton de ayuda -->
<?php for($j = 0; $j< count($Modal_list); $j++) { ?>
<div id="POP<?php echo $j ?>" class="reveal-modal big" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="row">
        <div class="small-12 end columns"  >
            <p class="functcomp"><?php echo $Modal_list[$j]["M_func"] ?></p>
        </div>
    </div>  
    <div class="row">
        <div class="large-12 columns">
            <ul class="especF">
                <li><?php echo $Modal_list[$j]["M_argg"][0] ?>: <?php echo $Modal_list[$j]["M_descg"][0] ?> </li>
                <li><span class="funct"><?php echo $Modal_list[$j]["M_argg"][1] ?></span>: <?php echo $Modal_list[$j]["M_descg"][1] ?> </li>
                <?php for($i = 0; $i< count($Modal_list[$j]["M_argdefines"]); $i++) { ?>
                <?php if($Modal_list[$j]["M_argdefines"][$i] != ""){ ?>
                <li><span class="param"><?php echo $Modal_list[$j]["M_argdefines"][$i] ?></span>: <?php echo $Modal_list[$j]["M_descdefines"][$i] ?></li>
                <?php } } ?>
                <?php for($i = 0; $i< count($Modal_list[$j]["M_arg"]); $i++) { ?>
                <?php if($Modal_list[$j]["M_arg"][$i] != ""){ ?>
                <li><?php echo $Modal_list[$j]["M_arg"][$i] ?>: <?php echo $Modal_list[$j]["M_desc"][$i] ?></li>
                <?php } } ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <span class='label desKY'> <?php echo $Palabra_Descripcion ?> </span>
            <p class="lead"><?php echo $Modal_list[$j]["M_descfun"] ?></p>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <span class='label desKY'> <?php echo $Palabra_Ejemplo ?> </span>
            <!-- Boton de copiar-->
            <center><button id="botoncopy<?php echo $j ?>" class= "button round" style="margin-bottom: 0px; padding-top: 5px; padding-left: 10px; padding-right: 10px;padding-bottom: 5px;">Copy code</button></center>
            <div id="editorpmotor<?php echo $j ?>">
                <?php echo $Modal_list[$j]["M_codigo"]?>
            </div>  
        </div>
    </div>
        
    <?php if($j == 0) { //animacion 1 ?>
    <div class="row"> 
        <div class="box1" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="notas briko 1" id="note" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes.png"  />
                <img alt="notas briko 2" id="note1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes1.png"  style="display:none"/>
                <img alt="notas briko 3" id="note2" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes2.png" style="display:none" />
                <img alt="notas briko 4" id="note3" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes3.png"  style="display:none"/>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="box2" style="text-align:center">
            <div class="small-12 columns" >
                <img alt="bocina encendida" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzersound.png" alt="Letter A" />
                <img alt="bocina apagada" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzer.png" alt="Letter B" style="display:none"/>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 1) { //animacion 2 ?>
    <div class="row"> 
        <div class="box3" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="notas briko 1"  id="note" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes.png"  />
                <img alt="notas briko 2"  id="note1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes1.png"  style="display:none"/>
                <img alt="notas briko 3"  id="note2" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes2.png" style="display:none" />
                <img alt="notas briko 4"  id="note3" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes3.png"  style="display:none"/>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="box4" style="text-align:center">
            <div class="small-12 columns" >
                <img alt="bocina encendida" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzersound.png" alt="Letter A" />
                <img alt="bocina apagada" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzer.png" alt="Letter B" style="display:none"/>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 2) { //animacion 3 ?>
    <div class="row"> 
        <div class="box5" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="notas briko 1" id="note" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes.png"  style="display:none" />
                <img alt="notas briko 2" id="note1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes1.png"  style="display:none"/>
                <img alt="notas briko 3" id="note2" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes2.png" />
                <img alt="notas briko 4" id="note3" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes3.png"  style="display:none"/>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="box6" style="text-align:center">
            <div class="small-12 columns" >
                <img alt="bocina encendida alto" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzersound.png" alt="Letter A" />
                <img alt="bocina encendida bajo" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzersoundlow.png" alt="Letter B" style="display:none"/>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 3) { //animacion 4 ?>
    <div class="row"> 
        <div class="box7" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="notas briko 1" id="note" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes.png"  />
                <img alt="notas briko 2" id="note1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes1.png"  style="display:none"/>
                <img alt="notas briko 3" id="note2" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes2.png" style="display:none" />
                <img alt="notas briko 4" id="note3" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes3.png"  style="display:none"/>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="box8" style="text-align:center">
            <div class="small-12 columns" >
                <img alt="bocina" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzersound.png" alt="Letter A" />
                <img alt="bocina 1" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzer.png" alt="Letter B" style="display:none"/>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if($j == 4) { //animacion 5 ?>
    <div class="row">
       <div class="box9" style="text-align:center">
            <div class="small-12 columns " >
                <img alt="notas briko 1" id="note" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes.png"  />
                <img alt="notas briko 2" id="note1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes1.png"  style="display:none"/>
                <img alt="notas briko 3" id="note2" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes2.png" style="display:none" />
                <img alt="notas briko 4" id="note3" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/notes3.png"  style="display:none"/>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="box10" style="text-align:center">
            <div class="small-12 columns" >
                <img alt="bocina" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzersound.png" alt="Letter A" />
                <img alt="bocina 1" class="Change_Image1" src="<?php echo base_url(); ?>images/modulos_anim/buzzer/buzzer.png" alt="Letter B" style="display:none"/>
            </div>
        </div>
    </div>
   </div>
    <?php } ?>
    
    
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!---------------------------------------------------------->

<!--Se modifica el editor de los modales (estilo y links) -->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var numx = parseInt("<?php echo $j ?>");
editorpop[numx] = ace.edit("editorpmotor<?php echo $j ?>");  //liga el editor declaradoe en html
editorpop[numx].getSession().setMode( xml_mode ); //pone el modo
editorpop[numx].setTheme("ace/theme/brikode"); //pone el tema
editorpop[numx].getSession().setTabSize(2);
editorpop[numx].getSession().setUseWrapMode(true);
editorpop[numx].setReadOnly(true);  // false to make it editable
</script> 
<!---------------------------------------------------------->

<script>
//listener para copiar el codigo en el editor de texto
$("#botoncopy<?php echo $j ?>").on("click",function(){ 
var numx = parseInt("<?php echo $j ?>");
var Strind_editor  =editorpop[numx].getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
</script>
    
<style>
#editorpmotor<?php echo $j ?> { 
margin-left: 15px;
  margin-top: 15px;
  height: 300px;
 font-size: 14px;
}    
</style>

<?php } ?>
<!---------------------------------------------------------->

<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor = ace.edit("editor");  //liga el editor declaradoe en html
editor.getSession().setMode( xml_mode ); //pone el modo
editor.setTheme("ace/theme/brikode"); //pone el tema
editor.getSession().setTabSize(2);
editor.getSession().setUseWrapMode(true);
editor.setReadOnly(true);  // false to make it editable
</script> 
<!---------------------------------------------------------->


<!-- Script donde tenemos todos los listeners --> 
<script>
var secretmessage = Array(0,0,0,0,0,0,0,0); //donde se guardaran las letras
var flagm = 0; //variable para saber en que letra va
var flagm2 = 0; //variable para saber si se equivoco y hay que resetear
var flagm3 = 0;  //para resetear si presionan en otro lado que no sea en la nada
var $tempimg = $("<img>"); //para crear una imagen temporal en la pagina
$tempimg.attr("src","<?php echo base_url(); ?>images/primerprograma/paso15.png");//carga la imagen secreta
$tempimg.attr("style","margin-left: 200px;");//la centra
  
    
//listener para copiar el codigo en el editor de texto
$("#buttoncopy").on("click",function(){ 
var Strind_editor  =editor.getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
   
//listener para generar el codigo cuando presionen el boton
$("#buttongenerate").on("click",function(){  
    var value = $('input:radio[name=funcionesradio]:checked').val().toString(); 
    var Name= $('#first').val();
    if(Name == ""){ Name="nombre";}
    var Port_s= $('#objectPORT').val();
    if(Name == ""){ Name="PORT1";}
    
    
    if(value ==1){
        var num2="OFF";
    var num1 = $('#object11').val().toString();
    if(num1 == ""){num1="ON";}
    if(num1 == "ON"){num2="OFF";}else{ num2="ON";}

    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "buzzerbk "+Name+"("+Port_s+");\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    Name + ".set(" + num1 +");\n  "+
    "delay(500);\n  "+
    Name + ".set(" + num2 +");\n  "+
    "delay(500);\n"+
    "}\n");
    }
    
    if(value ==2){
    var num1 = $('#object21').val().toString();
    if(num1 == ""){num1="500";}
    var num2 = $('#object22').val().toString();
    if(num2 == ""){num2="500";}
    var num3 = (parseInt(num1) + parseInt(num2) + 200).toString();
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "buzzerbk "+Name+"("+Port_s+");\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    Name + ".beep(" + num1 + "," + num2 +");\n  "+
    "delay("+num3+");\n"+
    "}\n");
    }
    
    if(value ==3){
    var num1 = $('#object31').val().toString();
    if(num1 == ""){num1="NOTE_B1";}
     editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "buzzerbk "+Name+"("+Port_s+");\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    Name + ".playtone(" + num1  +");\n  "+
    "delay(1000);\n"+
    "}\n");
    }
    
    if(value ==4){
    var num1 = $('#object41').val().toString();
    if(num1 == ""){num1="NOTE_B1";}
    var num2 = $('#object42').val().toString();
    if(num2 == ""){num2="500";}
    var num3 = (parseInt(num2) + 200).toString();
     editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "buzzerbk "+Name+"("+Port_s+");\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    Name + ".playtone(" + num1 + "," + num2 + ");\n  "+
    "delay("+num3+");\n"+
    "}\n");
    }
    
    if(value ==5){
    var num1 = $('#object51').val().toString();
    if(num1 == ""){num1="NOTE_B1";}
    var num2 = $('#object52').val().toString();
    if(num2 == ""){num2="500";}
    var num3 = $('#object53').val().toString();
    if(num3 == ""){num3="500";}
    editor.gotoLine(1);
    editor.setValue(
    "//<?php echo $Comentario1 ?>\n\n"+
    "buzzerbk "+Name+"("+Port_s+");\n\n" +
    "//<?php echo $Comentario2 ?>\n" +
    "code(){\n  " + 
    Name + ".play(" + num1 + "," + num2 + "," + num3 + ");\n  "+
    "}\n");
    }
    


});    
  
//listener para mover el radio button
$("#object11").on("click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[0].checked  = true; 
});  
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object21").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[1].checked  = true; 
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object21').val(); 
    if(value != ""){
        if(value >65000){
        $('#object21').val(65000);  
        }
        if(value <0){
            $('#object21').val(0);  
        }
    }
});
    
//listener para mover el radio button
$("#object22").on("click keypress keyup keydown ",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[1].checked  = true; 

     var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object22').val(); 
    if(value != ""){
        if(value >65000){
        $('#object22').val(65000);  
        }
        if(value <0){
            $('#object22').val(0);  
        }
    }
});  

//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object31").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[2].checked  = true;
});


//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object41").on("keypress keyup keydown click",function(){  
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[3].checked  = true;
});

//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object42").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[3].checked  = true;
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object42').val(); 
    if(value != ""){
        if(value >65000){
        $('#object42').val(65000);  
        }
        if(value <0){
            $('#object42').val(0);  
        }
    }
});
    
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object51").on("keypress keyup keydown click",function(){  
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[4].checked  = true;
});

//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object52").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[4].checked  = true;
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object52').val(); 
    if(value != ""){
        if(value >65000){
        $('#object52').val(65000);  
        }
        if(value <0){
            $('#object52').val(0);  
        }
    }
});
 
//listener para limitar la entrada de los inputs numericos y mover el radio button
$("#object53").on("keypress keyup keydown click",function(){  //redirige a otra pagina
    ///pone el radio button en la linea
    var bbradio= $('input:radio[name=funcionesradio]');
    bbradio[4].checked  = true;
    
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
    if (!(((key <58 && key > 47 ) || (key <106 && key > 95 ) || key==12 || key==40 || key==45 || key==46 || key==8) )) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    var value = $('#object53').val(); 
    if(value != ""){
        if(value >65000){
        $('#object53').val(65000);  
        }
        if(value <0){
            $('#object53').val(0);  
        }
    }
});

    
//listener para detectar cuando el nombre de los modulos cambie y asi poder actualizar todas las fucniones con ese mismo nombre
$("#first").on("keypress keyup keydown",function(){  //redirige a otra pagina
    ///si detecta una tecla que no sea letra numero o guion bajo
    var event = event || window.event;  // get event object
    var key = event.keyCode || event.which; // get key cross-browser
  
    if (!((key <91 && key > 47) || (key <123 && key > 96) || key==189 || key==8 || key==46 || key==16 || (key <40 && key > 32 ) || key==12 || key==40 || key==95)) { //Space bar key code, dots, signs, etc
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    if (((key <58 && key > 47 ) || (key <40 && key > 32 ) || key==12 || key==40 || key==45) && ($('#first').val().length ==0) ) { //block numbers in first digit
        //Prevent default action, which is inserting space
        if (event.preventDefault) event.preventDefault(); //normal browsers
            event.returnValue = false; //IE
        }
    
    //cambia textos
    //console.log("entro");
    flagm3 = 1; //para recetear el mensaje secreto
    var value = $('#first').val();
    if(value == ""){
    $('#fun1').text("nombre." );  //da el nombre del modulo en las funciones   
    $('#fun2').text("nombre." );  //da el nombre del modulo en las funciones  
    $('#fun3').text("nombre." );  //da el nombre del modulo en las funciones  
    $('#fun4').text("nombre." );  //da el nombre del modulo en las funciones  
    $('#fun5').text("nombre." );  //da el nombre del modulo en las funciones  
    }else{
    $('#fun1').text(value+"." );  //da el nombre del modulo en las funciones
    $('#fun2').text(value+"." );  //da el nombre del modulo en las funciones
    $('#fun3').text(value+"." );  //da el nombre del modulo en las funciones
    $('#fun4').text(value+"." );  //da el nombre del modulo en las funciones
    $('#fun5').text(value+"." );  //da el nombre del modulo en las funciones
    }
    
});
    
//listener para detectar si presionas teclas en cualquier lugar de la pagino y si escriben el mensaje secreto salga la imagen secreta
document.addEventListener("keypress", function(){
    flagm2 = 0; 
    var letters = Array(98,114,105,107,111,98,111,116);  //brikobot
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    if(chCode==letters[0] && flagm==0 && flagm2==0){ secretmessage[0]=chCode;flagm=1;flagm2 = 1;} 
    if(chCode==letters[1] && flagm==1 && flagm2==0){ secretmessage[1]=chCode;flagm=2;flagm2 = 1;} 
    if(chCode==letters[2] && flagm==2 && flagm2==0){ secretmessage[2]=chCode;flagm=3;flagm2 = 1;} 
    if(chCode==letters[3] && flagm==3 && flagm2==0){ secretmessage[3]=chCode;flagm=4;flagm2 = 1;} 
    if(chCode==letters[4] && flagm==4 && flagm2==0){ secretmessage[4]=chCode;flagm=5;flagm2 = 1;} 
    if(chCode==letters[5] && flagm==5 && flagm2==0){ secretmessage[5]=chCode;flagm=6;flagm2 = 1;} 
    if(chCode==letters[6] && flagm==6 && flagm2==0){ secretmessage[6]=chCode;flagm=7;flagm2 = 1;} 
    if(chCode==letters[7] && flagm==7 && flagm2==0){ secretmessage[7]=chCode;flagm=0;flagm2 = 1;} 


    if(flagm2 ==0 || flagm3 ==1){
        flagm3 = 0;
        for(var i=0;i<secretmessage.length;i++){secretmessage[i]=0;flagm=0;}
        $tempimg.remove();
    }
    
    if(secretmessage[0]==letters[0] && secretmessage[1]==letters[1] && secretmessage[2]==letters[2] && secretmessage[3]==letters[3] && secretmessage[4]==letters[4] && secretmessage[5]==letters[5] && secretmessage[6]==letters[6] && secretmessage[7]==letters[7]){
        ////mensaje completado con exito
        $("body").append($tempimg); //agrega la imagen temporal a la pagina
        console.log("Secret message activated");
        for(var i=0;i<secretmessage.length;i++){secretmessage[i]=0;flagm=0;}
    }
});
    
</script>


<!--Para saber cuando la pagina es llamada desde un link y abra un pop-->
<script>
//cuando la pagina esta lista
$(document).ready(function() {   
<?php echo "var msg = '" .$refe_var . "'" ?>; //guarda la variable del php en javascript que se trajo como argumento al cargar la pagin a
  
console.log(msg);
if(msg == "bocina1"){
  $('#POP0').foundation('reveal','open');   
}
                              
if(msg == "bocina2"){
  $('#POP1').foundation('reveal','open');   
}
                              
if(msg == "bocina3"){
  $('#POP2').foundation('reveal','open');   
}
                              
if(msg == "bocina4"){
  $('#POP3').foundation('reveal','open');   
}
                              
if(msg == "bocina5"){
  $('#POP4').foundation('reveal','open');   
}
                                                                                   
<?php $_SESSION['type message'] = ""; ?>  //resetea
});

</script>

<!-- Agremas unas librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<script>
  $(document).foundation();

  var doc = document.documentElement;
  doc.setAttribute('first', navigator.userAgent);
</script>

  </body>
</html>