<link href='<?php echo base_url(); ?>css/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>css/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url(); ?>js/moment.min.js'></script>
<script src='<?php echo base_url(); ?>js/fullcalendar.js'></script>
<script src='<?php echo base_url(); ?>js/es.js'></script>
<script src='<?php echo base_url(); ?>js/jquery-ui.min.js'></script>
<div class="row ">  
    <div class='columns large-12'>

        <div id='calendar'></div>
    </div>
</div>


<script>

    $(document).ready(function() {

        var zone = "05:30";  //Change this to your timezone
        var url1="http://briko.cc/cal";
    $.ajax({
        url: url1,
        type: 'POST', // Send post data
        data: 'type=fetch',
        async: false,
        success: function(s){
            //alert(s);
            json_events = s;
        }
    });


    var currentMousePos = {
        x: -1,
        y: -1
    };
        jQuery(document).on("mousemove", function (event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

        /* initialize the external events
        -----------------------------------------------------------------*/

        $('#external-events .fc-event').each(function() {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });


        /* initialize the calendar
        -----------------------------------------------------------------*/

        $('#calendar').fullCalendar({
            events: JSON.parse(json_events),
            //events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
            utc: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'none'
            },
            editable: false,
            droppable: false,

            slotDuration: '00:30:00',
            eventReceive: function(event){
                var title = event.title;
                var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
                $.ajax({
                    url: url1,
                    data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
                    type: 'POST',
                    dataType: 'json',
                    success: function(response){
                        event.id = response.eventid;
                        $('#calendar').fullCalendar('updateEvent',event);
                    },
                    error: function(e){
                        console.log(e.responseText);

                    }
                });
                $('#calendar').fullCalendar('updateEvent',event);
                console.log(event);
            },
            eventDrop: function(event, delta, revertFunc) {
                var title = event.title;
                var start = event.start.format();
                var end = (event.end == null) ? start : event.end.format();
                $.ajax({
                    url: url1,
                    data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
                    type: 'POST',
                    dataType: 'json',
                    success: function(response){
                        if(response.status != 'success')                            
                        revertFunc();
                    },
                    error: function(e){                     
                        revertFunc();
                        alert('Error processing your request: '+e.responseText);
                    }
                });
            },
            eventClick: function(event) {
                //console.log(event);
                if (event.url) {
                    window.open(event.url);
                    //alert(event.url);
                    return false;
                }
            },
            eventResize: function(event, delta, revertFunc) {
                console.log(event);
                var title = event.title;
                var end = event.end.format();
                var start = event.start.format();
                update(title,start,end,event.id);
            },
            eventDragStop: function (event, jsEvent, ui, view) {
                if (isElemOverDiv()) {
                    var con = confirm('Are you sure to delete this event permanently?');
                    if(con == true) {
                        $.ajax({
                            url: url1,
                            data: 'type=remove&eventid='+event.id,
                            type: 'POST',
                            dataType: 'json',
                            success: function(response){
                                console.log(response);
                                if(response.status == 'success'){
                                    $('#calendar').fullCalendar('removeEvents');
                                    getFreshEvents();
                                }
                            },
                            error: function(e){ 
                                alert('Error processing your request: '+e.responseText);
                            }
                        });
                    }   
                }
            }
        });

    function getFreshEvents(){
        $.ajax({
            url: url1,
            type: 'POST', // Send post data
            data: 'type=fetch',
            async: false,
            success: function(s){
                freshevents = s;
            }
        });
        
    }


    

    });

</script>
<script>

$(document).foundation();

var doc = document.documentElement;
doc.setAttribute('data-useragent', navigator.userAgent);
</script>
</body>
</html>
