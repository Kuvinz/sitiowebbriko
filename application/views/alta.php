<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<div class="row"  data-equalizer>
  <div class="large-12  columns">

    <div class="row">
      <button id="nuevo" class="round comK">Sube tu Proyecto</button>
    <table id="myTable" class="display dataTable" width="100%">
      <thead>
        <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                
                <th>Eliminar</th>
        </tr>
      </thead>
      <tbody>
        <!-- Codigo para imprimir todos los proyectos-->
        <?php
                $base= base_url();
                //print_r($proyecto);
                foreach ($proyecto as $value)
                {
                  echo "<tr >";
                  echo "<td class='proyselect' onclick=abrirP('".$base."',".$value->ID_Proyecto.")>".$value->Nombre."</td>";
                  echo "<td class='proyselect' onclick=abrirP('".$base."',".$value->ID_Proyecto.")>".$value->Descripcion."</td>";
                  echo "<td><img alt='Borrar proyecto' style='cursor:pointer; width:10%' class='delete' onCLick=Proyecto('".$base."',".$value->ID_Proyecto.") src='".$base."images/trash_p.png' border='0'></td>";
                    echo "</tr>";
                }
              ?>
      </tbody>
    </table>
      

    </div>
    
    
    
    
  </div>
</div>
 <br>
          <br>
      </div>
     

<script type="text/javascript">
          //listener del boton del principal para la compra 
    $("#nuevo").on("click",function(){  //abre pop
        window.open('http://briko.cc/nuevo-proyecto','_self');
    }); 
    function abrirP (url,id)
    {
      window.location.assign(url+"index.php/proyecto/"+id);
    }
    function Proyecto1(url,id)
    {
      window.location.assign(url+"index.php/modificar/"+id);
    }  
  function Proyecto(url,valor)
      {
        var val=valor;
        var url1=url+"index.php/eliminarP/"+valor;
        //alert(url1);
      $(function () 
      {
        //-----------------------------------------------------------------------
        // 2) Send a http request with AJAX http://api.jqueryom/jQuery.ajax/
        //-----------------------------------------------------------------------
        $.ajax({                                      
          url: url1,                  //the script to call to get data          
          data: "",                        //you can insert url argumnets here to pass to api.php
                                           //for example "id=5&parent=6"
          dataType: 'json',                //data format      
          success: function(data)          //on recieve of reply
          {
              window.location.reload(true);
          } 
        });
      });
      window.location.reload(true);
    }
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/DataTables/media/js/jquery.dataTables.js"></script>

<script type="text/javascript">
$(document).ready(function() {
$('#myTable').dataTable({
            "oSearch":{
                "bCaseInsensitive": true,
                "sSearch":""
            },
            "oLanguage": {
                "sProcessing": "Buscando...",
                "sZeroRecords": "No hay elementos para mostrar",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "< Primera",
                    "sLast": "Última >",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sEmptyTable": "No hay elementos para mostrar",
                "sInfo":"Mostrando _START_ a _END_ de un total de _TOTAL_ proyectos",
                "sInfoFiltered": " - Filtrado de un total de _MAX_ proyectos",
                "sLengthMenu": "<div class='row'><div class='large-2 columns'>Mostrar</div><div class='large-4 columns'> _MENU_</div><div class='large-2 columns'>proyectos</div><div class='large-6 columns'></div></div>",
                "sInfoEmpty": "Mostrando 0 al 0 de 0 registros"
            }
        });});
</script>
<script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
  </body>
</html>
