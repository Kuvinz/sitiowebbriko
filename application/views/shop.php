<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>
<div class="row principal">
      <img src="<?php echo base_url();?>images/premium.jpg">
</div>
<div class="row"  data-equalizer>
  <div class="large-12  columns">

    <div class="row">
      <div class="large-4  columns">
        <img src="<?php echo base_url();?>images/StarterparaWEB.png">
      
      </div>
      <div class="large-7  columns">
        <div class="row">
          <div class="large-12  columns">
          
            <h1 class="tKits">Starter Kit $1499</h1>
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <span class="label desK">Descripción:</span>
            
            <p class="text-justify desT">Es el kit ideal para jóvenes y niños que quieren aprender programación por primera vez, con este kit vas a poder realizar proyectos como una lámpara inteligente, juegos de destreza o un piano activado con movimiento, además de desarrollar tus propias ideas.</p>
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <span class="label desK">Incluye:</span>
            <img src="<?php echo base_url();?>images/in1.png">
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" id="formStarter">
              <input type="hidden" name="cmd" value="_s-xclick">
              <input type="hidden" name="hosted_button_id" value="EB6CHXWD5QHFC">
              <button class="submitStarter round comK">Comprar</button>
            </form>
            
          </div>
        </div>
      
      </div>
      

    </div>
    <div class="row">
      <div class="large-4  columns">
        <img src="<?php echo base_url();?>images/RobotparaWEB.png">
      
      </div>
      <div class="large-7  columns">
        <div class="row">
          <div class="large-12  columns">
            <h1 class="tKits">Robot Kit $2999</h1>
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
              
            <span class="label desK">Descripción:</span>
            <p class="text-justify desT">Es el kit perfecto si quieres aprender programación y robótica, está enfocado a construir proyectos como un robot sumo o un robot seguidor de línea , además de usar las piezas para dar vida a tus propias creaciones. </p>
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <span class="label desK">Incluye:</span>
            <img src="<?php echo base_url();?>images/in2.png">
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" id="formRobot">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="2FVZPN6ZHJ42N">
            <button class="submitRobot round comK">Comprar</button>
          </form>
            
          </div>
        </div>
      
      </div>
      

    </div>
    <div class="row">
      <div class="large-4  columns">
        <img src="<?php echo base_url();?>images/PremiumparaWEB.png">
      
      </div>
      <div class="large-7  columns">
        <div class="row">
          <div class="large-12  columns">
            <h1 class="tKits">Premium Kit $3999</h1>
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <span class="label desK">Descripción:</span>
            
            <p class="text-justify desT">No importa si estas empezando o si ya eres un maker o diseñador experimentado, con este kit podrás construir una cantidad prácticamente ilimitada de proyectos, incluye todo lo necesario para construir desde proyectos de robótica, automatizar tu casa, así como controlar tu briko por medio de tu celular o internet.</p>
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <span class="label desK">Incluye:</span>
            <img src="<?php echo base_url();?>images/in2.png">
          </div>
        </div>
        <div class="row">
          <div class="small-12  columns">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" id="formPrem">
              <input type="hidden" name="cmd" value="_s-xclick">
              <input type="hidden" name="hosted_button_id" value="JVPTE9BBYQZHE">
           
              <button class="submitPrem round comK">Comprar</button>
            </form>
            
          </div>
        </div>
      
      </div>
      

    </div>
    
    
  </div>
</div>
 <br>
          <br>
      </div>
      <script type="text/javascript">
        $(document).ready(function(){
        $(".submitStarter").click(function(){
           $("#formStarter").submit();
        });
        $(".submitRobot").click(function(){
           $("#formRobot").submit();
        });
        $(".submitPrem").click(function(){
           $("#formPrem").submit();
        });
      });
    </script>
    <script>
        $(document).foundation();
      </script>
    
    
    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
  </body>
</html>