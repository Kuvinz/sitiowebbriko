<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 

<div class="row"  id="div1">
  <div class="large-12  columns">

<!------------------------------------------------------------------------------>
  
<!--Slide show, titulo principal y descripcion-->     
<div class="row" >
        <div class="small-6 columns" id="tituloPrincipal">
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 2; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/prin<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li>  
<?php }else{ ?>  
   <li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/prin<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li>   
 <?php } ?>
 <?php } ?>
    <li data-orbit-slide="headline-1">
  <div class="flex-video">
        <iframe width="400" height="215" src="//www.youtube.com/embed/syXS6qOqDwc" frameborder="0" allowfullscreen></iframe>
  </div>
  </li>
</ul>
</div>
<div class="small-6 end columns" >
    <h1 class="h1classbk">Carrito briko con app:</h1>
    <span class='label desKY'> Descripci&oacute;n: </span>
    <ul class="especF">
        <li>
            <p class="pclassbk text-justify">Este proyecto consta de construir un carrito con las piezas mecanicas de briko, para después poder programarlo con la app.</p>
        </li>
    </ul>
</div>
</div>

<!-- cintillo se separacion -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <img alt="briko cintillo separacion" width= "200%"src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cintillo2.png"; >
        </div>
    </div>
            
<!-- titulo "pasos" -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Pasos del proyecto:</h1>
        </div>
    </div>
      
<!-- tabs para entrar a diferentes funciones -->
    <div class="row">
        <div class="small-12 columns ">
            <ul class="tabs" id="myTabs" data-tab>
                <li class="tab-title active"><a href="#panel1">Paso 1</a></li>
                <li class="tab-title"><a href="#panel2">Paso 2</a></li>
                <li class="tab-title"><a href="#panel3">Paso 3</a></li>
                <li class="tab-title"><a href="#panel4">Paso 4</a></li>
                <li class="tab-title"><a href="#panel5">Paso 5</a></li>
                <li class="tab-title"><a href="#panel6">Paso 6</a></li>
                <li class="tab-title"><a href="#panel7">Paso 7</a></li>
                </ul>
        </div>
    </div>
      
<!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content" id="myTabscontainer">
      
<!------------------------------------------------------------------------------>
      
<!-- Paso 1 -->
<!-- tab -->
<div class="content active" id="panel1">
    <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Material:</h1>
        </div>
    </div>
    
<!-- titulode agreagar modulos a comprar -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <p class=" pclassbk">Agrega las piezas de briko utilizadas en este proyecto a tu carrito de compra:</p>
        </div>
    </div>
    
    <!-- Boton de compra-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="agregacompra" class= "button round">Agregar</button>
        </div>
    </div>
    
<!--piezas del proyecto -->
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> bk7</li>
                <p class=" pclassbk text-justify"><span class="param">Controlador principal de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="modulo principal b k 7" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/bk7.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> brikos de motor</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de motor de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko motor" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/motor.png"; >
    </div>
</div>

<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> briko de leds</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de leds de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko led" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/leds.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> briko de distancia</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de distancia de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="brikoo sensor de distancia" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/sensdis.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> llantas</li>
                <p class=" pclassbk text-justify"><span class="param">Módulo de distancia de briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="accesorio llanta" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/llanta.png"; >
    </div>
</div>
    
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> rueda loca</li>
                <p class=" pclassbk text-justify"><span class="param">Una rueda loca de metal con sus tornillos para montarla.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="accesorio rueda loca" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/ruedaloca.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> piso</li>
                <p class=" pclassbk text-justify"><span class="param">La pieza mecanica que forma el piso de un carrito.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza3.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> vigas</li>
                <p class=" pclassbk text-justify"><span class="param">La pieza mecanica larga en forma de viga.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion viga" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/viga.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">2 </span> uniones 90</li>
                <p class=" pclassbk text-justify"><span class="param">Las piezas chiquitas que sirven para unir mas piezas.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion union 3" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/union3.png"; >
    </div>
</div>  
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> pared</li>
                <p class=" pclassbk text-justify"><span class="param">La pieza cuadrada que sirve para montar módulos.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="pieza de construccion" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pieza1.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">6 </span> tornillos chicos</li>
                <p class=" pclassbk text-justify"><span class="param">Tornillos para el maestro y los modulos "menos" el de motor.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="tornillo chico" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tornilloch.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">4 </span> tornillos grandes</li>
                <p class=" pclassbk text-justify"><span class="param">Tornillos para los modulos de motor.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="tornillo grande" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tornillog.png"; >
    </div>
</div>
    
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">10 </span> tuercas</li>
                <p class=" pclassbk text-justify"><span class="param">Tuercas para sujetar los tornillos.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
       <img alt="tuerca" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/tuerca.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">4 </span> cables</li>
                <p class=" pclassbk text-justify"><span class="param">Cables para conectar tus modulos al bk7.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="cable briko" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cablebriko.png"; >
    </div>
</div>

<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> pila</li>
                <p class=" pclassbk text-justify"><span class="param">Pila para alimentar tu carrito.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="bateria" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/pila.png"; >
    </div>
</div>
    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> cable micro usb</li>
                <p class=" pclassbk text-justify"><span class="param">Cable para programar tu briko.</span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="cable micro usb" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cablemicrousb.png"; >
    </div>
</div>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
      
<!-- arreglo para guardar los mensajes del slider -->
<?php $pasos = array("1- Montar el briko de distancia.","2-: Montar el briko de distancia.","3- Montar el briko de motor al piso.","4- Montar el briko de motor al piso.","5- Insertar las llantas en los motores."," 6-: Atornillar el bk7 al piso.","7- Montar la rueda loca al piso.","8- Colocar las 2 uniones 90 en la parte frontal del piso.","9- Unir el sensor de distancia a la parte frontal del piso.","10- Montar el briko de leds.","11- Ensamblar 2 vigas en la parte trasera del piso. ", "12- Montar los leds en las vigas puestas en la parte trasera del piso.","13- Vista final.","14- Vista final en carrito real."); ?>        
        
<!-- Paso 2 -->
<!-- tab -->
<div class="content" id="panel2">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Armemos el carrito:</h1>
        </div>
    </div>
           
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns">
            <ul class="especF">
                <li>
            <p id="textslider1" class="pclassbk text-justify">Sigue los pasos de las imagenes de abajo para armar el carrito.</p>
                </li>
             </ul>
        </div>
    </div>
    
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< count($pasos); $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso2<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
      <div class="orbit-caption">
    <?php echo $pasos[$i]; ?>
    </div>
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso2<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
       <div class="orbit-caption">
    <?php echo $pasos[$i]; ?>
    </div>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
<!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 3 -->
<!-- tab -->
<div class="content" id="panel3">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Conectar los módulos y programar:</h1>
        </div>
    </div>
              
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Para poder usar la aplicación hay que conectar nuestro briko de leds en el puerto "5", el briko de motor izquierdo en el puerto "2", el briko de motor derecho en el puerto "6", y por último el briko de distancia en el puerto "4".</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Despúes de conectar todos los brikos conectamos nuestro bk7 a la computadora y le cargamos el siguiente codigo desde brikode:</p>
                </li>
            </ul>
        </div>
    </div>
        
<!-- Boton de copiar-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button id="botoncopy" class= "button round">Copiar el código</button>
        </div>
    </div>
    
<!--ponemos un editor de texto-->
    <div class="row">
        <div class="large-12 columns">
            <div id="editor">
#define Solicitud_distance 'd'
#define Solicitud_motor1 'M'
#define Solicitud_motor2 'm'
#define Solicitud_led 'L'
#define Solicitud_stop 'S'

motorbk LeftM(PORT2);   //Declara el modulo MotorBK en el puerto 1
motorbk RightM(PORT6);   //Declara el modulo MotorBK en el puerto 6
distancebk sensor(PORT4);   //Declara el modulo MotorBK en el puerto 4
ledsbk leds(PORT5);   //Declara el modulo leds en el puerto 5
int read_data_decimal = 0;
int switch_var = 0;

stringbk Data; //para recibir la data
int distance = 0;

code() {

    bk7btread(&Data, 6);

    if (Data.stringbuffer[0] > 0 && Data.stringbuffer[0] != '0' ) {
        bk7print(Data.stringbuffer[0]);
        switch (Data.stringbuffer[0]) {
            case Solicitud_led:
                read_data_decimal = String_2_decimal(Data.stringbuffer[2], Data.stringbuffer[3], Data.stringbuffer[4], Data.stringbuffer[5]);
                if (Data.stringbuffer[1] == '1') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, RED);
                    } else {
                        leds.color(RED);
                    }
                }
                if (Data.stringbuffer[1] == '2') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, BLUE);
                    } else {
                        leds.color(BLUE);
                    }
                }
                if (Data.stringbuffer[1] == '3') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, GREEN);
                    } else {
                        leds.color(GREEN);
                    }
                }
                if (Data.stringbuffer[1] == '4') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, PURPLE);
                    } else {
                        leds.color(PURPLE);
                    }
                }
                if (Data.stringbuffer[1] == '5') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, YELLOW);
                    } else {
                        leds.color(YELLOW);
                    }
                }
                if (Data.stringbuffer[1] == '6') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, BLACK);
                    } else {
                        leds.color(BLACK);
                    }
                }
                if (Data.stringbuffer[1] == '7') {
                    if (read_data_decimal != 6) {
                        leds.color(read_data_decimal, WHITE);
                    } else {
                        leds.color(WHITE);
                    }
                }
                break;

            case Solicitud_motor1:
                read_data_decimal = String_2_decimal(Data.stringbuffer[2], Data.stringbuffer[3], Data.stringbuffer[4], Data.stringbuffer[5]);
                if (Data.stringbuffer[1] == '1') {
                    LeftM.set(LEFT, read_data_decimal );
                }
                if (Data.stringbuffer[1] == '2') {
                    LeftM.set(RIGHT, read_data_decimal );
                }
                if (Data.stringbuffer[1] == '3') {
                    LeftM.set(STOP);
                }
                break;

            case Solicitud_motor2:
                read_data_decimal = String_2_decimal(Data.stringbuffer[2], Data.stringbuffer[3], Data.stringbuffer[4], Data.stringbuffer[5]);
                if (Data.stringbuffer[1] == '1') {
                    RightM.set(LEFT, read_data_decimal );
                }
                if (Data.stringbuffer[1] == '2') {
                    RightM.set(RIGHT, read_data_decimal );
                }
                if (Data.stringbuffer[1] == '3') {
                    RightM.set(STOP);
                }
                break;

            case Solicitud_distance:
                distance =  sensor.read();
                read_data_decimal = String_2_decimal(Data.stringbuffer[2], Data.stringbuffer[3], Data.stringbuffer[4], Data.stringbuffer[5]);

                if (Data.stringbuffer[1] == '1') {
                    if (distance == read_data_decimal) {
                        bk7btwrite('1');
                    } else {
                        bk7btwrite('0');
                    }
                }
                if (Data.stringbuffer[1] == '2') {
                    if (distance != read_data_decimal) {
                        bk7btwrite('1');
                    } else {
                        bk7btwrite('0');
                    }
                }
                if (Data.stringbuffer[1] == '3') {
                    if (distance >= read_data_decimal) {
                        bk7btwrite('1');
                    } else {
                        bk7btwrite('0');
                    }
                }
                if (Data.stringbuffer[1] == '4') {
                    if (distance <= read_data_decimal) {
                        bk7btwrite('1');
                    } else {
                        bk7btwrite('0');
                    }
                }
                if (Data.stringbuffer[1] == '5') {
                    if (distance > read_data_decimal) {
                        bk7btwrite('1');
                    } else {
                        bk7btwrite('0');
                    }
                }
                if (Data.stringbuffer[1] == '6') {
                    if (distance < read_data_decimal) {
                        bk7btwrite('1');
                    } else {
                        bk7btwrite('0');
                    }
                }
                break;

            case Solicitud_stop: //apaga todo
                RightM.set(STOP);
                LeftM.set(STOP);
                leds.color(BLACK);
                break;

            default:
                break;
        }

    } //end if data

    ////seguridad para que no se llene el buffer de serial
    if (Serial1.available() > 17) { //3 mensajes
        bk7btclean();
    }

    Data.stringbuffer[0] = '0'; //resetea
    delay(1); //un poco de delay

}

int String_2_decimal(char digit1, char digit2, char digit3, char digit4) {
    int Value = 0;
    int deci = ((int)(digit1 - '0'));
    Value = (deci * 1000) + Value;
    deci = ((int)(digit2 - '0'));
    Value = (deci * 100) + Value;
    deci = ((int)(digit3 - '0'));
    Value = (deci * 10) + Value;
    deci = ((int)(digit4 - '0'));
    Value = deci + Value;

    return (Value);
}
            </div>
        </div>
    </div>
    <br>
            
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 4; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso3<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso3<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
               
<!-- Paso 4 -->
<!-- tab -->
<div class="content" id="panel4">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Probemos la aplicación:</h1>
        </div>
    </div>
              
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Lo primero que tenemos que hacer es abrir la aplicación de briko. Una vez abierta nos vamos a BKCode y comenzaremos a programar.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Una vez abierta, damos click en el boton que dice "scan" y nos conectamos a el briko.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Ya que estemos conectado, con el boton "add line" podemos agregar mas lineas de codigo y con el boton de "clear lines" podemos borrar las lineas vacias.</p>
                </li>
                 <li>
            <p class="pclassbk text-justify">Con el bote de basuro borramos el contenido de una linea.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">En la aplicación se puede ver un letrero que dice "select" en esa parte seleccionaremos que modulo o instrucción deseamos usar.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Para comenzar usaremos el módulo de leds seleccionando "led color", una vez seleccionado, en el cuadro de alado seleccionamos que quieres que haga el briko (color que prendera)  y por ultimo en el tercer cuadro seleccionamos que led queramos que prenda, si lo dejamos vacio prenderan los 5 leds.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Por último hay que presionar el boton de "run" para correr el programa.</p>
                </li>
             </ul>
        </div>
    </div>
    
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 6; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso4<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso4<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 5 -->
<!-- tab -->
<div class="content" id="panel5">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Ahora movamos un motor:</h1>
        </div>
    </div>
              
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Ahora en lugar de seleccionar los leds seleccionamos "motor1 set" que movera el motor de la izquierda, en el siguiente bloque le decimos que gire a la derecha("right") y que valla a una velocidad de 255.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Si queremos usar el motor de la derecha seleccionamos "motor2 set".</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Para correr el programa presionamos nuevamente el boton de "run".</p>
                </li>
             </ul>
        </div>
    </div>
    
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 3; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso5<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso5<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>    
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 6 -->
<!-- tab -->
<div class="content" id="panel6">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Programemos el sensor de distancia:</h1>
        </div>
    </div>
      
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
               <li>
            <p class="pclassbk text-justify">Ahora combinaremos una instrucción if con el sensor de distancia y los leds en donde haremos que si el sensor detecta un objeto a menos de 15 centimetros prenda los leds en azul y si no detecta nada que prendan en rojo.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Para lograr esto primero debemos agregar 6 lineas con el boton "Add line", despues en la primera linea ponemos la instrucción "if distance" luego menor(<) y luego ponemos el número 15. En la segunda linea ponemos los leds que prendan en azul y en la tercera linea cerramos la instrucción "if distance" con "end if". </p>
                </li>
                <li>
            <p class="pclassbk text-justify">Despúes en la cuarta linea ponemos la instrucción "if distance" luego mayor igual(>=) y luego ponemos el número 15. En la quinta linea ponemos los leds que prendan en rojo y en la sexta linea cerramos la instrucción "if distance" con "end if". </p>
                </li>
                <li>
            <p class="pclassbk text-justify">Para correr el programa presionamos nuevamente el boton de "run"</p>
                </li>
             </ul>
        </div>
    </div>
    
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 2; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso6<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso6<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>   
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 7 -->
<!-- tab -->
<div class="content" id="panel7">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Unamos todo:</h1>
        </div>
    </div>
      
<!-- Explicacion general del paso-->
    <div class="row">
        <div class="small-12 columns" >
            <ul class="especF">
                <li>
            <p class="pclassbk text-justify">Por último uniremos todo lo que hemos utilizado hasta ahorita, combinaremos una instrucción if con el sensor de distancia y los leds en donde haremos que si el sensor detecta un objeto a menos de 20 centimetros prenda los leds en rojo y el robot gire y si no detecta nada que prendan en azul y el robot se siga derecho.</p>
                </li>
                <li>
            <p class="pclassbk text-justify">Para lograr esto primero debemos agregar 10 lineas con el boton "Add line", despues en la primera linea ponemos la instrucción "if distance" luego mayor igual(>=) y luego ponemos el número 20. En la segunda linea ponemos los leds que prendan en azul, en la tercera linea ponemos que el motor izquierdo gire a la derecha a una velocidad de 255, en la cuarta linea ponemos que el motor derecho gire a la izquierda a una velocidad de 255  y en la quinta linea cerramos la instrucción "if distance" con "end if". </p>
                </li>
                <li>
            <p class="pclassbk text-justify">Despues en la sexta linea ponemos la instrucción "if distance" luego menor(<) y luego ponemos el número 20. En la septima linea ponemos los leds que prendan en rojo, en la octava linea ponemos que el motor izquierdo gire a la derecha a una velocidad de 255, en la novena linea ponemos que el motor derecho gire a la derecha a una velocidad de 255  y en la décima linea cerramos la instrucción "if distance" con "end if". </p>
                </li>
                <li>
            <p class="pclassbk text-justify">Para correr el programa presionamos nuevamente el boton de "run"</p>
                </li>
             </ul>
        </div>
    </div>
    
<!--Slide show-->     
<div class="row" >
        <div class="large-12 columns" >
<ul id = "slideim1" class="example-orbit" data-orbit data-options="animation:slide;
                                                                pause_on_hover:true;
                                                                timer: false;
                                                                slide_number: false; 
                                                            navigation_arrows:true;"  >
 <?php for($i = 0; $i< 2; $i++) { ?>    
  <?php if($i !=1){ ?>  
  <center><li> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso7<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"  />
  </li></center>  
<?php }else{ ?>  
   <center><li class="active"> 
    <img src="<?php echo base_url(); ?>images/proyectopage/carritoapp/paso7<?php echo $i+1; ?>.jpg" alt="slide<?php echo $i; ?>"/>
  </li></center>   
 <?php } ?>
 <?php } ?>
</ul>
</div>
</div>
<br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center">
            <button class="back button round">Atras</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
 
</div>
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>
      
<!--modal para mandar un mensaje de error -->
<!--crea un mensaje de alerta tipo modal-->
<div id="first-modal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h2 id="modalTitle">Ups.</h2>
      <p class="lead">Hay un problema con este link.</p>
      <p>Este link no esta disponible por el momento pero nuestro equipo esta trabajando en ello, agradecemos tu comprensión.</p>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>      
        
<!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var editor = ace.edit("editor");  //liga el editor declaradoe en html
editor.getSession().setMode( xml_mode ); //pone el modo
editor.setTheme("ace/theme/brikode"); //pone el tema
editor.getSession().setTabSize(2);
editor.getSession().setUseWrapMode(true);
editor.setReadOnly(true);  // false to make it editable
</script> 

<script>
    
//detecta cuando cambias de pestana haga refresh del slider
$('#myTabs').on('toggled', function (event, tab) {
    //guarda el nombre del tab donde estas
    //vuelve a cargar los sliders para que aparescan
    $(document).foundation('orbit', 'reflow');
  });
        
//para que tambien haga refresh del slider cuando presion el boton de la clase next
$('#myTabscontainer').on('click', '.next', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').next().addClass('active');
       $(document).foundation('orbit', 'reflow');
});

//para que tambien haga refresh del slider cuando presion el boton de la clase back
$('#myTabscontainer').on('click', '.back', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').prev().addClass('active');
       $(document).foundation('orbit', 'reflow');
});
    
    
//listener para copiar el codigo en el editor de texto
$("#botoncopy").on("click",function(){ 
var Strind_editor  =editor.getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
 
$("#agregacompra").on("click",function(){  //redirige a otra pagina
    $('#first-modal').foundation('reveal','open');
});    
    
</script>

<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>