
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->      	     
	          <div class="row" style="margin-top:1rem;">	  
	            <div class="large-12 columns">
	              	<div class="row">
			      		<div class="columns large-8">
			      			<div class="row">
			      				<div class="columns large-8">
			      					<h1 id="tProy"><?php echo $info[0]->Nombre; ?></h1><!--Nombre, dificultad y tiempo que se trae de la bd -->
						      	</div>
						      	<div class="columns large-2">
			      					<img alt="Tiempo para completar el proyecto nivel <?php echo $info[0]->Tiempo; ?>" src="<?php echo base_url();?>images/T/<?php echo $info[0]->Tiempo; ?>.png" style="width: 50px; float: right;">
						      	</div>
						      	<div class="columns large-2">
			      					<img  alt="Dificultad del proyecto nivel <?php echo $info[0]->Dificultad; ?>" src="<?php echo base_url();?>images/D/<?php echo $info[0]->Dificultad; ?>.png" style="width: 50px; float: right;">
						      	</div>
				            </div>
	      					
			      		</div>
			      		<div class="columns large-4">
			      			<div class="row">
			      				<div class="columns large-6">
					      			<img src="<?php echo base_url();?>images/briko.png">
					      			
					      		</div>
					      		<div class="columns large-6">
					      			
					      			<img src="<?php echo base_url();?>images/jack.png">
					      		</div>
					      	</div>
			      		</div>
	              	</div>
	              	<div class="row">
	              		<div class="columns large-8">
							<img src="<?php echo base_url();?>images/proyectos/<?php echo $info[0]->Imagen; ?>">
							
	              		</div>
	              		
	              		<div class="columns large-4"><!--Codigo repetitivo para los mudolos del proyecto -->
	              			<?php
					            $base= base_url(); 
					            foreach ($mods as $value)
					            {
					            	echo "<div class='row'>";
							        echo "<div class='columns large-6'>";
							      	echo "<img src='".$base."images/modulos/".$value->ID_modulo.".png'>";
							      	echo "</div>";
							      	echo "<div class='columns large-6'>";
							      	echo "<img src='".$base."images/numeros/".$value->Puerto.".png' style='float: right; width: 60px; margin-right: 52px;'>";
							      	echo "</div>";
							      	echo "</div>";
					            }
					        ?>
					      	
	              		</div>
	              	</div>
	            </div>     
	          </div>
	          <div class="row">
	          	<div class="large-12 columns " style="padding-top: 2rem;">
	          		<img src="<?php echo base_url();?>images/Comments1.png" style="width:100%"><!-- Comentarios del programa-->
		          		<p id="Commts"><?php echo $info[0]->Descripcion; ?>
		          		</p>
	            </div>

	          </div>
	          <div class="row" style="margin-top:-1rem;">
	          	<div class="large-12 columns" >
	          		<img src="<?php echo base_url();?>images/CYP.png" style="width:77%">
	          	</div>
	          </div>
			  <div class="row" style="margin-bottom: 4em;" >
	          	<div class="large-12 columns" >
	          		<pre class="prettyprint linenums lang-c" ><!-- CODIGO -->
<?php echo $info[0]->Codigo; ?>
					</pre>
	            </div>
	            <div class="large-9 columns" >
	          		
	            </div>
	          </div>
     
	     
	        </div>
	        <br>
	        <br>
      </div>
		<script>
		    $(document).foundation();
		  </script>
		
		
		<script>
	      $(document).foundation();

	      var doc = document.documentElement;
	      doc.setAttribute('data-useragent', navigator.userAgent);
	    </script>
	</body>
</html>