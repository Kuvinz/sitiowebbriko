<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<div class="row principal">
	<div class="columns large-12">
		<div class="row principal videoPrincipalRow"  data-equalizer>

    	<img alt="Error 404"  src="<?php echo base_url();?>images/muchosBrikos.JPG" >

		<div class="overlay-container ">
			<div class="large-8 large-offset-2 columns overlay">
				<div class="row principal" style="text-align:center;">
	    			<h2 id="tituloE">¡Error 404!</h2>
		    	</div>
		    	<div class="row principal" style="text-align:center;">
		    		<p id="textoE"><strong>La p&aacute;gina que estabas buscando parece haber sido movida, eliminada o no existe</strong></p>
		    		<p id="textoE"><strong>Puedes regresar <a class="linkE" onclick="goBack()">a la p&aacute;gina anterior,</a> o ir a nuestra <a class="linkE" href="<?php echo base_url()?>">p&aacute;gina principal.</a></strong></p>
		    	</div>
		    	
		    </div>
		</div>
    </div>
	</div>
</div>

<script>
function goBack() {
    window.history.back();
}
</script>
<script>
    $(document).foundation();
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
  </script>
  </body>
</html>