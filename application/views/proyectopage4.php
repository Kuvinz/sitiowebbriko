<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFLKL9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFLKL9');</script>
<!-- End Google Tag Manager -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57402676-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Aplica el formato-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/proyectopage.css">
<!-- declaramos las librerias para modificar el editor, y lo modificamos -->    
<script src="<?php echo base_url(); ?>js/ace/src-min/ace.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/ace_grammar.js" ></script>
<script src="<?php echo base_url(); ?>js/ace/briko_grammar.js" ></script> 

<!--para el slide show -->
<link rel="stylesheet" href="<?php echo base_url(); ?>js/slideshow_jquery/refineslide.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>js/slideshow_jquery/refineslide-theme-briko.css" />
<script src="<?php echo base_url(); ?>js/slideshow_jquery/jquery.refineslide.js" ></script> 
<script>
$(function () {
$('.demo').refineSlide({
maxWidth: 850, // set to native image width (px)
transition            : 'cubeH',  // String (default 'cubeV'): Transition type ('custom', 'random', 'cubeH', 'cubeV', 'fade', 'sliceH', 'sliceV', 'slideH', 'slideV', 'scale', 'blockScale', 'kaleidoscope', 'fan', 'blindH', 'blindV')
fallback3d            : 'sliceV', // String (default 'sliceV'): Fallback for browsers that support transitions, but not 3d transforms (only used if primary transition makes use of 3d-transforms)
customTransitions     : [],       // Arr (Custom transitions wrapper)
perspective           : 1000,     // Perspective (used for 3d transforms)
useThumbs             : true,     // Bool (default true): <a href="http://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a> type thumbnails
useArrows             : true,    // Bool (default false): Navigation type previous and next arrows
thumbMargin           : 3,        // Int (default 3): Percentage width of thumb margin
autoPlay              : false,    // Int (default false): Auto-cycle slider
delay                 : 5000,     // Int (default 5000) Time between slides in ms
transitionDuration    : 800,      // Int (default 800): Transition length in ms
startSlide            : 0,        // Int (default 0): First slide
keyNav                : true,     // Bool (default true): Use left/right arrow keys to switch slide
captionWidth          : 50,       // Int (default 50): Percentage of slide taken by caption
arrowTemplate         :'<div class="row" ><div class="column small-2" ><div style="float: left;" class="rs-arrows"><a href="#" class="rs-prev"></a></div></div><div class="column small-2 small-offset-8"><div style="float: right;" class="rs-arrows"><a href="#" class="rs-next"></a></div></div></div>', // String: The markup used for arrow controls (if arrows are used). Must use classes '.rs-next' & '.rs-prev'
onInit                : function () {}, // Func: User-defined, fires with slider initialisation
onChange              : function () {}, // Func: User-defined, fires with transition start
afterChange           : function () {}  // Func: User-defined, fires after transition end

});
});
</script>

<!---------------------informacion de la base de datos----------------->
<!--para hacer que salgan mensajes al hacer hover en los puertos -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/tooltips/stylebriko.css" />

<!--para hacer los text area blancos y que tengan autoresize -->
<style>
.textareawhite{
background-color:#ffffff !important;
border-style: none !important ;
margin-bottom:0 !important;
box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0) !important;
}
/*los datos aqui tienen que ser iguales a los de la letra del textarea para que funcione(bueno la letra .05 mas grande para asegurar que entre) */
.hiddendiv {
    display: none;
    min-height: 50px;
    padding: 5px;
    font-size: 1.75em;
    font-family: "MAXWELLREGULAR";
    white-space: pre-wrap;
    word-wrap: break-word;
}   
</style>
<!--*****************parte de general********************** -->
<?php 
//aqui obtenemos toda la informacion del arreglo que enviamos como parametro
// --------------------informacion general ---------------------------------

$general=$info['general'][0];
$imagenes=$info['imagenes'];
$videos=$info['videos'];
$imagenesarraygeneral=array();
$videosarray=array();
foreach ($imagenes as $value)
{
    if($value['ID_crearP']==1)
        array_push($imagenesarraygeneral,$value['Imagen']);
}

foreach ($videos as $value)
{
    $url = $value['Video'];
    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
    $id = "https://www.youtube.com/embed/".$matches[1];
    array_push($videosarray,$id);
}
?>
<?php $ID_proyecto = $general['ID_Proyecto'] ?>
<?php $ID_usuario = $info['autor'][0]['Nombre']; ?>
<?php $Nombre_gen_prin = $general['Nombre'] ?>
<?php $Desc_gen_prin = $general['Descripcion'] ?>
<?php $Tiempo_gen_prin = $general['Tiempo']?>
<?php $Level_gen_prin = $general['Dificultad'] ?>

<?php $Images_gen_array = $imagenesarraygeneral; ?>
<?php $Video_gen_array = $videosarray; ?>
<!--************parte de material************************ -->
<?php // -----------------------------material ---------------------------
$modulos=$info['modulos'];
//$cantidadmodulos=array_count_values($modulos);
//$modulos=array_unique($modulos);
$modulosarray=array();
$canmod1=array();
$modarray1=array();
$puertos=array();
foreach ($modulos as $value)
{
    array_push($modulosarray,$value['ID_modulo']);
    array_push($puertos,$value['Puerto']);
}
$modarrconec=$modulosarray;
$cantidadmodulos=array_count_values($modulosarray);
foreach ($cantidadmodulos as $value) {
    array_push($canmod1,$value);   
}
$modulosarray=array_unique($modulosarray);
foreach ($modulosarray as $value) {
    array_push($modarray1,$value);   
}

echo "<br>";
?>
<?php $Modulosid_mat_array = $modarray1; ?>
<?php $Moduloscant_mat_array = $canmod1; ?>
<?php $Modulosnom_mat_array = [ "bk7","Briko de botones.","Sensor de distancia.","Briko de bocina.","Briko de leds.","Briko de perilla.","Sensor de temperatura.", "Sensor de luz.","Briko de display.","Briko de motor."]; ?>
<?php $Modulosdesc_mat_array = [ "Controlador principal de briko","Módulo de botones de briko.","Sensor de distancia de briko.","Módulo de bocina  de briko.","Módulo de leds  de briko.","Módulo de perilla  de briko.","Sensor de temperatura  de briko.", "Sensor de luz  de briko.","Módulo de display  de briko.","Módulo de motor  de briko."]; ?>
<?php $Modulosimg_mat_array = [ "bk7.png", "botones.png", "sensdis.png", "buzzer.png", "leds.png" ,"pot.png" ,"senstemp.png", "sensluz.png" ,"display.png" ,"motor.png"]; ?>

<?php
$material=$info['material'];
$materialarray=array();
$cantidadmaterialarray=array();
foreach ($material as $value) {
    array_push($materialarray,$value['ID_Pieza']);
       array_push($cantidadmaterialarray,$value['cantidad']);
}

?>
<?php $Piezasid_mat_array = $materialarray; ?>
<?php $Piezascant_mat_array = $cantidadmaterialarray; ?>
<?php $Piezasnom_mat_array = [ "", "L1","C1","O1","Y1","C2","O2","P1"]; ?>
<?php $Piezasdesc_mat_array = [ "", "Union con forma de viga.","Union cuadrada chica.","Union octagonal grande.","Union en forma de 'Y'.","Union en forma de 'C'.","Union octagonal chica.","Piso grande."]; ?>
<?php $Piezasimg_mat_array = [ "", "viga.png", "pieza1.png", "pieza2.png", "union3.png", "union2.png" ,"union1.png" ,"pieza3.png"]; ?>

<?php
$accesorios=$info['accesorios'];
$accesoriosarray=array();
$cantidadaccarray=array();
foreach ($accesorios as $value) {
    array_push($accesoriosarray,$value['ID_accesorio']);
    array_push($cantidadaccarray,$value['cantidad']);
}
?>
<?php $Accesid_mat_array = $accesoriosarray; ?>
<?php $Accescant_mat_array = $cantidadaccarray; ?>
<?php $Accesnom_mat_array = [ "", "Llanta","Rueda loca","Tornillo chico","Tornillo grande","Cable briko","Tuerca","Remache", "Cable micro usb","Bateria"]; ?>
<?php $Accesdesc_mat_array = [ "", "Llanta de briko.","Rueda loca de briko.","Tornillo chico de briko.","Tornillo grande de briko.","Cable de briko.","Tuerca de briko.","Remache de briko.", "Cable micro usb largo.","bateria de briko."]; ?>
<?php $Accesimg_mat_array = [ "", "llanta.png", "ruedaloca.png", "tornilloch.png", "tornillog.png", "cablebriko.png" ,"tuerca.png" ,"remache.png", "cablemicrousb.png" , "pila.png"] ; ?>

<?php
$adjuntos=$info['adjuntos'];
$adjarray=array();
$nomadjarray=array();
$desadjarray=array();

foreach ($adjuntos as $value) {
    if($value['tipo_adjunto']==0)
    {
        array_push($adjarray,$value['Archivo']);
        array_push($nomadjarray,$value['Nombre']);
        array_push($desadjarray,$value['Descripcion']);
    }
}
?>
<?php $Extranom_mat_array = $nomadjarray; ?>
<?php $Extradesc_mat_array = $desadjarray; ?>
<?php $Extraimg_mat_array = [ "extranueva.png"]; ?>
<?php $Extraimg_file_array = $adjarray; ?>

<!--************parte de construir************************ -->
<?php
$textos=$info['textos'];
$imagenesarraycon=array();
$descG;
$descPaso=array();
//print_r($textos);
foreach ($imagenes as $value)
{
    if($value['ID_crearP']==3)
        array_push($imagenesarraycon,$value['Imagen']);
}

foreach ($textos as $value) {

    if($value['ID_crearP']==3)
    {
        if($value['Paso']==0)
            $descG=$value['Texto'];
        else
        {
            array_push($descPaso,$value['Texto']);
        }
        
    }
}

?>

 <?php $Desc_const_prin = $descG; ?>
 <?php $Desc_constp_prin = $descPaso; ?>
 <?php $Img_constp_prin = $imagenesarraycon; ?>

<!--************parte de conectar************************ -->
<?php $Modulosid_conect_array = $modarrconec; ?>
<?php $Puertos_conect_array = $puertos; ?>
<?php $Modulosimg_conect_array = [ "bk7.png", "botones.png", "sensdis.png", "buzzer.png", "leds.png" ,"pot.png" ,"senstemp.png", "sensluz.png" ,"display.png" ,"motor.png"]; ?>

<!--************parte de construir************************ -->
<?php
$codigos=$info['codigo'];
//print_r($codigos);
$descGPro;
$descPasoPro=array();
$codigosarray=array();
//print_r($textos);
foreach ($codigos as $value)
{
    array_push($codigosarray,$value['Codigo']);
}

foreach ($textos as $value) {

    if($value['ID_crearP']==5)
    {
        if($value['Paso']==0)
            $descGPro=$value['Texto'];
        else
        {
            array_push($descPasoPro,$value['Texto']);
        }
        
    }
}

?>
 <?php $Desc_prog_prin = $descGPro ?>
 <?php $Desc_progp_prin = $descPasoPro; ?>
 <?php $Codigo_progp_prin = $codigosarray; 
 ?>

<!------------------------------------------------------------------------------>    
<div class="row"  id="div1">
  <div class="large-12  columns">

<!-- informacion de la pestana de general -->
<!--Slide show, titulo principal y descripcion-->     
<div class="row" >
        <div class="small-6 columns">
               
<ul class="demo">
<?php for($i = 0; $i< count($Images_gen_array); $i++) { ?>
<li class="group" style="z-index: 1; opacity: 0;"> 
 <a href="#"><center><img alt="proyecto" width="400" style="opacity: 1;" src="<?php echo base_url(); ?>uploads/<?php echo $ID_proyecto ?>/<?php echo $Images_gen_array[$i] ?>" alt="slide<?php echo $i; ?>"  /></center></a>
</li>
<?php } ?>
<?php for($j = $i; $j< (count($Video_gen_array) + $i); $j++) { ?> 
<li class="group">
  <div class="flex-video">
<img alt="boton play video" style="opacity: 1; display: none;" src="<?php echo base_url(); ?>images/proyectopage/iconosextra/play.png" />
      <center><iframe width="400" src="<?php echo $Video_gen_array[$j-$i] ?>" frameborder="0" allowfullscreen></iframe></center>
  </div>
  </li>
<?php } ?>

</ul>  
</div>


<div class="small-6 end columns" >
    <h1 class="h1classbk" style="
    margin-bottom: 0px;"><?php echo  $Nombre_gen_prin ?></h1>
    <h2 class="h2classbk" style="
    margin-top: 0px;">por <?php echo  $ID_usuario  ?></h2>
    <span class='label desKY'> Descripci&oacute;n: </span>
        <p class="pclassbk text-justify"><?php echo  $Desc_gen_prin ?></p>
    <div class="small-2 columns" >
    <p style="text-align:center !important" class="pclassbk text-justify">Tiempo</p>
    <center><img alt="Tiempo del proyecto nivel <?php echo  $Tiempo_gen_prin ?>" width= "100%"src="<?php echo base_url(); ?>images/Time<?php echo  $Tiempo_gen_prin ?>-120.png"; ></center>
    </div>
    <div class="small-2 columns" >
    <p style="text-align:center !important" class="pclassbk text-justify">Nivel</p>
    <center><img alt="Dificultad del proyecto nivel <?php echo  $Level_gen_prin ?>" width= "100%"src="<?php echo base_url(); ?>images/Level<?php echo  $Level_gen_prin ?>-120.png"; ></center>
    </div>
    <!--division vacia si no hay boton de teach -->
    <div class="small-8 end columns" >
    <!-- Botones-->
        <center><button style="margin-top:30px !important" id="button_teach" class="button round">EDU</button></center>
    </div>
    <script>
        $("#button_teach").on("click",function(){  //redirige a otra pagina
            window.open("<?php echo base_url(); ?>index.php/edu/<?php echo $ID_proyecto; ?>","_self");
        });
    </script>
</div>
</div>
      
<!-- cintillo se separacion -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <img alt="briko cintillo" width= "200%"src="<?php echo base_url(); ?>images/proyectopage/modulosimages/cintillo2.png"; >
        </div>
    </div>
            
<!-- titulo "pasos" -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Pasos del proyecto:</h1>
        </div>
    </div>
      
<!-- tabs para entrar a diferentes funciones -->
    <div class="row">
        <div class="small-12 columns ">
            <ul class="tabs" id="myTabs" data-tab>
                <li class="tab-title active"><a href="#panel1">Material</a></li>
                <li class="tab-title"><a href="#panel2">Construir</a></li>
                <li class="tab-title"><a href="#panel3">Conectar</a></li>
                <li class="tab-title"><a href="#panel4">Programar</a></li>
            </ul>
        </div>
    </div>
      
<!-- Crea division donde se guardan todos los tabs -->
    <div class="tabs-content" id="myTabscontainer">
      
<!------------------------------------------------------------------------------>
      
<!-- Paso 1 -->
<!-- tab -->
<div class="content active" id="panel1">
    <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Material:</h1>
        </div>
    </div>
    
<!-- titulode agreagar modulos a comprar -->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <p class=" pclassbk">Agrega las piezas de briko utilizadas en este proyecto a tu carrito de compra:</p>
        </div>
    </div>
    
    <!-- Boton de compra-->
    <div class="row">
        <div class="small-12 columns" >
            <center><button id="agregacompra" class= "button round">Agregar</button></center>
        </div>
    </div>
    
<!--piezas del proyecto -->
    
<!--maestro -->    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> <?php echo  $Modulosnom_mat_array[0] ?></li>
                <p class=" pclassbk text-justify"><span class="param"><?php echo  $Modulosdesc_mat_array[0] ?></span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="brikos" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo  $Modulosimg_mat_array[0] ?>"; >
    </div>
</div>    
    
<!--modulos-->   
<?php for($j = 0; $j< (count($Modulosid_mat_array)); $j++) { ?>     
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun"><?php echo  $Moduloscant_mat_array[$j] ?>  </span> <?php echo  $Modulosnom_mat_array[intval($Modulosid_mat_array[$j])] ?></li>
            <p class=" pclassbk text-justify"><span class="param"><?php echo  $Modulosdesc_mat_array[intval($Modulosid_mat_array[$j])] ?></span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="brikos" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo  $Modulosimg_mat_array[intval($Modulosid_mat_array[$j])] ?>"; />
    </div>
</div>
<?php } ?> 
    
<!--piezas mecanicas-->
<?php for($j = 0; $j< (count($Piezasid_mat_array)); $j++) { ?>     
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun"><?php echo  $Piezascant_mat_array[$j] ?>  </span> <?php echo  $Piezasnom_mat_array[intval($Piezasid_mat_array[$j])] ?></li>
            <p class=" pclassbk text-justify"><span class="param"><?php echo  $Piezasdesc_mat_array[intval($Piezasid_mat_array[$j])] ?></span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="piezas mecanicas" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo  $Piezasimg_mat_array[intval($Piezasid_mat_array[$j])] ?>"; />
    </div>
</div>
<?php } ?> 
    
<!--accesorios-->
 <?php for($j = 0; $j< (count($Accesid_mat_array)); $j++) { ?>     
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun"><?php echo  $Accescant_mat_array[$j] ?>  </span> <?php echo  $Accesnom_mat_array[intval($Accesid_mat_array[$j])] ?></li>
            <p class=" pclassbk text-justify"><span class="param"><?php echo  $Accesdesc_mat_array[intval($Accesid_mat_array[$j])] ?></span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="accesorios" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo  $Accesimg_mat_array[intval($Accesid_mat_array[$j])] ?>"; />
    </div>
</div>
<?php } ?> 
    
<!--extras-->
 <?php for($j = 0; $j< (count($Extranom_mat_array)); $j++) { ?> 
 <?php if($Extranom_mat_array[$j]!=""){ ?>    
<div class="row">
    <div class="small-7 columns small-offset-2" >
        <ul class="especF">
            <li class="libk"><span class="numfun">1 </span> <?php echo  $Extranom_mat_array[$j] ?></li>
            <p class=" pclassbk text-justify"><span class="param"><?php echo  $Extradesc_mat_array[$j] ?></span></p>
        </ul>
    </div>
    <div class="small-3 end columns" >
        <img alt="briko material extra imagen" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo  $Extraimg_mat_array[0] ?>"; />
    </div>
    <?php if($Extraimg_file_array[$j] != ""){ ?>
    <div class="row">
    <div class="small-12 columns" >
    <center><a href="<?php echo base_url(); ?>uploads/<?php echo $ID_proyecto ?>/<?php echo  $Extraimg_file_array[$j] ?>" download="<?php echo  $Extraimg_file_array[$j] ?>"><button class= "round">Descargar archivo</button></a></center>
        </div>
    </div>
    <?php } ?> 
</div>
<?php } ?> 
<?php } ?> 
    
    <br>
    
    <!-- Botones-->
    <div class="row">
        <div class="small-12 columns" >
            <center><button class= "next button round">Siguiente</button></center>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
       
<!-- Paso 2 -->
<!-- tab -->
<div class="content" id="panel2">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Construir:</h1>
        </div>
    </div>
    
     <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <p class=" pclassbk"><?php echo $Desc_const_prin ?></p>
        </div>
    </div>
    
 <?php for($i = 0; $i< (count($Desc_constp_prin)); $i++) { ?>             
<!-- cada paso-->
    <div class="row">
        <div class="small-12 columns">
            <h2 class="h2classbk">Paso <?php echo $i+1 ?>:</h2>
            <ul class="especF">
            <li>
            <p id="textslider1" class="pclassbk"><?php echo $Desc_constp_prin[$i] ?></p>
                </li>
             </ul>
        </div>
    </div>
    
    <?php if($Img_constp_prin[$i] != ""){ ?>
    <div class="row">
        <div class="small-12 columns">
            <center><img alt="imagen proyecto" src="<?php echo base_url(); ?>uploads/<?php echo $ID_proyecto ?>/<?php echo $Img_constp_prin[$i] ?>" /></center>
        </div>
    </div>
    <?php } ?> 
 
 <?php } ?>     
 
<br>
    
<!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>
        
<!-- Paso 3 -->
<!-- tab -->
<div class="content" id="panel3">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Conecta:</h1>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <p class=" pclassbk">Conecta tus brikos como se muestra a continuación:</p>
        </div>
    </div>
 
    <div class="row tt-wrapper" >  
    <div class="small-2 small-offset-1 end columns">
        <?php for($i = 1; $i<= 4; $i++) { ?> 
        <div class="div_hover">
        <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3"><?php echo($i) ?></span></label>
        <?php $varx =-1; ?>
        <?php for($j = 0; $j< (count($Puertos_conect_array)); $j++) { if($Puertos_conect_array[$j]==$i) { $varx = $j;} } ?> 
        <?php if( $varx >=0) { ?>
           <center><img alt="brikos" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo $Modulosimg_conect_array[intval($Modulosid_conect_array[$varx])] ?>" /></center> 
        <span class="tooltiplabel"><?php echo $Modulosnom_mat_array[intval($Modulosid_conect_array[$varx])];?></span>
        <?php }else { ?>
        <center><img  style="padding-bottom:10px" width="30%" src="<?php echo base_url(); ?>images/proyectopage/iconosextra/numeros/num<?php echo $i ?>.png" /></center>
        <span class="tooltiplabel">No hay briko conectado</span>
        <?php } ?> 
        </div>
        <?php } ?> 
        </div>  
       
        <div class="small-6 end columns">
        <center><img alt="briko maestro numeros" style="margin-top: 150px;" width="50%" src="<?php echo base_url(); ?>images/proyectopage/iconosextra/bk7/bk7numeros700.png"; ></center>
    </div> 
        
      <div class="small-2 end columns">
        <?php for($i = 5; $i<= 7; $i++) { ?> 
          <div class="div_hover">
        <label style="color:#ffffff;text-align:center;margin-bottom: 10px;" class="desKY" >Puerto <span class="numberspan spanbkspe3"><?php echo($i) ?></span></label>
        <?php $varx =-1; ?>
        <?php for($j = 0; $j< (count($Puertos_conect_array)); $j++) { if($Puertos_conect_array[$j]==$i) { $varx = $j;} } ?> 
        <?php if( $varx >=0) { ?>
           <center><img alt="brikos" src="<?php echo base_url(); ?>images/proyectopage/modulosimages/<?php echo $Modulosimg_conect_array[intval($Modulosid_conect_array[$varx])] ?>" /></center>  
        <span class="tooltiplabel2"><?php echo $Modulosnom_mat_array[intval($Modulosid_conect_array[$varx])];?></span>
        <?php }else { ?>
        <center><img alt="brikos numeros" style="padding-bottom:10px" width="30%" src="<?php echo base_url(); ?>images/proyectopage/iconosextra/numeros/num<?php echo $i ?>.png" /></center>
        <span class="tooltiplabel2">No hay briko conectado</span>
        <?php } ?>  
        </div>
        <?php } ?> 
        </div>
    </div> 


    
  <br>      
    <!-- Botones-->
    <div class="row">
        <div class="small-3 columns small-offset-2" >
            <button class="back button round">Atras</button>
        </div>
        <div class="small-2 end small-offset-3 columns">
            <button class= "next button round">Siguiente</button>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>

<!-- arreglo para guardar la configuracion de los editores de codigo -->
<script>
var editor= new Array();
</script>
<!-- Paso 4 -->
<!-- tab -->
<div class="content" id="panel4">
     <!--titulo-->
    <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <h1 class="h1classbk">Programa:</h1>
        </div>
    </div>
          
 <div class="row">
        <div class="small-12 columns" style="text-align:center" >
            <p style="text-align:center;" class="pclassbk textareawhite"><?php echo $Desc_prog_prin ?></p>
        </div>
    </div>
 <script>
////agrega una div invisible que toma el texto y hace resize del text area
$(function() {
    var txt = $('#programtextareades'),
        hiddenDiv = $(document.createElement('div')),
        content = '<br>';
    
    hiddenDiv.addClass('hiddendiv');

    $('body').append(hiddenDiv);
    content = content +  txt.val();
    content = content.replace(/\n/g, '<br>');
    content = content + '<br>';
    hiddenDiv.html(content + '<br class="lbr">');
    txt.css('height', hiddenDiv.height());
    $('body').remove(".hiddendiv");
});
</script>    
 <?php for($i = 0; $i< (count($Desc_progp_prin)); $i++) { ?>             
<!-- cada paso-->
    <div class="row">
        <div class="small-12 columns">
            <h2 class="h2classbk">Paso <?php echo $i+1 ?>:</h2>
            <p style="text-align:justify;" class="pclassbk textareawhite"><?php echo $Desc_progp_prin[$i] ?></p>
        </div>
    </div>
<script>
////agrega una div invisible que toma el texto y hace resize del text area
$(function() {
    var txt = $('#progtextareai<?php echo $i ?>'),
        hiddenDiv = $(document.createElement('div')),
        content = '<br>';
    
    hiddenDiv.addClass('hiddendiv');

    $('body').append(hiddenDiv);
    content = content +  txt.val();
    content = content.replace(/\n/g, '<br>');
    content = content + '<br>';
    hiddenDiv.html(content + '<br class="lbr">');
    txt.css('height', hiddenDiv.height());
    $('body').remove(".hiddendiv");
});
</script>      

    <?php if($Codigo_progp_prin[$i] != ""){ ?>
    
    <!--ponemos un editor de texto-->
    <div class="row">
        <div class="large-12 columns">
            <div id="editorp<?php echo $i ?>">
<?php echo $Codigo_progp_prin[$i] ?>
            </div>
        </div>
    </div>
    
    <!-- Boton de copiar-->
    <div class="row">
        <div class="small-12 columns">
            <center><button id="botoncopy<?php echo $i ?>" class= "button round">Copiar código</button></center>
        </div>
    </div>
    
    <!--Se modifican las variables del editor de la pagina principal-->
<script>
////colores se cambian en el theme, y los key words se agegan en el briko_grammar
var xml_mode = AceGrammar.getMode(xml_grammar); //obtiene la informacion del briko grammar 
var numx = parseInt("<?php echo $i ?>");
editor[numx] = ace.edit("editorp<?php echo $i ?>");  //liga el editor declaradoe en html
editor[numx].getSession().setMode(xml_mode); //pone el modo
editor[numx].setTheme("ace/theme/brikode"); //pone el tema
editor[numx].getSession().setTabSize(2);
editor[numx].getSession().setUseWrapMode(true);
editor[numx].setReadOnly(true);  // false to make it editable
</script> 

<script>
//listener para copiar el codigo en el editor de texto
$("#botoncopy<?php echo $i ?>").on("click",function(){ 
var numx = parseInt("<?php echo $i ?>");
var Strind_editor  =editor[numx].getValue();
copyToClipboard(Strind_editor);
}); 
//funcion para mandar string al clipboard
function copyToClipboard(element) {
///crea un temporal text area en el body para guardar el texto, seleccionarlo, y copiarlo
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
}
</script>
    
<style>
#editorp<?php echo $i ?> { 
  margin-left: 15px;
  margin-top: 15px;
  height: 400px;
 font-size: 25px;
}    
</style>
 <?php } ?>   
 <?php } ?>    
 
<br>
      
    <!-- Botones-->
    <div class="row">
        <div class="small-12 columns">
            <center><button class="back button round">Atras</button></center>
        </div>
    </div>
    
</div>
<!------------------------------------------------------------------------------>

</div>
<!-- terminacion de las divisiones principales-->
</div>
 <br>
 <br>
</div>
      
<!--modal para mandar un mensaje de error -->
<!--crea un mensaje de alerta tipo modal-->
<div id="first-modal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h2 id="modalTitle">Ups.</h2>
      <p class="lead">Hay un problema con este link.</p>
      <p>Este link no esta disponible por el momento pero nuestro equipo esta trabajando en ello, agradecemos tu comprensión.</p>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
        


<script>
    
//detecta cuando cambias de pestana
$('#myTabs').on('toggled', function (event, tab) {
    //guarda el nombre del tab donde estas
    //vuelve a cargar los sliders para que aparescan
    $(document).foundation('orbit', 'reflow');
  });
        
$('#myTabscontainer').on('click', '.next', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').next().addClass('active');
       $(document).foundation('orbit', 'reflow');
});
    
$('#myTabscontainer').on('click', '.back', function(event) {
       event.preventDefault();
       $('.active').removeClass('active').prev().addClass('active');
       $(document).foundation('orbit', 'reflow');
});
    
$("#agregacompra").on("click",function(){  //redirige a otra pagina
    $('#first-modal').foundation('reveal','open');
});
 
</script>

<!-- librerias de foundation -->
<script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>

    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
      </script>

  </body>
</html>