<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="shortcut icon" href="<?php echo base_url();?>images/brikon.ico?521017215"/>
  <title>briko</title>
    
<!--Aqui vamos a incluir los cripts de la libreria de blocky de google (los bloques y su funcionalidad, todos los cores de los bloques de blocky)-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/blockly_compressed.js"></script>
<!--este tiene la informacion de todos los bloques-->
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/blocks_compressed.js"></script>
<!--aqui dentro se incluyen las librerias para generar el codigo -->
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/arduino_compressed.js"></script>
<!--esta libreria es para poner el idioma en ingles -->
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/msg/js/en.js"></script>
<!--estas son para dar funcionalidad a la pagina-->
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/apps/blocklyduino/Blob.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/apps/blocklyduino/spin.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/apps/blocklyduino/FileSaver.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>js/blocky/apps/blocklyduino/blockly_helper.js"></script>
    
<!--librerias de jquery-->
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="<?php echo base_url();?>js/vendor/jquery.js"></script>

    
<!--*****NOTA*******
Para cambiar al .briko se modifico el archivo blockly_helper.js
-->
      
<style>
.button_round{
    background-color: #008CBA;
    color: white;
    box-shadow: none !important;
    opacity: 1;
}

.button_round:hover{
    opacity: .6;
}
    
</style>
    
<script>
    
/**
 * List of tab names.
 * @private
 */
var TABS_ = ['blocks', 'brikode', 'xml'];

var selected = 'blocks';

/**
 * Switch the visible pane when a tab is clicked.
 * @param {string} clickedName Name of tab clicked.
 */
function tabClick(clickedName) {
  // If the XML tab was open, save and render the content.
  if (document.getElementById('tab_xml').className == 'tabon') {
    var xmlTextarea = document.getElementById('content_xml');
    var xmlText = xmlTextarea.value;
    var xmlDom = null;
    try {
      xmlDom = Blockly.Xml.textToDom(xmlText);
    } catch (e) {
      var q =
          window.confirm('Error parsing XML:\n' + e + '\n\nAbandon changes?');
      if (!q) {
        // Leave the user on the XML tab.
        return;
      }
    }
    if (xmlDom) {
      Blockly.mainWorkspace.clear();
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xmlDom);
    }
  }

  if (document.getElementById('tab_blocks').className == 'tabon') {
    Blockly.mainWorkspace.setVisible(false);
  }
  // Deselect all tabs and hide all panes.
  for (var i = 0; i < TABS_.length; i++) {
    var name = TABS_[i];
    document.getElementById('tab_' + name).className = 'taboff';
    document.getElementById('content_' + name).style.visibility = 'hidden';
  }

 // Select the active tab.
  selected = clickedName;
  document.getElementById('tab_' + clickedName).className = 'tabon';
  // Show the selected pane.
  document.getElementById('content_' + clickedName).style.visibility =
      'visible';
  renderContent();
  if (clickedName == 'blocks') {
    Blockly.mainWorkspace.setVisible(true);
  }
  Blockly.fireUiEvent(window, 'resize');
}

/**
 * Populate the currently selected pane with content generated from the blocks.
 */
function renderContent() {
  var content = document.getElementById('content_' + selected);
  // Initialize the pane.
  if (content.id == 'content_blocks') {
    // If the workspace was changed by the XML tab, Firefox will have performed
    // an incomplete rendering due to Blockly being invisible.  Rerender.
    Blockly.mainWorkspace.render();
  } else if (content.id == 'content_xml') {
    var xmlTextarea = document.getElementById('content_xml');
    var xmlDom = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
    xmlTextarea.value = xmlText;
    xmlTextarea.focus();
  /*} else if (content.id == 'content_javascript') {
    content.innerHTML = Blockly.JavaScript.workspaceToCode();
  } else if (content.id == 'content_dart') {
    content.innerHTML = Blockly.Dart.workspaceToCode();
  } else if (content.id == 'content_python') {
    content.innerHTML = Blockly.Python.workspaceToCode();*/
  } else if (content.id == 'content_brikode') {
    //content.innerHTML = Blockly.Arduino.workspaceToCode();
    var arduinoTextarea = document.getElementById('content_brikode');
    arduinoTextarea.value = Blockly.Arduino.workspaceToCode();
    arduinoTextarea.focus();
  }
}

/**
 * Compute the absolute coordinates and dimensions of an HTML element.
 * @param {!Element} element Element to match.
 * @return {!Object} Contains height, width, x, and y properties.
 * @private
 */
function getBBox_(element) {
  var height = element.offsetHeight;
  var width = element.offsetWidth;
  var x = 0;
  var y = 0;
  do {
    x += element.offsetLeft;
    y += element.offsetTop;
    element = element.offsetParent;
  } while (element);
  return {
    height: height,
    width: width,
    x: x,
    y: y
  };
}

/**
 * Initialize Blockly.  Called on page load.
 */
function init() {
  //window.onbeforeunload = function() {
  //  return 'Leaving this page will result in the loss of your work.';
  //};

  var container = document.getElementById('content_area');
  var onresize = function(e) {
    var bBox = getBBox_(container);
    for (var i = 0; i < TABS_.length; i++) {
      var el = document.getElementById('content_' + TABS_[i]);
      el.style.top = bBox.y + 'px';
      el.style.left = bBox.x + 'px';
      // Height and width need to be set, read back, then set again to
      // compensate for scrollbars.
      el.style.height = bBox.height + 'px';
      el.style.height = (2 * bBox.height - el.offsetHeight) + 'px';
      el.style.width = bBox.width + 'px';
      el.style.width = (2 * bBox.width - el.offsetWidth) + 'px';
    }
    // Make the 'Blocks' tab line up with the toolbox.
    if (Blockly.mainWorkspace.toolbox_.width) {
      document.getElementById('tab_blocks').style.minWidth =
          (Blockly.mainWorkspace.toolbox_.width - 38) + 'px';
          // Account for the 19 pixel margin and on each side.
    }
  };
  window.addEventListener('resize', onresize, false);

  var toolbox = document.getElementById('toolbox');
  ///aqui se agrega el toolbox a la pagina
  Blockly.inject(document.getElementById('content_blocks'),
      {grid:
          {spacing: 25,
           length: 3,
           colour: '#ccc',
           snap: true},
       media: 'http://briko.cc/js/blocky/media/',  //hay que subirlo a la pagina para que funcione
       toolbox: toolbox});

  auto_save_and_restore_blocks();

  //load from url parameter (single param)
  //http://stackoverflow.com/questions/2090551/parse-query-string-in-javascript
  var dest = unescape(location.search.replace(/^.*\=/, '')).replace(/\+/g, " ");
  if(dest){
    load_by_url(dest);
  }
}
  </script>
  <style>
    html, body {
      height: 100%;
    }
    body {
      background-color: #fff;
      font-family: sans-serif;
      margin: 0;
      overflow: hidden;
    }
    h1 {
      font-weight: normal;
      font-size: 140%;
      margin-left: 5px;
      margin-right: 5px;
    }

    /* Tabs */
    #tabRow>td {
      border: 1px solid #ccc;
    }
    td.tabon {
      border-bottom-color: white !important;
      background-color: #008ACA;
      padding: 5px 19px;
      opacity:1;
      color:white;
    }
    td.taboff {
      cursor: pointer;
      padding: 5px 19px;
      opacity:1;
      color:black;
    }
    td.taboff:hover {
      background-color: #008ACA;
      opacity:.6;
      color:white;
    }
    td.tabmin {
      border-top-style: none !important;
      border-left-style: none !important;
      border-right-style: none !important;
    }
    td.tabmax {
      border-top-style: none !important;
      border-left-style: none !important;
      border-right-style: none !important;
      width: 99%;
      text-align: right;
    }

    table {
      border-collapse: collapse;
      margin: 0;
      padding: 0;
      border: none;
    }
    td {
      padding: 0;
      vertical-align: top;
    }
    .content {
      visibility: hidden;
      margin: 0;
      padding: 1ex;
      position: absolute;
      direction: ltr;
    }
    pre.content {
      overflow: scroll;
    }
    #content_blocks {
      padding: 0;
    }
    .blocklySvg {
      border-top: none !important;
    }
    #content_xml {
      resize: none;
      outline: none;
      border: none;
      font-family: monospace;
      overflow: scroll;
    }
    button {
      padding: 1px 1em;
      font-size: 90%;
      border-radius: 4px;
      border: 1px solid #ddd;
      background-color: #eee;
      color: black;
    }
    button.launch {
      border: 1px solid #d43;
      background-color: #d43;
      color: white;
    }
    button:active {
      border: 1px solid blue !important;
    }
    button:hover {
      box-shadow: 2px 2px 5px #888;
    }
  </style>
</head>
<body onload="init()">
  <table height="100%" width="100%">
    <tr>
      <td>
        <h1><a href="http://briko.cc/"><img alt="Logo briko" style="width:10%" src="<?php echo base_url();?>images/LogoBriko250.png" ></a></h1>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%">
          <tr id="tabRow" height="1em">
            <td id="tab_blocks" class="tabon" onclick="tabClick('blocks')">Blocks</td>
            <td class="tabmin">&nbsp;</td>
            <td id="tab_brikode" class="taboff" onclick="tabClick('brikode')">Brikode</td>
            <td class="tabmin">&nbsp;</td>  <!--ahorita esta no se vera-->
            <td style="display: none !important; border-color:#ffffff !important;" id="tab_xml" class="taboff" onclick="tabClick('xml')">XML</td>
            <td class="tabmax">
              <button class="button_round"  type="button" onclick="uploadClick()">Upload</button>
              <button class="button_round" type="button" onclick="resetClick()">Reset</button>
              <button class="button_round" onclick="discard()">Discard</button>
              <button class="button_round" onclick="saveCode()">Save briko Code</button>
              <button class="button_round" onclick="save()">Save Blocks</button>
              <button class="button_round" id="fakeload">Load Blocks</button>
              <input class="button_round" type="file" id="load" style="display: none;"/>
              <!--button class="launch" onclick="runJS()">Run Program</button-->
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="99%" id="content_area"></td>
    </tr>
  </table>
  <div id="content_blocks" class="content"></div>
  <textarea id="content_brikode" class="content" readonly wrap="off"></textarea>
  <textarea id="content_xml" class="content" wrap="off"></textarea>

  <xml id="toolbox" style="display: none">
    <category name="Logic">
      <block type="controls_if"></block>
      <block type="logic_compare"></block>
      <block type="logic_operation"></block>
      <block type="logic_negate"></block>
    </category>
    <category name="Control">
      <block type="base_delay">
        <value name="DELAY_TIME">
          <block type="math_number">
            <field name="NUM">1000</field>
          </block>
        </value>
      </block>
      <block type="controls_for">
        <value name="FROM">
          <block type="math_number">
            <field name="NUM">1</field>
          </block>
        </value>
        <value name="TO">
          <block type="math_number">
            <field name="NUM">10</field>
          </block>
        </value>
      </block>
      <block type="controls_whileUntil"></block>
    </category>
    <category name="Math">
      <block type="math_number"></block>
      <block type="math_arithmetic"></block>
      <block type="base_map">
        <value name="DMAX">
          <block type="math_number">
            <field name="NUM">180</field>
          </block>
        </value>
      </block>
    </category>
    <category name="Variables" custom="VARIABLE"></category>
    <category name="Functions" custom="PROCEDURE"></category>
    <sep></sep>
    <category name="Brikos">
      <block type="ledsbk_m"></block>
      <block type="displaybk_m"></block>
      <block type="buzzerbk_m"></block>
      <block type="buttonsbk_m"></block>
      <block type="lightbk_m"></block>        
      <block type="motorbk_m"></block>
      <block type="knobbk_m"></block>
      <block type="distancebk_m"></block> 
      <block type="temperaturebk_m"></block>
      <block type="relaybk_m"></block>
      <block type="servobk_m"></block>
    </category>
    <category name="LEDS">
      <block type="color"></block>
      <block type="set_color"></block>
      <block type="set_color_num"></block>
      <block type="set_color_num2"></block>
      <block type="set_color_num3"></block>
      <block type="led_num_led"></block>
      <block type="set_255_val_led"></block>
      <block type="brightness"></block>
      <block type="set_255_val_led"></block>
    </category>
      </category>
      <category name="Display">
      <block type="dis_print"></block>
      <block type="set_dis_print"></block>
      <block type="dis_printindi"></block>
      <block type="set_dis_printindi"></block>
      <block type="set_segment_num"></block>
      <block type="set_dis_num"></block>
      <block type="dis_printcust"></block>
      <block type="set_dis_printcust"></block>
      <block type="set_segmentcust_num"></block>
      <block type="set_discust_num"></block>
      <block type="dis_erase"></block>
      <block type="set_segmentcust_num"></block>
    </category>
    <category name="Buzzer">
      <block type="buzzer_set"></block>
      <block type="set_buzzer_set_inp"></block>
      <block type="buzzer_beep"></block>
      <block type="set_buzzer_beep"></block>
      <block type="set_buzzer_beep_inp"></block>
      <block type="buzzer_playtone"></block>
      <block type="set_buzzer_playtone"></block>
      <block type="set_buzzer_playtone2"></block>
      <block type="set_buzzer_playtone_inp"></block>
      <block type="set_buzzer_beep_inp"></block>
      <block type="buzzer_play"></block>
      <block type="set_buzzer_play"></block>
      <block type="set_buzzer_playtone_inp"></block>
      <block type="set_buzzer_beep_inp"></block>
    </category>
    <category name="Buttons">
      <block type="boton_read"></block>
      <block type="boton_read_set_inp"></block>
      <block type="boton_readbits"></block>
    </category>
    <category name="Light sensor">
      <block type="Luz_read"></block>
    </category>
    <category name="Motor">
      <block type="motor_set"></block>
      <block type="set_motor_set"></block>
      <block type="set_motor_set2"></block>
      <block type="set_motor_set_inp"></block>
      <block type="set_motor_set_inp2"></block>
    </category>
    <category name="Knob">
      <block type="perilla_read"></block>
    </category>
    <category name="Distance sensor">
      <block type="distance_read"></block>
      <block type="distance_read_inp"></block>
    </category>
    <category name="Temperature">
      <block type="temperature_read"></block>
    </category>
    <category name="Relay">
      <block type="relay_set"></block>
      <block type="set_relay_set_inp"></block>
    </category>
    <category name="Servo">
      <block type="servo_set"></block>
      <block type="set_servo_set_inp"></block>
    </category>
    <category name="bk7 basics">
      <block type="maestro_led"></block>
      <block type="set_maestro_led_inp"></block>
      <block type="maestro_write"></block>
      <block type="maestro_print"></block>
      <block type="text"></block>
      <block type="maestro_read"></block>
      <block type="maestro_read2"></block>
      <block type="maestro_read2_ifvar"></block>
      <block type="maestro_clean"></block>
    </category>

  </xml>  
    
</body>
</html>