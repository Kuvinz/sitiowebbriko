
<div class="row principal" >
  	<div class="large-12 columns">
  		<div class="row principal" id="FootT" data-equalizer>
  			<div class="medium-2 small-offset-1 columns">
  				<br>
  				<h1 id="siteF">SiteMap</h1>
  				<ul class="no-bullet" id="ulLinks">
  				  <li>Comienza Ahora
              <ul>
                <li id="instala">Instalación</li>
                <li id="primer">Mi primer programa</li>
                <li id="modulo">Módulos</li>
              </ul>
            </li>
  				  <li><a href="<?php echo base_url();?>proyectos">Proyectos</a></li>
  				  <li>Referencia</a>
              <ul>
                <li id="refe">Referencia</li>
                <li id="apren">Aprende</li>
              </ul>
            </li>
  				  <li><a class="comparBu">Comprar</a></li>
            <script type="text/javascript">
              $('.comparBu').on('click', function() {
                $('#ModalCorreo').foundation('reveal','open');
                  window.setTimeout(function(){window.open('https://playbusiness.mx/proyectos/briko-robotics','_self');}, 7000);
              });
            </script>
  				  <li><a href="<?php echo base_url();?>eventos">Eventos</a>	    </li>
                  <li><a href="http://briko.cc/politica">Política de privacidad</a></li>
				  </ul>
                
  			</div>
  			<div class="medium-6  columns">
  				<br>
  				<div class="row">
  					<h1 id="siguenos">S&iacute;guenos</h1>
  				</div>
  				<div class="row">
  					<div class="small-3 small-offset-1 columns">
  						<img alt="briko twitter" style="cursor:pointer;" id="twI" src="<?php echo base_url();?>images/tw.png" >
  					</div>
  					<div class="small-3 small-offset-1 columns">
  						<img alt="briko Facebook" style="cursor:pointer;" id="fbI" src="<?php echo base_url();?>images/fb.png" >
  					</div>
  					<div class="small-3 small-offset-1 columns">
  						<img alt="briko YouTube" style="cursor:pointer;" id="ytI" src="<?php echo base_url();?>images/yt.png" >
  					</div>
  				</div>
  				<div class="row">
  					<h1 id="tmail">D&eacute;janos tu email y nos pondremos en contacto :)!</h1>
  				</div>
  				<div class="row" id="fcont">
  					<form class="form-grp clearfix grpelem" id="widgetu2536" method="post" enctype="multipart/form-data" action="/">
  						<div class="small-10 columns" id="texform">
  							<input class="wrapped-input" placeholder="correo@ejemplo.com" title="correo@ejemplo.com" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" spellcheck="false" id="widgetu2538_input" name="Email" tabindex="1"/>	
  						</div>
  						<div class="small-2 columns" id="butform" style="height: 65px;">
  							<input class="submit-btn NoWrap grpelem button expand" id="u2542-17" type="submit" value="Enviar" tabindex="2"/><!-- state-based BG images -->
  						</div>
  						
     				</form>
            
  				</div>
          <div class="row">
            <h1 id="rmail"></h1>
          </div>
  				

  			</div>
  			<div class="large-3  end columns" style="text-align:center;">
	  				<br>
	  				<br>
	  				<br>
	  				<br>
	  				<h1 id="tmail">O escríbenos a:</h1>
	  				<h1 id="siteF">contacto@briko.cc</h1>

	  				
	  			</div>
  		</div>
  	</div>
</div>
<!-- formualrio de registro//// -->
<script>
    /* attach a submit handler to the form */
    $("#widgetu2536").submit(function(event) {

        /* stop form from submitting normally */
        event.preventDefault();

        /* get some values from elements on the page: */
        var $form = $(this), term = $form.find('input[name="Email"]').val();

        /* Send the data using post */
        var posting = $.post('http://briko.cc/scripts/form-u2536.php', {
          Email: term
        }).done(function() {
            $('#rmail').empty().append("Gracias por tu correo, nos pondremos pronto en contacto.");
          })
          .fail(function() {
            alert( "error" );
          })
          .always(function() {
            
        });

        /* Put the results in a div */
        posting.done(function(data) {
            var content = $(data);
            $("#fcont").append(content);
        });
    });
</script>
<!--  /////////////////////////////////////////////////////7-->
<script>
            /* attach a submit handler to the form */
            $("#searchForm").submit(function(event) {

                /* stop form from submitting normally */
                event.preventDefault();

                /* get some values from elements on the page: */
                var $form = $(this),
                    term = $form.find('input[name="s"]').val(),
                    url = $form.attr('action');

                /* Send the data using post */
                var posting = $.post(url, {
                    s: term
                });

                /* Put the results in a div */
                posting.done(function(data) {
                    var content = $(data).find('#content');
                    $("#result").empty().append(content);
                });
            });
        </script>
<!-- listeners para crear la lista -->
<script>

function prepareListFoot() {
  $('#ulLinks').find('li:has(ul)')
    .click( function(event) {
      if (this == event.target) {
        $(this).toggleClass('expanded');
        $(this).children('ul').toggle('medium');
      }
      return false;
    })
    .addClass('collapsed')
    .children('ul').hide();
  };
 
  $(document).ready( function() {

      prepareListFoot();
  });
  $("#refe").on("click",function(){  //abre pop
    window.open("http://briko.cc/referencia","_self");
  });
  $("#apren").on("click",function(){  //abre pop
    window.open("http://briko.cc/aprende","_self");
  });

  $("#instala").on("click",function(){  //abre pop
    window.open("http://briko.cc/instalacion","_self");
  });
  $("#primer").on("click",function(){  //abre pop
    window.open("http://briko.cc/primer-programa","_self");
  });
  $("#modulo").on("click",function(){  //abre pop
    window.open("http://briko.cc/brikos","_self");
  });

  $("#ytI").on("click",function(){  //abre pop
    window.open("https://www.youtube.com/channel/UC6oWg8Ehc9K1qmWTHcvoTIw","_blank");
  });
  $("#fbI").on("click",function(){  //abre pop
    window.open("https://www.facebook.com/BrikoES","_blank");
  });
  $("#twI").on("click",function(){  //abre pop
    window.open("https://twitter.com/BrikoES","_blank");
  });
  
    
</script>
