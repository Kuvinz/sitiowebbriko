<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referencia extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('calendario_mod');
        $this->load->helper('url');
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->load->library('user_agent');
        $this->lang->load('auth');
    }
	public function index()
	{
        $this->load->helper('url');
        $this->load->library('session');
        $_SESSION['type message'] = "";  //inicializa la variable en nada
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $idusuario = $infoUs->id;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('referencia');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('referencia');
            $this->load->view('footer');
        }
	}
    
    public function Link_var($type_T,$page_T)
	{
        $this->load->helper('url');
        redirect($page_T."/".$type_T,'refresh');  //redirige a otro php
	}
    
    public function aprende()
    {
        $this->load->library('session');
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $idusuario = $infoUs->id;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('aprende');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('aprende');
            $this->load->view('footer');
        }
    }
    public function aprendeTemp()
    {
        $this->load->library('session');
        if($this->ion_auth->logged_in())
        {
            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $idusuario = $infoUs->id;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('aprendeTemp');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('aprendeTemp');
            $this->load->view('footer');
        }
        
    }
    
}
