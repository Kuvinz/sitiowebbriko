<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comienza extends CI_Controller {
    
    //no hay index, porlo que no hay /comienza solo
    public function __construct(){
        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('calendario_mod');
        $this->load->helper('url');
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->load->library('user_agent');
        $this->lang->load('auth');
    }
	public function instalacion()
	{
        
        $this->load->library('session');
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('instalacion');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('instalacion');
            $this->load->view('footer');
        }
	}
    
    public function descargawin()
	{
        $name = "brikode_windows.zip";
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('download');
        $varurl =  base_url();
        $ppath =  $varurl."assets/uploads/".$name;
        $data =  file_get_contents($ppath); // Read the file's contents
        force_download($name, $data);  
	}
    
    public function descargaios()
	{
        $name = "brikode_windows.zip";
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('download');
        $varurl =  base_url();
        $ppath =  $varurl."assets/uploads/".$name;
        $data =  file_get_contents($ppath); // Read the file's contents
        force_download($name, $data);      
	}
    
    public function comienzaprimerp()
	{
        $this->load->library('session');
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('primerprograma');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('primerprograma');
            $this->load->view('footer');
        }
	}
    
    public function moduloled($refe_var)
	{
        $this->load->library('session');

        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloled',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloled',$arrvar);
            $this->load->view('footer');
        }
	}
    
    public function modulodistancia($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulodistancia',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulodistancia',$arrvar);
            $this->load->view('footer');
        }
	}
    public function  modulotemperatura($refe_var)
    {
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulotemperatura',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulotemperatura',$arrvar);
            $this->load->view('footer');
        }
    }
    public function  modulorelay($refe_var)
    {
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulorelay',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulorelay',$arrvar);
            $this->load->view('footer');
        }
    }
    public function  moduloluz($refe_var)
    {
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloluz',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloluz',$arrvar);
            $this->load->view('footer');
        }
    }
    
    public function modulomotor($refe_var)
    {
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
            
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulomotor',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulomotor',$arrvar);
            $this->load->view('footer');
        }  
    }

    public function modulobocina($refe_var)
    {
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulobocina',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulobocina',$arrvar);
            $this->load->view('footer');
        }  
    }

    
    public function moduloknob($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloknob',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloknob',$arrvar);
            $this->load->view('footer');
        }
	}
    
    public function modulodisplay($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulodisplay',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulodisplay',$arrvar);
            $this->load->view('footer');
        }
	}
    
    public function moduloboton($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloboton',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloboton',$arrvar);
            $this->load->view('footer');
        }
	}
    
    public function modulomaestro($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulomaestro',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('modulomaestro',$arrvar);
            $this->load->view('footer');
        }
	}
    
    public function moduloservo($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloservo',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloservo',$arrvar);
            $this->load->view('footer');
        }
	}
    
    public function moduloextra($refe_var)
	{
        $this->load->library('session');
        
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloextra',$arrvar);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('header',$arr);
            $this->load->view('moduloextra',$arrvar);
            $this->load->view('footer');
        }
	}
    
  
    
}
