<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('calendario_mod');
        $this->load->helper('url');
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		$this->load->library('user_agent');
		$this->lang->load('auth');
    }
    public function lanzamiento()
    {
    	redirect('https://playbusiness.mx/proyectos/briko-robotics','refresh');  
    }
    public function error()
    {
    	$this->output->set_status_header('404'); 
        $data['content'] = 'error_404';
    	if($this->ion_auth->logged_in())
        {
        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;

            
            $arr=array('usuario'=>$data);
            
            
		    $this->load->view('header',$arr);
		    $this->load->view('error404');
		    $this->load->view('footer');
            
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

		    $this->load->view('header',$arr);
		    $this->load->view('error404');
		    $this->load->view('footer');
            
		}
    }
    
     public function politica()
	{
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

             ///checa si es movil o no
            $this->load->library('user_agent');
            if($this->agent->is_mobile()){
                 $this->load->view('Mheader',$arr);
                $this->load->view('politica');
			     $this->load->view('Mfooter');
            }else{
			    $this->load->view('header',$arr);
                $this->load->view('politica');
                $this->load->view('footer');
            }
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            ///checa si es movil o no
            $this->load->library('user_agent');
            if($this->agent->is_mobile()){
                 $this->load->view('Mheader',$arr);
                $this->load->view('politica');
			     $this->load->view('Mfooter');
            }else{
			    $this->load->view('header',$arr);
                $this->load->view('politica');
                $this->load->view('footer');
            }
        }
	}
    
	public function index()
	{
		$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		if ($this->ion_auth->logged_in())
			{
				$infoUs = $this->ion_auth->user()->row(); // get current user login details
				$usuarioxx=$infoUs->username;
				$data['nombre']=$usuarioxx;

				$arr=array('usuario'=>$data);
				if($this->agent->is_mobile())
				{
	                 $this->load->view('Mheader',$arr);
				     $this->load->view('Mhome');
				     $this->load->view('Mfooter');
	            }
	            else
	            {
				    $this->load->view('header',$arr);
				    $this->load->view('home');
				    $this->load->view('footer');
	            }
			}	
			else
			{
				$data['nombre']="no";
				$arr=array('usuario'=>$data);
				if($this->agent->is_mobile())
				{
                 	$this->load->view('Mheader',$arr);
			     	$this->load->view('Mhome');
			     	$this->load->view('Mfooter');
	            }
	            else
	            {
				    $this->load->view('header',$arr);
				    $this->load->view('home');
				    $this->load->view('footer');
	            }
			}		
			

       /* if($this->session->userdata('logged_in'))
        {

        	$session_data = $this->session->userdata('logged_in');
            $idusuario = $session_data['usuario'];
            $usuario=$session_data['nombre'];
            $correo=$session_data['correo'];

            $data['nombre']=$usuario;
            
            $arr=array('usuario'=>$data);
            
             ///checa si es movil o no
            $this->load->library('user_agent');
            if($this->agent->is_mobile()){
                 $this->load->view('Mheader',$arr);
			     $this->load->view('Mhome');
			     $this->load->view('Mfooter');
            }else{
			     $this->load->view('header',$arr);
			     $this->load->view('home');
			     $this->load->view('footer');
            }
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			$this->load->library('user_agent');
            if($this->agent->is_mobile()){
                 $this->load->view('Mheader',$arr);
			     $this->load->view('Mhome');
			     $this->load->view('Mfooter');
            }else{
			     $this->load->view('header',$arr);
			     $this->load->view('home');
			     $this->load->view('footer');
            }
		}*/
			/*
			$this->load->view('header');
            $this->load->view('home');
            $this->load->view('footer');*/
	}
	public function reward()
	{
		
        if($this->ion_auth->logged_in())
        {
			$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);
            
		    $this->load->view('header',$arr);
		    $this->load->view('reward');
		    $this->load->view('footer');
            
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

		    $this->load->view('header',$arr);
		    $this->load->view('reward');
		    $this->load->view('footer');
            
		}
			/*
			$this->load->view('header');
            $this->load->view('home');
            $this->load->view('footer');*/
	}
	public function calendario()
	{

        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);
            
            
		    $this->load->view('header',$arr);
		    $this->load->view('calendario');
		    $this->load->view('footer');
            
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

		    $this->load->view('header',$arr);
		    $this->load->view('calendario');
		    $this->load->view('footer');
            
		}
			/*
			$this->load->view('header');
            $this->load->view('home');
            $this->load->view('footer');*/
	}
	public function calinfo()
	{
		$type=$this->input->post('type');
		if($type == 'new')
		{
			$startdate = $this->input->post('startdate').'+'.$this->input->post('zone');
			$title = $this->input->post('title');
			$this->calendario_mod->newEv($title,$startdate);
		}

		if($type == 'changetitle')
		{
			$eventid = $this->input->post('eventid');
			$title = $this->input->post('title');
			$this->calendario_mod->changetitle($eventid,$title);
		}

		if($type == 'resetdate')
		{
			$title = $this->input->post('title');
			$startdate = $this->input->post('start');
			$enddate = $this->input->post('end');
			$eventid = $this->input->post('eventid');
			$this->calendario_mod->resetdate($title,$startdate,$enddate,$eventid);
		}

		if($type == 'remove')
		{
			$eventid = $this->input->post('eventid');
			$this->calendario_mod->remove($eventid);
		}

		if($type == 'fetch')
		{
			$this->calendario_mod->fetch();
		}

	}

}
