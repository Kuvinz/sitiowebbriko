<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coding extends CI_Controller {
    
    //no hay index, porlo que no hay /comienza solo
    
	public function blockscode()
	{
        
        $this->load->library('session');
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $idusuario = $session_data['usuario'];
            $usuario=$session_data['nombre'];
            $correo=$session_data['correo'];

            $data['nombre']=$usuario;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('blockscode');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('blockscode');
        }
	}
    
    
    public function blockscode2($refe_var)
	{
        $arrvar =array('refe_var'=>$refe_var);  //para saber si esta pagina fue llamada desde referencias o no
        
        $this->load->library('session');
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $idusuario = $session_data['usuario'];
            $usuario=$session_data['nombre'];
            $correo=$session_data['correo'];

            $data['nombre']=$usuario;
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('blockscode2',$arrvar);
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('blockscode2',$arrvar);
        }
	}
    
    
     public function blockscode3()
	{
        $this->load->library('session');
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['nombre']=$session_data['nombre'];
            $data['idusu']=$session_data['usuario'];
            $var= array('usuario'=>$data);

            $this->load->helper('url');
            $this->load->view('blockscode3',$var);  //le pasa el id del usuario
        }
        else
        {
             $this->load->helper('url');
             echo ("<script>
             alert('Necesitas registrarte antes de poder programar con brikoblocks');
             window.location.href='http://briko.cc','self';
             </script>");
             //redirect('http://briko.cc','refresh');  //redirige a otro php
        }
	}
    
    ///trae la data de la base de datos a una pagina en blanco
    public function savepostdata($id)
    {
		$this->load->model('blockscode');
		$this->blockscode->postdata($id); //carga la pagina con lo de la base de datos
        $this->blockscode->row_delete($id); //borra eso que se guardo en la base de datos
	}
    
    ///pagina a la que llega el post y guarda la data en la base de datos
    public function codepost()
    {
        $id_user=$this->input->post('id_user');
    	$code=$this->input->post('code');
        $status=$this->input->post('status');
		$this->load->model('blockscode');
		$this->blockscode->savedata($id_user,$code,$status); 
	}
  
    
}
