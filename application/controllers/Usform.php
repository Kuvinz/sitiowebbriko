<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usform extends CI_Controller {
    
    //no hay index, porlo que no hay /comienza solo

    public function __construct(){
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("usuarios");
        $this->load->helper('security');
    }

    public function existe_us()
    {
        $this->load->model("usuarios");
        $nombre_tienda=$this->input->post('nombre_tienda');
        //echo $this->input->post('nombre_tienda');
        echo $this->usuarios->exi_us($nombre_tienda);
        
    }
 public function existe_email()
    {
        $this->load->model("usuarios");
        $correo=$this->input->post('correo');
        //echo $this->input->post('nombre_tienda');
        echo $this->usuarios->exi_mail($correo);        
    }

    public function check_nick()
    {
        $this->load->model("usuarios");
        $nombre_tienda=$this->input->post('nick');
        //echo $this->input->post('nombre_tienda');
        $existe=$this->usuarios->exi_us($nombre_tienda);
        if($existe==0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function check_mail()
    {
        $this->load->model("usuarios");
        $correo=$this->input->post('mail');
        //echo $this->input->post('nombre_tienda');
        $existe=$this->usuarios->exi_mail($correo);  
        if($existe==0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function registro()
    {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('mail', 'Correo', 'trim|required');
        $this->form_validation->set_rules('nick', 'NickName', 'trim|required|callback_check_nick');
        $this->form_validation->set_rules('nacimiento', 'Fecha de nacimiento', 'trim|required|callback_check_mail');
        $this->form_validation->set_rules('genero', 'Genero', 'trim|required');
        $this->form_validation->set_rules('passwd', 'Contraseña', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->helper('url');
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            
            $this->load->view('header',$arr);
            
            $this->load->view('home');
            $this->load->view('footer');
        }
        else
        {
            $this->load->helper(array('form', 'url'));
            $this->load->library('session');
            $this->load->model("usuarios");

            $nombre=$this->input->post('nombre');
            $mail=$this->input->post('mail');
            $nick=$this->input->post('nick');
            $nacimiento=$this->input->post('nacimiento');
            $genero=$this->input->post('genero');
            
            $pass=sha1($this->input->post('passwd'),FALSE);

            $this->usuarios->alta($nombre,$mail,$nick,$nacimiento,$genero,$pass);

            $result = $this->usuarios->login($mail, $pass);
             
            //print_r($result);
            if($result)
            {
                $sess_array = array();
                foreach($result as $value)
                {
                    $sess_array = array(
                        'usuario' => $value->UID,
                        'correo' => $value->Correo,
                        'nombre' => ucfirst( strtolower( $value->Nombre ) )
                    );
                    $this->session->set_userdata('logged_in', $sess_array);
                }
            }
            redirect('misproyectos','refresh');
        }
    }
    public function logoutU()
    {
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();

        redirect(base_url()."home", 'refresh');
    }

    public function log()
    {
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');

        if(!$this->session->userdata('logged_in'))
        {
            $this->form_validation->set_rules('usuE', 'Usuario', 'trim|required');
            $this->form_validation->set_rules('passE', 'Password', 'trim|required|callback_check_database');
            
            if($this->form_validation->run() == FALSE)
            {
                //Si la validacion falla
                redirect(base_url()."home",'refresh');
            }
            else
            {
                //Si las credenciales son correctas
                $session_data = $this->session->userdata('logged_in');
                redirect('misproyectos','refresh');
            }   
        }
        else
        {
            redirect('misproyectos','refresh');
        }       
    }

    public function check_database($password)
    {
        $this->load->model("usuarios");
         //Validacion correcta
        $usuario = $this->input->post('usuE');
        //consulta a base de datos
        
        $pass  =sha1($password,FALSE);
        $result = $this->usuarios->login($usuario, $pass);
         //print_r($result);
        //print_r($result);
        if($result)
        {
            
            //echo $valor;
            $sess_array = array();
            foreach($result as $value)
            {
                    $sess_array = array(
                    'usuario' => $value->UID,
                    'correo' => $value->Correo,
                    'admin'=>0,
                    'nombre' => ucfirst( strtolower( $value->Nombre ) )
                    );

                    $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Usuario o password inválido');
            //echo "noo";
            return false;
        }
    }

    public function mailCompra()
    {
        $this->load->model("usuarios");
        $Correo=$this->input->post('Correo');
        $Nombre=$this->input->post('Nombre');
        $perfil=$this->input->post('perfil');
        //echo $this->input->post('nombre_tienda');
        $this->load->library('email');

            $subject = 'Agradecimiento';
            $message = '<h1>Hola '.$Correo.'</h1>
            <p>Muchas gracias por tu interés en nuestro producto, en este momento estamos preparando el lanzamiento oficial de briko que se hará a través de una campaña de Crowdfundig a finales de Octubre. Nosotros te comunicaremos cuando podrás adquirir un kit briko, durante la campaña el costo de los kits tendrá un descuento con respecto al precio final de venta, así que te recomendamos aprovechar esta oportunidad y ser parte de la comunidad briko!.</p>
            <br>
            <h2>Siguenos en nuestras redes</h2>
            <br>
            <div align="center">
            <a href="https://twitter.com/@BrikoEs"><img  alt="briko twitter" src="http://briko.cc/images/twitterbr.png"></a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="https://www.facebook.com/BrikoES/"><img alt="briko facebook" src="http://briko.cc/images/facebookbr.png"></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.youtube.com/channel/UC6oWg8Ehc9K1qmWTHcvoTIw"><img alt="briko youtube" src="http://briko.cc/images/youtubebr.png"></a>
            </div>
            <br>
            
            ';

            // Get full html:
            $body =
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset='.strtolower(config_item('charset')).'" />
    <title>'.html_escape($subject).'</title>
    <style type="text/css">
        body {
            font-family:  Helvetica, sans-serif;
            font-size: 16px;
        }
        p{
         text-align: justify;
        } 
        h2{
          text-align: center;  
        }
        #redes
        {
         text-align: center;     
        }

    </style>
</head>
<body>
'.$message.'
</body>
</html>';
            // Also, for getting full html you may use the following internal method:
            //$body = $this->email->full_html($subject, $message);

            $result = $this->email
                ->from('equipo@briko.cc')
                ->to($Nombre)
                ->subject($subject)
                ->message($body)
                ->send();
echo $this->usuarios->nuevoMail($Correo,$Nombre,$perfil);
            exit;
        
        
        
    }

    public function resetpasswd()
    {
        $mailr=$this->input->post('email');
        if(isset($mailr) && !empty($mailr))
        {
            $this->load->library('form_validation');
            //primero checamos si es un email valido o no
            $this->form_validation->set_rules('email', 'Correo', 'trim|required|min_length[6]|max_length[50]|valid_email|xss_clean');

            if($this->form_validation->run()==FALSE)
            {
                //email no valido regresamos y mostramos los errores
                //esperemos que no ocurra debido a que hacemos validacion desde la vista
                $data['nombre']="no";
            
                $arr=array('usuario'=>$data);
                $this->load->view('header',$arr);

                $this->load->view('resetpasswd',array('email'=>'Error en el correo'));
                $this->load->view('footer');
            }
            else
            {
                $email=trim($this->input->post('email'));
                $result=$this->usuarios->existe_mail($email);  
                //echo "paso la validacion";
                if($result)
                {
                    //Si existio el correo lo que obtenemos es el nombre
                    //echo "paso la validacion y si encontro el correo";
                    $this->send_reset_password_email($email,$result);
                    $data['nombre']="no";
            
                    $arr=array('usuario'=>$data);
                    $this->load->view('header',$arr);
                    $this->load->view('resetpasswdsent',array('email'=>$email));
                    $this->load->view('footer');
                }
                else
                {
                    $data['nombre']="no";
            
                    $arr=array('usuario'=>$data);
                    $this->load->view('header',$arr);
                    $this->load->view('resetpasswd',array('error'=>'Email no registrado en briko'));
                    $this->load->view('footer');
                }
            }
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            $this->load->view('header',$arr);

            $this->load->view('resetpasswd');
            $this->load->view('footer');
        }
    }
    private function send_reset_password_email($email,$nick)
    {
        $this->load->library('email');
        $email_code=md5($this->config->item('salt').$nick);
//echo $email_code;
            $subject = 'Restablece tu contraseña para acceder a briko';
            $message = '<h1>Estimado '.$nick.'</h1>
            <p>QUeremos ayudaert para reestablecer tu contraseña! Sigue el siguiente link para reestablecer tu contraseña.</p>
            <p><strong><a href="'.base_url().'reset_password_form/'.$email.'/'.$email_code.'">Link para restablecer contraseña</a></strong></p>
            <p>--</p>
            <p>!Muchas Gracias¡</p>
            <p>briko Team</p>            
            ';
            // Get full html:
            $body =
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset='.strtolower(config_item('charset')).'" />
    <title>'.html_escape($subject).'</title>
    <style type="text/css">
        body {
            font-family:  Helvetica, sans-serif;
            font-size: 16px;
        }
        p{
         text-align: justify;
        } 
        h1{
          text-align: center;  
        }
       

    </style>
</head>
<body>
'.$message.'
</body>
</html>';
            // Also, for getting full html you may use the following internal method:
            //$body = $this->email->full_html($subject, $message);

            $result = $this->email
                ->from('equipo@briko.cc')
                ->to($email)
                ->subject($subject)
                ->message($body)
                ->send();
                


    }

    public function reset_password_form($email,$email_code)
    {
        if(isset($email,$email_code))
        {
            $email=trim($email);
            $email_hash=sha1($email.$email_code);
            $verified=$this->usuarios->verify_reset_password_code($email,$email_code);

            if($verified)
            {
                $data['nombre']="no";
            
                $arr=array('usuario'=>$data);
                $this->load->view('header',$arr);

                $this->load->view('view_update_password',array('email_hash'=>$email_hash,'email_code'=>$email_code,'email'=>$email));
                $this->load->view('footer');
            }
            else
            {
                $data['nombre']="no";
            
                $arr=array('usuario'=>$data);
                $this->load->view('header',$arr);

                $this->load->view('resetpasswd',array('error'=>'Hubo un problema con el link. porfavor vuelve a enviar el correo','email'=>$email));
                $this->load->view('footer');
            }
        }
    }

    public function update_password()
    {
        $mailup=$this->input->post('email');
        $hashmailup=$this->input->post('email_hash');
        $codemailup=$this->input->post('email_code');
        if(!isset($mailup,$hashmailup)||$hashmailup!==sha1($mailup.$codemailup))
        {
            die('Error updating your password');
        }

        $this->load->library('form_validation');
        
        $this->load->library('form_validation');
            //primero checamos si es un email valido o no
        $this->form_validation->set_rules('email_hash', 'Email Hash', 'trim|required');
        $this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[6]|max_length[50]|matches[pass_conf]|xss_clean');
        $this->form_validation->set_rules('pass_conf', 'Confirmed Password', 'trim|required|min_length[6]|max_length[50]|xss_clean');

        if($this->form_validation->run()==FALSE)
        {
            //usuario no paso la validacion y se regresa
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            $this->load->view('header',$arr);

            $this->load->view('view_update_password',array('email_hash'=>$this->input->post('email_hash'),'email_code'=>$this->input->post('email_code'),'email'=>$this->input->post('email')));
            $this->load->view('footer');
        }
        else
        {
            //acctualizacion correcta
            //regresamos  true si fue exitoso
            $this->load->model("usuarios");
            $result=$this->usuarios->update_passwd();
            if($result)
            {
                 $data['nombre']="no";
            
                $arr=array('usuario'=>$data);
                $this->load->view('header',$arr);
                $this->load->view('view_update_password_succes',array('email_hash'=>$this->input->post('email_hash'),'email_code'=>$this->input->post('email_code'),'email'=>$this->input->post('email')));
                $this->load->view('footer');
            }
            else
            {
                 $data['nombre']="no";
            
                $arr=array('usuario'=>$data);
                

                
                $this->load->view('header',$arr);
                $this->load->view('view_update_password',array('error'=>'Problema actualizando tu contraseña. Porfavor manda un correo a contacto@briko.cc','email_hash'=>$this->input->post('email_hash'),'email_code'=>$this->input->post('email_code'),'email'=>$this->input->post('email')));
                $this->load->view('footer');
            }
        }
    }
  
    
}
