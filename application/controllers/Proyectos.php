<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('calendario_mod');
        $this->load->helper('url');
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		$this->load->library('user_agent');
		$this->lang->load('auth');
    }
	

	public function home()
	{
		
        if($this->ion_auth->logged_in())
        {

        	$session_data = $this->ion_auth->logged_in();
            $idusuario = $session_data['usuario'];
            $usuario=$session_data['nombre'];
            $correo=$session_data['correo'];

            $data['nombre']=$usuario;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('home');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('home');
            $this->load->view('footer');
		}

	}
	public function index()
	{
		
		
		$this->load->model('proy_mod');
		$proyectos=$this->proy_mod->tproyectos();

		$proyectosnew=$this->proy_mod->todosProyectos();
		$imagenesnew=$this->proy_mod->imagenesProyectos();
		//print_r($proyectosnew);
	    $proye['mBot']=$this->proy_mod->proyxmod(1);
	    $proye['mDis']=$this->proy_mod->proyxmod(2);
	    $proye['mBuz']=$this->proy_mod->proyxmod(3);
	    $proye['mLed']=$this->proy_mod->proyxmod(4);
	    $proye['mPot']=$this->proy_mod->proyxmod(5);
	    $proye['mTemp']=$this->proy_mod->proyxmod(6);
	    $proye['mLuz']=$this->proy_mod->proyxmod(7);
	    $proye['mDisp']=$this->proy_mod->proyxmod(8);
	    $proye['mMot']=$this->proy_mod->proyxmod(9);


		//print_r($proye);
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('proyectos',array( "proyecto" => $proyectos,'proyxmod'=>$proye,'holap'=>$proyectosnew,'imagenesnew'=>$imagenesnew));
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
           	$this->load->view('proyectos.php',array( "proyecto" => $proyectos,'proyxmod'=>$proye,'holap'=>$proyectosnew,'imagenesnew'=>$imagenesnew));
            $this->load->view('footer');
		}
	}
	public function shop()
	{
		
		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;

            
            
            $arr=array('usuario'=>$data);

			
			
            
			$this->load->view('header',$arr);
			$this->load->view('shop');
			
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			$this->load->view('header',$arr);
            $this->load->view('shop');
            $this->load->view('footer');
		}
	}
	public function guardar_p()
	{
		
		$this->load->model('proy_mod');
		$this->load->view('header');
		$proyectos=$this->proy_mod->guardarp();
	}
	public function glosario()
	{
		
		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('glosario');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('glosario');
            $this->load->view('footer');
		}
	}

	public function snake()
	{
		$data['nombre']="no";
        $arr=array('usuario'=>$data);
		$this->load->view('header',$arr);
		$this->load->model('proy_mod');
		$puntaje=$this->proy_mod->puntaje();
		//print_r($puntaje);
		$this->load->view('snake.php',array( "pun" => $puntaje));
		//$this->load->view('footer');
	}

	public function puntaje()
	{
		
		
		$this->load->model('proy_mod');
		$puntaje=$this->proy_mod->puntaje1();
		
	}

	public function altaeventos()
	{
		if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);
			
			$this->load->view('header',$arr);
			$this->load->view('aevento');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);
			
			$this->load->view('header',$arr);
            $this->load->view('aevento');
            $this->load->view('footer');
		}
	}

	public function eventos()
	{
		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('eventos');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('eventos');
            $this->load->view('footer');
		}
	}
	public function agradecimiento()
	{
		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('agradecimiento');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('agradecimiento');
            $this->load->view('footer');
		}
	}
	public function comienza()
	{
		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('comienza');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('comienza');
            $this->load->view('footer');
		}
	}

	public function proyectoinfo($proye)
	{
		
		
		$this->load->model('proy_mod');
		$this->proy_mod->proyectost($proye);
		//print_r($proyectos);
		//echo json_encode($proyectos);
	}

	public function proyecto()
	{
		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('proyectos');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('proyectos');
            $this->load->view('footer');
		}

	}

	public function infoproye($proye)
	{
		
		$this->load->model('proy_mod');
		$info=$this->proy_mod->info($proye);

		
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);
			
			if($info=="false")
		    {
		      redirect('proyectos','refresh');
		    }
		    else
		    {
				$modulos=$this->proy_mod->modulos($proye);
				//print_r($info);
				//print_r($modulos);
				$this->load->view('header',$arr);
				$this->load->view('proyecto.php',array( "mods" => $modulos ,"info" => $info));
				$this->load->view('footer');
			}
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            if($info=="false")
		    {
		      redirect('proyectos','refresh');
		    }
		    else
		    {
				$modulos=$this->proy_mod->modulos($proye);
				//print_r($info);
				//print_r($modulos);
				$this->load->view('header',$arr);
				$this->load->view('proyecto.php',array( "mods" => $modulos ,"info" => $info));
				$this->load->view('footer');
			}
			
		}
		
		

	}

	public function alta()
	{
		
		
		$this->load->model('proy_mod');
		//print_r($this->ion_auth->logged_in());

        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$idusuario = $infoUs->id;
			$data['nombre']=$usuarioxx;

            $proyectos=$this->proy_mod->tproyectosxusu($idusuario);
            
            $arr=array('usuario'=>$data);
           
			
			$this->load->view('header',$arr);
			$this->load->view('alta', array( "proyecto" => $proyectos ));
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            //echo "no entro por que no existe logged_in";
			
			redirect(base_url(),"refresh");
		}
		
	}

	public function alta_proyectos()
	{
		$this->load->model('proy_mod');
		//$this->usuarios->img_perfil($this->input->post('cuenta'),$file_name);

		$modulos=$this->input->post('lista_modulos');
        $nom=$this->input->post('nombre');
        $desc=$this->input->post('descripcion');
        $video=$this->input->post('video');
        $puertos=$this->input->post('lista_p');
        $imagen=$this->input->post('imagen');
        $tiempo=$this->input->post('tiempo');
        $dificultad=$this->input->post('dificultad');
        $codigo=$this->input->post('codigo');
        
        print_r($modulos);
        
        $puertos_m=array_filter($puertos);

        $modulos_k=array_keys($modulos);


        $id_proy=$this->proy_mod->alta($nom,$desc,$video,$imagen,$tiempo,$dificultad,$codigo);

        foreach($modulos_k as $key)
        {
            $this->proy_mod->modxproy($id_proy,$modulos[$key],$puertos_m[$key]);
        }

        
	}
	public function modulos()
	{
        
        $_SESSION['type message'] = ""; //inicializa la variable
        if($this->ion_auth->logged_in())
        {

        	$infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
			$this->load->view('modulos');
			$this->load->view('footer');
		}
		else
		{
			$data['nombre']="no";
            
            $arr=array('usuario'=>$data);

			
			$this->load->view('header',$arr);
            $this->load->view('modulos');
            $this->load->view('footer');
		}	
	}
	public function proyectoFiltro($modulo)
	{

		
		$this->load->model('proy_mod');
		$proyectos=$this->proy_mod->proyectosFiltro($modulo);

	}
	
}
