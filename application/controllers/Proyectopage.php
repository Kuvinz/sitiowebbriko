<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectopage extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        
        $this->load->library('session');
        $this->load->model('calendario_mod');
        $this->load->helper('url');
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->load->library('user_agent');
        $this->lang->load('auth');
    }
	public function page1()
	{
         
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
			$usuarioxx=$infoUs->username;
			$data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage1');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage1');
            $this->load->view('footer');
        }
        
	}
    
    public function page2()
	{
        
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage2');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage2');
            $this->load->view('footer');
        }
	}
    
    public function page3()
	{
        
        if($this->ion_auth->logged_in())
        {
            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;

            $this->load->model('proyecto_mod');
            $cat=$this->proyecto_mod->cate();
            
            
            $arr=array('usuario'=>$data,'categoria'=>$cat);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage3',$arr);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('home');
            $this->load->view('footer');
        }
	}
    
    
    public function page6($id)
	{
        
        $this->load->model('proyecto_mod');   
        $info=$this->proyecto_mod->infoProyectteach($id);
        if($info==false)
        {
            redirect('proyectopage4/'.$id,'refresh');  
        }
        $info=json_encode($info);
        $info=json_decode($info,true);
        //print_r($info);
        if($this->ion_auth->logged_in())
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);
            $arr1=array('info'=>$info);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage6',$arr1);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            $arr1=array('info'=>$info);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage6',$arr1);
            $this->load->view('footer');
        }
	}
    
    public function page5($bandera)
	{
        
        if($this->ion_auth->logged_in()&&$bandera==1)
        {

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx;
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('proyectopage5');
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('home');
            $this->load->view('footer');
        }
	}
    
    public function page4($id)
	{
        
        $this->load->model('proyecto_mod');
        $info=$this->proyecto_mod->infoProyect($id);
        
        $info=json_encode($info);
        $info=json_decode($info,true);
        //print_r($info);
        if($this->ion_auth->logged_in())
        {
            if($info==false)
            {
                redirect('misproyectos','refresh');  
            }
            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $data['nombre']=$usuarioxx; 
            
            $arr=array('usuario'=>$data);
            $arr1=array('info'=>$info);
            
            $this->load->view('header',$arr);

            $this->load->view('proyectopage4',$arr1);
            $this->load->view('footer');
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            $arr1=array('info'=>$info);

            if($info==false)
            {
                redirect('welcome','refresh');  
            }
            $this->load->view('header',$arr);
            $this->load->view('proyectopage4',$arr1);
            $this->load->view('footer');
        }
	}

    public function modProy($id)
    {
        
        if($this->ion_auth->logged_in())
        {
            
            

            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $idusuario = $infoUs->id;
            $data['nombre']=$usuarioxx;

            $this->load->model('proyecto_mod');
            $duenoP=$this->proyecto_mod->duenoProyect($id);
            if($duenoP!=$idusuario)
            {
                redirect('misproyectos','refresh');  
            }
            else
            {
                $info=$this->proyecto_mod->infoProyect($id);
                $cat=$this->proyecto_mod->cate();
                $info=json_encode($info);
                $info=json_decode($info,true);

                if($info==false)
                {
                    redirect('misproyectos','refresh');  
                }
                else
                {
                   
            
                    $arr=array('usuario'=>$data);
                    $arr1=array('info'=>$info,'categoria'=>$cat);
                    print_r($arr1);
                    $this->load->view('header',$arr);

                    $this->load->view('proyectopage7',$arr1);
                    $this->load->view('footer'); 
                }
            }
        }
        else
        {
            $info=$this->proyecto_mod->infoProyect($id);
            
            $info=json_encode($info);
            $info=json_decode($info,true);
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);
            $arr1=array('info'=>$info);
            if($info==false)
            {
                redirect('proyectos','refresh');  
            }
            $this->load->view('header',$arr);
            $this->load->view('proyectos',$arr1);
            $this->load->view('footer');
        }

    }
    
    
    
    
    
    //fucnion para subir las imagenes al servidor
    function do_upload()
	{
        
    
    
    if($this->ion_auth->logged_in())
    {
        $maxsizep = '10000'; //in kilobytes
        //print_r ($_FILES);
            $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $idusuario = $infoUs->id;
            $data['nombre']=$usuarioxx;
        $name_array = array();
        $this->load->model('proyecto_mod');
        
        // ------------------Guardar toda la informacion del primer pso, info general --------------------------------
        $bandera=0;
        //Declaracion de variables para guardar la informacion
        $nombreProyecto=$this->input->post('nombreProyecto');
        $descripcionProyecto=$this->input->post('descripcionProyecto');
        $categorias=$this->input->post('categorias');
        $videos=$this->input->post('Lista_video_paso11');
        $tiempo=$this->input->post('tiempo');
        $dificultad=$this->input->post('dificultad');
        
        //Dar de alta el proyecto y obtener el id del proyecto con el que vamo s a guardar toda la informacion
        $idproyecto=$this->proyecto_mod->altaproyecto($idusuario,$nombreProyecto,$descripcionProyecto,$tiempo,$dificultad);
        $this->session->set_userdata('id_proy',$idproyecto);
        $album = $idproyecto;  //nombre general para la carpeta
        //Guardamos todas las categorias del proyecto 
        foreach ($categorias as $value)
        {
            if($value==1)
            {
                $bandera=1;
            }
            $this->proyecto_mod->categorias($value,$idproyecto);
        }
        
        
        //Guardamos los videos que se dieron de alta para el proyecto
        if(isset($videos))
        {
            foreach ($videos as  $value)
            {
                $this->proyecto_mod->videos($value,$idproyecto);
            }
        }

        
        //$this->proyecto_mod->imagenes("nombre-imagen","no de paso",$idproyecto,3);

        
        //falta foreach del nombre de las imagenes

        //  ------------------------Guardar toda la informacion del segundo paso -----------

        ////para el paso material
        
        $keys = array();
        $Piezas_name=$this->input->post('Lista_name');
        if(isset($Piezas_name))
        {
            $Piezas_name=$this->input->post('Lista_name');
            $Piezas_des=$this->input->post('Lista_desc');
            $keys=array_keys($Piezas_name);
        }
        

        $Modulos=json_decode($this->input->post('Dragjson'),true);
        $piezasmeca=json_decode($this->input->post('Dragjson2'),true);
        $accesorios=json_decode($this->input->post('Dragjson3'),true);

        //print_r($Modulos);
        //Guardamos todos los modulos que se utilizan en el proyecto junto con su puerto 
        foreach ($Modulos as $value)
        {
            $this->proyecto_mod->modulos($idproyecto,$value['id'],$value['puerto']);
        }

        //Guardamos el material que se utiliza piezas mecanicas
        foreach ($piezasmeca as $value)
        {
            $this->proyecto_mod->material($idproyecto,$value['id'],$value['cantidad']);
        }

        //Guardamos el material que se utiliza accesorios
        foreach ($accesorios as $value)
        {
            $this->proyecto_mod->accesorios($idproyecto,$value['id'],$value['cantidad']);
        }

        // ------------------------Guardar toda la informacion del tercer  paso -----------

        //variables del paso 3
        $desArmar=$this->input->post('paso3desc');

        //Guardamos la descripcion general del paso 3 de construir
        $this->proyecto_mod->texto($idproyecto,0,htmlspecialchars($desArmar,ENT_QUOTES),3);
        $paso3Texto=$this->input->post('Lista_desc_paso3');
        if(isset($paso3Texto))
        {
            $paso3Texto=$this->input->post('Lista_desc_paso3');
            $keyspaso3=array();
            $keyspaso3=array_keys($paso3Texto);
            //Guardamos el material que se utiliza accesorios
            foreach ($keyspaso3 as $value)
            {
                $this->proyecto_mod->texto($idproyecto,$value+1,htmlspecialchars($paso3Texto[$value],ENT_QUOTES),3);
            }
        }

        // ------------------------Guardar toda la informacion del quinto  paso -----------
        $desProgramar=$this->input->post('paso5desc');
        $paso5Texto=$this->input->post('Lista_desc_paso5');
        

        //Guardamos la descripcion general del paso 5 de codigo
        $this->proyecto_mod->texto($idproyecto,0,htmlspecialchars($desArmar,ENT_QUOTES),5);
        if(isset($paso5Texto))
        {
            $paso5Codigo=$this->input->post('Lista_codigo_paso5');
            $keyspaso5=array();
            $keyspaso5=array_keys($paso5Texto);
            //Guardamos el material que se utiliza accesorios
            foreach ($keyspaso5 as $value)
            {
                $this->proyecto_mod->texto($idproyecto,$value+1,htmlspecialchars($paso5Texto[$value],ENT_QUOTES),5);
                $this->proyecto_mod->codigo($idproyecto,$value+1,htmlspecialchars($paso5Codigo[$value], ENT_QUOTES),5);
            }
        }
        



/*
        print_r("Informacion General");
        echo "<br>";
        echo "Nombre proyecto: ".$this->input->post('nombreProyecto');
        echo "<br>";
        echo "Descripcion proyecto: ".$this->input->post('descripcionProyecto');
        echo "<br>";
        echo "Lista de Video: ";
        print_r($this->input->post('Lista_video_paso11'));
        echo "<br>";
        echo "Tiempo: ".$this->input->post('tiempo');
        echo "<br>";
        echo "Nivel: ".$this->input->post('dificultad');
        echo "<br>";
        echo "Categorias: ";
        print_r($this->input->post('categorias'));
        echo "<br>";

         print_r("Material");
        echo "<br>";
        echo "Modulos: ";
        print_r($Modulos);
        echo "<br>";
        echo "Piezas Mecanicas: ";
        print_r($piezasmeca);
        echo "<br>";
        echo "Accesorios: ";
        print_r($accesorios);
        echo "<br>";
        print_r($Piezas_name);
        echo "<br>";
        print_r($Piezas_des);
        echo "<br>";echo "<br>";

        print_r("Construir");
        echo "<br>";
        print_r($this->input->post('paso3desc'));
        echo "<br>";
        print_r($this->input->post('Lista_desc_paso3'));
        echo "<br>";echo "<br>";

        print_r("Programar");
        echo "<br>";
        print_r($this->input->post('paso5desc'));
        echo "<br>";
        print_r($this->input->post('Lista_desc_paso5'));
        echo "<br>";
        print_r($this->input->post('Lista_codigo_paso5'));
        echo "<br>";echo "<br>";
*/
// -------------------------------------imagenes userfile 1
        if (isset($_FILES['userfile1'])) {    
        $Namef = $idproyecto."_"."general"."_";
        $value = $_FILES['userfile1'];
        $count = count($value["name"]);
$contadorimagen=0;
            for($s=0; $s<=$count-1; $s++) {

                if($value['name'][$s]==""||$value['type'][$s]=="")
                {
                    $this->proyecto_mod->imagenes("",$s+1,$idproyecto,3);
                    continue;
                }
            $_FILES['userfile']['name']=  $value['name'][$s];
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['file_name'] = $Namef.$s;  //pone el nombre al archivo con numeros
            $config['upload_path'] = './uploads/'.$album;
            $config['allowed_types'] = '*';
            $config['max_size'] = $maxsizep;  //in kilobytes
            $config['max_width']  = '0';  //0 is no limit
            $config['max_height']  = '0'; //0 is no limit
            //carga la configuracion y le da un nombre especial
            $this->load->library('upload', $config,'paso1upload');
            // create an album if not already exist in uploads dir
            if (!is_dir('uploads')) {
                mkdir('./uploads', 0777, true);
            }
            if (!is_dir('uploads/'.$album)) {
                mkdir('./uploads/'.$album, 0777, true);
            }
            $this->paso1upload->do_upload();
            $data = $this->paso1upload->data();
            $name_array[] = $data['file_name'];
            //guardar la imagen en la BD

            //echo $Namef.".".str_replace("e","",substr($value['type'][$s],6,4));
            $this->proyecto_mod->imagenes($name_array[$contadorimagen],$s+1,$idproyecto,1);       
            $contadorimagen++;
            }
            $names= implode(',', $name_array);
        
            ///Para poder hacer resize de las iamgenes
            
            
            $this->load->library('image_lib');
            ///hace resize de las imagenes png
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.png';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.png';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;


            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            }
        
            ///hace resize de las imagenes jpg
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.jpg';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.jpg';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            } 
          }
//echo phpinfo();
//print_r($_FILES);

// ----------------------------------------- archivos userfile 2
        if (isset($_FILES['userfile2'])) {  
        
        unset($name_array);
        $name_array = array();  
        $Namef = $idproyecto."_"."material"."_";
        $value = $_FILES['userfile2'];
        $count = count($value["name"]);

            for($s=0; $s<=$count-1; $s++) {
            $_FILES['userfile']['name']=  $value['name'][$s];
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['file_name'] = $value['name'][$s];  //pone el nombre al archivo con numeros
            $config['upload_path'] = './uploads/'.$album;
            $config['allowed_types'] = '*';
            $config['max_size']	= $maxsizep;  //in kilobytes
            $config['max_width']  = '0';  //0 is no limit
            $config['max_height']  = '0'; //0 is no limit
            //carga la configuracion y le da un nombre especial
            $this->load->library('upload', $config,'paso2upload');
            // create an album if not already exist in uploads dir
            if (!is_dir('uploads')) {
                mkdir('./uploads', 0777, true);
            }
            if (!is_dir('uploads/'.$album)) {
                mkdir('./uploads/'.$album, 0777, true);
            }
            $this->paso2upload->do_upload();
            $data = $this->paso2upload->data();
            $name_array[] = $data['file_name'];
            //print_r($data['file_name']);  //nombre del archivo con terminacion
            //Guardamos todos los adjuntos que va a tener el archivo, que son los objetos otros que usami¿oes en el proyecto
            $nom= $Piezas_name[$s];
            $de= $Piezas_des[$s];
            
                $this->proyecto_mod->adjuntos($idproyecto,$nom,$de,$name_array[$s]);
            
            }
            $names= implode(',', $name_array);
            

            ///Para poder hacer resize de las iamgenes
            $this->load->library('image_lib');
            ///hace resize de las imagenes png
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.png';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.png';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            }
        
            ///hace resize de las imagenes jpg
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.jpg';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.jpg';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            } 
	      }
        
        
        ////para el paso material
        $name_array = array();
        if (isset($_FILES['userfile3'])) {  
        $Namef = $idproyecto."_"."armar"."_";
        $value = $_FILES['userfile3'];
        $count = count($value["name"]);
        //print_r($value['name']);
        $contadorimagen=0;
            for($s=0; $s<=$count-1; $s++) {
                
                if($value['name'][$s]=="")
                {
                    $this->proyecto_mod->imagenes("",$s+1,$idproyecto,3);
                    continue;
                }
            $_FILES['userfile']['name']=  $value['name'][$s];
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['file_name'] = $Namef.$s;  //pone el nombre al archivo con numeros
            $config['upload_path'] = './uploads/'.$album;
            $config['allowed_types'] = '*';
            $config['max_size']	= $maxsizep;  //in kilobytes
            $config['max_width']  = '0';  //0 is no limit
            $config['max_height']  = '0'; //0 is no limit
            //carga la configuracion y le da un nombre especial
            $this->load->library('upload', $config,'paso3upload');
            // create an album if not already exist in uploads dir
            if (!is_dir('uploads')) {
                mkdir('./uploads', 0777, true);
            }
            if (!is_dir('uploads/'.$album)) {
                mkdir('./uploads/'.$album, 0777, true);
            }
            $this->paso3upload->do_upload();
            $data = $this->paso3upload->data();
            $name_array[] = $data['file_name'];
            //guardar la imagen en la BD
            //print_r($name_array);
            
            
            $this->proyecto_mod->imagenes($name_array[$contadorimagen],$s+1,$idproyecto,3);       
            $contadorimagen++;
            }
            $names= implode(',', $name_array);
            
            ///Para poder hacer resize de las iamgenes
            $this->load->library('image_lib');
            ///hace resize de las imagenes png
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.png';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.png';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            }
        
            ///hace resize de las imagenes jpg
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.jpg';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.jpg';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            } 
	      } 
          if($bandera==1)
          {
            if($this->ion_auth->logged_in())
            {

                $infoUs = $this->ion_auth->user()->row(); // get current user login details
                $usuarioxx=$infoUs->username;
                $idusuario = $infoUs->id;
                $data['nombre']=$usuarioxx;
                
                $arr=array('usuario'=>$data,'proyecto'=>$idproyecto);

                
                redirect('contenido-edu/1','refresh'); 
            }
            else
            {
                $data['nombre']="no";
                
                $arr=array('usuario'=>$data);

                
                $this->load->view('header',$arr);
                $this->load->view('home');
                $this->load->view('footer');
            }
          }
          else
          {
            redirect('misproyectos','refresh');    
          }
        
        
        }
        else
        {
            $data['nombre']="no";
            
            $arr=array('usuario'=>$data);

            
            $this->load->view('header',$arr);
            $this->load->view('home');
            $this->load->view('footer');
        }
        
}

public function eliminarP($value)
{
    $this->load->model("proyecto_mod");
    
    $usuario=$this->proyecto_mod->elimP($value);
    return true;
}
    
    //fucnion para subir las imagenes al servidor
    function do_upload2()
	{
        
    
    
    if($this->ion_auth->logged_in())
    {
        $infoUs = $this->ion_auth->user()->row(); // get current user login details
            $usuarioxx=$infoUs->username;
            $idusuario = $infoUs->id;
            $data['nombre']=$usuarioxx;
        //$IDProyecto = $session_data['id_proy'];
        $IDProyecto = $this->input->post('pID');
        $maxsizep = '10000'; //in kilobytes
        
        $album = $IDProyecto;  //nombre general para la carpeta
        $name_array = array();
        $this->load->model('proyecto_mod');
        
        /*
        print_r("Informacion General");
        echo "<br>";
        echo "Nombre proyecto: ".$this->input->post('Teach_nombre_p');
        echo "<br>";
        echo "Objetivo proyecto: ".$this->input->post('Teach_objetivo_p');
        echo "<br>";
        echo "Antes del proyecto: ".$this->input->post('Teach_antes_p');
        echo "<br>";
        echo "Despues del proyecto: ".$this->input->post('Teach_durante_p');
        echo "<br>";
        echo "Lista de titulos: ";
        print_r($this->input->post('Lista_nombre_t'));
        echo "<br>";
        echo "Lista de desripcion: ";
        print_r($this->input->post('Lista_obj_t0'));
        echo "<br>";
        echo "Lista de codigos: ";
        print_r($this->input->post('Lista_obj_t2'));
        echo "<br>";
        echo "Lista de nombres: ";
        print_r($this->input->post('Lista_filen_t'));
        echo "<br>";
        */
        
        
        $this->proyecto_mod->texto($IDProyecto,1,htmlspecialchars($this->input->post('Teach_nombre_p'),ENT_QUOTES),6);
        $this->proyecto_mod->texto($IDProyecto,2,htmlspecialchars($this->input->post('Teach_objetivo_p'),ENT_QUOTES),6);
        $this->proyecto_mod->texto($IDProyecto,3,htmlspecialchars($this->input->post('Teach_antes_p'),ENT_QUOTES),6);
        $this->proyecto_mod->texto($IDProyecto,4,htmlspecialchars($this->input->post('Teach_durante_p'),ENT_QUOTES),6);
        
        
        $Titulo_t = $this->input->post('Lista_nombre_t');
        $Desc_t = $this->input->post('Lista_obj_t0');
        if (isset($Titulo_t)) {
        $Titulo_t_pasos = array_keys($Titulo_t);//regresa un arreglo con los indices
        
        foreach ($Titulo_t_pasos as $value)
        {
            $this->proyecto_mod->secciones($IDProyecto,htmlspecialchars($Titulo_t[$value],ENT_QUOTES),htmlspecialchars($Desc_t[$value],ENT_QUOTES),$value+1);
        }
        }
        
         
        $Codigos_t = $this->input->post('Lista_obj_t2');
        if (isset($Codigos_t)) {
        $Codigos_t_pasos = array_keys($Codigos_t);//regresa un arreglo con los indices
        foreach ($Codigos_t_pasos as $value)
        {
            $this->proyecto_mod->codigo($IDProyecto,$value+1,htmlspecialchars($Codigos_t[$value],ENT_QUOTES),6);
        }    
        }
        
        ////para el paso de secciones
        $name_array = array();
        if (isset($_FILES['userfile1'])) {  
        $Namef = $IDProyecto."_"."seccion"."_";
        $value = $_FILES['userfile1'];
        $count = count($value["name"]);
        $contadorimagen=0;
            for($s=0; $s<=$count-1; $s++) {

                if($value['name'][$s]==""||$value['type'][$s]=="")
                {
                    $this->proyecto_mod->imagenes("",$s+1,$IDProyecto,3);
                    continue;
                }
            $_FILES['userfile']['name']=  $value['name'][$s];
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['file_name'] = $Namef;  //pone el nombre al archivo con numeros
            $config['upload_path'] = './uploads/'.$album;
            $config['allowed_types'] = '*';
            $config['max_size']	= $maxsizep;  //in kilobytes
            $config['max_width']  = '0';  //0 is no limit
            $config['max_height']  = '0'; //0 is no limit
            //carga la configuracion y le da un nombre especial
            $this->load->library('upload', $config,'teach1upload');
            // create an album if not already exist in uploads dir
            if (!is_dir('uploads')) {
                mkdir('./uploads', 0777, true);
            }
            if (!is_dir('uploads/'.$album)) {
                mkdir('./uploads/'.$album, 0777, true);
            }
            $this->teach1upload->do_upload();
            $data = $this->teach1upload->data();
            $name_array[] = $data['file_name'];
            //guardar la imagen en la BD
            
            $this->proyecto_mod->imagenes($name_array[$contadorimagen],$s+1,$IDProyecto,6);       
            $contadorimagen++;
                
            }
            $names= implode(',', $name_array);
            
            ///Para poder hacer resize de las iamgenes
            $this->load->library('image_lib');
            ///hace resize de las imagenes png
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.png';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.png';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            }
        
            ///hace resize de las imagenes jpg
            for($s=0; $s<=$count-1; $s++) {
            $config['image_library'] = 'gd2';
            if($s==0){
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.'.jpg';
            }else{
            $config['source_image'] = './uploads/'.$album.'/'.$Namef.$s.'.jpg';    
            }
            //$config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 100;
            $config['height']   = 100;

            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            } 
	      } 
        
        ////para el paso de secciones
        $name_array = array();
        if (isset($_FILES['userfile2'])) {  
        $value = $_FILES['userfile2'];
        $count = count($value["name"]);
            for($s=0; $s<=$count-1; $s++) {
            $_FILES['userfile']['name']=  trim($value['name'][$s]);
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['file_name'] = $value['name'][$s];  //pone el nombre al archivo con numeros
            $config['upload_path'] = './uploads/'.$album;
            $config['allowed_types'] = '*';
            $config['max_size']	= $maxsizep;  //in kilobytes
            $config['max_width']  = '0';  //0 is no limit
            $config['max_height']  = '0'; //0 is no limit
            //carga la configuracion y le da un nombre especial
            $this->load->library('upload', $config,'teach2upload');
            // create an album if not already exist in uploads dir
            if (!is_dir('uploads')) {
                mkdir('./uploads', 0777, true);
            }
            if (!is_dir('uploads/'.$album)) {
                mkdir('./uploads/'.$album, 0777, true);
            }
            $this->teach2upload->do_upload();
            $data = $this->teach2upload->data();
            $name_array[] = $data['file_name'];
            //print_r($data['file_name']);  //nombre del archivo con terminacion
            //Guardamos todos los adjuntos que va a tener el archivo, que son los objetos otros que usami¿oes en el proyecto
            $file_name = $this->input->post('Lista_nombre_t');
            $nom= $file_name[$s];
            
            $this->proyecto_mod->adjuntos2($IDProyecto,trim($nom),'',trim($value['name'][$s]));    
                
             }
            $names= implode(',', $name_array); 
	      }  
        }
        redirect('misproyectos','refresh');  
    }
    
      
}