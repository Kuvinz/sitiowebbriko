<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Calendario_mod extends CI_Model
{
	public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }
    public function newEv($title,$startdate)
    {
        $query_rol="INSERT INTO calendar(title, startdate, enddate, allDay) VALUES('$title','$startdate','$startdate','false')";
        $last_id=$this->db->query($query_rol);
        $lastid=$this->db->insert_id();
        echo json_encode(array('status'=>'success','eventid'=>$lastid));
    }
    public function changetitle($eventid,$title)
    {

        $query_rol="UPDATE calendar SET title='$title' where id='$eventid'";
        $mod=$this->db->query($query_rol);
        if($mod)
            echo json_encode(array('status'=>'success'));
        else
            echo json_encode(array('status'=>'failed'));
    }
    public function resetdate($title,$startdate,$enddate,$eventid)
    {
        $query_rol="UPDATE calendar SET title='$title', startdate = '$startdate', enddate = '$enddate' where id='$eventid'";
        $mod=$this->db->query($query_rol);
        if($mod)
            echo json_encode(array('status'=>'success'));
        else
            echo json_encode(array('status'=>'failed'));
    }

    public function remove($eventid)
    {
        $query_rol="DELETE FROM calendar where id='$eventid'";
        $mod=$this->db->query($query_rol);
        if($mod)
            echo json_encode(array('status'=>'success'));
        else
            echo json_encode(array('status'=>'failed'));
    }

    public function fetch()
    { 
        $events = array();
        $query_rol="SELECT * FROM calendar";
        $mod=$this->db->query($query_rol)->result_array();
        foreach($mod as $fetch)
        {
            $e = array();
            $e['id'] = $fetch['id'];
            $e['title'] = $fetch['title'];
            $e['start'] = $fetch['startdate'];
            $e['end'] = $fetch['enddate'];
            $e['url'] = $fetch['url'];

            $allday = ($fetch['allDay'] == "true") ? true : false;
            $e['allDay'] = $allday;

            array_push($events, $e);
        }
        echo json_encode($events);
    }

}