<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Usuarios extends CI_Model
{
	public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }
    
    public function nuevoMail($nom,$mail,$perfil)
    {
        $this->load->database();
        $query='INSERT INTO correoscompra(Nombre,Correo,Perfil) 
        VALUES (\''.$nom.'\',\''.$mail.'\',\''.$perfil.'\')';
        $rol=$this->db->query($query);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

	public function alta($nom,$mail,$nick,$nacimiento,$genero,$passwd)
	{
        $this->load->database();
		$query='INSERT INTO usuarios(Nombre,Correo,Nickname,Edad,Genero,Passwd) 
		VALUES (\''.$nom.'\',\''.$mail.'\',\''.$nick.'\','.$nacimiento.',\''.$genero.'\',\''.$passwd.'\')';
        $rol=$this->db->query($query);
        
	}

    public function exi_us($nom)
    {
        $this->load->database();
        $query='SELECT Nickname FROM usuarios WHERE Nickname=\''.$nom.'\'';
        $todos = $this->db->query($query); 
        //echo $query;
        return $todos->num_rows();
    }
    public function exi_mail($nom)
    {
        $this->load->database();
        $query='SELECT Correo FROM usuarios WHERE Correo=\''.$nom.'\'';
        $todos = $this->db->query($query); 
        //echo $query;
        return $todos->num_rows();
    }
    public function existe_mail($nom)
    {
        $this->load->database();
        $query='SELECT Nickname,Correo FROM usuarios WHERE Correo=\''.$nom.'\' LIMIT 1';
        $todos = $this->db->query($query); 
        $row=$todos->row();
        return ($todos->num_rows() === 1 && $row->Correo) ? $row->Nickname : false;
    }

	public function login($usuario,$pass)
    {
        $this->load->database();

        $query='SELECT UID,Passwd,Correo,Nombre FROM usuarios WHERE (Correo=\''.$usuario.'\' OR Nickname=\''.$usuario.'\') AND Passwd=\''.$pass.'\'';
        $todos = $this->db->query($query);        
        //echo $query;
        if($todos -> num_rows() >= 1)
        {

            return $todos->result();
        }
        else
        {
            return false;
        }
    }

    public function verify_reset_password_code($email,$code)
    {
        $this->load->database();
        $query='SELECT Nickname,Correo FROM usuarios WHERE Correo=\''.$email.'\' LIMIT 1';
        $todos = $this->db->query($query); 
        $row=$todos->row();
        if($todos->num_rows() === 1)
        {
            return ($code==md5($this->config->item('salt').$row->Nickname))?true:false;            
        }
        else
        {
            return false;
        }
        
    }

    public function update_passwd()
    {
        $email=$this->input->post('email');
        $pass=sha1($this->input->post('pass'),FALSE);

        $sql='UPDATE usuarios SET Passwd=\''.$pass.'\' WHERE Correo=\''.$email.'\' LIMIT 1';
        $this->db->query($sql);
        if($this->db->affected_rows()===1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}