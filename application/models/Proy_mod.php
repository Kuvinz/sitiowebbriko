<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Proy_mod extends CI_Model
{
	public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }
    public function puntaje()
    {
    	$query_rol='SELECT max(Score) as Score,Nombre FROM snake ';
        $last_id=$this->db->query($query_rol)->result();
        return $last_id;
    } 

    public function puntaje1()
    {
    	$query_rol='SELECT max(Score) as Score,Nombre FROM snake ';
        $last_id=$this->db->query($query_rol)->result();
        echo json_encode($last_id);
    }

    public function guardarp()
    {
    	$nombre=$this->input->get('Nombre');
    	$score=$this->input->get('Score');
    	$query='UPDATE snake SET Nombre=\''.$nombre.'\',Score='.$score;
        $rol=$this->db->query($query);
    }
	public function alta($nom,$desc,$video,$imagen,$tiempo,$dificultad,$codigo)
	{

		$query='INSERT INTO proyectos(Nombre,Descripcion,Dificultad,Tiempo,Video,Imagen,Codigo) 
		VALUES (\''.$nom.'\',\''.$desc.'\','.$dificultad.','.$tiempo.',\''.$video.'\',\''.$imagen.'\',\''.$codigo.'\')';
        $rol=$this->db->query($query);
        
        $query_rol='SELECT ID_proy FROM proyectos ORDER BY ID_proy DESC LIMIT 1';
        $last_id=$this->db->query($query_rol)->result();
        return $last_id[0]->ID_proy;
	}
	public function modxproy($proyecto,$modulo,$puerto)
	{
		$query='INSERT INTO modxproy(ID_proyecto,ID_modulo,Puerto) 
		VALUES ('.$proyecto.','.$modulo.','.$puerto.')';
        $rol=$this->db->query($query);
	}

	public function info($proye)
	{
		$query_rol='SELECT * FROM proyectos WHERE ID_proy='.$proye;
        $info=$this->db->query($query_rol);

        if ($info->num_rows() > 0)
		{
		    return $info->result();
		} 
		else
		{
		    return "false";
		}
        //return $info;

	}

	public function modulos($proye)
	{
		$query_rol='SELECT * FROM modxproy WHERE ID_proyecto='.$proye;
        $mod=$this->db->query($query_rol)->result();
        return $mod;
	}

	public function tproyectos()
    {
        $query_rol='SELECT * FROM proyectos';
        $proy=$this->db->query($query_rol)->result();
        return $proy;
    }
    /*
        ---------------------------Proyectos nuevos querys
    */
    public function todosProyectos()
    {
        $query_rol='SELECT * FROM proyecto';
        $proy=$this->db->query($query_rol)->result();
        return $proy;
    }

    public function imagenesProyectos()
    {
        $query_rol='SELECT * FROM imagenes  WHERE Imagen like "%_general_0.%"';
        $proy=$this->db->query($query_rol)->result();
        return $proy;
    }

    public function videosProyectos($proyecto)
    {
        $query_rol='SELECT * FROM videos where ID_proyecto='.$proyecto;
        $proy=$this->db->query($query_rol)->result();
        return $proy;
    }

    /*

    */
    public function tproyectosxusu($usu)
    {
        $query_rol='SELECT * FROM proyecto WHERE ID_usuario='.$usu;
        $proy=$this->db->query($query_rol)->result();
        return $proy;
    }
	public function proyectost($proye)
	{
		$query_rol='SELECT ID_Proyecto,Descripcion,Nombre,Dificultad,Tiempo FROM proyecto WHERE ID_Proyecto='.$proye;
        $proy=$this->db->query($query_rol)->result();
        echo json_encode($proy);
	}

    public function proyectosFiltro($modulo)
    {
        $query_rol='SELECT * FROM modxproy WHERE ID_modulo='.$modulo;
        $proy=$this->db->query($query_rol)->result();
        echo json_encode($proy);
    }

    public function proyxmod($modulo)
    {
        $query_rol='SELECT ID_proyecto FROM modulosxproyectos WHERE ID_modulo='.$modulo;
        $proy=$this->db->query($query_rol)->result();
        return $proy;

    }

	

}