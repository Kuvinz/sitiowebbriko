<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Proyecto_mod extends CI_Model
{
	public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }
    public function duenoProyect($id)
    {
        $query_rol='SELECT ID_Usuario FROM proyecto WHERE ID_proyecto='.$id.' LIMIT 1';
        $last_id=$this->db->query($query_rol)->result();
        return $last_id[0]->ID_Usuario;  
    }
    public function cate()
    {
        $query_rol='SELECT * FROM categoria';
        $mod=$this->db->query($query_rol)->result();
        return $mod;
    }
    public function altaproyecto($usu,$nom,$desc,$t,$d)
    {
        $query='INSERT INTO proyecto(ID_Usuario,Nombre,Descripcion,Tiempo,Dificultad) 
        VALUES ('.$usu.',\''.$nom.'\',\''.$desc.'\','.$t.','.$d.')';
        $rol=$this->db->query($query);
        
        $query_rol='SELECT ID_proyecto FROM proyecto ORDER BY ID_proyecto DESC LIMIT 1';
        $last_id=$this->db->query($query_rol)->result();
        return $last_id[0]->ID_proyecto;
    }

    public function piezas($nom,$desc,$tipo)
    {
        $query='INSERT INTO piezas(Nombre,Descripcion,ID_tipo) 
        VALUES (\''.$nom.'\',\''.$desc.'\','.$tipo.')';
        $rol=$this->db->query($query);
    }

    public function modulos($proyecto,$modulo,$puerto)
    {
        $query='INSERT INTO modulosxproyectos(ID_proyecto,ID_modulo,puerto) 
        VALUES ('.$proyecto.','.$modulo.','.$puerto.')';
        $rol=$this->db->query($query);
    }

    public function material($proyecto,$pieza,$cantidad)
    {
        $query='INSERT INTO material(ID_proyecto,ID_pieza,cantidad) 
        VALUES ('.$proyecto.','.$pieza.','.$cantidad.')';
        $rol=$this->db->query($query);
    }
    public function accesorios($proyecto,$acc,$cantidad)
    {
        $query='INSERT INTO accesoriosxproyecto(ID_proyecto,ID_accesorio,cantidad) 
        VALUES ('.$proyecto.','.$acc.','.$cantidad.')';
        $rol=$this->db->query($query);
    }


    public function adjuntos($proyecto,$nombre,$desc,$archivo)
    {

        $query='INSERT INTO adjuntos(ID_proyecto,Nombre,Descripcion,Archivo,tipo_adjunto) 
        VALUES ('.$proyecto.',\''.$nombre.'\',\''.$desc.'\',\''.$archivo.'\',0)';
        //echo $query;
        $adjunto=$this->db->query($query);
    }

    public function categorias($cat,$proy)
    {
        $query='INSERT INTO categoriaxproy(ID_categoria,ID_proyecto) 
        VALUES ('.$cat.','.$proy.')';
        $rol=$this->db->query($query);
    }

    public function videos($video,$proy)
    {
        $query='INSERT INTO videos(ID_proyecto,Video) 
        VALUES ('.$proy.',\''.$video.'\')';
        $rol=$this->db->query($query);
    }

    public function texto($proy,$paso,$desc,$crearP)
    {
        $query='INSERT INTO textos(ID_proyecto,ID_crearP,Paso,Texto) 
        VALUES ('.$proy.','.$crearP.','.$paso.',\''.$desc.'\')';
        $rol=$this->db->query($query);
    }

    public function codigo($proy,$paso,$codigo,$crearP)
    {
        $query='INSERT INTO codigo(ID_proyecto,ID_crearP,Paso,Codigo) 
        VALUES ('.$proy.','.$crearP.','.$paso.',\''.$codigo.'\')';
        $rol=$this->db->query($query);
    }
  
public function imagenes($nombreimg,$paso,$proyecto,$crearP)
    {
        $query='INSERT INTO imagenes(ID_proyecto,ID_crearP,Paso,Imagen) 
        VALUES ('.$proyecto.','.$crearP.','.$paso.',\''.$nombreimg.'\')';
        $rol=$this->db->query($query);
    }
	
    public function elimP($value)
    {
        $query='DELETE FROM proyecto WHERE ID_proyecto='.$value;
        $this->db->query($query);   
        
        $query2='DELETE FROM adjuntos WHERE ID_proyecto='.$value;
        $this->db->query($query2);   
        
        $query3='DELETE FROM categoriaxproy WHERE ID_proyecto='.$value;
        $this->db->query($query3);   
        
        $query4='DELETE FROM codigo WHERE ID_proyecto='.$value;
        $this->db->query($query4);   
        
        $query5='DELETE FROM imagenes WHERE ID_proyecto='.$value;
        $this->db->query($query5);   
        
        $query6='DELETE FROM material WHERE ID_proyecto='.$value;
        $this->db->query($query6);   
        
        $query7='DELETE FROM modulosxproyectos WHERE ID_proyecto='.$value;
        $this->db->query($query7);   

        $query8='DELETE FROM textos WHERE ID_proyecto='.$value;
        $this->db->query($query8);  

        $query9='DELETE FROM videos WHERE ID_proyecto='.$value;
        $this->db->query($query9);   

    }
    //para guardar la informacion en la base de datos en la parte de secciones
    public function secciones($proyecto,$titulo,$descripcion,$paso)
    {
        $query='INSERT INTO secciones(ID_proyecto,Titulo,Descripcion,Paso) 
        VALUES ('.$proyecto.',\''.$titulo.'\',\''.$descripcion.'\','.$paso.')';
        $rol=$this->db->query($query);
    }
    
    public function adjuntos2($proyecto,$nombre,$desc,$archivo)
    {

        $query='INSERT INTO adjuntos(ID_proyecto,Nombre,Descripcion,Archivo,tipo_adjunto) 
        VALUES ('.$proyecto.',\''.$nombre.'\',\''.$desc.'\',\''.$archivo.'\',1)';
        //echo $query;
        $adjunto=$this->db->query($query);
    }
    
    

    /* ----------------------------------------------------------- */
     #aqui vamos a hace las peticines de información  por proyecto
	
    public function infoProyect($proy)
    {
        $query_proy='SELECT * FROM proyecto WHERE ID_proyecto='.$proy;
        $proyecto=$this->db->query($query_proy)->result();

        $query_adjuntos='SELECT * FROM adjuntos WHERE tipo_adjunto=0 and ID_proyecto='.$proy;
        $adjuntos=$this->db->query($query_adjuntos)->result();

        $query_categorias='SELECT categoriaxproy.ID_categoria as ID_categoria, categoria.Nombre as Nombre FROM categoriaxproy, categoria WHERE ID_proyecto ='.$proy.' AND categoriaxproy.ID_categoria = categoria.ID_categoria' ;
        $categorias=$this->db->query($query_categorias)->result();

        $query_codigo='SELECT * FROM codigo WHERE ID_proyecto='.$proy;
        $codigo=$this->db->query($query_codigo)->result();

        $query_imagenes='SELECT * FROM imagenes WHERE ID_proyecto='.$proy;
        $imagenes=$this->db->query($query_imagenes)->result();

        $query_material='SELECT material.ID_Pieza as ID_Pieza, material.cantidad as cantidad, piezas.Nombre as Nombre FROM material, piezas WHERE material.ID_Pieza = piezas.ID_Pieza AND material.ID_Proyecto ='.$proy.' ORDER BY material.ID_Pieza';
        $material=$this->db->query($query_material)->result();

        $query_textos='SELECT * FROM textos WHERE ID_proyecto='.$proy;
        $textos=$this->db->query($query_textos)->result();

        $query_videos='SELECT * FROM videos WHERE ID_proyecto='.$proy;
        $videos=$this->db->query($query_videos)->result();

        $query_modulos='SELECT Puerto, ID_modulo, COUNT( * ) AS cantidad, Nombre FROM modulosxproyectos, modulos WHERE ID_proyecto ='.$proy.' AND ID_mod = ID_modulo GROUP BY ID_modulo';
        $modulos2=$this->db->query($query_modulos)->result();

        $query_modulos='SELECT Puerto, ID_modulo FROM modulosxproyectos WHERE ID_proyecto ='.$proy;
        $modulos=$this->db->query($query_modulos)->result();

        $query_accesorios='SELECT accesoriosxproyecto.ID_accesorio as ID_accesorio, acessorios.Nombre as Nombre, accesoriosxproyecto.cantidad as cantidad FROM accesoriosxproyecto, acessorios WHERE accesoriosxproyecto.ID_accesorio = acessorios.ID_accesorio AND accesoriosxproyecto.ID_proyecto ='.$proy.' ORDER BY accesoriosxproyecto.ID_accesorio';
        $accesorios=$this->db->query($query_accesorios)->result();
        
        if (empty($proyecto)) {
            return false;
        }   
        else
        {
            $query_us='SELECT Nombre FROM usuarios WHERE UID='.$proyecto[0]->ID_Usuario;
            $autor=$this->db->query($query_us)->result();

            $infoProyectoCompleta=array('autor'=>$autor,'general'=>$proyecto,'adjuntos'=>$adjuntos,'categorias'=>$categorias,'codigo'=>$codigo,'imagenes'=>$imagenes,'material'=>$material,'textos'=>$textos,'videos'=>$videos,'modulos'=>$modulos,'modulos2'=>$modulos2,'accesorios'=>$accesorios);
            return $infoProyectoCompleta;    
        }

        //return $infoProyectoCompleta;
    }
    
    
    ///para obtener la data de la base de datos para la parte de teach
    public function infoProyectteach($proy)
    {
        $query_secc='SELECT * FROM secciones WHERE ID_proyecto='.$proy.' ORDER BY Paso';
        $secciones=$this->db->query($query_secc)->result();

        $query_adjuntos='SELECT * FROM adjuntos WHERE ID_proyecto='.$proy.' AND tipo_adjunto=1';
        $adjuntos=$this->db->query($query_adjuntos)->result();
        
        $query_textos='SELECT * FROM textos WHERE ID_proyecto='.$proy.' AND ID_crearP=6 ORDER BY Paso';
        $textos=$this->db->query($query_textos)->result();

        $query_codigo='SELECT * FROM codigo WHERE ID_proyecto='.$proy.' AND ID_crearP=6 ORDER BY Paso';
        $codigo=$this->db->query($query_codigo)->result();

        $query_imagenes='SELECT * FROM imagenes WHERE ID_proyecto='.$proy.' AND ID_crearP=6 ORDER BY Paso';
        $imagenes=$this->db->query($query_imagenes)->result();

        $infoProyectoCompleta=array('secciones'=>$secciones,'adjuntos'=>$adjuntos,'codigo'=>$codigo,'imagenes'=>$imagenes,'textos'=>$textos);
        if (empty($textos)) {
            return false;
        }   
        else
        {
            return $infoProyectoCompleta;    
        }
        
    }
	

}