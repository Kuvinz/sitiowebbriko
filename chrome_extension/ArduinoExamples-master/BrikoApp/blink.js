var String_hex_v ="";  //donde se guardara el hex del codigo

var connectionId = -1;   //id de la coneccion que se abre al abrir el puerto
var flash_max_size = 28672;  //memoria maxima del atmega del bootloader
var flashPageSizeBytes = 128;  //256 //128   //bytes por pagina de la memoria
var Page_number  =224;    //112   //224  //paginas de la memoria
var serial_cont=0;
var ff_bytesd=0;
var error_flg=0;
var hex_ff;
var Page_end_lim = 1;
var force_var = 0;
var interval_1;  //para activar un timeout por si no recive toda la data el briko
var interval_2;  //se activa por el primer timeout para mandar
var timeout_flag_return=0;
var timeout_flag_return2=0;

function init(){   //fucnion para inicializar todos los onclick
    
    
    ///listener para los mensajes que llegan de la pagina web
    chrome.runtime.onMessageExternal.addListener(
  function(request, sender, sendResponse) {
      
     //***********************genera los puertos
    if (request.action == "generateports"){
        $('#port-picker').empty();  //borra lo que hay en el select
        Searchdevices(); //pone los devices en un select para despues poder obtenerlos
        return false; //no regresa nada   
        }
      
    //*************************si pido los puertos
    if (request.action == "portsearch"){
      var array_forjson = [];
    //saca la informacion del select
    var xselect = document.getElementById("port-picker");
    for (var i = 0; i < xselect.length; i++) {
        array_forjson.push({name:xselect.options[i].value}); //agrega al arreglo 
    }    
    sendResponse({  //json de respuesta
            "ports" : array_forjson
       });
    return true;  //falso si no queremos que regrese algo
    }
      
    //***********************resetea el briko
    if (request.action == "reset"){
        Resetbriko(request.data);
        return false; //no regresa nada   
    }  
      
    //***********************carga programa, aqui hace chequeo si el programa esta bien compilado o no y regresa exito o error
    if (request.action == "load"){
        String_hex_v = request.data2;  //guarda el hex
        Senddata(request.data); //carga el programa
        sendResponse({  //json de respuesta
            "status" : "ok"
       });
        return true; //regresa el estado  
    } 
      
      
      
      
  });
    
    
    
function Searchdevices(){
    chrome.serial.getDevices(function(devices) {
        for (var i = 0; i < devices.length; i++) {
            $('select#port-picker').append('<option value="' + devices[i].path + '">' + devices[i].path + '</option>');
        }
    }); 
};
    
function Openport(Portselected){
   chrome.serial.connect(Portselected, {bitrate: 57600}, function(info) {
                connectionId = info.connectionId;
   });
};
    
function Closeport(){
  chrome.serial.disconnect(connectionId, function(result) {
  });
};
  
function Resetbriko(Portselected){
    ////reset////////// al abrir el puerto a 1200 y cerrarlo resetea el briko
   chrome.serial.connect(Portselected, {bitrate: 1200}, function(info) {
                connectionId = info.connectionId;
   });  ///end open port
  setTimeout(function(){
  chrome.serial.disconnect(connectionId, function(result) {
                console.log('Connection with id: ' + connectionId + ' closed');
  });  ///end close port
        
//////////////////////////
    },500); // end timeout(espera 200 milis y luego hace la funcion) 
};
    
    
function Senddata(Portselected){
    //inicializa todas las variables y manda borrar el chip, cuando el chip conteste pasara a cargar el codigo con el listener de serial
    serial_cont=0; //inicializa
    ff_bytesd=0;  //para recorrer las paginas
    Page_end_lim=1;  //para saber si ya son paginas en blanco o no
    error_flg=0;  //para saber si hubo un error
    force_var=0; //para saber si estamos escribiendo la direccion otravez despues de tener un error
    Openport(Portselected);  //abre el puerto serial
    setTimeout(function(){
        avr109_erasechip(); //borra lo que hay en el chip 
    },100);
};    

    
// Convert string to ArrayBuffer
var convertStringToArrayBuffer=function(str) {
  var buf=new ArrayBuffer(str.length);
  var bufView=new Uint8Array(buf);
  for (var i=0; i<str.length; i++) {
    bufView[i]=str.charCodeAt(i);
  }
  return buf;
}

function Serialflush(){  //para limpiar el buffer de salida
  chrome.serial.flush(connectionId, function(sendInfo) {});   
}
       
function avr109_erasechip(){
    chrome.serial.send(connectionId, convertStringToArrayBuffer('e'),function(sendInfo) {});
}
    
function avr109_setaddress(reg_address){
    var size_address = "";
    size_address=String.fromCharCode(0xFF&(reg_address>>8));
    size_address=size_address + (String.fromCharCode(0xFF&reg_address));
chrome.serial.send(connectionId, convertStringToArrayBuffer('A'),function(sendInfo) {});
setTimeout(function(){
chrome.serial.send(connectionId,convertStringToArrayBuffer(size_address),function(sendInfo) {});
},20);
}    
    
function avr109_exitbootloader(){
    String_hex_v = ""; //resetea el string
    chrome.serial.send(connectionId, convertStringToArrayBuffer('E'),function(sendInfo) {});
    setTimeout(function(){
        Closeport();  //cierra el puerto depsues de acabar con el bootloader
    },100);
}
    

///funcion que crea un arreglo con todo lo del hex donde el indice del arreglo es igual a la direccion donde debe ser guardado en memoria
var hex_to_array = function(hex_string){
    
    var out  =new ArrayBuffer(flash_max_size);  //crea donde se va guardar
    
    //chip borrado empieza en 'unos' el byte
    for(var w=0;w<flash_max_size;w++){ out[w] = 255; }  
    if(hex_string.length > flash_max_size){  //no puede superar el maximo de la memoria
        ///error
    } else {  
        var char_separated;
        var temp_size ;
        var j;
        var u;
        var x;
        for(var i=0;i<hex_string.length;i++){
            char_separated = hex_string.charAt(i);
            if(char_separated == ':'){  //nueva linea
                //tamano de la data
                temp_size = (parseInt(hex_string.substring(i+1,i+3),16)); 
                j = (parseInt(hex_string.substring(i+3,i+7),16));  //direccion
                if(temp_size != 0){
                    x=9;
                    for(u=0;u<temp_size;u++){
                       //guarda data
                        out[j+u] = parseInt(hex_string.substring(i+x+u,i+x+u+2),16); 
                        x++;
                    }
                }
            }
        }
    }  
    return out;
}

var flash_hex_page = function(hex_array,start){
    var size_page = "";
    size_page=String.fromCharCode(0xFF&(flashPageSizeBytes>>8));
    size_page=size_page + (String.fromCharCode(0xFF&flashPageSizeBytes));
    
    ///checa si las paginas ya estan en unos ya no las manda, ya que son paginas vacias
    var pagewrite = "";
    var F_counter_m = 0;
    for(var w=0; w<(flashPageSizeBytes); w++){ //copy page into array
      pagewrite =pagewrite+(String.fromCharCode(hex_array[start+w])); 
     if(hex_array[start+w]==255){
         F_counter_m++;
     }
    }

if(F_counter_m<(flashPageSizeBytes)){
chrome.serial.send(connectionId, convertStringToArrayBuffer('B'),function(sendInfo) {});
setTimeout(function(){
chrome.serial.send(connectionId,convertStringToArrayBuffer(size_page),function(sendInfo){});
setTimeout(function(){
chrome.serial.send(connectionId, convertStringToArrayBuffer('F'),function(sendInfo) {});
setTimeout(function(){
chrome.serial.send(connectionId,convertStringToArrayBuffer(pagewrite),function(sendInfo){});
},20);
},40);
},20);
return 1;
}else{
return 2;   
}
}
    
///funcion que se activa si el chip no contesta
function myTimer(){
//si el chip no contesto en cierto tiempo entra aqui
if(timeout_flag_return2==1){
   ff_bytesd++; 
}
  error_flg=0;
  serial_cont=1;
  timeout_flag_return=1;
  interval_2 = setInterval(function(){myTimer2()}, 50);//activa el interval2 
  clearInterval(interval_1);  //desactiva el interval
}

//para mandar bytes hasta que salga del error
function myTimer2(){
///rellena con 1
chrome.serial.send(connectionId, convertStringToArrayBuffer(String.fromCharCode(255)),function(sendInfo) {});   
}
    
///declara el listener se serial para que mande todo solo al recibir un caracter de respuesta
chrome.serial.onReceive.addListener(onRead);    
 function onRead(readInfo) {
if(readInfo.data!=null){ //solo si hay data
 //desactiva los intervalos que estan por si falla al mandar la data para que lo vuelva a mandar
  clearInterval(interval_1);  //desactiva el interval
  clearInterval(interval_2);  //desactiva el interval2
    //guarda lo que llego
  var uint8View = new Uint8Array(readInfo.data);
  var value = String.fromCharCode(uint8View[0]);
  Serialflush(); //reset buffer
  readInfo.data=null;  //reset buffer
  //console.log("data "+ value.toString()); //debuge el error
  //console.log("estado "+ serial_cont.toString()); //debuge el error
  ///remplaza el \n por 1  
  value = value.replace(/(\r\n|\n|\r)/gm,"1");
    
   ///para ignorar el ultimo success que regrese el briko 
     if(serial_cont>=3){  
         if(value!="1" && serial_cont==3){  //error al salir
           avr109_exitbootloader();  
           serial_cont=3; //acaba por completo
         } 
         if(value=="1"){  //error al salir
         //resetea todo menos el serial_cont
        serial_cont=10; //acaba por completo
        ff_bytesd=0; //el que va aumentando las paginas
        Page_end_lim=1;  //el que nos dice si hay todavia data que escribir o no en las paginas
        error_flg=0;
        force_var=0;
        $('span#status').html('finish');  //cambia texto de span  
         }
     }else{ 
         
if(value=="1" && error_flg==0 && force_var==0 && timeout_flag_return==0){  //casi optimo donde no hay ningun error
    //console.log("caso 1"); //debuge el error
    switch(serial_cont){
        case 0:  //primero manda la direccion 0 para empezar a escribir desde ahi, tiene autoincrement de direccion mientras le escribimos
        avr109_setaddress(0); //manda la direccion 0 para empezar a escribir ahi
        hex_ff = hex_to_array(String_hex_v); //convierte el codigo a un arreglo
        serial_cont=1; //para pasar al siguiente paso
        break;
            
        case 1: //empieza a escribir por paginas la data, si el page_end_lim se hace dos acaba todo porque detecto que ya no hay mas que escribir, y activa un time out por si el chip no contesta y no trabe todo el proceso
        interval_1 = setInterval(function(){myTimer()}, 1000);//activa el interval de un segundo(timeout)
        Page_end_lim=flash_hex_page(hex_ff,((ff_bytesd)*flashPageSizeBytes));
        ff_bytesd++; //aumenta uno para pasar a la siguiente pagina
        if(ff_bytesd<Page_number){ //si todavia hay paginas que escribir continua, sino acaba
        serial_cont=1;
        }else{
        serial_cont=2;  
        }
        if(Page_end_lim==2){  //detecto paginas en blanco y acaba
        avr109_exitbootloader();  //cierra el bootloader
        serial_cont=3;
        }
        break;
            
        case 2: //acaba la carga y cierra el bootloader
        avr109_exitbootloader(); //cierra el bootloader
        serial_cont=3;
        break;                
     }
}
        
     if(value=="1" && error_flg==1 && force_var==0 && timeout_flag_return==0){ //depues de pasar por un error llega aqui para continuar
         console.log("caso 2"); //debuge el error
         //incrementa para pasar a la siguiente instruccion
         if(serial_cont==2){serial_cont=3;}
         if(serial_cont==1 && (ff_bytesd<Page_number)){serial_cont=1;
                                                      ff_bytesd++;}
         if(serial_cont==1 && (ff_bytesd>=Page_number)){serial_cont=2;}
         if(serial_cont==0){serial_cont=1;}
         timeout_flag_return2=0;
      switch(serial_cont){
    
        case 1: 
        interval_1 = setInterval(function(){myTimer()}, 1000);//activa el interval de un segundo(timeout)
        Page_end_lim=flash_hex_page(hex_ff,((ff_bytesd)*flashPageSizeBytes));
        ff_bytesd++;
        if(Page_end_lim==2){
        avr109_exitbootloader();
        serial_cont=3;
        }
        break;
            
        case 2: 
        serial_cont=3;
        avr109_exitbootloader();
        break;
                     
    }   
         error_flg=0;  //resetea
     }
           
    if(value != "1" && error_flg==1 && force_var==1 && timeout_flag_return==0){  //error al poner la direccion nuevamente, asi que repite
       //console.log("caso 3"); //debuge el error
       avr109_setaddress((ff_bytesd)*flashPageSizeBytes);  
       serial_cont=1;
       force_var=1;  
    }
         
     if(value == "1" && error_flg==1 && force_var==1 && timeout_flag_return==0){  //fue exitosa la escritura de la direccion, asi que hace que se cumpla el de abajo para escribir la pagina
       //console.log("caso 4"); //debuge el error
       force_var=0; 
        value ="2";
    }
       
     if((value != "1"  && force_var==0) || (timeout_flag_return==1)){  //si falla el caso optimo pasa aqui para repetir el ultimo mensaje
         //console.log("caso 5"); //debuge el error
         if(serial_cont==1 && ff_bytesd==0 && value != "2" && timeout_flag_return!=1){serial_cont=0;}
         if(serial_cont==2 && ff_bytesd==Page_number){serial_cont=1;}
         if(serial_cont==1 && error_flg==0 ){ff_bytesd--;serial_cont=5;} //si falla al escribir una pagina hay que volver a pooner la direccion
         timeout_flag_return=0;
     
    switch(serial_cont){
        case 0: 
        avr109_setaddress(0);
        break;
            
        case 1: 
        interval_1 = setInterval(function(){myTimer()}, 1000);//activa el interval de un segundo(timeout)
        Page_end_lim=flash_hex_page(hex_ff,((ff_bytesd)*flashPageSizeBytes));
        Page_end_lim=1; //ignoro aqui lo que diga la funcion
        force_var=0;
        timeout_flag_return2=1; //para sumar y que luego reste si esque falla dos veces esta funcion
        break;
            
        case 2: 
        avr109_exitbootloader();
        break;   
            
        case 5:  //vuelve a poner la direccion para poder volver escribir
        avr109_setaddress((ff_bytesd)*flashPageSizeBytes);  
        serial_cont=1;
        force_var=1;
        break;
              
     }
         error_flg=1;
     }
         
     }
     
}

};
    


}  //end init
    
    


onload = function() {   ///hace al acabar de cargar la pagina  
    init();   ///inicializa los listeners
};
