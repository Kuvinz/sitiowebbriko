///esta pagina siempre corre mientras este habilitada la aplicacion(solo habilitada no activa), para activarla solo leo que llega un mensaje de la pagina
//y crea la pagina, ya dentro de la pagina si le llega un mensaje de cerrar la ventana, la cierra y desactiva los listeners de la extencion para cargar, buscar puertos y resetear

///listener para los mensajes que llegan de la pagina web
    chrome.runtime.onMessageExternal.addListener(
  function(request, sender, sendResponse) {
      
     //***********************genera los puertos
    if (request.action == "activate_app"){
            ///crea la ventana principal
            chrome.app.window.create('main.html', {
                bounds: {
                top: 0,
                left: 0,
                width: 0,
                height: 0
                },
            hidden: true
            });
        }
    });