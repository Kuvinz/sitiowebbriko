function SerialMonitor (api) {
  this.readingInfo = null;
  this.serial = api.serial;
  // The prowser % slowdown at which to close the serial monitor.
  this.spamThresh = 10;
  // Use this for post-disconnect hook
  this.postDisconnectHook = function () {};
  this.onRawMessageReceived = new Event();
  this.onMessageSend = new Event();
}

// - connect to open a serial monitor that behaves well with
// regard to spamming devices
// - disconnect to gracefully disconnect
// - write to send data to the device.

SerialMonitor.prototype = {
  // === API ===
  connect: function (port, baudrate, readCb, closeCb, connectedCb) {
    dbg("SerialRead connecting to port:", port);
    var self = this;
    if (typeof baudrate !== "number") baudrate = Number(baudrate);

    function _closeCb (val) {
      dbg("Serial monitor return value:", val);
      self.disconnect(function() {
        closeCb("monitor", String(val));
      });
    }

    this.disconnect(function () {
      self.doConnect(port, baudrate, readCb, _closeCb, connectedCb);
    });
  },

  disconnect: function (cb) {
    var self = this, callback = (cb || this.postDisconnectHook);

    if (this.readingInfo) {
      if (typeof this.readingInfo.handler === 'function') {
        this.serial.onReceive.removeListener(this.readingInfo.handler);
      }

      if (typeof this.readingInfo.closeHandler === 'function') {
        this.serial.onReceiveError.removeListener(
          this.readingInfo.closeHandler);
      }
      var connectionId = this.readingInfo.connectionId;
      this.readingInfo = null;
      this.serial.disconnect(connectionId, function (ok) {
        // Probably wont reach here anyway.
        if (!ok) {
          console.warn("Failed to disconnect: ", connectionId);
          callback(null, errno.UNKNOWN_MONITOR_ERROR);
        } else {
          dbg("Disconnected ok:", connectionId);
          callback(null, 0);
        }
      });

      // Cleanup syncrhronously
      dbg('Clearing readingInfo:', connectionId);
      return;
    }

    callback(null, 'disconnect');
  },

  reconnect: function (cb) {
    log.log("Reconnecting...");
    if (!this.readingInfo) {
      throw Error("Tried to reconnect a not-connected serial monitor.");
    }

    var self = this, connArgs = self.readingInfo.connectArgs;
    this.disconnect(function () {
      self.connect.apply(self, connArgs);
    });
  },

  write: function (strData, cb) {
    var self = this, callback = cb || function () {};

    if (!this.readingInfo) {
      callback();
      return;
    }

    var data = new ArrayBuffer(strData.length),
        bufferView = new Uint8Array(data);
    for (var i = 0; i < strData.length; i++) {
      bufferView[i] = strData.charCodeAt(i);
    }

    dbg("Sending data:", bufferView, "from string:", strData);
    this.serial.send(self.readingInfo.connectionId, data, function (sendInfo) {
      if (!sendInfo) {
        console.error("No connection to serial monitor");
        callback();
        return;
      }

      if (sendInfo.error) {
        console.error("Failed to send through",
                      self.readingInfo,":", sendInfo.error);
        callback();
        return;
      }

      dbg("Sent bytes:", sendInfo.bytesSent, "connid: ");
      callback(sendInfo.bytesSent);
    });
  },


  // === Helpers ===
  readingHandlerFactory: function (connectionId, readCb, closeCb) {
    var self = this,
        srh = this.singleResponseHanlder.bind(this, connectionId, readCb, closeCb);

    dbg("Reading Info:", this.readingInfo);
    if (readCb !== this.readingInfo.callbackUsedInHandler) {
      this.readingInfo.callbackUsedInHandler = readCb;
      this.readingInfo.handler = srh; // For non burst mode
    }

    return this.readingInfo.handler;
  },

  // If the system seems slow close the serial monitor
  spamGuard: function (closeCb) {
    // NOTE: to test this you can:
    //
    // socat PTY,link=$HOME/cu.fake PTY,link=$HOME/COM
    // sudo ln -s $HOME/cu.fake /dev/cu.fake
    // <open serial monitor>
    // i=0; sdate=$(date +%s); freq=0.1; while true; do i=$(($i+1)); if [[ $(date +%s) -ne $sdate ]]; then sdate=$(date +%s); echo "Requests/sec: $i"; fi;  echo "Hello $i" > COM2; sleep $freq; done & pid=$!; sleep 10; kill $pid
    //
    // Change the freq var to send at other frequencies.
    //
    if (!this.readingInfo.spamGuardCalls) {
      this.readingInfo.spamGuardCalls = 0;
    }
    var self = this, sgc = ++self.readingInfo.spamGuardCalls;

    // Token api call to check responsivenes
    this.serial.getConnections(function (devs) {
      // Spamguardcalls increased by more than 3 while waiting.
      if (self.readingInfo.spamGuardCalls - sgc > (window.bfSpamGuard || self.spamThresh)) {
        log.error("Spamming device:", self.readingInfo.spamGuardCalls - sgc);
        closeCb(errno.SPAMMING_DEVICE);
      }
    });
  },

  _getBufferSize: function (buffer_) {
    return buffer_.reduce(function (a, b) {
      return a.length + b.length;
    });
  },

  // closeCb does the right thing (cleaning up) and baudrate is a
  // number and we are not connected at this point. Use `connect` to
  // not care about the internal state of SerialMonitor.
  doConnect: function (port, baudrate, readCb, closeCb, connectedCb) {
    var self = this;

    // Fail if you find an open connection to our port.
    this.serial.getConnections(function (cnxs){
      if (cnxs.some(function (c) {return c.name == port;})) {
        console.error("Serial monitor connection already open.");
        closeCb(errno.RESOURCE_BUSY);
        return;
      }

      self.serial.connect(port, {bitrate: baudrate, name: port}, function (info) {
        if (!info) {
          console.error("Failed to connect serial:", {bitrate: baudrate, name: port});
          closeCb(errno.RESOURCE_BUSY);
          return;
        }

        dbg("Serial connected to: ", info);
        self.readingInfo = info;
        self.readingInfo.connectArgs = [port, baudrate, readCb, closeCb];
        self.readingInfo.handler = self.readingHandlerFactory(
          self.readingInfo.connectionId, readCb, closeCb);

        self.readingInfo.closeHandler = function (info) {
          log.error("Read error:", info);
          if (info.connectionId != self.readingInfo.connectionId) return;

          var retVal = 0;
          if (info.error != "device_lost") {
            // XXX: make return values for each error.
            retVal = errno.UNKNOWN_MONITOR_ERROR;
          }

          closeCb(retVal);
        };

        self.serial.onReceive.addListener(self.readingInfo.handler);
        self.serial.onReceiveError.addListener(self.readingInfo.closeHandler);

        if (connectedCb) {
          connectedCb();
        }
      });
    });
  },

  bufferedSend: function (chars, cb) {
    this.onRawMessageReceived.dispatch(chars);
    // The event caller may have disconnected.
    if (!this.readingInfo) return;

    // FIXME: if the last line does not end in a newline it should
    // be buffered
    // There are three possible issues (solutions):
    // - Output not readable if it is not delimited by new lines (new line split)
    // - Large lines creates large buffers (timeout/buffersize)
    // - Large lines are buffered for ever (timeout)
    var msgs = chars
          .map(function (c) {return String.fromCharCode(c);})
          .reduce(function (a,b) {return a+b;}, "")
          .split("\n"),
        buffer_head = this.readingInfo.buffer_,
        buffer_tail = this.readingInfo.buffer_.pop() || '',
        msgs_head = msgs.shift() || '',
        tail_msgs = msgs,
        self = this;


    this.readingInfo.buffer_ = buffer_head.concat([buffer_tail + msgs_head])
      .concat(tail_msgs);

    function __flushBuffer() {
      var ret = self.readingInfo.buffer_.join("\n");
      self.readingInfo.buffer_ = [];
      // dbg("Flushing to serial monitor bytes: ", ret.length);
      cb("chrome-serial", ret);
    }

    if (this._getBufferSize(self.readingInfo.buffer_) > self.bufferSize) {
      console.log("SerialMonitor buffer overflow, info:", self.readingInfo);
      __flushBuffer();
      return;
    }

    if (!self.readingInfo.flushing) {
      self.readingInfo.flushing = true;
      setTimeout(function () {
        if (self.readingInfo && self.readingInfo.buffer_.length > 0) {
          self.readingInfo.overflowCount = 0;
          __flushBuffer();
          self.readingInfo.flushing = false;
        }
      }, 100);
    }
  },

  singleResponseHanlder: function  (connectionId, readCb, closeCb, readArg) {
    // A message for a different connection. Ignore
    if (!readArg || readArg.connectionId != connectionId) {
      return;
    }

    if (!this.readingInfo) {
      console.warn("Connection closed but callback not yet unregistered");
      return;
    }


    // If we use the BabelFish overloaded versions of addListener
    // we will receive an array instead of ArrayBuffer.
    var chars = readArg.data;
    if (readArg.data instanceof ArrayBuffer) {
      // this.spamGuard(closeCb);
      // If we use the raw chrome api calls we should check for a
      // spamming device.

      var bufferView = new Uint8Array(readArg.data);
      chars = [].slice.call(bufferView);
    }

    if (!this.readingInfo.buffer_) {
      this.readingInfo.buffer_ = [];
    }

    this.bufferedSend(chars, readCb);
  }
};


function connect_bk(){
 var Serialbk = new SerialMonitor(1);
 Serialbk.apply(connect,[COM12,1200,0,0,0]);    
}

function disconnect_bk(){
 var Serialbk = new SerialMonitor(1);
 Serialbk.apply(disconnect,[0]);   
}

function reset(){
 setTimeout(connect_bk(), 1000); //wait ten seconds before continuing   
 setTimeout(disconnect_bk(), 1000); //wait ten seconds before continuing    
}