/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'CarritoNuevo',
                            type: 'image',
                            rect: ['480px', '0px', '560px', '335px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"CarritoNuevo.png",'0px','0px']
                        },
                        {
                            id: 'AN3_1',
                            type: 'image',
                            rect: ['0px', '9px', '344px', '176px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"AN3%20%281%29.png",'0px','0px']
                        },
                        {
                            id: 'nombres',
                            symbolName: 'nombres',
                            type: 'rect',
                            rect: ['76px', '9px', '344', '112', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '430px', '180px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    labels: {
                        "uno": 0
                    },
                    data: [
                        [
                            "eid56",
                            "left",
                            0,
                            0,
                            "linear",
                            "${AN3_1}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid57",
                            "left",
                            500,
                            0,
                            "linear",
                            "${AN3_1}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid4",
                            "left",
                            0,
                            500,
                            "linear",
                            "${CarritoNuevo}",
                            '960px',
                            '480px'
                        ],
                        [
                            "eid38",
                            "left",
                            0,
                            500,
                            "linear",
                            "${nombres}",
                            '76px',
                            '76px'
                        ]
                    ]
                }
            },
            "nombres": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'aprende1',
                            opacity: '1',
                            rect: ['0px', '230px', '344px', '112px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/aprende1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'crea1',
                            opacity: '1',
                            rect: ['67px', '120px', '209px', '112px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/crea1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'juega1',
                            opacity: '1',
                            rect: ['34px', '342px', '275px', '112px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/juega1.png', '0px', '0px']
                        },
                        {
                            rect: ['34px', '-111px', '275px', '112px', 'auto', 'auto'],
                            id: 'juega12',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/juega1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            overflow: 'hidden',
                            rect: [null, null, '344px', '112px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    labels: {
                        "dos": 0
                    },
                    data: [
                        [
                            "eid24",
                            "left",
                            1000,
                            0,
                            "linear",
                            "${juega1}",
                            '34px',
                            '34px'
                        ],
                        [
                            "eid54",
                            "opacity",
                            2500,
                            0,
                            "linear",
                            "${crea1}",
                            '1',
                            '1'
                        ],
                        [
                            "eid51",
                            "left",
                            3000,
                            0,
                            "linear",
                            "${juega12}",
                            '34px',
                            '34px'
                        ],
                        [
                            "eid25",
                            "top",
                            500,
                            500,
                            "linear",
                            "${juega1}",
                            '3px',
                            '112px'
                        ],
                        [
                            "eid33",
                            "top",
                            1500,
                            500,
                            "linear",
                            "${juega1}",
                            '112px',
                            '222px'
                        ],
                        [
                            "eid52",
                            "top",
                            2500,
                            500,
                            "linear",
                            "${juega12}",
                            '-111px',
                            '0px'
                        ],
                        [
                            "eid32",
                            "top",
                            1500,
                            500,
                            "linear",
                            "${crea1}",
                            '-110px',
                            '0px'
                        ],
                        [
                            "eid55",
                            "top",
                            2500,
                            500,
                            "linear",
                            "${crea1}",
                            '0px',
                            '120px'
                        ],
                        [
                            "eid27",
                            "top",
                            500,
                            500,
                            "linear",
                            "${aprende1}",
                            '-110px',
                            '0px'
                        ],
                        [
                            "eid34",
                            "top",
                            1500,
                            500,
                            "linear",
                            "${aprende1}",
                            '0px',
                            '110px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("Juega_edgeActions.js");
})("EDGE-264274151");
