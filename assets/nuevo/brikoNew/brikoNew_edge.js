/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'MAXWELLREGULAR, san-serif': '<link href=\'/MAXWELLREGULAR.ttf\' rel=\'stylesheet\' type=\'text/css\'>',
            'MAXWELLREGULAR, sans-serif': '<link ref=\"stylesheet\" href=\"stylesheet.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\" />',
            'MAXWELLREGULAR': '<link href=\"stylesheet.css\" rel=\"stylesheet\" type=\"text/css\" >'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'FondoGraciasCompra',
                            type: 'image',
                            rect: ['0%', '0%', '100%', '100%', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"FondoGraciasCompra.png",'0px','0px']
                        },
                        {
                            id: 'Text4',
                            type: 'text',
                            rect: ['2.5%', '68.4%', '42%', '15.8%', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">En breve nos pondremos en contacto contigo vía correo electrónico​</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR, sans-serif', [130, "%"], "rgba(255,255,255,1)", "700", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Text5',
                            type: 'text',
                            rect: ['2.4%', '85.8%', '42%', '10%', 'auto', 'auto'],
                            opacity: '0.000000',
                            text: "<p style=\"margin: 0px;\">Para darte los detalles de la mejor inversión de tu vida.​</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR, sans-serif', [130, "%"], "rgba(255,255,255,1)", "700", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Text3',
                            type: 'text',
                            rect: ['2.4%', '46.7%', '42%', '10%', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">​La generación que hace realidad todas sus ideas y Proyectos de una manera fácil y rápida.</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR, sans-serif', [130, "%"], "rgba(255,255,255,1)", "700", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['0.1%', '4.3%', '100%', '15.8%', 'auto', 'auto'],
                            opacity: '0.0085470085470086',
                            text: "<p style=\"margin: 0px;\">​Gracias por tu compra</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR', [437.5, "%"], "rgba(255,255,255,1.00)", "700", "none", "", "break-word", "normal"]
                        },
                        {
                            id: 'Text2',
                            type: 'text',
                            rect: ['2.4%', '31.9%', '42%', '11.7%', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">​¡Ahora ya eres parte de la nueva generación <span style=\"font-weight: 800;\">briko</span>!</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR, sans-serif', [130, "%"], "rgba(255,255,255,1)", "700", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '100%', '100%', 'auto', 'auto'],
                            sizeRange: ['25%','100%','',''],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid307",
                            "opacity",
                            1000,
                            500,
                            "linear",
                            "${Text2}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid316",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${Text5}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid313",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${Text5}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid309",
                            "opacity",
                            1500,
                            500,
                            "linear",
                            "${Text3}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid323",
                            "width",
                            0,
                            0,
                            "linear",
                            "${Text}",
                            '99.96%',
                            '99.96%'
                        ],
                        [
                            "eid305",
                            "opacity",
                            0,
                            1000,
                            "linear",
                            "${Text}",
                            '0.0085470085470086',
                            '1'
                        ],
                        [
                            "eid351",
                            "font-size",
                            0,
                            0,
                            "linear",
                            "${Text4}",
                            '130%',
                            '130%'
                        ],
                        [
                            "eid321",
                            "left",
                            0,
                            0,
                            "linear",
                            "${Text}",
                            '0.05%',
                            '0.05%'
                        ],
                        [
                            "eid349",
                            "font-size",
                            0,
                            0,
                            "linear",
                            "${Text3}",
                            '130%',
                            '130%'
                        ],
                        [
                            "eid348",
                            "font-size",
                            0,
                            0,
                            "linear",
                            "${Text2}",
                            '130%',
                            '130%'
                        ],
                        [
                            "eid350",
                            "font-size",
                            0,
                            0,
                            "linear",
                            "${Text5}",
                            '130%',
                            '130%'
                        ],
                        [
                            "eid311",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${Text4}",
                            '0.000000',
                            '1'
                        ]
                    ]
                }
            },
            "Boton": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '159px', '64px', 'auto', 'auto'],
                            borderRadius: ['50%', '50%', '50%', '50%'],
                            id: 'Ellipse',
                            stroke: [0, 'rgba(0,0,0,1)', 'none'],
                            type: 'ellipse',
                            fill: ['rgba(219,0,0,1.00)']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '159px', '64px']
                        }
                    }
                },
                timeline: {
                    duration: 5894,
                    autoPlay: true,
                    data: [
                        [
                            "eid232",
                            "background-color",
                            5894,
                            0,
                            "linear",
                            "${Ellipse}",
                            'rgba(219,0,0,1.00)',
                            'rgba(219,0,0,1.00)'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("brikoNew_edgeActions.js");
})("EDGE-85448280");
