/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      

      Symbol.bindElementAction(compId, symbolName, "${Stage}", "touchstart", function(sym, e) {
         sym.stop();
         sym.play("uno");// insert code to be run when an element gains focus
         // insert code to be run when a user touches the object (for touch devices only)

      });
      //Edge binding end

      

      

      

      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         
         		function isScrolledIntoView(elem) {
         			var docViewTop = $(window.parent.document).scrollTop();
         			var docViewBottom = docViewTop + $(window.parent.document).height();
         			//console.log(docViewTop);
         			
         			var elemTop = elem.offset().top;
         			var elemBottom = elemTop + elem.height();
         			
         			return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
         			  && (elemBottom+1000 <= docViewBottom) &&  (elemTop >= docViewTop) && (docViewTop>100) );
         		}		  
         
         		var element = sym.getSymbolElement();
         		
         			$(window.parent.document).on("scroll", function(e) {
         				if(isScrolledIntoView(element)) {
         					console.log('Start me up');	
         					sym.stop();
         					sym.play("uno");
         					$(window).off("scroll");
         				}
         			});
         
         
         
         
               });

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'Boton'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${Ellipse}", "click", function(sym, e) {
         // insert code for mouse click here
         // Hide an element 
         sym.$("_112").hide();
         

      });
      //Edge binding end

   })("Boton");
   //Edge symbol end:'Boton'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-85448280");