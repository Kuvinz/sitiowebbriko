/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'MaxwellRegular': '<link ref=\"stylesheet\" href=\"stylesheet.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\" />'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'TPaso1',
                            type: 'text',
                            rect: ['0px', '0px', '1200px', '92px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​ 1.- Conoce brikode</p><p style=\"margin: 0px;\">​</p>",
                            align: "center",
                            font: ['MaxwellRegular', [72, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                        },
                        {
                            id: 'paso1',
                            type: 'image',
                            rect: ['0px', '108px', '819px', '620px', 'auto', 'auto'],
                            clip: 'rect(23.255859375px 746.9072265625px 496.744140625px 111.62744140625px)',
                            fill: ["rgba(0,0,0,0)",im+"paso1.png",'0px','0px']
                        },
                        {
                            id: 'Borrar_texto2',
                            type: 'rect',
                            rect: ['576px', '147px', '184px', '319px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,1)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'Borrar_texto3',
                            type: 'rect',
                            rect: ['565px', '187px', '235px', '72px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,1)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'Borrar_texto4',
                            type: 'rect',
                            rect: ['525px', '506px', '235px', '72px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,1)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'Borrar_texto1',
                            type: 'rect',
                            rect: ['546px', '306px', '175px', '79px', 'auto', 'auto'],
                            fill: ["rgba(255,255,255,1)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'Herramientas',
                            type: 'text',
                            rect: ['482px', '150px', '472px', '43px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">Barra de herramientas</p><p style=\"margin: 0px;\">​</p>",
                            align: "center",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Botones',
                            type: 'text',
                            rect: ['472px', '218px', '403px', '37px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">Barra de botones</p>",
                            align: "center",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Nproyecto',
                            type: 'text',
                            rect: ['497px', '186px', '403px', '37px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">Nombre del proyecto</p>",
                            align: "center",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Editor_titulo',
                            type: 'text',
                            rect: ['429px', '287px', '403px', '37px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">Editor de texto</p><p style=\"margin: 0px;\">​</p>",
                            align: "center",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Area_estado',
                            type: 'text',
                            rect: ['464px', '487px', '429px', '37px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">Área de estado<br></p>",
                            align: "center",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'consola',
                            type: 'text',
                            rect: ['464px', '524px', '403px', '37px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">Consola de mensajes<br></p>",
                            align: "center",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        },
                        {
                            id: 'Editor_texto',
                            type: 'text',
                            rect: ['534px', '324px', '403px', '92px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​Área donde vas a escribir y editar tu programa</p>",
                            align: "left",
                            font: ['MaxwellRegular', [30, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                            textStyle: ["", "", "", "", "none"]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1200px', '700px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("PrimerP_edgeActions.js");
})("EDGE-559212858");
