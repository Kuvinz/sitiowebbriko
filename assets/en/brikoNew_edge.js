/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'MAXWELLREGULAR, san-serif': '<link href=\'/MAXWELLREGULAR.ttf\' rel=\'stylesheet\' type=\'text/css\'>',
            'MAXWELLREGULAR, sans-serif': '<link ref=\"stylesheet\" href=\"stylesheet.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\" />',
            'MAXWELLREGULAR': '<link href=\"stylesheet.css\" rel=\"stylesheet\" type=\"text/css\" >'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: '_62',
                            type: 'image',
                            rect: ['337px', '-101px', '725px', '725px', 'auto', 'auto'],
                            clip: 'rect(22.5px 427.5px 130px 235px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"6.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_52',
                            type: 'image',
                            rect: ['336px', '-100px', '725px', '725px', 'auto', 'auto'],
                            clip: 'rect(510px 625px 702.5px 432.5px)',
                            opacity: '0.9600000381469727',
                            fill: ["rgba(0,0,0,0)",im+"5.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_42',
                            type: 'image',
                            rect: ['336px', '-100px', '725px', '725px', 'auto', 'auto'],
                            clip: 'rect(290px 697.5px 430px 560px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"4.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_32',
                            type: 'image',
                            rect: ['336px', '-100px', '725px', '725px', 'auto', 'auto'],
                            clip: 'rect(75px 667.5px 242.5px 502.5px)',
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"3.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_22',
                            type: 'image',
                            rect: ['336px', '-103px', '725px', '725px', 'auto', 'auto'],
                            clip: 'rect(285px 160px 435px 25px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"2.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_12',
                            type: 'image',
                            rect: ['436px', '0px', '525px', '525px', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"1.png",'0px','0px']
                        },
                        {
                            id: '_72',
                            type: 'image',
                            rect: ['338px', '-99px', '725px', '725px', 'auto', 'auto'],
                            clip: 'rect(345.73779296875px 281.5576171875px 401.721435546875px 111.47509765625px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"7.png",'0px','0px'],
                            transform: [[],['1'],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_82',
                            type: 'image',
                            rect: ['336px', '-100px', '725px', '725px', 'auto', 'auto'],
                            overflow: 'visible',
                            clip: 'rect(80.39208984375px 387.70556640625px 289.385986328125px 333.37255859375px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"8.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_102',
                            type: 'image',
                            rect: ['336px', '-100px', '725px', '725px', 'auto', 'auto'],
                            overflow: 'visible',
                            clip: 'rect(146.9388427734375px 588.26513671875px 333.1630859375px 397.958984375px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"10.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_112',
                            type: 'image',
                            rect: ['336px', '-99px', '725px', '725px', 'auto', 'auto'],
                            overflow: 'visible',
                            clip: 'rect(344.89794921875px 610.7138671875px 396.428466796875px 444.89794921875px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"11.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: '_122',
                            type: 'image',
                            rect: ['336px', '-99px', '725px', '725px', 'auto', 'auto'],
                            overflow: 'visible',
                            clip: 'rect(412.2447509765625px 571.9384765625px 582.142822265625px 404.08154296875px)',
                            opacity: '0.000000',
                            fill: ["rgba(0,0,0,0)",im+"12.png",'0px','0px'],
                            transform: [[],[],[],['0.72414','0.72414']]
                        },
                        {
                            id: 'Titulito1',
                            type: 'image',
                            rect: ['-450px', '0', '450px', '300px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Titulito1.png",'0px','0px']
                        },
                        {
                            id: 'Rectangle',
                            type: 'rect',
                            rect: ['14px', '325px', '167px', '37px', 'auto', 'auto'],
                            borderRadius: ["50px", "50px", "50px", "50px 50px"],
                            opacity: '0',
                            fill: ["rgba(61,177,229,1.00)"],
                            stroke: [0,"rgb(0, 0, 0)","none"]
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['14px', '318px', '169px', '57px', 'auto', 'auto'],
                            opacity: '0',
                            text: "<p style=\"margin: 0px;\">​Includes</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR', [40, "px"], "rgba(255,255,255,1.00)", "700", "none", "", "break-word", "normal"]
                        },
                        {
                            id: 'Piezas',
                            type: 'image',
                            rect: ['289px', '392px', '161px', '161px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Piezas.png",'0px','0px']
                        },
                        {
                            id: 'CablesPlanos',
                            type: 'image',
                            rect: ['435px', '392px', '161px', '161px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"CablesPlanos.png",'0px','0px']
                        },
                        {
                            id: 'Bateria',
                            type: 'image',
                            rect: ['0px', '390px', '161px', '161px', 'auto', 'auto'],
                            clip: 'rect(0px 156.1220703125px 161px 0px)',
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Bateria.png",'0px','0px']
                        },
                        {
                            id: 'USB',
                            type: 'image',
                            rect: ['148px', '389px', '161px', '161px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"USB.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '960px', '650px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 5500,
                    autoPlay: true,
                    data: [
                        [
                            "eid255",
                            "top",
                            5250,
                            0,
                            "linear",
                            "${Bateria}",
                            '390px',
                            '390px'
                        ],
                        [
                            "eid228",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_52}",
                            '-100px',
                            '-100px'
                        ],
                        [
                            "eid193",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_122}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid101",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_112}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid35",
                            "rotateZ",
                            3500,
                            0,
                            "linear",
                            "${_72}",
                            '1deg',
                            '1deg'
                        ],
                        [
                            "eid117",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_82}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid138",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_52}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid130",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_72}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid121",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_102}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid134",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_22}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid88",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_102}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid55",
                            "opacity",
                            4000,
                            250,
                            "linear",
                            "${_102}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid56",
                            "opacity",
                            4250,
                            250,
                            "linear",
                            "${_102}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid106",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_32}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid90",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_72}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid33",
                            "opacity",
                            3000,
                            250,
                            "linear",
                            "${_72}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid34",
                            "opacity",
                            3250,
                            250,
                            "linear",
                            "${_72}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid220",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_32}",
                            '-100px',
                            '-100px'
                        ],
                        [
                            "eid219",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_112}",
                            '-99px',
                            '-99px'
                        ],
                        [
                            "eid93",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_42}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid24",
                            "opacity",
                            1500,
                            250,
                            "linear",
                            "${_42}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid25",
                            "opacity",
                            1750,
                            250,
                            "linear",
                            "${_42}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid225",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_62}",
                            '-101px',
                            '-101px'
                        ],
                        [
                            "eid222",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_122}",
                            '-99px',
                            '-99px'
                        ],
                        [
                            "eid226",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_72}",
                            '-99px',
                            '-99px'
                        ],
                        [
                            "eid278",
                            "font-size",
                            2500,
                            0,
                            "linear",
                            "${Text}",
                            '40px',
                            '40px'
                        ],
                        [
                            "eid197",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_102}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid243",
                            "opacity",
                            1250,
                            250,
                            "linear",
                            "${Text}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid251",
                            "opacity",
                            2250,
                            250,
                            "linear",
                            "${CablesPlanos}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid100",
                            "width",
                            5500,
                            0,
                            "linear",
                            "${_12}",
                            '525px',
                            '525px'
                        ],
                        [
                            "eid234",
                            "left",
                            0,
                            500,
                            "linear",
                            "${Titulito1}",
                            '-450px',
                            '0px'
                        ],
                        [
                            "eid118",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_82}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid122",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_102}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid110",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_42}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid227",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_22}",
                            '-103px',
                            '-103px'
                        ],
                        [
                            "eid249",
                            "opacity",
                            2000,
                            250,
                            "linear",
                            "${Piezas}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid223",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_82}",
                            '-100px',
                            '-100px'
                        ],
                        [
                            "eid133",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_22}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid102",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_112}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid189",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_32}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid257",
                            "top",
                            2500,
                            0,
                            "linear",
                            "${Piezas}",
                            '392px',
                            '392px'
                        ],
                        [
                            "eid281",
                            "left",
                            2500,
                            0,
                            "linear",
                            "${Text}",
                            '14px',
                            '14px'
                        ],
                        [
                            "eid91",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_22}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid9",
                            "opacity",
                            500,
                            250,
                            "linear",
                            "${_22}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid10",
                            "opacity",
                            750,
                            250,
                            "linear",
                            "${_22}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid126",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_62}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid125",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_62}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid87",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_122}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid63",
                            "opacity",
                            5000,
                            250,
                            "linear",
                            "${_122}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid64",
                            "opacity",
                            5250,
                            250,
                            "linear",
                            "${_122}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid221",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_42}",
                            '-100px',
                            '-100px'
                        ],
                        [
                            "eid245",
                            "opacity",
                            1500,
                            250,
                            "linear",
                            "${Bateria}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid185",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_12}",
                            '436px',
                            '436px'
                        ],
                        [
                            "eid105",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_32}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid247",
                            "opacity",
                            1750,
                            250,
                            "linear",
                            "${USB}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid203",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_22}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid109",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_42}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid89",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_82}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid52",
                            "opacity",
                            3500,
                            250,
                            "linear",
                            "${_82}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid53",
                            "opacity",
                            3750,
                            250,
                            "linear",
                            "${_82}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid58",
                            "opacity",
                            4500,
                            0,
                            "linear",
                            "${_82}",
                            '1',
                            '1'
                        ],
                        [
                            "eid201",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_72}",
                            '338px',
                            '338px'
                        ],
                        [
                            "eid113",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_122}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid99",
                            "height",
                            5500,
                            0,
                            "linear",
                            "${_12}",
                            '525px',
                            '525px'
                        ],
                        [
                            "eid191",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_42}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid254",
                            "top",
                            2500,
                            0,
                            "linear",
                            "${USB}",
                            '389px',
                            '389px'
                        ],
                        [
                            "eid94",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_52}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid27",
                            "opacity",
                            2000,
                            250,
                            "linear",
                            "${_52}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid28",
                            "opacity",
                            2250,
                            250,
                            "linear",
                            "${_52}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid98",
                            "opacity",
                            2500,
                            3000,
                            "linear",
                            "${_52}",
                            '1',
                            '0.9600000381469727'
                        ],
                        [
                            "eid218",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_12}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid96",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_112}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid60",
                            "opacity",
                            4500,
                            250,
                            "linear",
                            "${_112}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid61",
                            "opacity",
                            4750,
                            250,
                            "linear",
                            "${_112}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid241",
                            "opacity",
                            1250,
                            250,
                            "linear",
                            "${Rectangle}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid129",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_72}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid4",
                            "opacity",
                            0,
                            250,
                            "linear",
                            "${_12}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid5",
                            "opacity",
                            250,
                            250,
                            "linear",
                            "${_12}",
                            '0.5',
                            '1'
                        ],
                        [
                            "eid92",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_32}",
                            '0',
                            '0'
                        ],
                        [
                            "eid22",
                            "opacity",
                            1000,
                            250,
                            "linear",
                            "${_32}",
                            '0',
                            '0.500000'
                        ],
                        [
                            "eid21",
                            "opacity",
                            1250,
                            250,
                            "linear",
                            "${_32}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid282",
                            "top",
                            2500,
                            0,
                            "linear",
                            "${Text}",
                            '318px',
                            '318px'
                        ],
                        [
                            "eid114",
                            "scaleY",
                            5500,
                            0,
                            "linear",
                            "${_122}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid187",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_112}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid95",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${_62}",
                            '0.000000',
                            '0.000000'
                        ],
                        [
                            "eid30",
                            "opacity",
                            2500,
                            250,
                            "linear",
                            "${_62}",
                            '0.000000',
                            '0.5'
                        ],
                        [
                            "eid31",
                            "opacity",
                            2750,
                            250,
                            "linear",
                            "${_62}",
                            '0.500000',
                            '1'
                        ],
                        [
                            "eid199",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_62}",
                            '337px',
                            '337px'
                        ],
                        [
                            "eid137",
                            "scaleX",
                            5500,
                            0,
                            "linear",
                            "${_52}",
                            '0.72414',
                            '0.72414'
                        ],
                        [
                            "eid195",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_82}",
                            '336px',
                            '336px'
                        ],
                        [
                            "eid253",
                            "top",
                            2500,
                            0,
                            "linear",
                            "${CablesPlanos}",
                            '392px',
                            '392px'
                        ],
                        [
                            "eid17",
                            "clip",
                            1000,
                            0,
                            "linear",
                            "${_22}",
                            [285,160,435,25],
                            [285,160,435,25],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid224",
                            "top",
                            5500,
                            0,
                            "linear",
                            "${_102}",
                            '-100px',
                            '-100px'
                        ],
                        [
                            "eid205",
                            "left",
                            5500,
                            0,
                            "linear",
                            "${_52}",
                            '336px',
                            '336px'
                        ]
                    ]
                }
            },
            "Boton": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '159px', '64px', 'auto', 'auto'],
                            borderRadius: ['50%', '50%', '50%', '50%'],
                            id: 'Ellipse',
                            stroke: [0, 'rgba(0,0,0,1)', 'none'],
                            type: 'ellipse',
                            fill: ['rgba(219,0,0,1.00)']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '159px', '64px']
                        }
                    }
                },
                timeline: {
                    duration: 5894,
                    autoPlay: true,
                    data: [
                        [
                            "eid232",
                            "background-color",
                            5894,
                            0,
                            "linear",
                            "${Ellipse}",
                            'rgba(219,0,0,1.00)',
                            'rgba(219,0,0,1.00)'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("brikoNew_edgeActions.js");
})("EDGE-85448280");
