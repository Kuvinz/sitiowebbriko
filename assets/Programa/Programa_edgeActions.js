/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "${Stage}", "focus", function(sym, e) {
         sym.stop();
         sym.play("uno");// insert code to be run when an element gains focus

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${Stage}", "mouseenter", function(sym, e) {
         sym.stop();
         sym.play("uno");// insert code to be run when the mouse enters an element

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'teclas'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3250, function(sym, e) {
         sym.play("dos");// insert code here

      });
      //Edge binding end

   })("teclas");
   //Edge symbol end:'teclas'

   //=========================================================
   
   //Edge symbol: 'texto'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.play("tres");// insert code here

      });
      //Edge binding end

   })("texto");
   //Edge symbol end:'texto'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-189591165");