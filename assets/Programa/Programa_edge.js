/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Compu',
                            type: 'image',
                            rect: ['0px', '-1px', '380px', '401px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Compu.png",'0px','0px']
                        },
                        {
                            id: 'AN3_2',
                            type: 'image',
                            rect: ['456px', '0px', '503px', '238px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"AN3%20%282%29.png",'0px','0px']
                        },
                        {
                            id: 'teclas2',
                            symbolName: 'teclas',
                            type: 'rect',
                            rect: ['14px', '276', '351', '105', 'auto', 'auto']
                        },
                        {
                            id: 'texto',
                            symbolName: 'texto',
                            type: 'rect',
                            rect: ['54px', '24', '279', '176', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '393px', '386px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 3250,
                    autoPlay: true,
                    labels: {
                        "uno": 0
                    },
                    data: [
                        [
                            "eid71",
                            "left",
                            719,
                            0,
                            "linear",
                            "${teclas2}",
                            '14px',
                            '14px'
                        ],
                        [
                            "eid70",
                            "left",
                            719,
                            0,
                            "linear",
                            "${texto}",
                            '54px',
                            '54px'
                        ],
                        [
                            "eid9",
                            "left",
                            0,
                            500,
                            "linear",
                            "${AN3_2}",
                            '960px',
                            '456px'
                        ]
                    ]
                }
            },
            "teclas": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['39px', '20px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla1',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['39px', '55px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla7',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['81px', '56px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla8',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['246px', '56px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla1Copy3',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['122px', '57px', '108px', '27px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla9',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['288px', '32px', '33px', '33px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla10',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['203px', '20px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla2',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['246px', '21px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla3',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['165px', '20px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla5',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['123px', '20px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla4',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        },
                        {
                            rect: ['81px', '20px', '28px', '28px', 'auto', 'auto'],
                            borderRadius: ['10px', '10px', '10px', '10px'],
                            opacity: '0',
                            id: 'tecla6',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(55,150,200,1.00)']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '351px', '105px']
                        }
                    }
                },
                timeline: {
                    duration: 3250,
                    autoPlay: true,
                    labels: {
                        "dos": 0
                    },
                    data: [
                        [
                            "eid29",
                            "opacity",
                            1500,
                            123,
                            "linear",
                            "${tecla8}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid30",
                            "opacity",
                            1623,
                            127,
                            "linear",
                            "${tecla8}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid27",
                            "opacity",
                            1250,
                            123,
                            "linear",
                            "${tecla2}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid28",
                            "opacity",
                            1373,
                            127,
                            "linear",
                            "${tecla2}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid19",
                            "opacity",
                            500,
                            125,
                            "linear",
                            "${tecla1}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid20",
                            "opacity",
                            625,
                            125,
                            "linear",
                            "${tecla1}",
                            '1',
                            '0'
                        ],
                        [
                            "eid37",
                            "opacity",
                            2500,
                            123,
                            "linear",
                            "${tecla1Copy3}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid38",
                            "opacity",
                            2623,
                            127,
                            "linear",
                            "${tecla1Copy3}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid22",
                            "opacity",
                            750,
                            123,
                            "linear",
                            "${tecla4}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid23",
                            "opacity",
                            873,
                            127,
                            "linear",
                            "${tecla4}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid25",
                            "opacity",
                            1000,
                            123,
                            "linear",
                            "${tecla5}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid26",
                            "opacity",
                            1123,
                            127,
                            "linear",
                            "${tecla5}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid33",
                            "opacity",
                            2000,
                            123,
                            "linear",
                            "${tecla7}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid34",
                            "opacity",
                            2123,
                            127,
                            "linear",
                            "${tecla7}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid39",
                            "opacity",
                            2750,
                            123,
                            "linear",
                            "${tecla3}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid40",
                            "opacity",
                            2873,
                            127,
                            "linear",
                            "${tecla3}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid31",
                            "opacity",
                            1750,
                            123,
                            "linear",
                            "${tecla9}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid32",
                            "opacity",
                            1873,
                            127,
                            "linear",
                            "${tecla9}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid35",
                            "opacity",
                            2250,
                            123,
                            "linear",
                            "${tecla10}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid36",
                            "opacity",
                            2373,
                            127,
                            "linear",
                            "${tecla10}",
                            '0.996510',
                            '0'
                        ],
                        [
                            "eid41",
                            "opacity",
                            3000,
                            123,
                            "linear",
                            "${tecla6}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid42",
                            "opacity",
                            3123,
                            127,
                            "linear",
                            "${tecla6}",
                            '0.996510',
                            '0'
                        ]
                    ]
                }
            },
            "texto": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['8px', '6px', '66px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(55,150,200,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par1',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​motorbk</p>',
                            type: 'text'
                        },
                        {
                            rect: ['8px', '41px', '92px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(55,150,200,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par3',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​code<span style=\"color: rgb(0, 0, 0);\">(){</span></p>',
                            type: 'text'
                        },
                        {
                            rect: ['8px', '132px', '48px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(55,150,200,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par10',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​<span style=\"color: rgb(0, 0, 0);\">}</span></p>',
                            type: 'text'
                        },
                        {
                            rect: ['8px', '63px', '46px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(0,0,0,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par4',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​motor.</p>',
                            type: 'text'
                        },
                        {
                            rect: ['8px', '81px', '110px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(0,0,0,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par6',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\"><span style=\"color: rgb(55, 150, 200);\">delay</span>(1000);</p>',
                            type: 'text'
                        },
                        {
                            rect: ['8px', '116px', '94px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(0,0,0,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par9',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\"><span style=\"color: rgb(55, 150, 200);\">delay</span>(1000);</p>',
                            type: 'text'
                        },
                        {
                            rect: ['45px', '63px', '110px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(0,0,0,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par5',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​<span style=\"color: rgb(55, 150, 200);\">set</span>(<span style=\"color: rgb(200, 55, 145);\">RIGHT</span>,255);</p>',
                            type: 'text'
                        },
                        {
                            rect: ['8px', '98px', '48px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(0,0,0,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par7',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​motor.</p>',
                            type: 'text'
                        },
                        {
                            rect: ['45px', '98px', '94px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(0,0,0,1.00)', 'normal', 'none', '', 'break-word', 'normal'],
                            id: 'par8',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​<span style=\"color: rgb(55, 150, 200);\">set</span>(<font color=\"#c83791\">LEFT</font>);</p>',
                            type: 'text'
                        },
                        {
                            textStyle: ['', '', '', '', 'none'],
                            rect: ['65px', '6px', '110px', '18px', 'auto', 'auto'],
                            font: ['Arial, Helvetica, sans-serif', [13, 'px'], 'rgba(200,55,145,1.00)', '400', 'none', 'normal', 'break-word', 'normal'],
                            align: 'left',
                            id: 'par2',
                            opacity: '0',
                            text: '<p style=\"margin: 0px;\">​<span style=\"color: rgb(0, 0, 0);\">motor(</span>PORT1<span style=\"color: rgb(0, 0, 0);\">);</span></p>',
                            type: 'text'
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '279px', '176px']
                        }
                    }
                },
                timeline: {
                    duration: 3250,
                    autoPlay: true,
                    labels: {
                        "tres": 0
                    },
                    data: [
                        [
                            "eid50",
                            "opacity",
                            1995,
                            250,
                            "linear",
                            "${par7}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid76",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par3}",
                            '92px',
                            '92px'
                        ],
                        [
                            "eid78",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par6}",
                            '110px',
                            '110px'
                        ],
                        [
                            "eid46",
                            "opacity",
                            1000,
                            245,
                            "linear",
                            "${par3}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid44",
                            "opacity",
                            505,
                            250,
                            "linear",
                            "${par1}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid53",
                            "opacity",
                            2745,
                            255,
                            "linear",
                            "${par10}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid47",
                            "opacity",
                            1245,
                            250,
                            "linear",
                            "${par4}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid52",
                            "opacity",
                            2500,
                            245,
                            "linear",
                            "${par9}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid80",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par9}",
                            '94px',
                            '94px'
                        ],
                        [
                            "eid79",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par7}",
                            '48px',
                            '48px'
                        ],
                        [
                            "eid49",
                            "opacity",
                            1745,
                            250,
                            "linear",
                            "${par6}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid51",
                            "opacity",
                            2245,
                            255,
                            "linear",
                            "${par8}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid72",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par5}",
                            '110px',
                            '110px'
                        ],
                        [
                            "eid45",
                            "opacity",
                            755,
                            245,
                            "linear",
                            "${par2}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid75",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par1}",
                            '66px',
                            '66px'
                        ],
                        [
                            "eid77",
                            "width",
                            3250,
                            0,
                            "linear",
                            "${par4}",
                            '46px',
                            '46px'
                        ],
                        [
                            "eid73",
                            "width",
                            3000,
                            250,
                            "linear",
                            "${par2}",
                            '110px',
                            '151px'
                        ],
                        [
                            "eid48",
                            "opacity",
                            1495,
                            250,
                            "linear",
                            "${par5}",
                            '0.000000',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("Programa_edgeActions.js");
})("EDGE-189591165");
