/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'MAXWELLREGULAR': '<link ref=\"stylesheet\" href=\"stylesheet.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\" />',
            'maxwellbold': '<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\" />'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Display',
                            type: 'image',
                            rect: ['773px', '72px', '277px', '219px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Display.png",'0px','0px']
                        },
                        {
                            id: 'DisplayG',
                            type: 'image',
                            rect: ['770px', '72px', '223px', '223px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"DisplayG.png",'0px','0px']
                        },
                        {
                            id: 'SensorDis',
                            type: 'image',
                            rect: ['-35px', '22px', '1349px', '637px', 'auto', 'auto'],
                            clip: 'rect(333.333251953125px 541.15673828125px 478.176513671875px 329.412109375px)',
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"SensorDis.png",'0px','0px'],
                            transform: [[],['13']]
                        },
                        {
                            id: 'SensDisG',
                            type: 'image',
                            rect: ['274px', '280px', '238px', '140px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"SensDisG.png",'0px','0px'],
                            transform: [[],['13']]
                        },
                        {
                            id: 'Botones',
                            type: 'image',
                            rect: ['286px', '80px', '291px', '225px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Botones.png",'0px','0px']
                        },
                        {
                            id: 'BotonesG',
                            type: 'image',
                            rect: ['354px', '83px', '223px', '217px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"BotonesG.png",'0px','0px']
                        },
                        {
                            id: 'BK7',
                            type: 'image',
                            rect: ['539px', '253px', '270px', '300px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"BK7.png",'0px','0px']
                        },
                        {
                            id: 'BK7G2',
                            type: 'image',
                            rect: ['533px', '248px', '283px', '279px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"BK7G.png",'0px','0px']
                        },
                        {
                            id: 'Text',
                            type: 'text',
                            rect: ['0px', '46px', '364px', '114px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​Presiona un módulo para más información.</p>",
                            align: "center",
                            font: ['MAXWELLREGULAR', [45, "px"], "rgba(55,150,200,1.00)", "normal", "none", "", "break-word", "normal"]
                        },
                        {
                            id: 'LEDs',
                            type: 'image',
                            rect: ['841px', '270px', '224px', '152px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"LEDs.png",'0px','0px'],
                            transform: [[],['-13']]
                        },
                        {
                            id: 'Motor',
                            type: 'image',
                            rect: ['694px', '529px', '164px', '181px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Motor.png",'0px','0px']
                        },
                        {
                            id: 'Perilla',
                            type: 'image',
                            rect: ['491px', '526px', '172px', '188px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Perilla.png",'0px','0px']
                        },
                        {
                            id: 'SensorDeLuz',
                            type: 'image',
                            rect: ['733px', '-6px', '250px', '153px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"SensorDeLuz.png",'0px','0px']
                        },
                        {
                            id: 'Bocina',
                            type: 'image',
                            rect: ['607px', '71px', '133px', '151px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Bocina.png",'0px','0px']
                        },
                        {
                            id: 'CablesPlanos',
                            type: 'image',
                            rect: ['1151px', '205px', '568px', '161px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"CablesPlanos.png",'0px','0px']
                        },
                        {
                            id: 'BuzzerG',
                            type: 'image',
                            rect: ['607px', '95px', '132px', '129px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '0.96',
                            fill: ["rgba(0,0,0,0)",im+"BuzzerG.png",'0px','0px']
                        },
                        {
                            id: 'SensLuzG',
                            type: 'image',
                            rect: ['731px', '-9px', '168px', '160px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"SensLuzG.png",'0px','0px']
                        },
                        {
                            id: 'SensorTemp',
                            type: 'image',
                            rect: ['331px', '-7px', '283px', '145px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"SensorTemp.png",'0px','0px']
                        },
                        {
                            id: 'SensTempG',
                            type: 'image',
                            rect: ['453px', '-13px', '159px', '163px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"SensTempG.png",'0px','0px']
                        },
                        {
                            id: 'LEDsG',
                            type: 'image',
                            rect: ['832px', '215px', '234px', '235px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"LEDsG.png",'0px','0px'],
                            transform: [[],['-13']]
                        },
                        {
                            id: 'MotorG',
                            type: 'image',
                            rect: ['704px', '527px', '153px', '161px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"MotorG.png",'0px','0px']
                        },
                        {
                            id: 'PerillaG',
                            type: 'image',
                            rect: ['497px', '529px', '153px', '165px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"PerillaG.png",'0px','0px']
                        },
                        {
                            id: 'USB',
                            type: 'image',
                            rect: ['1145px', '46px', '568px', '149px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"USB.png",'0px','0px']
                        },
                        {
                            id: 'SERVOAZUL_1',
                            type: 'image',
                            rect: ['816px', '466px', '199px', '150px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"SERVOAZUL%20%281%29.png",'0px','0px'],
                            transform: [[],['29']]
                        },
                        {
                            id: 'SERVOGRIS2',
                            type: 'image',
                            rect: ['816px', '466px', '198px', '123px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"SERVOGRIS.png",'0px','0px'],
                            transform: [[],['29']]
                        },
                        {
                            id: 'RELEVADORAZUL',
                            type: 'image',
                            rect: ['340px', '463px', '199px', '133px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"RELEVADORAZUL.png",'0px','0px'],
                            transform: [[],['-29']]
                        },
                        {
                            id: 'RELEVADORGRIS2',
                            type: 'image',
                            rect: ['334px', '465px', '199px', '107px', 'auto', 'auto'],
                            cursor: 'pointer',
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"RELEVADORGRIS.png",'0px','0px'],
                            transform: [[],['-29']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1349px', '825px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 18000,
                    autoPlay: true,
                    labels: {
                        "uno": 0,
                        "bk7": 500,
                        "dis": 1500,
                        "button": 2500,
                        "temp": 3500,
                        "buz": 4500,
                        "luz": 5500,
                        "disp": 6500,
                        "leds": 7500,
                        "motor": 8500,
                        "kno": 9500,
                        "usb": 10500,
                        "usb2": 11864,
                        "plan": 13750,
                        "plan2": 15000,
                        "servo": 16500,
                        "relay": 17500
                    },
                    data: [
                        [
                            "eid64",
                            "opacity",
                            5500,
                            500,
                            "linear",
                            "${SensLuzG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid65",
                            "opacity",
                            6000,
                            131,
                            "linear",
                            "${SensLuzG}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid407",
                            "opacity",
                            17500,
                            355,
                            "linear",
                            "${RELEVADORAZUL}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid408",
                            "opacity",
                            17855,
                            145,
                            "linear",
                            "${RELEVADORAZUL}",
                            '1',
                            '0'
                        ],
                        [
                            "eid30",
                            "opacity",
                            1500,
                            500,
                            "linear",
                            "${SensorDis}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid34",
                            "opacity",
                            2000,
                            121,
                            "linear",
                            "${SensorDis}",
                            '1',
                            '0'
                        ],
                        [
                            "eid82",
                            "opacity",
                            7500,
                            500,
                            "linear",
                            "${LEDs}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid85",
                            "opacity",
                            8000,
                            250,
                            "linear",
                            "${LEDs}",
                            '1',
                            '0'
                        ],
                        [
                            "eid384",
                            "top",
                            7750,
                            0,
                            "linear",
                            "${LEDs}",
                            '270px',
                            '270px'
                        ],
                        [
                            "eid102",
                            "opacity",
                            9500,
                            500,
                            "linear",
                            "${Perilla}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid103",
                            "opacity",
                            10000,
                            250,
                            "linear",
                            "${Perilla}",
                            '1',
                            '0'
                        ],
                        [
                            "eid26",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${BK7G2}",
                            '1',
                            '0.33333333333333'
                        ],
                        [
                            "eid31",
                            "opacity",
                            1000,
                            131,
                            "linear",
                            "${BK7G2}",
                            '0.333333',
                            '1'
                        ],
                        [
                            "eid78",
                            "opacity",
                            7500,
                            500,
                            "linear",
                            "${LEDsG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid84",
                            "opacity",
                            8000,
                            250,
                            "linear",
                            "${LEDsG}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid55",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${Botones}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid56",
                            "opacity",
                            3000,
                            149,
                            "linear",
                            "${Botones}",
                            '1',
                            '0'
                        ],
                        [
                            "eid87",
                            "opacity",
                            8500,
                            500,
                            "linear",
                            "${MotorG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid91",
                            "opacity",
                            9000,
                            250,
                            "linear",
                            "${MotorG}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid240",
                            "opacity",
                            16500,
                            363,
                            "linear",
                            "${SERVOAZUL_1}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid399",
                            "opacity",
                            16863,
                            137,
                            "linear",
                            "${SERVOAZUL_1}",
                            '1',
                            '0'
                        ],
                        [
                            "eid392",
                            "top",
                            1500,
                            0,
                            "linear",
                            "${SensorDis}",
                            '22px',
                            '22px'
                        ],
                        [
                            "eid385",
                            "left",
                            7750,
                            0,
                            "linear",
                            "${LEDs}",
                            '841px',
                            '841px'
                        ],
                        [
                            "eid38",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${BotonesG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid42",
                            "opacity",
                            3000,
                            149,
                            "linear",
                            "${BotonesG}",
                            '0',
                            '1'
                        ],
                        [
                            "eid124",
                            "left",
                            10500,
                            1364,
                            "linear",
                            "${USB}",
                            '1145px',
                            '790px'
                        ],
                        [
                            "eid125",
                            "left",
                            11864,
                            636,
                            "linear",
                            "${USB}",
                            '790px',
                            '1145px'
                        ],
                        [
                            "eid44",
                            "opacity",
                            3500,
                            500,
                            "linear",
                            "${SensTempG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid50",
                            "opacity",
                            4000,
                            126,
                            "linear",
                            "${SensTempG}",
                            '0',
                            '1'
                        ],
                        [
                            "eid58",
                            "opacity",
                            4500,
                            500,
                            "linear",
                            "${BuzzerG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid62",
                            "opacity",
                            5000,
                            102,
                            "linear",
                            "${BuzzerG}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid48",
                            "opacity",
                            3500,
                            500,
                            "linear",
                            "${SensorTemp}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid49",
                            "opacity",
                            4000,
                            126,
                            "linear",
                            "${SensorTemp}",
                            '1',
                            '0'
                        ],
                        [
                            "eid89",
                            "opacity",
                            8500,
                            500,
                            "linear",
                            "${Motor}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid90",
                            "opacity",
                            9000,
                            250,
                            "linear",
                            "${Motor}",
                            '1',
                            '0'
                        ],
                        [
                            "eid404",
                            "opacity",
                            17500,
                            355,
                            "linear",
                            "${RELEVADORGRIS2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid409",
                            "opacity",
                            17855,
                            145,
                            "linear",
                            "${RELEVADORGRIS2}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid70",
                            "opacity",
                            6500,
                            500,
                            "linear",
                            "${DisplayG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid74",
                            "opacity",
                            7000,
                            146,
                            "linear",
                            "${DisplayG}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid126",
                            "left",
                            13750,
                            1250,
                            "linear",
                            "${CablesPlanos}",
                            '1151px',
                            '780px'
                        ],
                        [
                            "eid127",
                            "left",
                            15000,
                            750,
                            "linear",
                            "${CablesPlanos}",
                            '780px',
                            '1151px'
                        ],
                        [
                            "eid340",
                            "rotateZ",
                            16500,
                            0,
                            "linear",
                            "${SERVOGRIS2}",
                            '29deg',
                            '29deg'
                        ],
                        [
                            "eid339",
                            "rotateZ",
                            16500,
                            0,
                            "linear",
                            "${SERVOAZUL_1}",
                            '29deg',
                            '29deg'
                        ],
                        [
                            "eid99",
                            "opacity",
                            9500,
                            500,
                            "linear",
                            "${PerillaG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid100",
                            "opacity",
                            10000,
                            250,
                            "linear",
                            "${PerillaG}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid324",
                            "top",
                            16500,
                            0,
                            "linear",
                            "${Motor}",
                            '529px',
                            '529px'
                        ],
                        [
                            "eid321",
                            "left",
                            16500,
                            0,
                            "linear",
                            "${MotorG}",
                            '704px',
                            '704px'
                        ],
                        [
                            "eid323",
                            "left",
                            16500,
                            0,
                            "linear",
                            "${Motor}",
                            '694px',
                            '694px'
                        ],
                        [
                            "eid104",
                            "top",
                            7000,
                            0,
                            "linear",
                            "${Display}",
                            '72px',
                            '72px'
                        ],
                        [
                            "eid60",
                            "opacity",
                            4500,
                            500,
                            "linear",
                            "${Bocina}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid61",
                            "opacity",
                            5000,
                            102,
                            "linear",
                            "${Bocina}",
                            '1',
                            '0'
                        ],
                        [
                            "eid67",
                            "opacity",
                            5500,
                            500,
                            "linear",
                            "${SensorDeLuz}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid68",
                            "opacity",
                            6000,
                            131,
                            "linear",
                            "${SensorDeLuz}",
                            '1',
                            '0'
                        ],
                        [
                            "eid236",
                            "opacity",
                            16500,
                            363,
                            "linear",
                            "${SERVOGRIS2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid398",
                            "opacity",
                            16863,
                            137,
                            "linear",
                            "${SERVOGRIS2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid322",
                            "top",
                            16500,
                            0,
                            "linear",
                            "${MotorG}",
                            '527px',
                            '527px'
                        ],
                        [
                            "eid72",
                            "opacity",
                            6500,
                            500,
                            "linear",
                            "${Display}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid73",
                            "opacity",
                            7000,
                            146,
                            "linear",
                            "${Display}",
                            '1',
                            '0'
                        ],
                        [
                            "eid24",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${BK7}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid32",
                            "opacity",
                            1000,
                            131,
                            "linear",
                            "${BK7}",
                            '1',
                            '0'
                        ],
                        [
                            "eid106",
                            "left",
                            7000,
                            0,
                            "linear",
                            "${Display}",
                            '773px',
                            '773px'
                        ],
                        [
                            "eid393",
                            "left",
                            1500,
                            0,
                            "linear",
                            "${SensorDis}",
                            '-35px',
                            '-35px'
                        ],
                        [
                            "eid28",
                            "opacity",
                            1500,
                            500,
                            "linear",
                            "${SensDisG}",
                            '1',
                            '0'
                        ],
                        [
                            "eid33",
                            "opacity",
                            2000,
                            121,
                            "linear",
                            "${SensDisG}",
                            '0',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("Principal_edgeActions.js");
})("EDGE-12672280");
