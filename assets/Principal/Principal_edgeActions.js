/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      

      

      

      

      

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BK7G2}", "mouseover", function(sym, e) {
         sym.play("bk7");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BK7G2}", "mouseout", function(sym, e) {
         sym.play("uno");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensDisG}", "mouseover", function(sym, e) {
         sym.play("dis");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensDisG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BotonesG}", "mouseover", function(sym, e) {
         sym.play("button");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BotonesG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensTempG}", "mouseover", function(sym, e) {
         sym.play("temp");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensTempG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BuzzerG}", "mouseover", function(sym, e) {
         sym.play("buz");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BuzzerG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensLuzG}", "mouseover", function(sym, e) {
         sym.play("luz");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensLuzG}", "mouseout", function(sym, e) {
         sym.play("uno");//nsert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${DisplayG}", "mouseover", function(sym, e) {
         sym.play("disp");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${DisplayG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 8000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${LEDs}", "mouseover", function(sym, e) {
         // insert code to be run when the mouse hovers over the object
         
      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${LEDsG}", "mouseover", function(sym, e) {
         sym.play("leds");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${LEDsG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 9000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${MotorG}", "mouseover", function(sym, e) {
         sym.play("motor");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${MotorG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 10000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${PerillaG}", "mouseover", function(sym, e) {
         sym.play("kno");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${PerillaG}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BK7G2}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-maestro/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 12500, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 15750, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 11864, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 15000, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${CablesPlanos}", "mouseover", function(sym, e) {
         sym.play("plan");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${CablesPlanos}", "mouseout", function(sym, e) {
         sym.play("plan2");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${USB}", "mouseover", function(sym, e) {
         sym.play("usb");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${USB}", "mouseout", function(sym, e) {
         sym.play("usb2");// insert code to be run when the mouse is moved off the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${LEDsG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-led/1", "_parent");// insert code for mouse click here// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensDisG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-distancia/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensLuzG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-luz/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensTempG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-temperatura/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${MotorG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-motor/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${DisplayG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-display/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BuzzerG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-bocina/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BotonesG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-boton/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${PerillaG}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-knob/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 16863, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SERVOGRIS2}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-servo/1", "_parent");// insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SERVOGRIS2}", "mouseover", function(sym, e) {
         sym.play("servo");// insert code to be run when the mouse hovers over the object

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SERVOGRIS2}", "mouseout", function(sym, e) {
         sym.play("uno");// insert code to be run when the mouse is moved off the object// introducir código que se ejecute cuando el ratón se mueva fuera del objeto

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SERVOGRIS2}", "touchstart", function(sym, e) {
         sym.play("servo");
         window.open("http://briko.cc/briko-servo/1", "_parent");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${PerillaG}", "touchstart", function(sym, e) {
         sym.play("kno");
         window.open("http://briko.cc/briko-knob/1", "_parent");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${MotorG}", "touchstart", function(sym, e) {
         sym.play("motor");
         window.open("http://briko.cc/briko-motor/1", "_parent");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${LEDsG}", "touchstart", function(sym, e) {
         sym.play("leds");
         window.open("http://briko.cc/briko-led/1", "_parent");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensTempG}", "touchstart", function(sym, e) {
         sym.play("temp");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-temperatura/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensLuzG}", "touchstart", function(sym, e) {
         sym.play("luz");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-luz/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BuzzerG}", "touchstart", function(sym, e) {
         sym.play("buz");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-bocina/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${DisplayG}", "touchstart", function(sym, e) {
         sym.play("disp");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-display/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${SensDisG}", "touchstart", function(sym, e) {
         sym.play("dis");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-distancia/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BotonesG}", "touchstart", function(sym, e) {
         sym.play("button");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-boton/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${BK7G2}", "touchstart", function(sym, e) {
         sym.play("bk7");// introducir código para que se ejecute cuando el usuario toque el objeto (solo para dispositivos táctiles)
         window.open("http://briko.cc/briko-maestro/1", "_parent");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 17855, function(sym, e) {
         sym.stop();// insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${RELEVADORGRIS2}", "touchstart", function(sym, e) {
         sym.play("relay");
         window.open("http://briko.cc/briko-servo/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${RELEVADORGRIS2}", "click", function(sym, e) {
         window.open("http://briko.cc/briko-relay/1", "_parent");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${RELEVADORGRIS2}", "mouseover", function(sym, e) {
         sym.play("relay");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${RELEVADORGRIS2}", "mouseout", function(sym, e) {
         sym.play("uno");

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-12672280");