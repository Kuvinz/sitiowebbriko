-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-09-2015 a las 02:00:20
-- Versión del servidor: 5.5.27
-- Versión de PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `briko`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesoriosxproyecto`
--

CREATE TABLE IF NOT EXISTS `accesoriosxproyecto` (
  `ID_proyecto` int(11) NOT NULL,
  `ID_accesorio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acessorios`
--

CREATE TABLE IF NOT EXISTS `acessorios` (
  `ID_accesorio` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_accesorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acessorios`
--

INSERT INTO `acessorios` (`ID_accesorio`, `Nombre`) VALUES
(1, 'Llanta'),
(2, 'Rueda Loca'),
(3, 'Tornillo chico'),
(4, 'Tornillo Grande'),
(5, 'Cable'),
(6, 'Tuerca'),
(7, 'Remache'),
(8, 'Cable Micro USB'),
(9, 'Bateria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adjuntos`
--

CREATE TABLE IF NOT EXISTS `adjuntos` (
  `ID_proyecto` int(11) NOT NULL,
  `tipo_adjunto` int(1) NOT NULL COMMENT '0=proy,1=teach',
  `Nombre` varchar(50) NOT NULL,
  `Descripcion` text NOT NULL,
  `Archivo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `ID_Categoria` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Descripcion` text NOT NULL,
  PRIMARY KEY (`ID_Categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`ID_Categoria`, `Nombre`, `Descripcion`) VALUES
(1, 'Teach', 'Area para crear material de educacion, pasos.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriaxproy`
--

CREATE TABLE IF NOT EXISTS `categoriaxproy` (
  `ID_categoria` int(11) NOT NULL,
  `ID_proyecto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo`
--

CREATE TABLE IF NOT EXISTS `codigo` (
  `ID_proyecto` int(11) NOT NULL,
  `ID_crearP` int(11) NOT NULL,
  `Codigo` text NOT NULL,
  `Paso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crearproy`
--

CREATE TABLE IF NOT EXISTS `crearproy` (
  `ID_crearP` int(11) NOT NULL,
  `Nombre` varchar(60) NOT NULL,
  `Descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_crearP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `crearproy`
--

INSERT INTO `crearproy` (`ID_crearP`, `Nombre`, `Descripcion`) VALUES
(1, 'General', 'tab general'),
(2, 'Material', 'Info Tab material'),
(3, 'Construir', 'Info tab construir'),
(4, 'Conectar ', 'Info conectar'),
(5, 'Programar', 'Info Programar'),
(6, 'Teach', 'Teach');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE IF NOT EXISTS `imagenes` (
  `ID_Proyecto` int(11) NOT NULL,
  `ID_crearP` int(11) NOT NULL,
  `Paso` int(11) NOT NULL,
  `Imagen` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `ID_Pieza` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `ID_Proyecto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`ID_Pieza`, `cantidad`, `ID_Proyecto`) VALUES
(1, 3, 18),
(2, 1, 18),
(3, 1, 18),
(5, 2, 18),
(1, 3, 19),
(2, 1, 19),
(3, 1, 19),
(5, 2, 19),
(1, 3, 20),
(2, 1, 20),
(3, 1, 20),
(5, 2, 20),
(1, 3, 21),
(2, 1, 21),
(3, 1, 21),
(5, 2, 21),
(1, 3, 22),
(2, 1, 22),
(3, 1, 22),
(5, 2, 22),
(1, 3, 23),
(2, 1, 23),
(3, 1, 23),
(5, 2, 23),
(1, 3, 24),
(2, 1, 24),
(3, 1, 24),
(5, 2, 24),
(1, 3, 25),
(2, 1, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE IF NOT EXISTS `modulos` (
  `ID_mod` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(150) NOT NULL,
  `declara` varchar(20) NOT NULL,
  `Descripcion` varchar(150) NOT NULL,
  PRIMARY KEY (`ID_mod`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`ID_mod`, `Nombre`, `declara`, `Descripcion`) VALUES
(1, 'Botones', 'botonbk', 'Reconoce cuando se presionan cualquiera de los 5 botones.'),
(2, 'Sensor Infrarrojo', 'distancebk', 'Detecta objetos y mide la distancia entre ellos.'),
(3, 'Bocina', 'buzzerbk', 'Suena la bocina en diferentes tonos y modos.'),
(4, 'Luces LED', 'ledsbk', 'Agrega luces de cualquier color y controla cuando y como se prenden'),
(5, 'Potenciómetro', 'knobbk', 'Varia manualmente cualquier variable girando la perilla.'),
(6, 'Sensor de temperatura', 'temperaturebk', 'Mide el factor de temperatura del medio ambiente.'),
(7, 'Sensor de luz', 'lightbk', 'Mide la cantidad de luz ya sea natural o artificial.'),
(8, 'Display numerico', 'displaybk', 'Despliega informacion con numeros'),
(9, 'Motor', 'motorbk', 'Genera movimientos y acciones roboticas de velocidad y direccion controlable'),
(10, 'Maestro', 'bk7', 'El modulo maestro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulosxproyectos`
--

CREATE TABLE IF NOT EXISTS `modulosxproyectos` (
  `ID_proyecto` int(11) NOT NULL,
  `ID_modulo` int(11) NOT NULL,
  `Puerto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modxproy`
--

CREATE TABLE IF NOT EXISTS `modxproy` (
  `ID_proyecto` int(11) NOT NULL,
  `ID_modulo` int(11) NOT NULL,
  `Puerto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modxproy`
--

INSERT INTO `modxproy` (`ID_proyecto`, `ID_modulo`, `Puerto`) VALUES
(9, 3, 1),
(10, 1, 1),
(10, 4, 2),
(11, 1, 1),
(11, 4, 2),
(12, 4, 1),
(13, 1, 2),
(13, 9, 1),
(14, 5, 2),
(14, 9, 1),
(15, 2, 2),
(15, 9, 1),
(16, 2, 2),
(16, 4, 1),
(17, 4, 1),
(18, 1, 1),
(18, 4, 2),
(19, 4, 1),
(20, 2, 2),
(20, 9, 1),
(21, 4, 2),
(21, 9, 1),
(22, 6, 2),
(22, 8, 1),
(23, 4, 1),
(23, 5, 2),
(24, 1, 1),
(24, 3, 2),
(25, 2, 1),
(25, 3, 2),
(26, 2, 2),
(26, 4, 1),
(27, 2, 1),
(28, 3, 2),
(28, 4, 1),
(29, 4, 1),
(30, 1, 1),
(30, 4, 2),
(31, 1, 2),
(31, 3, 1),
(31, 4, 3),
(32, 2, 2),
(32, 3, 1),
(33, 4, 1),
(34, 4, 1),
(35, 1, 2),
(35, 4, 1),
(36, 4, 1),
(36, 5, 2),
(37, 2, 2),
(37, 4, 1),
(38, 1, 3),
(38, 3, 2),
(38, 4, 1),
(39, 4, 1),
(40, 5, 1),
(40, 9, 2),
(41, 1, 4),
(41, 3, 1),
(41, 4, 3),
(42, 4, 3),
(43, 9, 1),
(44, 2, 1),
(44, 3, 2),
(45, 2, 4),
(45, 4, 5),
(45, 9, 1),
(45, 9, 7),
(46, 1, 1),
(46, 3, 3),
(46, 1, 2),
(47, 5, 3),
(47, 9, 1),
(47, 9, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `piezas`
--

CREATE TABLE IF NOT EXISTS `piezas` (
  `ID_Pieza` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Descripcion` text NOT NULL,
  PRIMARY KEY (`ID_Pieza`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `piezas`
--

INSERT INTO `piezas` (`ID_Pieza`, `Nombre`, `Descripcion`) VALUES
(1, 'L1', 'cepillito'),
(2, 'C1', 'Cuadrado chico'),
(3, 'O1', 'Octagono chico'),
(4, 'Y1', 'Union en Y'),
(5, 'C2', 'Union en C'),
(6, 'O2', 'Union en octagono'),
(7, 'P1', 'Piso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
  `ID_Proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Usuario` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Descripcion` text NOT NULL,
  `Tiempo` int(11) NOT NULL,
  `Dificultad` int(11) NOT NULL,
  PRIMARY KEY (`ID_Proyecto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE IF NOT EXISTS `proyectos` (
  `ID_proy` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(150) NOT NULL,
  `Descripcion` varchar(300) NOT NULL,
  `Dificultad` int(1) NOT NULL,
  `Tiempo` int(1) NOT NULL,
  `Video` varchar(300) NOT NULL,
  `Imagen` varchar(300) NOT NULL,
  `Codigo` text NOT NULL,
  PRIMARY KEY (`ID_proy`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`ID_proy`, `Nombre`, `Descripcion`, `Dificultad`, `Tiempo`, `Video`, `Imagen`, `Codigo`) VALUES
(9, 'Afinador Musical', 'Afinador musical con buzzzer', 2, 2, 'https://www.youtube.com/embed/AEiS8uRDIek', 'Afinadormusical.jpg', '\nbuzzerbk bocina (PORT1);//se declara el modulo de buzzer en color verde luego se le nombra bocina en color negro y por ultimo se declara el puerto en lettras mayusculas y color azul. \nint tiempo=1000;\n\ncode() {//Escribe aqui tu codigo\n\nbocina.playtone(NOTE_C6,tiempo);// se escucha la nota DO en la sexta octava durante un segundo.\ndelay(tiempo);\nbocina.playtone(NOTE_D6,tiempo);// se escucha la nota RE en la sexta octava durante un segundo.\ndelay(tiempo);\nbocina.playtone(NOTE_E6,tiempo);// se escucha la nota MI en la sexta octava durante un segundo.\ndelay(tiempo);\nbocina.playtone(NOTE_F6,tiempo);// se escucha la nota FA en la sexta octava durante un segundo.\ndelay(tiempo);\nbocina.playtone(NOTE_G6,tiempo);// se escucha la nota SOL en la sexta octava durante un segundo.\ndelay(tiempo);\nbocina.playtone(NOTE_A6,tiempo);// se escucha la nota LA en la sexta octava durante un segundo.\ndelay(tiempo);\nbocina.playtone(NOTE_B6,tiempo);// se escucha la nota SI en la sexta octava durante un segundo.\ndelay(tiempo);\n\n\n}\n'),
(10, 'Elije secuencia de leds', 'Cada que presionas un botón es una secuencia diferente de leds ', 2, 2, '-', 'Botonessecuencia.jpg', '\nbuttonsbk Buttons(PORT1);                                           //declara el modulo de botones en el puerto 1\nledsbk leds(PORT2);                                                      //declara el modulo de leds en el puerto 2\n\nint boton;                                                                           //declara la variable boton\n\ncode(){                                                                                //ciclo que corre el codigo\n\n  boton = Buttons.read();                                                //variable boton obtiene el valor de Buttons.read() la cual va a leer el boton que se presiona\n  \n  if (boton ==1)                                                                 //si el boton que se presiona es igual a 1 se cumple lo siguiente\n  \n  {\n    leds.color(1,BLUE);                                                   //prende el led 1 de color azul\n    delay(300);                                                                  //espera 300 milisegundos\n    leds.color(BLACK);                                                   //prende los leds negros\n    leds.color(2,BLUE);                                                  //prende el led 2 de color azul\n    delay(300);                                                                 //espera 300 milisegundos                 \n    leds.color(BLACK);\n    leds.color(3,BLUE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(4,BLUE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(5,BLUE);\n    delay(300);\n    leds.color(BLACK);\n  }\n  \n  \n  if (boton ==2)                                                              //si el boton que se presiona es igual a 2\n  \n  {\n    leds.color(5,RED);                                                //prende el led 5 de color azul\n    delay(300);                                                               //espera 300 milisegundos\n    leds.color(BLACK);                                                //prende los leds de color negro\n    leds.color(4,RED);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(3,RED);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(2,RED);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(1,RED);\n    delay(300);\n    leds.color(BLACK);\n  }\n  \n \n  if (boton ==3)                                                              //si el boton que se presiona es igual a 3\n  \n  {\n    leds.color(3,GREEN);                                                //prende el led 3 de color azul\n    delay(300);                                                               //espera 300 milisegundos\n    leds.color(BLACK);                                                //prende los leds de color negro\n    leds.color(2,GREEN);\n    leds.color(4,GREEN);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(1,GREEN);\n    leds.color(5,GREEN);\n    delay(300);\n    leds.color(BLACK);\n  }\n  \n  \n  if (boton ==4)                                                             //si el boton que se presiona es igual a 4\n  \n  {\n    leds.color(1,WHITE);                                               //prende el led 1 de color azul\n    delay(300);                                                              //espera 300 milisegundos\n    leds.color(BLACK);                                               //prende los leds de color negro\n    leds.color(1,WHITE);\n    leds.color(2,WHITE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(1,WHITE);\n    leds.color(2,WHITE);\n    leds.color(3,WHITE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(1,WHITE);\n    leds.color(2,WHITE);\n    leds.color(3,WHITE);\n    leds.color(4,WHITE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(1,WHITE);\n    leds.color(2,WHITE);\n    leds.color(3,WHITE);\n    leds.color(4,WHITE);\n    leds.color(5,WHITE);\n    delay(300);\n    leds.color(BLACK);\n  }\n  \n  \n  if (boton ==5)                                                                    //si el boton que se presiona es igual a 5\n  \n  {\n    leds.color(5,ORANGE);                                                      //prende el led 5 de color azul\n    delay(300);                                                                     //espera 300 milisegundos\n    leds.color(BLACK);                                                      //prende los leds de color negro\n    leds.color(5,ORANGE);\n    leds.color(4,ORANGE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(5,ORANGE);\n    leds.color(4,ORANGE);\n    leds.color(3,ORANGE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(5,ORANGE);\n    leds.color(4,ORANGE);\n    leds.color(3,ORANGE);\n    leds.color(2,ORANGE);\n    delay(300);\n    leds.color(BLACK);\n    leds.color(5,ORANGE);\n    leds.color(4,ORANGE);\n    leds.color(3,ORANGE);\n    leds.color(2,ORANGE);\n    leds.color(1,ORANGE);\n    delay(300);\n    leds.color(BLACK);\n  }\n\n}\n'),
(11, 'Caja Fuerte', 'Se configurara una contraseña con los botones', 4, 4, '-', 'default.png', '\nbuttonsbk boton (PORT1);\nledsbk luz (PORT2);\nint i,c,num;\nconst byte codigo[5] = {2,1,3,5,2};// se declara un arreglo,un elemento que se utiliza para guardar una cantidad deseada de carateres, en este caso numeros enteros, y acomodarlos en orden.\nbyte clave[5] = {0,0,0,0,0};\n\ncode() {//Escribe aqui tu codigo\n  \n   num=0;\n     c=0;\n     while(c<5)// se utiliza la funcion de while para evaluar el valor de la variable c, ya que el arreglo tiene 5 lugares pero se comienza a contar desde 0 el while se detiene una vez que c es igual a 5.\n       {\n         clave[c]= boton.read();// a su vez c ira aumentando cada vez que se introduzaca un digito del codigo de la caja fuerte.\n         if(clave[c] != 0){// el espacio "c" del arreglo toma el valor del numero que se precione en ese mometo.\n           c++;// aqui c aumenta en una unidad.\n           while(boton.read() != 0){}\n         }\n         delay(10);\n       }\n  for (i=0;i<5;i++)// con la funcion for se comparan los valores de de la clave itroducida y el codigo correcto.\n    {//la funcion del for es repetir un ciclo un numero determinado de veces en este caso si incializa la variable i en 0; se declara que el ciclo se repetira mientras i sea menor a 5 y que i aumentara en una unidad cada vez que se repita el ciclo.(que en este caso se repetira 5 veces ya que i comienza en 0)\n    \n      if(clave[i]==codigo[i])// si el codigo es igual la variable num tomara el valor de uno.\n       {\n         num=1;\n       }\n       else// si es diferente i tomara el valor de 5 y la funcion for terminara\n       {\n         i=5;\n         num=0; // num tomara el valor de .\n       }\n    }\n   if(num==1)\n     {\n       luz.color(GREEN);// si la clave es correcta los leds se prenden de color verde.\n       delay (3000);\n       luz.color(BLACK);\n     }\n     else\n     {\n       luz.color(RED);// si el codigo es incorrecto los leds se prenderan de color verde.\n       delay (3000);\n       luz.color (BLACK);\n     }\n     for(i=0;i<5;i++)// se le  da un vlaor de cero a cada uno de los espacios del arreglo "clave".\n       {\n         clave[i]=0;\n       }\n  \n\n}\n\n'),
(12, 'Cascada de Leds', 'Se programa una Cascada de Leds', 2, 3, 'https://www.youtube.com/embed/YVFuTDD14lY', 'Cascadadeleds.jpg', '\r\nledsbk luces(PORT1);// se declara el nombre de los leds y el puerto en el cual esta conectado. En este caso se llamn luces y estan en el puerto 1\r\nint num=250;// se declara una variable del tipo entero que se utilizara para definir el tiempo que dura cada comando\r\n\r\ncode() {\r\n  \r\nluces.color(1,RED);// se prenden lod leds de manera secuencial y la intensidad de los mismos  y luego disminulle de maanera \r\nluces.brightness(51);// en este caso la intensidad es de 51\r\ndelay(num);//El primer led permanecera prendido en color rojo por la cantidad en milisegundos que valga la variable "num"\r\nluces.color(2,RED);\r\nluces.color(1,YELLOW);\r\nluces.brightness(102);\r\ndelay(num);\r\nluces.color(3,RED);\r\nluces.color(2,YELLOW);\r\nluces.color(1,WHITE);\r\nluces.brightness(153);\r\ndelay(num);\r\nluces.color(4,RED);\r\nluces.color(3,YELLOW);\r\nluces.color(2,WHITE);\r\nluces.color(1,MAGENTA);\r\nluces.brightness(204);\r\ndelay(num);\r\nluces.color(5,RED);\r\nluces.color(4,YELLOW);\r\nluces.color(3,WHITE);\r\nluces.color(2,MAGENTA);\r\nluces.color(1,BLUE);\r\nluces.brightness(255);\r\ndelay(num);\r\nluces.color(5,YELLOW);\r\nluces.color(4,WHITE);\r\nluces.color(3,MAGENTA);\r\nluces.color(2,BLUE);\r\nluces.color(1,BLACK);\r\nluces.brightness(204);\r\ndelay(num);\r\nluces.color(5,WHITE);\r\nluces.color(4,MAGENTA);\r\nluces.color(3,BLUE);\r\nluces.color(2,BLACK);\r\nluces.color(1,BLACK);\r\nluces.brightness(153);\r\ndelay(num);\r\nluces.color(5,MAGENTA);\r\nluces.color(4,BLUE);\r\nluces.color(3,BLACK);\r\nluces.color(2,BLACK);\r\nluces.color(1,BLACK);\r\nluces.brightness(102);\r\ndelay(num);\r\nluces.color(5,BLUE);\r\nluces.color(4,BLACK);\r\nluces.color(3,BLACK);\r\nluces.color(2,BLACK);\r\nluces.color(1,BLACK);\r\nluces.brightness(51);\r\ndelay(num);\r\nluces.color(BLACK);\r\n}\r\n\r\n'),
(13, 'Controlar motor con botones', 'Por medio del modulo de botones controlar las funciones del motor: velocidad, direccion, encendido y apagado ', 3, 3, 'https://www.youtube.com/embed/ww4gksga5zU', 'Controladormotorbotones.jpg', '\nmotorbk motor (PORT1);                                                               //declara el modulo de motor en el puerto 1\nbuttonsbk Buttons (PORT2);                                                       //declara el modulo de botones en el puerto 2\nint boton;                                                                                                       //declara la variable de boton \nint n=150;                                                                                                      //declara la variable n con el valor de 150\nint y;                                                                                                                    //declara la variable y\n\ncode() {                                                                                                              //ciclo que corre el codigo\n\n   boton = Buttons.read();                                                               //variable boton obtiene el valor de Buttons.read() la cual va a leer el boton que se presiona\n   bk7write(boton);                                                                                //imprime que boton presionas \n   \n   if (boton == 1)                                                                                        //si el b oton es igual a 1 se cumple lo siguiente\n   {\n       motor.set (RIGHT,n);                                                                //gira el motor en direccion a las manecillas del reloj a la valocidad especificada en la variable n\n   }\n   if (boton == 2)                                                                                      //si el boton es igual a 2 se cumple lo siguiente\n   {\n     motor.set(RIGHT,y = n+50);                                                //gira el motor en direccion a las manecillas del reloj a la velocidad especificada en la variable y\n   }\n   if (boton ==3)                                                                                        //si el boton es igual a 3 se cumple lo siguiente\n   {\n     motor.set(RIGHT,y -50);                                                          //gira el motor en direccion a las manecillas del reloj a la velocidad especificada en la variable y menos 50\n   }\n   if(boton==4)                                                                                        //si el boton es igual a 4 se cumple lo siguiente\n   {\n     motor.set(LEFT,y);                                                                       //gira el motor en direccion contraria a las manecillas del reloj a la velocidad especificada en y \n   }\n   if(boton==5)                                                                                        //si el boton es igual a 5 se cumple lo siguiente\n   {\n     motor.set(OFF);                                                                             //apaga el motor\n   }\n}\n'),
(14, 'Cambia la dirección del motor', 'Si se gira la perilla hacia la izquierda la direccion del giro del motor ira en direccion contraria a las manecillas del reloj, si se gira a la derecha el motor ira en direccion a las manecillas del reloj', 2, 2, '-', 'Direccionmotorperilla.jpg', '\nmotorbk motor (PORT1);                        //Declara el motor en el puerto 1\nknobbk modulo_pot(PORT2);           //Declara el potenciometro en el puerto 2\nint pot;                                                                        //Variable para guardar el valor del potenciometro\n\ncode() {                                                                                //ciclo que corre el codigo\n\n pot = modulo_pot.read();                              //Variable pot obtiene el valor de modulo_pot.read() la cual va a leer el valor del potenciometro\n  bk7write(pot);                                                 //Imprime en pantalla\n\nif ( pot < 127)                                                  //Si el potenciometro tiene un valor menor a 127 se cumple lo siguiente\n   { \n     motor.set(LEFT,255);                                   //Prende el motor a la velocidad deseada girando en sentido contrario a las manecillas del reloj\n   }\nif ( pot > 127)                                                  //Si el potenciometro tiene un valor mayor a 127 se cumple lo siguiente\n  {\n      motor.set(RIGHT,255);                                //Prende el motor a la velocidad deseada girando en sentido de las manecillas del reloj\n  }\n}\n'),
(15, 'Dirección en base  a distancia', 'El motor gira en direccion de las manecillas del reloj, pero si la distancia es muy pequeña la dirección del giro cambia', 2, 2, '-', 'direccionmotorsensordistancia.jpg', 'motorbk motor(PORT1);                                                            //declara el modulo de motor en el puerto 1\ndistancebk sensor_sharp(PORT2);                                       //declara el modulo de distancia en el puerto 2\nfloat distance_cm;                                                                     //variable para guardar la distancia en cm \n\ncode() {                                                                                                          //ciclo que corre el codigo\n\n  distance_cm=sensor_sharp.read(CM);                             //variable distance_cm obtiene el valor de sensor_sharp.read(CM) la cual va a leer la distancia en cm\n  bk7write(distance_cm);                                                         //imprime la distancia en cm\n  delay(250);                                                                               //espera 250 milisegundos\n  \n  motor.set(RIGHT,255);                                                         //Prende el motor a la velocidad deseada girando en sentido de las manecillas del reloj \n  \n    if (distance_cm < 20)                                                         //Si la distancia es menor a 20 se cumple la siguiente\n    {\n      motor.set(LEFT);                                                              //Cambia la direccion en la que gira el motor\n    }\n\n\n}\n'),
(16, 'Regla de Leds', 'Dependiendo de la distancia va cambiando el led que se prende y el color del led', 3, 3, 'https://www.youtube.com/embed/Yrd7k7ZNhD4', 'Distanciaprendeledporled.jpg', '\r\nledsbk leds(PORT1);                                                                    //declara el modulo de leds en el puerto 1\r\ndistancebk sensor_sharp(PORT2);                                           //declara el modulo de distancia en el puerto 2\r\nfloat distance_cm;                                                                         //variable para guardar la distancia en cm   \r\n\r\ncode(){                                                                                             //ciclo que repite el codigo\r\n\r\n  distance_cm = sensor_sharp.read(CM);                              //variable distance_cm obtiene el valor de sensor_sharp.read(CM) la cual va a leer la distancia en cm\r\n  bk7write(distance_cm);                                                            //imprime en pantalla\r\n  \r\n  if (distance_cm > 10 && distance_cm < 70 )                       //si la distancia es mayor que 10 y menor que 70 se cumple lo siguiente\r\n  {\r\n    leds.color(1,GREEN);                                                             //prende el led 1 de color verde\r\n    leds.brightness(150);                                                            //cambia la intensidad del led a 150\r\n  }\r\n  \r\n  if (distance_cm > 20 && distance_cm < 70 )                      //si la distancia es mayor que 20 y menor a 70 se cumple lo siguiente\r\n  {\r\n    leds.color(2,AQUA);                                                                //prende el led 2 de color aqua\r\n    leds.brightness(175);                                                            //cambia la intensidad de los leds a 175\r\n   }\r\n  \r\n  if (distance_cm > 30 && distance_cm < 70)                       //si la distancia es mayor a 30 y menor a 70 se cumple lo siguiente\r\n  {\r\n    leds.color(3,ORANGE);                                                         //prende el led 3 de color naranja\r\n    leds.brightness(200);                                                           //cambia la intensidad de los leds a 200\r\n  }\r\n   \r\n  if (distance_cm > 40 && distance_cm < 70)                       //si la distancia es mayor a 40 y menor a 70 se cumple lo siguiente\r\n  {\r\n    leds.color(4,RED);                                                                  //prende el led 4 de color rojo\r\n    leds.brightness(225);                                                            //cambia la intensidad de los leds a 225\r\n  }\r\n  if (distance_cm > 50 && distance_cm < 70 )                      //si la distancia es mayor a 50 y menor a 70 se cumple lo siguiente\r\n {\r\n   leds.color(5,PURPLE);                                                           //prende el led 5 de color morado\r\n   leds.brightness(255);                                                              //cambia la intensidad de los leds a 255\r\n }\r\n  else                                                                                              //si cualquiera de las otras condiciones es falsa se cumple lo siguiente\r\n  {\r\n   leds.color(BLACK);                                                                  //prende los leds de color negro\r\n  }\r\n}\r\n\r\n\r\n'),
(17, 'Lampara', 'Se realiza una lapara con los Leds', 1, 1, '-', 'Lampara.jpg', '\r\n\r\nledsbk luces(PORT1);// se declara el nombre de los leds y el puerto en el cual esta conectado. En este caso se llamn luces y estan en el puerto 1\r\n int rojo,verde,azul;// se declaran 3 veriables del tipo entero que utilizaremos para regular la intencidad deseada en cada convinacion de colores\r\n\r\ncode() {//Escribe aqui tu codigo\r\n\r\nrojo=36;// se declara la intencidad de rojo en el color final\r\nverde=230;// se declara la intencidad de verde en el color final\r\nazul=200;// se declara la intencidad de azul en el color final\r\n\r\nluces.color(rojo,verde,azul);//se prenden los leds acomodando las variables siempre en este orden, primero Rojo, luego verde y al fina azul.\r\n\r\n}\r\n'),
(18, 'Lampara con botones', 'Se realiza una lapara con los Leds con Botones', 2, 2, '-', 'Lamparaconbotones.jpg', '\r\n\r\nbuttonsbk boton(PORT1);\r\nledsbk luz(PORT2);\r\nint c;\r\n\r\ncode() {          //Escribe aqui tu codigo\r\n  \r\n  c=boton.read();\r\n   switch(c)// en este switch/ case se valua el valor que toma c dependiendo del boton que fue prcionado, dependiendo del boton que fue pecionado se prenden los leds de diferentes colores.\r\n    {\r\n      case 1: \r\n       luz.color(RED);\r\n       c=0;\r\n       delay(100);\r\n       break;\r\n      case 2: \r\n       luz.color(ORANGE);\r\n       c=0;\r\n       delay(100);\r\n       break;\r\n      case 3: \r\n       luz.color(GREEN);\r\n       c=0;\r\n       delay(100);\r\n       break;\r\n      case 4: \r\n       luz.color(BLUE);\r\n       c=0;\r\n       delay(100);\r\n       break;\r\n      case 5: \r\n       luz.color(WHITE);\r\n       c=0;\r\n       delay(100);\r\n       break;\r\n       default:// siempre que se utiliza un switch/ case de debe de utilizar un default para decarar que es lo que sucede cuando ninguno de los casos se cumple.\r\n      luz.color(BLACK);  \r\n  }\r\n       \r\n      \r\n    }\r\n  \r\n'),
(19, 'Luces de Navidad', 'Luces de navidad con leds Briko', 1, 1, '-', 'LucesNavidad.jpg', '\r\n\r\nledsbk led1(PORT1),led2(PORT2),led3(PORT3),led4(PORT4),led5(PORT5);\r\nint c =1;\r\ncode() {\r\nled1.color(5,RED);\r\nled1.color(4,BLACK);\r\nled1.color(3,BLACK);\r\nled1.color(2,BLACK);\r\nled1.color(1,BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,YELLOW);\r\nled1.color(4,RED);\r\nled1.color(3,BLACK);\r\nled1.color(2,BLACK);\r\nled1.color(1,BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,WHITE);\r\nled1.color(4,YELLOW);\r\nled1.color(3,RED);\r\nled1.color(2,BLACK);\r\nled1.color(1,BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,BLUE);\r\nled1.color(4,WHITE);\r\nled1.color(3,YELLOW);\r\nled1.color(2,RED);\r\nled1.color(1,BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nwhile(c==1){\r\nled1.color(5,MAGENTA);\r\nled1.color(4,BLUE);\r\nled1.color(3,WHITE);\r\nled1.color(2,YELLOW);\r\nled1.color(1,RED);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,BLACK);\r\nled1.color(4,MAGENTA);\r\nled1.color(3,BLUE);\r\nled1.color(2,WHITE);\r\nled1.color(1,YELLOW);\r\nled2.color(5,RED);\r\nled2.color(4,BLACK);\r\nled2.color(3,BLACK);\r\nled2.color(2,BLACK);\r\nled2.color(1,BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,BLACK);\r\nled1.color(4,BLACK);\r\nled1.color(3,MAGENTA);\r\nled1.color(2,BLUE);\r\nled1.color(1,WHITE);\r\nled2.color(5,YELLOW);\r\nled2.color(4,RED);\r\nled2.color(3,BLACK);\r\nled2.color(2,BLACK);\r\nled2.color(1,BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,BLACK);\r\nled1.color(4,BLACK);\r\nled1.color(3,BLACK);\r\nled1.color(2,MAGENTA);\r\nled1.color(1,BLUE);\r\nled2.color(5,WHITE);\r\nled2.color(4,YELLOW);\r\nled2.color(3,RED);\r\nled2.color(2,BLACK);\r\nled2.color(1,BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1.color(5,BLACK);\r\nled1.color(4,BLACK);\r\nled1.color(3,BLACK);\r\nled1.color(2,BLACK);\r\nled1.color(1,MAGENTA);\r\nled2.color(5,BLUE);\r\nled2.color(4,WHITE);\r\nled2.color(3,YELLOW);\r\nled2.color(2,RED);\r\nled2.color(1,BLACK);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled1. color(BLACK);\r\nled2.color(5,MAGENTA);\r\nled2.color(4,BLUE);\r\nled2.color(3,WHITE);\r\nled2.color(2,YELLOW);\r\nled2.color(1,RED);\r\nled3.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled2.color(5,BLACK);\r\nled2.color(4,MAGENTA);\r\nled2.color(3,BLUE);\r\nled2.color(2,WHITE);\r\nled2.color(1,YELLOW);\r\nled3.color(5,RED);\r\nled3.color(4,BLACK);\r\nled3.color(3,BLACK);\r\nled3.color(2,BLACK);\r\nled3.color(1,BLACK);\r\nled1.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled2.color(5,BLACK);\r\nled2.color(4,BLACK);\r\nled2.color(3,MAGENTA);\r\nled2.color(2,BLUE);\r\nled2.color(1,WHITE);\r\nled3.color(5,YELLOW);\r\nled3.color(4,RED);\r\nled3.color(3,BLACK);\r\nled3.color(2,BLACK);\r\nled3.color(1,BLACK);\r\nled1.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled2.color(5,BLACK);\r\nled2.color(4,BLACK);\r\nled2.color(3,BLACK);\r\nled2.color(2,MAGENTA);\r\nled2.color(1,BLUE);\r\nled3.color(5,WHITE);\r\nled3.color(4,YELLOW);\r\nled3.color(3,RED);\r\nled3.color(2,BLACK);\r\nled3.color(1,BLACK);\r\nled1.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled2.color(5,BLACK);\r\nled2.color(4,BLACK);\r\nled2.color(3,BLACK);\r\nled2.color(2,BLACK);\r\nled2.color(1,MAGENTA);\r\nled3.color(5,BLUE);\r\nled3.color(4,WHITE);\r\nled3.color(3,YELLOW);\r\nled3.color(2,RED);\r\nled3.color(1,BLACK);\r\nled1.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled2. color(BLACK);\r\nled3.color(5,MAGENTA);\r\nled3.color(4,BLUE);\r\nled3.color(3,WHITE);\r\nled3.color(2,YELLOW);\r\nled3.color(1,RED);\r\nled1.color(BLACK);\r\nled4.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\n\r\nled3.color(5,BLACK);\r\nled3.color(4,MAGENTA);\r\nled3.color(3,BLUE);\r\nled3.color(2,WHITE);\r\nled3.color(1,YELLOW);\r\nled4.color(5,RED);\r\nled4.color(4,BLACK);\r\nled4.color(3,BLACK);\r\nled4.color(2,BLACK);\r\nled4.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled3.color(5,BLACK);\r\nled3.color(4,BLACK);\r\nled3.color(3,MAGENTA);\r\nled3.color(2,BLUE);\r\nled3.color(1,WHITE);\r\nled4.color(5,YELLOW);\r\nled4.color(4,RED);\r\nled4.color(3,BLACK);\r\nled4.color(2,BLACK);\r\nled4.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled3.color(5,BLACK);\r\nled3.color(4,BLACK);\r\nled3.color(3,BLACK);\r\nled3.color(2,MAGENTA);\r\nled3.color(1,BLUE);\r\nled4.color(5,WHITE);\r\nled4.color(4,YELLOW);\r\nled4.color(3,RED);\r\nled4.color(2,BLACK);\r\nled4.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled3.color(5,BLACK);\r\nled3.color(4,BLACK);\r\nled3.color(3,BLACK);\r\nled3.color(2,BLACK);\r\nled3.color(1,MAGENTA);\r\nled4.color(5,BLUE);\r\nled4.color(4,WHITE);\r\nled4.color(3,YELLOW);\r\nled4.color(2,RED);\r\nled4.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled3. color(BLACK);\r\nled4.color(5,MAGENTA);\r\nled4.color(4,BLUE);\r\nled4.color(3,WHITE);\r\nled4.color(2,YELLOW);\r\nled4.color(1,RED);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled5.color(BLACK);\r\ndelay(100);\r\nled4.color(5,BLACK);\r\nled4.color(4,MAGENTA);\r\nled4.color(3,BLUE);\r\nled4.color(2,WHITE);\r\nled4.color(1,YELLOW);\r\nled5.color(5,RED);\r\nled5.color(4,BLACK);\r\nled5.color(3,BLACK);\r\nled5.color(2,BLACK);\r\nled5.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled4.color(5,BLACK);\r\nled4.color(4,BLACK);\r\nled4.color(3,MAGENTA);\r\nled4.color(2,BLUE);\r\nled4.color(1,WHITE);\r\nled5.color(5,YELLOW);\r\nled5.color(4,RED);\r\nled5.color(3,BLACK);\r\nled5.color(2,BLACK);\r\nled5.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled4.color(5,BLACK);\r\nled4.color(4,BLACK);\r\nled4.color(3,BLACK);\r\nled4.color(2,MAGENTA);\r\nled4.color(1,BLUE);\r\nled5.color(5,WHITE);\r\nled5.color(4,YELLOW);\r\nled5.color(3,RED);\r\nled5.color(2,BLACK);\r\nled5.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled4.color(5,BLACK);\r\nled4.color(4,BLACK);\r\nled4.color(3,BLACK);\r\nled4.color(2,BLACK);\r\nled4.color(1,MAGENTA);\r\nled5.color(5,BLUE);\r\nled5.color(4,WHITE);\r\nled5.color(3,YELLOW);\r\nled5.color(2,RED);\r\nled5.color(1,BLACK);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled4. color(BLACK);\r\nled5.color(5,MAGENTA);\r\nled5.color(4,BLUE);\r\nled5.color(3,WHITE);\r\nled5.color(2,YELLOW);\r\nled5.color(1,RED);\r\nled1.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled5.color(5,BLACK);\r\nled5.color(4,MAGENTA);\r\nled5.color(3,BLUE);\r\nled5.color(2,WHITE);\r\nled5.color(1,YELLOW);\r\nled1.color(5,RED);\r\nled1.color(4,BLACK);\r\nled1.color(3,BLACK);\r\nled1.color(2,BLACK);\r\nled1.color(1,BLACK);\r\nled4.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled5.color(5,BLACK);\r\nled5.color(4,BLACK);\r\nled5.color(3,MAGENTA);\r\nled5.color(2,BLUE);\r\nled5.color(1,WHITE);\r\nled1.color(5,YELLOW);\r\nled1.color(4,RED);\r\nled1.color(3,BLACK);\r\nled1.color(2,BLACK);\r\nled1.color(1,BLACK);\r\nled4.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\nled5.color(5,BLACK);\r\nled5.color(4,BLACK);\r\nled5.color(3,BLACK);\r\nled5.color(2,MAGENTA);\r\nled5.color(1,BLUE);\r\nled1.color(5,WHITE);\r\nled1.color(4,YELLOW);\r\nled1.color(3,RED);\r\nled1.color(2,BLACK);\r\nled1.color(1,BLACK);\r\nled4.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);led5.color(5,BLACK);\r\nled5.color(4,BLACK);\r\nled5.color(3,BLACK);\r\nled5.color(2,BLACK);\r\nled5.color(1,MAGENTA);\r\nled1.color(5,BLUE);\r\nled1.color(4,WHITE);\r\nled1.color(3,YELLOW);\r\nled1.color(2,RED);\r\nled1.color(1,BLACK);\r\nled4.color(BLACK);\r\nled2.color(BLACK);\r\nled3.color(BLACK);\r\ndelay(100);\r\n}\r\n}\r\n'),
(20, 'Velocidad en base a distancia', 'La velocidad del motor va a estar controlada con el sensor de distancia, mientras que la distancia sea menor el motor irá mas lento y si es mas lejos el motor irá más rápido', 3, 3, '-', 'Motorcontroladopordistancia.jpg', '\r\nmotorbk motor(PORT1);                                                                                 //declara el modulo de motor en el puerto 1 \r\ndistancebk sensor_sharp(PORT2);                                                         //declara el modulo de distancia en el puerto 2\r\nfloat distance_cm;                                                                                                 //variable para guardar la distancia en cm\r\nint numero;                                                                                                                   //variable numero \r\n\r\ncode() {                                                                                                                              //ciclo que corre el codigo\r\n  \r\n  distance_cm=sensor_sharp.read(CM);                                         ////variable distance_cm obtiene el valor de sensor_sharp.read(CM) la cual va a leer la distancia en cm\r\n  bk7write(distance_cm);                                                                               //imprime en pantalla la distancia \r\n  delay(250);                                                                                                                 //espera 250 milisegundos\r\n  numero=map(sensor_sharp.read(),6,80,100,255);           //Lee el sensor de distancia y convierte la lectura de un numero de 6 a 80 a uno de 100 a 255 con una regla de 3 \r\n  motor.set(RIGHT,numero);                                                                      //gira el motor a la derecha a la velocidad que detecte numero\r\n\r\n  }\r\n'),
(21, 'Motor controla Leds', 'Según la velocidad que tenga el motor va a ser el color de los leds', 3, 3, '-', 'Motorleds.jpg', '\r\n\r\nmotorbk motor(PORT1);                                                             //declara el modulo de motor en el puerto 1\r\nledsbk luces(PORT2);                                                                //declara el modulo de leds en el puerto 2\r\n\r\nint n;                                                                                                //declara variable n\r\n\r\ncode() {                                                                                           //ciclo que corre el codigo\r\n\r\n  n = random (255);                                                                     //variable n con numeros random entre 0 y 255\r\n  bk7write(n);                                                                                //imprime en pantalla el valor de n\r\n  \r\nif( n<= 85)                                                                                     //si n es menor o igual a 85\r\n   {\r\n      motor.set(RIGHT,100);                                                        //gira el motor en direccion a las manecillas del reloj a la velocidad especificada\r\n      luces.color(RED);                                                                 //prende los leds de color rojo\r\n      delay(500);                                                                            //espera 500 milisegundos \r\n   }\r\n if( n>=86 && n<=175)                                                               //si el n es mayor o igual a 86 y menor o igual a 175\r\n    {\r\n       motor.set(RIGHT,175);                                                      //gira el motor en direccion a las manecillas del reloj a la velocidad especificada\r\n       luces.color(BLUE);                                                            //prende los leds de color azul \r\n       delay(500);                                                                          //espera 500 milisegundos\r\n    }\r\nif (n>=176 && n<=255)                                                            //si n es mayor o igual a 176 y menor o igual a 255\r\n    {\r\n        motor.set(RIGHT,255);                                                   //gira el motor en direccion a las manecillas del reloj a la velocidad especificada\r\n        luces.color(GREEN);                                                      //prende los leds de color verde \r\n        delay(500);                                                                        //espera 500 milisegundos\r\n    }\r\n    \r\n    luces.color(BLACK);                                                            //prende los leds de color negro\r\n}\r\n'),
(22, 'Termometro', 'Un Termometro con el sensor de Temperatura', 2, 2, '-', 'default.png', '\r\n displaybk segments(PORT1);                                                 //Declara el modulo de display en el puerto 1\r\n temperaturebk sensor_temperatura(PORT2);      //Declara el modulo de temperatura en el puerto 2\r\n float temp_C;                                                                                          //detecta la temperatura en centigrados\r\n\r\n code() {                                                                                                  //ciclo que corre el codigo\r\n    \r\n    temp_C=sensor_temperatura.read(C);          //lee la temperatura\r\n    segments.print(temp_C);                                             //Muestra la temperatura en el display\r\n\r\n }\r\n'),
(23, 'Perilla controla losLeds', 'Según vayas girando la perilla se irán cambiando los colores y la intensidad de los leds', 3, 3, '-', 'Perillacoloresdeleds.jpg', '\r\n\r\nledsbk leds(PORT1);                                         //declara el modulo de leds en el puerto 1\r\nknobbk modulo_pot(PORT2);\r\nint pot;\r\nint b;                                                                      //declara la variable b\r\nint r;                                                                      //declara la variable r\r\nint g;                                                                     //declara la variable g\r\n\r\n\r\ncode(){                                                                 //ciclo que corre el codigo \r\n     pot = modulo_pot.read();\r\n     r = random(pot);                                         //variable r con la función random especificada en 255\r\n     g = random(pot);                                      //variable g con la funcion random especificada en 255\r\n     b = random(pot);                                       //variable b con la funcion random especificada en 255\r\n     leds.color(r,g,b);                                         //los leds prenden de color aleatorio\r\n     leds.brightness(pot);\r\n     delay(500-pot);                                              //espera 500 milisegundos menos el valor del pot\r\n }\r\n'),
(24, 'Piano con Botones', 'Cada botón es una nota diferente. Crea tus propias melodias con el piano', 4, 4, '-', 'Pianobotones.jpg', '\r\n\r\nbuttonsbk teclas(PORT1);          //Declara el modulo de botones en el Puerto1\r\nbuzzerbk bocina(PORT2);          //Declara el modulo de buzzer en el Puerto2\r\n\r\nint casos=0;                            //Declara una variable llamada casos\r\nint tiempo=200;                       //Declara una variable llamada tiempo\r\n\r\ncode() {\r\n \r\n casos = teclas.read();              //Lee el boton que presionas y lo guarda en la variable casos\r\n  \r\nswitch(casos){             //Selecciona un caso dependiendo del valor que tiene la variable casos\r\n\r\ncase 1:           //Caso 1 que se realiza si la variable casos es igual a 1\r\n  bocina.playtone(NOTE_C6,tiempo);        //Hace sonar la bocina a cierto tono por un determinado tiempo\r\n  delay(tiempo);          //Espera en milisegundos\r\n  break;                      //Fin del caso\r\n  \r\ncase 2:           //Caso 2 que se realiza si la variable casos es igual a 2\r\n  bocina.playtone(NOTE_D6,tiempo);      //Hace sonar la bocina a cierto tono por un determinado tiempo\r\n  delay(tiempo);          //Espera en milisegundos\r\n  break;                      //Fin del caso\r\n  \r\ncase 3:          //Caso 3 que se realiza si la variable casos es igual a 3\r\n  bocina.playtone(NOTE_E6,tiempo);      //Hace sonar la bocina a cierto tono por un determinado tiempo\r\n  delay(tiempo);          //Espera en milisegundos\r\n  break;                      //Fin del caso\r\n  \r\ncase 4:          //Caso 4 que se realiza si la variable casos es igual a 4\r\n  bocina.playtone(NOTE_F6,tiempo);      //Hace sonar la bocina a cierto tono por un determinado tiempo\r\n  delay(tiempo);          //Espera en milisegundos\r\n  break;                      //Fin del caso\r\n  \r\ncase 5:          //Caso 5 que se realiza si la variable casos es igual a 5\r\n  bocina.playtone(NOTE_G6,tiempo);      //Hace sonar la bocina a cierto tono por un determinado tiempo\r\n  delay(tiempo);          //Espera en milisegundos\r\n  break;                      //Fin del caso\r\n  \r\ndefault:                                  \r\n bocina.turn(OFF);              //apaga la bocina \r\n \r\n}\r\n }\r\n'),
(25, 'Piano sin Botones', 'Según la distancia va a ir cambiando la nota que se escuche', 3, 3, '-', 'Pianosinbotones.jpg', '\r\n\r\ndistancebk sensor_sharp(PORT1);                                   //declara el modulo de distancia en el puerto 1\r\nfloat distance_cm;                                                                 //variable para guardar la distancia en cm\r\nbuzzerbk buzzer(PORT2);                                                    //declara el modulo de buzzer en el puerto 2\r\n\r\ncode(){                                                                                      //ciclo que repite el codigo\r\n\r\n  distance_cm = sensor_sharp.read(CM);                       //lee la distancia en cm\r\n  bk7write(distance_cm);                                                     //imprime en pantalla\r\n  \r\n   if (distance_cm > 10 & distance_cm < 15)                 //si la distancia es mayor que 10 y menor que 20\r\n  {\r\n    buzzer.playtone(NOTE_C6);                                             //toca nota musical en el buzzer\r\n     delay(250);\r\n  }\r\n  \r\n  if (distance_cm > 15 & distance_cm < 30)                   //si la distancia es mayor a 20 y menor a 30\r\n  {\r\n    buzzer.playtone(NOTE_D6);                                          //toca una nota musical en el buzzer\r\n    delay(250);  \r\n }\r\n  \r\n  if (distance_cm > 30 & distance_cm < 45)                   //si la distancia es mayor a 30 y menor a 40\r\n  {\r\n    buzzer.playtone(NOTE_E6);                                          //toca una nota muscial en el buzzer\r\n    delay(250);  \r\n}\r\n   \r\n  if (distance_cm > 45 & distance_cm < 60)                  //si la distancia es mayor a 40 y menor a 50\r\n  {\r\n    buzzer.playtone(NOTE_F6);                                          //toca una nota musical en el buzzer\r\n    delay(250);  \r\n}\r\n    if (distance_cm > 60 & distance_cm < 75)                //si la distancia es mayor a 50 y menor a 60\r\n  {\r\n    buzzer.playtone(NOTE_G6);                                         //toca una nota musical en el buzzer\r\n     delay(250);\r\n  }\r\n    else\r\n {\r\n    buzzer.playtone(OFF);                                                    //apaga el buzzer \r\n }\r\n}\r\n\r\n\r\n\r\n'),
(26, 'Sensor prende Leds', 'Los leds prenderán cada que el sensor detecte un objeto pasar por ahí', 2, 2, '-', 'Porteriadeleds.jpg', '\r\n\r\ndistancebk sensor_sharp(PORT2);                              //declara el modulo de distancia en el puerto 2\r\nfloat distance_cm;                                                            //variable para guardar la distancia en cm\r\nledsbk leds(PORT1);                                                       //declara el modulo de leds en el puerto 1\r\n\r\ncode(){                                                                                 //ciclo que repite el codigo\r\n\r\n  distance_cm = sensor_sharp.read(CM);                  //lee la distancia en cm\r\n  bk7write(distance_cm);                                                //imprime en pantalla \r\n\r\n  if (distance_cm > 10 & distance_cm < 50)               //condicion que detecta si algo se encuentra entre 10 y 50 cm\r\n  {\r\n    leds.color(GREEN);                                                    //prende los leds de color verde\r\n    delay(500);                                                                   //espera 500 milisegundos  \r\n  }\r\n  else                                                                                  //si la condicion no se cumple\r\n  {\r\n    leds.color(BLACK);                                                      //prende los leds negros\r\n  }\r\n}\r\n\r\n'),
(27, 'Regla Invisible en pulgadas', 'Regla Invisible en pulgadas', 1, 1, '-', 'default.png', '\r\n\r\ndistancebk sensor_sharp(PORT1);        //Declara el Modulo de distancia en el puerto 1\r\nfloat distance_in;                     //Variable para guardar la distancia en pulgadas\r\n\r\n//Codigo principal que corre por siempre\r\ncode(){\r\n\r\n  distance_in = sensor_sharp.read(IN);     // Lee la distancia en pulgadas\r\n  bk7print(distance_in);                   // Imprime en pantalla\r\n  delay(250);                              // Espera 250 milisegundos\r\n\r\n}'),
(28, '5 Leds 5 Notas', 'Cada que prende un led es una nota diferente para cada uno de los leds', 3, 3, '-', 'Secuencia5leds5notas.jpg', '\r\n\r\nledsbk leds(PORT1);                                                       //declara el modulo de leds en el puerto 1\r\nbuzzerbk buzzer(PORT2);                                                //declara el modulo de buzzer en el puerto 2\r\nint i;                                                                                      //declara la variable i\r\n\r\ncode(){                                                                                  //ciclo que repite el codigo\r\n{ \r\nfor (i=1;i<=1;i++)                                                                //empieza en 1, debe ser menor igual a 1 y cada ciclo suma uno\r\n  { \r\n    leds.color(i,GREEN);                                                   //prende el led que se encuentre en la variable i de color verde\r\n    buzzer.playtone(1050);                                                //toca una nota musical en el buzzer\r\n    delay(500);                                                                    //espera 500 milisegundos \r\n    leds.color(i,BLACK);                                                   //apaga el led \r\n  }\r\n   buzzer.playtone(OFF);                                                  //apaga el buzzer\r\nfor (i=2;i<=2;i++)                                                              //empieza en 2, debe ser menor igual a 2 y cada ciclo suma uno\r\n {\r\n   leds.color(i,BLUE);                                                      //prende el led que se encuentre en la variable i de color azul\r\n   buzzer.playtone(1870);                                                //toca una nota musical en el buzzer\r\n   delay(500);                                                                    //espera 500 milisegundos\r\n   leds.color(i,BLACK);                                                   //apaga el led\r\n  }\r\n   buzzer.playtone(OFF);                                                //apaga el buzzer\r\nfor (i=3;i<=3;i++)                                                            //empieza en 3, debe ser menor igual a 3 y cada ciclo suma uno\r\n {\r\n   leds.color(i,ORANGE);                                              //prende el led que se encuentre en la variable i de color naranja \r\n   buzzer.playtone(1568);                                              //toca una nota muscial en el buzzer\r\n   delay(500);                                                                  //espera 500 milisegundos\r\n   leds.color(i,BLACK);                                                 //apaga el led\r\n  }\r\n   buzzer.playtone(OFF);                                              //apaga el buzzer\r\n for (i=4;i<=4;i++)                                                          //empieza en 4, debe ser menor igual a 4 y cada ciclo suma uno\r\n {\r\n   leds.color(i,YELLOW);                                             //prende el led que se encuentre en la variable i de color amarillo\r\n   buzzer.playtone(3729);                                            //toca una nota musical en el buzzer  \r\n   delay(500);                                                                //espera 500 milisegundos\r\n   leds.color(i,BLACK);                                                //apaga el led\r\n  }\r\n   buzzer.playtone(OFF);                                             //apaga el buzzer\r\nfor (i=5;i<=5;i++)                                                          //empieza en 5, debe ser menor igual a 5 y cada ciclo suma uno\r\n {\r\n   leds.color(i,WHITE);                                                //prende el led que se encuentre en la variable i de color blanco\r\n   buzzer.playtone(659);                                             //toca una nota en el buzzer\r\n   delay(500);                                                               //espera 500 milisegundos \r\n   leds.color(i,BLACK);                                               //apaga el led\r\n  }\r\n   buzzer.playtone(OFF);                                            //apaga el buzzer\r\n }\r\n delay(250);                                                                 //espera 250 milisegundos \r\n}\r\n');
INSERT INTO `proyectos` (`ID_proy`, `Nombre`, `Descripcion`, `Dificultad`, `Tiempo`, `Video`, `Imagen`, `Codigo`) VALUES
(29, 'Semaforo', 'Crear un semaforo con briko', 3, 3, '-', 'Semaforo.jpg', '\r\n\r\nledsbk luces(PORT1);                                          // se declara el nombre de los leds y el puerto en el cual esta conectado. En este caso se llamn luces y estan en el puerto 1\r\n\r\ncode(){                                                                              //Comienza el ciclo \r\n   \r\n    luces.color(1,GREEN);//se enciende el primer led de color verde\r\n    luces.color(2,BLACK);//el segundo led se matiene apagado hasta la linea 20 del codigo\r\n    luces.color(3,BLACK);// el tercer led se matiene apagado hasta la linea 25 del codigo\r\n    delay(3000);// se declara que la orden anterior se mantendra por 3000 mili segundos = 3 segundos\r\n    luces.color(BLACK);// se apagan las luces para que comienze el parpadeo del semaforo para pasar al color amarillo\r\n    delay(500);\r\n    luces.color(1,GREEN);\r\n    delay(500);\r\n    luces.color(BLACK);\r\n    delay(500);\r\n    luces.color(1,GREEN);\r\n    delay(500);\r\n    luces.color(BLACK);\r\n    delay(500);\r\n    luces.color(1,GREEN);\r\n    delay(500);// Termina el parpadeo\r\n    luces.color(2,YELLOW);// se enciende el led en la segunda posicion de color amarillo\r\n    luces.brightness(100);// El led se enciende con una intensidad de 100 (la minima intensidad es de 0 y la maxima de 255)\r\n    luces.color(1,BLACK);// se apaga el led en la posicion 1 que volvera a encender una vez que el cilo termine y vuelva a comenzar\r\n    luces.color(3,BLACK);\r\n    delay(2500);// el led en la segunda posicion \r\n    luces.color(3,RED);// lod El led en la tercera posicion se enciende de color rojo y los demas se apagan\r\n    luces.color(2,BLACK);\r\n    luces.color(1,BLACK);\r\n    delay(3000);\r\n   }// termina el ciclo y vuelve a comenzar\r\n    \r\n'),
(30, 'Stop it', 'Con el módulo de botones, atrapa al led rojo en la posición que se encuentre, mientras más  veces lo atrapes irá subiendo la dificultad del juego', 4, 4, '-', 'Stopit.jpg', '\r\n\r\nbuttonsbk Buttons(PORT1);                                             //declara el modulo de botones en el puerto 1\r\nledsbk leds(PORT2);                                                        //declara el modulo de leds en el puerto 2\r\n\r\nint boton;                                                                             //declara la variable boton\r\nint count=1;                                                                        //declara la variable count igual a 1\r\nint var;                                                                                 //declara la variable var\r\nint del = 700;                                                                     //declara la variable del igual a 700\r\nint n;                                                                                   //declara la variable n\r\n\r\n\r\ncode(){                                                                               //ciclo que corre el codigo\r\n  leds.brightness(5);                                                      //brillo de leds en 5\r\n  leds.color(BLUE);                                                        //prende los leds azules\r\n  leds.color(count, RED);                                              //prende el led 1 de color rojo\r\n  \r\n  while (var < del)                                                           //ciclo que corre mientras que var sea menor a del \r\n  \r\n  {\r\n   boton = Buttons.read();                                             //la variable boton obtendra el valor de Buttons.read()\r\n   bk7write(boton);                                                         //imprime en pantalla el boton que se presiona\r\n   if (boton > 0 && boton != count)                              //ciclo que compara si el boton es mayor a 0 y diferente a count\r\n   {\r\n     leds.color(RED);                                                    //prende los leds de color rojo\r\n     delay(200);                                                              //espera 200 milisegundos \r\n     del = del + 20;                                                        //se le suma a la variable del 20 \r\n   }\r\n   if (boton == count)                                                   //si el boton que se presiona es igual a count\r\n    {\r\n     leds.color(GREEN);                                              //prende los leds de color verde\r\n     if (del>20)                                                               //si del es mayor a 20\r\n     {\r\n     del = del - 20;                                                         //se le resta a la variable del 20\r\n     }\r\n     else                                                                          //si no se cumple lo anterior\r\n     {\r\n      for(n=0;n<10;n++)                                                //ciclo que corre de n igual a 0 hasta que sea menor a 10 \r\n          {\r\n           leds.color(GREEN);                                        //prende los leds de color verde\r\n           delay(100);                                                       //espera 100 milisegundos\r\n           leds.color(BLACK);                                        //prende los leds de color negro \r\n           delay(100);                                                       //espera 100 milisegundos \r\n      \r\n             }\r\n            del = 700;                                                       //variable del igual a 700\r\n     }\r\n     \r\n     delay(300);                                                            //espera 300 milisegundos \r\n     count = random(4);                                             //count con un random de 4 números \r\n          \r\n     var = 10000;                                                         //variable var con valor de 10000\r\n    \r\n    } \r\n    var++;                                                                     //se le va sumando de uno por uno a la variable var\r\n    delay(1);                                                                //espera 1 milisegundo\r\n  }\r\n  \r\n  var = 0;                                                                     //variable var es igual a 0\r\n  \r\n  count = count + 1;                                                  //suma 1 a la variable count\r\n  \r\n  if (count>5)                                                             //condicion que indica que si count es mayo a 5\r\n  \r\n  {\r\n    count = 1;                                                             //count es igual a 1\r\n  }\r\n  \r\n}\r\n'),
(31, 'Timbre con Leds', 'Cada que se presiona el botón suena el buzzer y prende los leds ', 2, 2, '-', 'Timbrebuzzerconleds.jpg', '\r\n\r\nbuzzerbk buzzer(PORT1);                                //declara el modulo de buzzer en el puerto 1\r\nbuttonsbk Buttons(PORT2);                           //declara el modulo de botones en el puerto 2\r\nledsbk luces(PORT3);\r\nint boton;                                                            //declara variable boton\r\n\r\ncode() {                                                                //ciclo que repite el codigo\r\n    \r\n    boton = Buttons.read();\r\n    \r\n  if ( boton == 1)                        //condicion que lee si presionas el boton 1\r\n  {\r\n     luces.color(BLUE);                                       //prende los leds azules\r\n    buzzer.playtone(1050);                                //toca nota musical en el buzzer\r\n    delay(500);                                                     //espera 500 milisegundos\r\n  }\r\n  else                                                                //si no se cumple\r\n  {\r\n    luces.color(BLACK);                                  //prende los leds negros\r\n    buzzer.turn(OFF);  \r\n}\r\n}\r\n'),
(32, 'Alarma-ART', 'Se elaborara un sensor con el cuál se podra interactuar acercandolo a paredes y dar advertencias de que tan cerca estes.', 2, 2, '-', 'default.png', '\r\nbuzzerbk mybocina(PORT1);                                           //se declara que se inicia el módulo de bocinas.\r\ndistancebk mysensor(PORT2);                                         //se declara que se inicia el módiulo de sensor de dsitancia.\r\nint distancia;                                                      //se declara una nueva variable llamada distancia.\r\n\r\ncode() {                                                            //Inicia aquí tu código.                                      \r\ndistancia=mysensor.read();                                          //se declara que distancia sera igual a mysensor por lo que la señal será interpretada de la misma manera.\r\nif (distancia<79){                                                  //se inicia una condición donde si la distancia es menor a 79 la bocina se activara.\r\n  mybocina.beep(500,1000);\r\n  delay(1000);\r\n}\r\nelse (distancia>80);{                                               //se inicia una condición donde si la ditancia es mayor a 80 la bocina permanecerá apagada.\r\nmybocina.turn(OFF);\r\n}\r\n\r\n}\r\n'),
(33, 'Arcoiris-ART', 'Consiste en recrear con los LEDs los colores de los arco iris con tiempos definidos para recrear los colores del arco iris', 2, 2, 'https://www.youtube.com/embed/IL0jLC0vlBs', 'default.png', '\r\nledsbk myleds(PORT1);                                 //En esta línea se declara el módulo que se utilizará (en este caso el módulo de LEDs) y se encuentran en color naranja, el nombre del módulo (en este caso "myleds") y el puerto donde se encuntra que aparece en letras azules. En este caso el puerto (PORT) esta en el puerto 1\r\n\r\n\r\ncode() {\r\n\r\n  myleds.color(RED);                                    //En esta línea se especifíca el color en que el módulo de LEDs debe prender según el color, en este caso el color es Rojo, que es el primer color del arco iris, así mismo al no poner el número de LED antes del color se declara que prenderán todos los LEDs del módulo del color indicado.\r\n  delay(1000);                                          //En esta línea se declara el tiempo que durara prendidos los LEDs con el comando "delay" y en consecuente el tiempo en milisegundos (Queda aclarar que para declarar que el tiempo sea de un segundo se debe de poner la cantidad 1000, como es el caso, para poder designar un segundo) que desea que duren los LEDs prendidos\r\n  myleds.color(ORANGE);                                 //En esta línea se declara el sigueinte color en que los LEDs deben de prender, en este caso es el color naranja que es el siguiente de los colores del arco iris.\r\n  delay(1000);                                          //En esta línea volveremos a especifícar el tiempo de duración en que prenderán los LEDs de color naranja, en este caso pondremos 1000 milisegundos lo que equivale a un segunodo.\r\n  myleds.color(YELLOW);                                 //En esta línea declaramos que el color en que los LEDs prenderán en esta ocación, serán de color amarillo por el orden de los colores del arco iris.\r\n  delay(1000);                                          //En esta línea declararemos el tiempo en qur durara prendidos los LEDs de color amarillo, en este caso será de 1 segundo o 1000 milisegundos\r\n  myleds.color(GREEN);                                  //En esta línea específicamos el siguiente color en el que prenderán los LEDs, que será verde\r\n  delay(1000);                                          //En esta línea declaramo que el tiempo en el que durarán prendidos los LEDs de color verde será de 1000 milisegundos que es lo mismo a 1 segundo\r\n  myleds.color(BLUE);                                   //En esta línea se declara que el siguiente color del arco iris  que es el color azul, del cual prenderán todo el módulo de LEDs\r\n  delay(1000);                                          //En esta línea específicamos el tiempo en el cual \r\n  myleds.color(PURPLE);                                 //En esta línea se establece el color morado en el módulo de LEDs\r\n  delay(1000);                                          //En esta línea se declara que el tiempo en el que durarán prendidos de color morado el módulo de LEDs, que en este caso será de 1000 milisegundo, es decir, 1 segundo\r\n    \r\n  }\r\n'),
(34, 'Bandera-ART', 'La dinámca del juego consiste en recrear una bandera de tres colores que a los niños les agrade.', 1, 1, 'https://www.youtube.com/embed/DdJK1r2yGo8', 'default.png', '\r\nledsbk myleds(PORT1);                            //En esta línea se declara el módulo que se utilizará (en este caso el módulo de LEDs) y se encuentran en color naranja, el nombre del módulo (en este caso "myleds") y el puerto donde se encuntra que aparece en letras azules. En este caso el puerto (PORT) esta en el puerto 1\r\n\r\ncode(){\r\n  \r\nmyleds.color(1,GREEN);                           //En esta línea se establece que en el módulo de LEDs, el LED número uno, el cual es el LED del extremo izquierdo, se prenda de color verde para iniciar con la bandera de México\r\nmyleds.color(2,WHITE);                           //En esta línea se establece que en el módulo de LEDs, el LED número 2, el cual se encuentra justo a la derecha del primer led, se prenda de color blanco, el color consecuente de la bandera mexicana\r\nmyleds.color(3,RED);                             //En esta línea se establece que en el´módulo de LEDs, el LED número 3, el cual se encuentra en consecuente al segundo LED, se prenda de color rojo, el último color de la bandera mexicana \r\n\r\n}\r\n'),
(35, 'COmbinar Colores-ART', 'Se elaborara una dinámica en la cual los niños podran jugar con el módulo de botónes y LED.', 1, 1, '-', 'default.png', '\r\nledsbk myleds(PORT1);                                     //declara el módulo en el que utilizan los LEDS\r\nbuttonsbk mybotones (PORT2);                              //declara el módulo en el que utilizan los botónes.\r\nint colores;               \r\n\r\ncode() {\r\ncolores=mybotones.read();                                 //se da la indicacion que colores sera igual a mybotonrs por lo que tendra que leer las señales que le serán mandadas.\r\nswitch (colores){                                         //se inicia una condición en la cual se presntan distintos casos en los cuales cada uno presentan diferentes valores y hacen acciónes diferentes. \r\ncase 1:                                                   //se inicia el caso 1 el cual se le dara ciertos valores.\r\nmyleds.color(GREEN);                                      //se declara que el LED debe de prender en color verde.\r\nbreak;                                                    //esta variable declara que el caso 1 ha terminado y se pueda iniciar con otro caso.\r\ncase 2:                                                   //se inicia el caso 2 el cual se le dara ciertos valores.\r\nmyleds.color(RED);                                        //se declara que el LED debe de prender en color rojo.\r\nbreak;                                                    //esta variable declara que el caso 2 ha terminado y se pueda iniciar con otro caso.\r\ncase 3:                                                   //se inicia el caso 3 el cual se le dara ciertos valores.                                                 \r\nmyleds.color(YELLOW);                                     //se declara que el LED debe de prender en color amarillo.\r\nbreak;                                                    //esta variable declara que el caso 3 ha terminado y se pueda iniciar con otro caso.\r\ncase 4:                                                   //se inicia el caso 4 el cual se le dara ciertos valores.\r\nmyleds.color(PINK);                                       //se declara que el LED debe de prender en color rosa.\r\nbreak;                                                    //esta variable declara que el caso 4 ha terminado y se pueda iniciar con otro caso.\r\ncase 5:                                                   //se inicia el caso 5 el cual se le dara ciertos valores.\r\nmyleds.color(BLUE);                                       //se declara que el LED debe de prender en color azul. \r\nbreak;                                                    //esta variable declara que el caso 5 ha terminado y se pueda iniciar con otro caso.\r\ndefault:                                                  //se declara que el valor debe de volver a su estado predeterminado.\r\nmyleds.color(BLACK);                                      //se declara que el LED no debe de prender de color.\r\nbreak;              \r\n}\r\n\r\n}\r\n'),
(36, 'Luces de tu casa -ART', 'La dinámca del juego consiste en recrear unas luces de tu casa que se pueden regular con una perilla.', 3, 2, 'https://www.youtube.com/embed/p8ajayIr698', 'default.png', 'ledsbk myleds(PORT1);                            //En esta línea se declara el módulo que se utilizará, en este caso el módulo de LEDs, y se encuentran en color naranja, el nombre del módulo (en este caso "myleds") y el puerto donde se encuntra que aparece en letras azules. En este caso el puerto (PORT) esta en el puerto 1\r\nknobbk mypot(PORT2);                             //En esta línea se declara el módulo que se utilizará, en este caso es el módulo de perilla, y se encuentra en color naranja, el nombre del módulo (en este caso "mypot") y el puerto donde se encuentra, que aparece en letras azules. En este caso el puerto (PORT) esta ubicado en el puerto número 2\r\nint perilla;           \r\ncode() {\r\n//Escribe aqui tu codigo\r\nperilla=mypot.read();                            //En esta línea se declara que el valor númerico de "KnobBK" llamado "mypot" se otorga a la variable "perilla"\r\nswitch(perilla){                                 //En esta línea se declara que con el comando "switch" que cambia el programa dependiendo del valor númerico del valor dado. En este caso el valor númerico es "perilla" que representa el valor o valores de la perilla\r\n  case 0:                                        //En esta línea se declara que cuando el valor sea cero del valor "perilla", ejecute el programa que se define en las siguientes líneas                             \r\n  myleds.color(255,255,255);                     //En esta línea se especifíca que el módulo de LEDs prenderá cuando el valor númerico de "perilla" sea 0. El módulo de LEDs prendera según la combiancion de intensidades RGB, en este caso será blanco.\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "perilla" cambie\r\n  case 20:                                       //En esta línea se específica que cuando el valor sea veinte del valor "perilla", se deberá ejecutar el programa de las siguientes líneas \r\n  myleds.color(220,220,220);                     //En esta línea se especifíca que el módulo de LEDs prendera con una intensidad diferente y menor a la que anterior, ya que el valor de "perilla" es distinta.\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "perilla" cambie\r\n  case 70:                                       //En esta línea se específica que cuando el valor de "perilla" sea igual a setenta, se ejecutará el programa que continua en las siguientes líneas\r\n  myleds.color(165,165,165);                     //En esta línea se declara que el módulo de LEDs prenderá con una intensidad menor, ya que el valor de "perilla" ha cambiado y vamos logrando que baje más su intensidad\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "perilla" cambie\r\n  case 100:                                      //En esta línea se establece qiue cuando el valor de "perilla" sea igual a 100, se ejecuten las lineas que siguen\r\n  myleds.color(100,100,100);                     //En esta línea se específica que cuando el valor de "perilla" sea igual a 100, el módulo de LEDs prenda con una intensidad menor, segun la combinación de colores RGB.\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "perilla" cambie\r\n  case 200:                                      //En esta línea se declara al valor de "perilla" en dosciento, lo cual cuando el valor sea correcto se ejctaran las líneas de código de abajo\r\n  myleds.color(50,50,50);                        //En esta línea se establece que el módulo de LEDs prenderá de una intenisdad muy baja, casi nula\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "perilla" cambie\r\n  case 250:                                      //En esta línea se específica que cuando el valor de "perilla" sea igual a dosciento cincuenta, se ejecuta el ultimo programa que se esta declarando abajo.\r\n  myleds.color(0,0,0);                           //En esta línea se declara que cuando el valo de doscinetos cincuenta de "perilla" sea detectado, el módulo de LEDs se apagará por completo.\r\n  break; \r\n}\r\n}\r\n\r\n'),
(37, '¡No choques!-ART', 'Se elaborara un sensor con el cuál se podra interactuar acercandolo a paredes y dar advertencias de que tan cerca estes.', 2, 2, '-', 'default.png', '\r\nledsbk myleds(PORT1);                                    //declara el módulo en el que utiliza LED.\r\n distancebk mysensor (PORT2);                             //declara el módulo en el que utiliza el sensor de distancia.\r\n int distancia;           \r\n\r\ncode() {\r\n  \r\n  distancia=mysensor.read();                              //se da la indicacion que distancia sera igual a mysensor por lo que tendra que leer las señales que le serán mandadas.\r\n\r\nif(distancia<=19){                                        //declara el inicio de una condición la cual nos indica que si la distancia es menor o igual que 19 el Led prendera en verde. \r\n  myleds.color(BLUE);                                     //declara el color que utilizara el LED.(Cualquier color)\r\n}\r\n\r\nelse(distancia>=20);{                                     //declara una condición la cual nos indica que si la distancia es mayor o igual que 20 el Led prendera en verde.\r\n  myleds.color(GREEN);}                                   //declara que el color que utilizara el Led sera Verde.\r\n\r\n\r\n}\r\n'),
(38, 'Piano-ART', 'Consiste en recrear un piano con 5 diferentes tipos de frecuencias que además de emitir sonidos, al tocar cada botón se prende de diferente color los LEDs', 3, 2, '-', 'default.png', '\r\nledsbk myleds(PORT1);                            //En esta línea se declara el módulo que se utilizará (en este caso el módulo de LEDs) y se encuentran en color naranja, el nombre del módulo (en este caso "myleds") y el puerto donde se encuntra que aparece en letras azules. En este caso el puerto (PORT) esta en el puerto 1\r\nbuzzerbk mybuzzer(PORT2);                        //En esta línea se establece el módulo de bocina el cual aparece en letras naranjas y además el nombre del módulo, el cual es en este caso "mybuzzer", y el puerto en donde se encuentra conectado (PORT) el cual es el número 2.\r\nbuttonsbk mybotones(PORT3);                      //En esta línea se declara el módulo de botones, el cual se encuentra en letras naranjas y además se declara el nombre (en este caso "mybotones") y el puerto (PORT) en donde se encuentra, el cual en el caso es el número 3.\r\nint botones;                                     //En esta línea se establece la variable "int" que otorga un valor númerico. En este caso nuestra variable se llamará "botones"\r\n\r\ncode() {\r\n//Escribe aqui tu codigo\r\n\r\nbotones=mybotones.read();                        //En esta línea se específica como una variable númerica el valor de "botones" para referirse al módullo de botones\r\n\r\nswitch(botones){                                 //En esta línea se establece que cambiarán las acciones o programas según el boton que presione con los comandos de "case"\r\n  case 1:                                        //En esta línea se declara que al presionar el botón número uno se ejecutará el programa o las líneas de código de abajo\r\n  mybuzzer.playtone(NOTE_C6,1000);                       //En esta línea se específica que la bocina tocara la nota C6 por un tiempo de 1000 milisegundos, que es igual a un segundo\r\n  myleds.color(RED);                             //En esta línea se declara que el módulo de LEDs, al presionar el botón número uno se prenderá de color rojo\r\n  delay(200);\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "botones" cambie\r\n  case 2:                                        //En esta línea se específica que cuando se apriete el botón número dos se ejecute el programa o las líneas de código de abajo\r\n  mybuzzer.playtone(NOTE_D6,1000);                       //En esta línea se establece que la bocina suene tocara la nota D6 por 1000 milisegundos.\r\n  myleds.color(GREEN);                           //En esta línea se especifica que cuando se presione el botón dos se prendan de verdes los LEDs\r\n  delay(200);\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "botones" cambie\r\n  case 3:                                        //En esta línea se establece que cuando el botón número 3 se presione, se ejecuten las líneas de abajo.\r\n  mybuzzer.playtone(NOTE_E6,1000);                      //En esta línea se declara que la bocina tocara la nota E6  por un tiempo de un segundo\r\n  myleds.color(BLUE);                            //En esta línea se establece que el módulo de LEDs prenda de color azul al ser presionado el botón número 3\r\n  delay(200);\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "botones" cambie\r\n  case 4:                                        //En esta línea se establece que al presionar el botón número 4 se ejecute el programa de abajo\r\n  mybuzzer.playtone(NOTE_F6,1000);                      //En esta línea se declara que la bocina tocara la nota F6 por un timepo de 1000 milisegundos \r\n  myleds.color(PINK);                            //En esta línea se establece que el módulo de LEDs prendera de color rosa al ser presionado el botón número 4\r\n  delay(200);\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "botones" cambie\r\n  case 5:                                        //En esta línea se establece que al presionar el botón número 5 se ejecutará el último programa o líneas de código\r\n  mybuzzer.playtone(NOTE_G6,1000);                      //En esta línea se declara que la bocina tocara la nota G6 por un tiempo de 1000 milisegundos o un segundo\r\n  myleds.color(YELLOW);                          //En esta línea se establece que los LEDs, al presionar el botón 5, se prenderán de amarillo\r\n  delay(200);\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "botones" cambie\r\n  default:                                       //En esta línea se establece la parte del código donde no se ejecuta ningun botón, por lo cual se espera a que una acción se ejecute \r\n  mybuzzer.turn(OFF);                            //En esta línea se declara que la bocina no emiirá ningún sonido por ningun tiempo\r\n  myleds.color(BLACK);                           //En esta línea se declara que los LEDs permanecerán apagados.\r\n  break;                                         //En esta línea se declara la variable "break" que funciona para que romper o deshacer el código escrito arriba cuando el variable númerico de "botones" cambie\r\n}\r\n\r\n\r\n}\r\n'),
(39, 'Semaforo-ART', 'Se elaborara semáforo con el cuál os niños puedan interactuar. ', 1, 1, 'https://www.youtube.com/embed/JLTI7CXUB-A', 'default.png', '\r\nledsbk myleds(PORT1);                                             //declara el módulo en el que utilizan los LEDS \r\ncode() {\r\n\r\n\r\nmyleds.color (1,GREEN);                                           //declara el color que utilizara el LED.(Verde)\r\nmyleds.color (2,GREEN);                                           //declara el color que utilizara el LED.(Verde)\r\nmyleds.color (3,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (4,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (5,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\ndelay(2000);                                                      //declara el tiempo en el que se detiene una acción.(2 segundos)\r\nmyleds.color (1,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (2,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (3,YELLOW);                                          //declara el color que utilizara el LED.(Amarillo)\r\nmyleds.color (4,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (5,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\ndelay (1000);                                                     //declara el tiempo en el que se detiene una acción.(1 segundo)     \r\nmyleds.color (1,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (2,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (3,BLACK);                                           //declara el color que utilizara el LED.(No se prende LED)\r\nmyleds.color (4,RED);                                             //declara el color que utilizara el LED.(Rojo)\r\nmyleds.color (5,RED);                                             //declara el color que utilizara el LED.(Rojo)\r\ndelay (2500);                                                     //declara el tiempo en el que se detiene una acción.(2.5 segundos)\r\n\r\n}\r\n'),
(40, 'Ventilador.ART', 'Se elaborara un sensor con el cuál se podra interactuar acercandolo a paredes y dar advertencias de que tan cerca estes.', 3, 2, 'https://www.youtube.com/embed/22onCqfW8qk', 'default.png', '\r\nknobbk myperilla(PORT1);                                          //declara el módulo en el que se utilizara la perilla.\r\nmotorbk mymotor(PORT2);                                           //declara el módulo en el que se utilizaran los motores.\r\nint perilla;                                                      //declara el nombre a una variablede tipo numérica\r\n\r\ncode(){\r\n \r\n  perilla=myperilla.read();                                         //se da la indicacion que perilla sera igual a myperilla por lo que tendra que leer las señales que le serán mandadas.                                      \r\n switch (perilla){                                                 //se inicia una condición en la cual se presntan distintos casos en los cuales cada uno presentan diferentes valores y hacen acciónes diferentes.\r\n case 20:                                                          //se inicia el caso 20 el cual se le dara ciertos valores.\r\n mymotor.set (LEFT,100);                                           //gira el motor a la izquierda a la velocidad de 255\r\n break;                                                            //esta variable declara que el caso 20 ha terminado y se pueda iniciar con otro caso.\r\n case 100:                                                         //se inicia el caso 100 el cual se le dara ciertos valores.\r\n mymotor.set(RIGHT,100);                                           //gira el motor a la derecha a la velocidad de 255\r\n break;                                                            //esta variable declara que el caso 100 ha terminado y se pueda iniciar con otro caso.  \r\n case 200:                                                         //se inicia el caso 200 el cual se le dara ciertos valores.\r\n mymotor.set(RIGHT,255);                                           //gira el motor a la derecha a la velocidad de 255\r\n break;                                                            //esta variable declara que el caso 200 ha terminado y se pueda iniciar con otro caso. \r\n case 254:                                                         //se inicia el caso 20 el cual se le dara ciertos valores.\r\n mymotor.set(STOP);                                                //se declara que se detiene el motor.\r\n break;                                                            //esta variable declara que el caso 300 ha terminado y se pueda iniciar con otro caso.\r\n   \r\n   }\r\n}\r\n'),
(41, 'Detector de Mentira', 'En este ejercicio conocerás como se utiliza los botones para cambiar de colores los LEDS y reproducir la bocina.', 2, 2, '-', 'default.png', '\r\n\r\nbuzzerbk bocina(PORT1);                                  //Declara el modulo de buzzer en el puerto 1\r\nledsbk focos(PORT3);                                            //Declara el modulo de leds en el puerto 2\r\nbuttonsbk botones(PORT4);                          //Declara el modulo de botones en el puerto 3\r\nint b;                                                                                      //Variable para detectar que boton se presiona\r\n\r\ncode() {                                                                 //Ciclo que corre el codigo\r\n\r\n  b=botones.read();                              //Detecta que boton se presiona\r\n    \r\n  if(b==1){                                                    //Si el boton es igual a 1 entonces lo que se cumple es lo siguiente\r\n    bocina.playtone(500);            //Toca una nota en el buzzer\r\n    focos.color(RED);                         //Prende los leds de color rojo\r\n   delay(500);                                       //Espera 500 milisegundos\r\n  }\r\n  \r\n  if (b==5){                                                               //Si el boton es igual a 5 entonces se cumple lo siguiente\r\n    bocina.playtone(500);                         //Toca una nota en el buzzer\r\n    focos.color(GREEN);                              //Prende los leds de color verde\r\n    delay(500);                                                       //Espera 500 milisegundos\r\n  }\r\n  \r\n  focos.color(BLACK);                          //Apaga los leds\r\n  bocina.turn(OFF);                              //Apaga la bocina \r\n  \r\n}\r\n\r\n'),
(42, 'Drag Racing', 'En este ejercicio conocerás como programan los LEDS para servir como semáforos.', 2, 2, '-', 'default.png', '\r\n\r\nledsbk leds(PORT3);                                                            //Declara el modulo de leds en el puerto 3\r\n\r\ncode() {                                                                                          //Ciclo que corre el codigo\r\n  \r\nleds.color(1,RED);                                                           //Prende el primer led de color rojo\r\ndelay(500);                                                                             //Espera 500 milisegundos\r\nleds.color(2,RED);                                                          //Prende el segundo led de color rojo\r\ndelay(500);                                                                             //Espera 500 milisegundos\r\nleds.color(3,RED);                                                         //Prende el tercer led de color rojo\r\ndelay(500);                                                                            //Espera 500 milisegundos\r\nleds.color(4,RED);                                                          //Prende el cuarto led de color rojo\r\ndelay(500);                                                                            //Espera 500 milisegundos\r\nleds.color(5,RED);                                                         //Prende el quinto led de color rojo\r\ndelay(500);                                                                           //Espera 500 milisegundos\r\nleds.color(GREEN);                                                    //Prende los leds de color verde\r\n\r\nwhile(1);                                                                                //Mientras que sea igual a 1 \r\n\r\n}\r\n'),
(43, 'Impresora', 'En este ejercicio conocerás como se utiliza el motor para imprimir en una hoja.', 1, 2, '-', 'default.png', '\r\n\r\nmotorbk motor1(PORT1);                                               //Declara el motor en el puerto 1\r\n                \r\ncode(){                                                                                          //Ciclo que corre el codigo\r\n\r\n    motor1.set(RIGHT,255);                                        //Gira el motor en direccion de las manecillas del reloj a la velocidad maxima \r\n\r\n}'),
(44, 'Sensor de alarma', 'En este ejercicio conocerás como programa el sensor de distancia como alarma de tiendas.', 2, 2, '-', 'default.png', '\r\n\r\ndistancebk sensor(PORT1);                              //Declara el modulo de sensor de distancia en el puerto 1\r\nbuzzerbk bocina(PORT2);                                   //Declara el modulo de buzzer en el puerto 2\r\nfloat distancia;                                                            //Variable para detectar la distancia\r\n\r\ncode(){                                                                                  //Ciclo para correr el codigo\r\n  \r\ndistancia = sensor.read();                               //Detecta la distancia\r\n\r\nif (distancia < 80){                                      //Si la distancia es menor a 80 entonces se cumple lo siguiente\r\n  bocina.beep(500,1000);            //Enciende el buzzer por 500 milisegundos y lo apaga por 1000\r\n  delay(1200);\r\n}  \r\n\r\nbocina.turn(OFF);                                     //Apaga la bocina \r\n\r\n}\r\n'),
(45, 'Hombre', 'Se realizara un robot autónomo que camine en una dirección y que esquive obstáculos', 3, 3, '-', 'default.png', '\r\n\r\nmotorbk motor1(PORT1);                                                  //Declara el modulo de motor 1 en el puerto 1\r\nmotorbk motor2(PORT7);                                                  //Declara el modulo de motor 2 en el puerto 7\r\ndistancebk sensor(PORT4);                                               //Declara el modulo de sensor de distancia en el puerto 4\r\nfloat distancia;                                                                             //Variable para detectar la distancia\r\nledsbk focos(PORT5);                                                              //Declara el modulo de leds en el puerto 5\r\n\r\ncode() {                                                                                                //Ciclo que corre el codigo\r\n\r\n  distancia = sensor.read();                                            //Detecta la distancia \r\n\r\nif (distancia < 20){                                                                 //Si la distancia detectada es menor a 20 entonces se cumple lo siguiente\r\n  \r\n  motor1.set(LEFT,125);                                                  //Gira el motor 1 en direccion contraria a las manecillas del reloj a la velocidad especificada\r\n  delay(500);                                                                                 //Espera 500 milisegundos\r\n  motor2.set(LEFT,125);                                                  //Gira el motor 2 en direccion contraria a las manecillas del reloj a la velocidad especificada\r\n  delay(500);                                                                                 //Espera 500 milisegundos\r\n  motor1.set(RIGHT,125);                                             //Gira el motor 1 en direccion de las manecillas del reloj a la velocidad especificada\r\n  motor2.set(LEFT,125);                                                  //Gira el motor 2 en direccion contraria a las manecillas del reloj a la velocidad especificada\r\n  \r\n}\r\n                                                                                                                   //Si la condicion no se cumple entonces se hace lo siguiente\r\nmotor1.set(LEFT,125);                                                    //Gira el motor 1 en direccion contraria a las manecillas del reloj  a la velocidad especificada\r\nmotor2.set(RIGHT,125);                                                //Gira el motor 2 en direccion a las manecillas del reloj a la velocidad especificada\r\n\r\n}\r\n'),
(46, 'Piano', 'En este ejercicio conocerás como se utilizan los botones para poder realizar un piano junto con la bocina.', 3, 3, '-', 'default.png', '\r\n\r\nbuttonsbk teclas1(PORT1);    // Declares a Button Module in Port1\r\nbuttonsbk teclas2(PORT2);   // Declares a Button Module in Port2\r\nbuzzerbk buzzer(PORT3);                      // Declares a Buzzer Module in Port4\r\n\r\nint casos=0;\r\nint casos2 = 0;\r\nint tiempo=200;\r\n\r\ncode(){\r\n  \r\n  casos = teclas1.read();\r\n  casos2 = teclas2.read(); \r\n  \r\n// Plays a different frequency in the buzzer according to the pressed button\r\n  \r\n  switch(casos){\r\n  case 1:  \r\n      buzzer.playtone(NOTE_B3,tiempo);       // Plays B3 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n  \r\n  case 2:\r\n      buzzer.playtone(NOTE_C4,tiempo);      // Plays C4 Note in buzzer\r\n       delay(tiempo);\r\n       break;\r\n  \r\n   case 3:\r\n      buzzer.playtone(NOTE_D4);      // Plays D4 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n  \r\n  case 4:\r\n      buzzer.playtone(NOTE_E4,tiempo);     // Plays E4 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n  \r\n  case 5:\r\n      buzzer.playtone(NOTE_F4,tiempo);     // Plays F4 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n      \r\n    default:\r\n    buzzer.turn(OFF);\r\n      \r\n  }\r\n  \r\n  switch(casos2){\r\n  \r\n  case 1:\r\n      buzzer.playtone(NOTE_G4,tiempo);     // Plays G4 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n  \r\n  case 2:\r\n      buzzer.playtone(NOTE_A4,tiempo);     // Plays A4 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n      \r\n    case 3:\r\n      buzzer.playtone(NOTE_B4,tiempo);     // Plays B4 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n      \r\n  case 4:\r\n      buzzer.playtone(NOTE_C5,tiempo);     // Plays C5 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n      \r\n  case 5:\r\n      buzzer.playtone(NOTE_D5,tiempo);     // Plays D5 Note in buzzer\r\n      delay(tiempo);\r\n      break;\r\n  \r\n   default:\r\n      buzzer.turn(OFF);             // Turns buzzer OFF \r\n}\r\n\r\n \r\n}\r\n'),
(47, 'Rampa', 'En este ejercicio realizarás una rampa en donde colocarás una canica y saldrá disparada.', 2, 3, '-', 'default.png', '\r\n\r\nmotorbk motor1(PORT1);                                                              //Declara el modulo de motor 1 en el puerto 1\r\nmotorbk motor2(PORT7);                                                              //Declara el modulo de motor 2 en el puerto 7\r\nknobbk potenciometro(PORT3);                                            //Declara el modulo de perilla en el puerto 3\r\nint pot;                                                                                                              //Variable para detectar que tanto gira la perilla\r\n\r\ncode() {                                                                                                          //Ciclo que corre el codigo\r\n \r\npot = potenciometro.read();                                              //Detecta cuanto se gira el potenciometro\r\n\r\npot = map(pot,0,270,0,255);                                            //Funcion map para cambiar el valor de 0 y 270 a una de 0 y 255\r\nmotor1.set(RIGHT,pot);                                                      //Motor 1 gira en direccion de las manecillas del reloj a la velocidad especificada\r\nmotor2.set(LEFT,pot);                                                          //Motor 2 gira en direccion contraria a las manecillas del reloj a la velocidad especificada\r\n\r\n}\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `ID_proyecto` int(11) NOT NULL,
  `Titulo` varchar(60) NOT NULL,
  `Descripción` varchar(500) NOT NULL,
  `Paso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `snake`
--

CREATE TABLE IF NOT EXISTS `snake` (
  `Nombre` varchar(15) NOT NULL,
  `Score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `snake`
--

INSERT INTO `snake` (`Nombre`, `Score`) VALUES
('Briko', 20),
('Kevin', 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textos`
--

CREATE TABLE IF NOT EXISTS `textos` (
  `ID_Proyecto` int(11) NOT NULL,
  `ID_crearP` int(11) NOT NULL,
  `Paso` int(11) NOT NULL,
  `Texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `ID_Tipo` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_Tipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`ID_Tipo`, `Nombre`) VALUES
(1, 'Piezas Mecanicas'),
(2, 'Otros'),
(3, 'Accesorios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `UID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(50) NOT NULL,
  `Fuid` bigint(20) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Edad` varchar(30) NOT NULL,
  `Passwd` varchar(100) NOT NULL,
  `Genero` varchar(1) NOT NULL,
  `Nickname` varchar(50) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UID`, `Fname`, `Fuid`, `Nombre`, `Correo`, `Edad`, `Passwd`, `Genero`, `Nickname`) VALUES
(1, '', 0, 'Kevin', 'kevin@gmail.com', '-2016', '8375bc4c958dacb0ad447d4e22364524320197e8', 'H', 'Kuvinz'),
(2, '', 0, 'Bruno', 'kevin@briko.cc', '-1991', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'H', 'brunoBot'),
(3, '', 0, 'Kevin', 'kuvin93@gmail.com', '-1991', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'O', 'Kuvinz'),
(4, '', 0, 'Omar', 'kevin.domvar@gmail.com', '-1991', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'O', 'Kuvinz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `ID_Proyecto` int(11) NOT NULL,
  `Video` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
