/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Logic blocks for Blockly.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';

goog.provide('Blockly.Blocks.brikode');

goog.require('Blockly.Blocks');

//////////******modulos******/////////////////////////////
var M_colors = 20;  ///color para bloques de led

var M_n_array = ['ledsbk_m','motorbk_m','displaybk_m','lightbk_m','buzzerbk_m','temperaturebk_m','buttonsbk_m','distancebk_m','knobbk_m','relaybk_m','servobk_m'];
var M_n2_array = ["ledsbk","motorbk","displaybk","lightbk","buzzerbk","temperaturebk","buttonsbk", "distancebk","knobbk","relaybk","servobk"];
var M_help_array = ['http://briko.cc/moduloled/1','http://briko.cc/modulomotor/1','http://briko.cc/modulodisplay/1','http://briko.cc/moduloluz/1','http://briko.cc/modulobocina/1','http://briko.cc/modulotemperatura/1','http://briko.cc/moduloboton/1','http://briko.cc/modulodistancia/1','http://briko.cc/moduloknob/1','http://briko.cc/modulorelay/1','http://briko.cc/moduloservo/1'];

Blockly.Blocks[M_n_array[0]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[0])
        .appendField(new Blockly.FieldVariableModule("led"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[0]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[1]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[1])
        .appendField(new Blockly.FieldVariableModule("motor"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[1]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[2]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[2])
        .appendField(new Blockly.FieldVariableModule("display"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[2]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[3]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[3])
        .appendField(new Blockly.FieldVariableModule("sensor1"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[3]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[4]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[4])
        .appendField(new Blockly.FieldVariableModule("buzzer"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[4]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[5]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[5])
        .appendField(new Blockly.FieldVariableModule("sensor2"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[5]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[6]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[6])
        .appendField(new Blockly.FieldVariableModule("button"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[6]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[7]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[7])
        .appendField(new Blockly.FieldVariableModule("sensor3"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[7]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[8]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[8])
        .appendField(new Blockly.FieldVariableModule("knob"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[8]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[9]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[9])
        .appendField(new Blockly.FieldVariableModule("relay"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[9]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks[M_n_array[10]] = {
  init: function() {
    this.appendDummyInput()
        .appendField(M_n2_array[10])
        .appendField(new Blockly.FieldVariableModule("servo"), "Name_M")
        .appendField(new Blockly.FieldDropdown([["PORT1", "PORT1"], ["PORT2", "PORT2"], ["PORT3", "PORT3"], ["PORT4", "PORT4"], ["PORT5", "PORT5"], ["PORT6", "PORT6"], ["PORT7", "PORT7"]]), "Port_M");
    this.setPreviousStatement(true, null);  //null es que se le puede conectar cualquier cosa
    this.setNextStatement(true, null);
    this.setColour(M_colors);
    this.setTooltip('');
    this.setHelpUrl(M_help_array[10]);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};
    
//////////////*************************/////////////////


/////////******************bloques de led**********////////////////////
var Led_blocks_color = 40;  ///color para bloques de led
var Led_input_color = 40;  ///color para input de leds
var Link_help = 'http://briko.cc/moduloled/1';

////funcion de brillo
Blockly.Blocks['brightness'] = {
  init: function() {
    this.appendValueInput("brigvals") //nombre de la entrada
        .setCheck("255_vals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("led"), "Name_M")
        .appendField(".brightness");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Led_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///entrada del brillo
Blockly.Blocks['set_255_val_led'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.Field255("20"), "Brig_c");
    this.setOutput(true, "255_vals");  //tipo de salida
    this.setColour(Led_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

///funcion de color
Blockly.Blocks['color'] = {
  init: function() {
    this.appendValueInput("colorvals") //nombre de la entrada
        .setCheck("color_vals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("led"), "Name_M")
        .appendField(".color");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Led_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///entrada de un solo color
Blockly.Blocks['set_color'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldColour("#ff0000"), "Color_c");
    this.setOutput(true, "color_vals");  //tipo de salida
    this.setColour(Led_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

///entrada de color mas input de selector o variable
Blockly.Blocks['set_color_num'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("LED#");
    this.appendValueInput("ledvals") //nombre de la entrada
        .setCheck("led_num_led_v"); //tipo de entrada que acepta
    this.appendDummyInput()
        .appendField(new Blockly.FieldColour("#ff0000"), "Color_c");
    this.setOutput(true, "color_vals");  //tipo de salida
    this.setColour(Led_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

///bloque selector de led
Blockly.Blocks['led_num_led'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"]]), "Led_num");
    this.setOutput(true, "led_num_led_v");  //tipo de salida
    this.setColour(Led_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

////bloque de puros inputs para el color
Blockly.Blocks['set_color_num2'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("RED");
    this.appendValueInput("led_color_red") //nombre de la entrada
    .setCheck("255_vals"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("GREEN");
    this.appendValueInput("led_color_green") //nombre de la entrada
    .setCheck("255_vals"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("BLUE");
    this.appendValueInput("led_color_blue") //nombre de la entrada
    .setCheck("255_vals"); //tipo de entrada que acepta
    this.setOutput(true, "color_vals");  //tipo de salida
    this.setColour(Led_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

////bloque de puros inputs para el color
Blockly.Blocks['set_color_num3'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("LED#");
    this.appendValueInput("ledvals") //nombre de la entrada
    .setCheck("led_num_led_v"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("RED");
    this.appendValueInput("led_color_red") //nombre de la entrada
    .setCheck("255_vals"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("GREEN");
    this.appendValueInput("led_color_green") //nombre de la entrada
    .setCheck("255_vals"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("BLUE");
    this.appendValueInput("led_color_blue") //nombre de la entrada
    .setCheck("255_vals"); //tipo de entrada que acepta
    this.setOutput(true, "color_vals");  //tipo de salida
    this.setColour(Led_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};
//////////////*********************************////////////////////////////

/////////******************bloques display**********////////////////////
var Display_blocks_color = 60;  ///color para bloques de led
var Display_input_color = 60;  ///color para input de leds
var Link_help = 'http://briko.cc/modulodisplay/1';

Blockly.Blocks['dis_print'] = {
  init: function() {
    this.appendValueInput("disprintvals") //nombre de la entrada
        .setCheck("dis_vals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("display"), "Name_M")
        .appendField(".print");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Display_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};


Blockly.Blocks['set_dis_print'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.Field999("100"), "dis_print_c");
    this.setOutput(true, "dis_vals");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['dis_printindi'] = {
  init: function() {
    this.appendValueInput("disprintindivals") //nombre de la entrada
        .setCheck("disindi_vals3") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("display"), "Name_M")
        .appendField(".printindividual");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Display_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks['set_dis_printindi'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Segment#");
    this.appendValueInput("segment_num") //nombre de la entrada
        .setCheck("disindi_vals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("#");
    this.appendValueInput("dis_num") //nombre de la entrada
        .setCheck("disindi_vals2"); //tipo de entrada que acepta
    this.setOutput(true, "disindi_vals3");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_segment_num'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"]]), "Seg_num");
    this.setOutput(true, "disindi_vals1");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

///bloque que selecciona el numero que mostrara el display
Blockly.Blocks['set_dis_num'] = {
  init: function() {
    this.appendDummyInput()
      .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"],["5", "5"],["6", "6"],["7", "7"],["8", "8"],["9", "9"]]), "Seg_nump");
    this.setOutput(true, "disindi_vals2");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};


Blockly.Blocks['dis_printcust'] = {
  init: function() {
    this.appendValueInput("disprintcustvals") //nombre de la entrada
        .setCheck("discust_vals3") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("display"), "Name_M")
        .appendField(".printcustom");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Display_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks['set_dis_printcust'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Segment#");
    this.appendValueInput("segmentcust_num") //nombre de la entrada
        .setCheck("discust_vals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("binary#");
    this.appendValueInput("discust_num") //nombre de la entrada
        .setCheck("discust_vals2"); //tipo de entrada que acepta
    this.setOutput(true, "discust_vals3");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_segmentcust_num'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"],["5", "5"]]), "Seg_num");
    this.setOutput(true, "discust_vals1");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

///bloque que selecciona el numero que mostrara el display
Blockly.Blocks['set_discust_num'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.Field255("20"), "Seg_nump");
    this.setOutput(true, "discust_vals2");  //tipo de salida
    this.setColour(Display_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};



Blockly.Blocks['dis_erase'] = {
  init: function() {
    this.appendValueInput("disprinterasevals") //nombre de la entrada
        .setCheck("discust_vals1") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("display"), "Name_M")
        .appendField(".erase");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Display_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

//////////////*********************************////////////////////////////

/////////******************bloques bocina**********////////////////////
var Buzzer_blocks_color = 80;  ///color para bloques de led
var Buzzer_input_color = 80;  ///color para input de leds
var Link_help = 'http://briko.cc/modulobocina/1';

Blockly.Blocks['buzzer_set'] = {
  init: function() {
    this.appendValueInput("buzzersetvals") //nombre de la entrada
        .setCheck("buzzer_setvals1") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("buzzer"), "Name_M")
        .appendField(".set");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Buzzer_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_buzzer_set_inp'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([["ON", "ON"], ["OFF", "OFF"]]), "buzzer_state");
    this.setOutput(true, "buzzer_setvals1");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['buzzer_beep'] = {
  init: function() {
    this.appendValueInput("buzzerbeepvals") //nombre de la entrada
        .setCheck("buzzer_beepvals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("buzzer"), "Name_M")
        .appendField(".beep");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Buzzer_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks['set_buzzer_beep'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Time on");
    this.appendValueInput("buzzerbeepvals1") //nombre de la entrada
        .setCheck("buzzer_beepvals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("Time off");
    this.appendValueInput("buzzerbeepvals2") //nombre de la entrada
        .setCheck("buzzer_beepvals1"); //tipo de entrada que acepta
    this.setOutput(true, "buzzer_beepvals");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_buzzer_beep_inp'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.Field16000("1000"), "buzzer_16000");
    this.setOutput(true, "buzzer_beepvals1");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['buzzer_playtone'] = {
  init: function() {
    this.appendValueInput("buzzerplaytonevals") //nombre de la entrada
        .setCheck("buzzer_playtonevals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("buzzer"), "Name_M")
        .appendField(".playtone");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Buzzer_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks['set_buzzer_playtone'] = {
  init: function() {
    this.appendValueInput("buzzerplaytonevals1") //nombre de la entrada
        .setCheck("buzzer_playtonevals1"); //tipo de entrada que acepta
    this.setOutput(true, "buzzer_playtonevals");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

Blockly.Blocks['set_buzzer_playtone2'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Note");
    this.appendValueInput("buzzerplaytonevals1") //nombre de la entrada
        .setCheck("buzzer_playtonevals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("Time");
    this.appendValueInput("buzzerplaytonevals2") //nombre de la entrada
        .setCheck("buzzer_beepvals1"); //tipo de entrada que acepta
    this.setOutput(true, "buzzer_playtonevals");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_buzzer_playtone_inp'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([
        ["NOTE_B1","NOTE_B1"],["NOTE_D2","NOTE_D2"],["NOTE_DS2","NOTE_DS2"],["NOTE_E2","NOTE_E2"],["NOTE_F2","NOTE_F2"],["NOTE_FS2","NOTE_FS2"],["NOTE_G2","NOTE_G2"],["NOTE_GS2","NOTE_GS2"],
        ["NOTE_A2","NOTE_A2"],["NOTE_AS2","NOTE_AS2"],["NOTE_B2","NOTE_B2"],["NOTE_C3","NOTE_C3"],["NOTE_CS3","NOTE_CS3"],["NOTE_D3","NOTE_D3"],["NOTE_DS3","NOTE_DS3"],["NOTE_E3","NOTE_E3"],
        ["NOTE_F3","NOTE_F3"],["NOTE_FS3","NOTE_FS3"],["NOTE_G3","NOTE_G3"],["NOTE_GS3","NOTE_GS3"],["NOTE_CS3","NOTE_CS3"],["NOTE_A3","NOTE_A3"],["NOTE_AS3","NOTE_AS3"],["NOTE_B3","NOTE_B3"],
        ["NOTE_C4","NOTE_C4"],["NOTE_CS4","NOTE_CS4"],["NOTE_D4","NOTE_D4"],["NOTE_DS4","NOTE_DS4"],["NOTE_E4","NOTE_E4"],["NOTE_F4","NOTE_F4"],["NOTE_FS4","NOTE_FS4"],["NOTE_G4","NOTE_G4"], 
        ["NOTE_GS4","NOTE_GS4"],["NOTE_A4","NOTE_A4"],["NOTE_AS4","NOTE_AS4"],["NOTE_B4","NOTE_B4"],["NOTE_C5","NOTE_C5"],["NOTE_CS5","NOTE_CS5"],["NOTE_D5","NOTE_D5"],["NOTE_DS5","NOTE_DS5"], 
        ["NOTE_E5","NOTE_E5"],["NOTE_F5","NOTE_F5"],["NOTE_FS5","NOTE_FS5"],["NOTE_G5","NOTE_G5"],["NOTE_GS5","NOTE_GS5"],["NOTE_A5","NOTE_A5"],["NOTE_AS5","NOTE_AS5"],["NOTE_B5","NOTE_B5"],                 
        ["NOTE_C6","NOTE_C6"],["NOTE_CS6","NOTE_CS6"],["NOTE_D6","NOTE_D6"],["NOTE_DS6","NOTE_DS6"],["NOTE_E6","NOTE_E6"],["NOTE_F6","NOTE_F6"],["NOTE_FS6","NOTE_FS6"],["NOTE_G6","NOTE_G6"],                 
        ["NOTE_GS6","NOTE_GS6"],["NOTE_A6","NOTE_A6"],["NOTE_AS6","NOTE_AS6"],["NOTE_B6","NOTE_B6"],["NOTE_C7","NOTE_C7"],["NOTE_CS7","NOTE_CS7"],["NOTE_D7","NOTE_D7"],["NOTE_DS7","NOTE_DS7"], 
        ["NOTE_E7","NOTE_E7"],["NOTE_F7","NOTE_F7"],["NOTE_FS7","NOTE_FS7"],["NOTE_G7","NOTE_G7"],["NOTE_GS7","NOTE_GS7"],["NOTE_A7","NOTE_A7"],["NOTE_AS7","NOTE_AS7"],["NOTE_B7","NOTE_B7"], 
        ["NOTE_C8","NOTE_C8"],["NOTE_CS8","NOTE_CS8"],["NOTE_D8","NOTE_D8"],["NOTE_DS8","NOTE_DS8"],["NOTE_DO","NOTE_DO"],["NOTE_RE","NOTE_RE"],["NOTE_MI","NOTE_MI"],["NOTE_FA","NOTE_FA"], 
        ["NOTE_SOL","NOTE_SOL"],["NOTE_LA","NOTE_LA"],["NOTE_SI","NOTE_SI"]]), "buzzer_note");
    this.setOutput(true, "buzzer_playtonevals1");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['buzzer_play'] = {
  init: function() {
    this.appendValueInput("buzzerplayvals") //nombre de la entrada
        .setCheck("buzzer_playvals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("buzzer"), "Name_M")
        .appendField(".play");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Buzzer_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

Blockly.Blocks['set_buzzer_play'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Note");
    this.appendValueInput("buzzerplayvals1") //nombre de la entrada
        .setCheck("buzzer_playtonevals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("Time on");
    this.appendValueInput("buzzerplayvals2") //nombre de la entrada
        .setCheck("buzzer_beepvals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("Time off");
    this.appendValueInput("buzzerplayvals3") //nombre de la entrada
        .setCheck("buzzer_beepvals1"); //tipo de entrada que acepta
    this.setOutput(true, "buzzer_playvals");  //tipo de salida
    this.setColour(Buzzer_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

//////////////*********************************////////////////////////////

/////////******************bloques botones**********////////////////////
var Boton_blocks_color = 100;  ///color para bloques de led
var Boton_input_color = 100;  ///color para input de leds
var Link_help = 'http://briko.cc/moduloboton/1';

Blockly.Blocks['boton_read'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldVariableModule("button"), "Name_M")
     .appendField(".read");
    this.appendValueInput("botonsetvals1") //nombre de la entrada
        .setCheck("boton_setvals1"); //tipo de entrada que acepta
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Boton_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['boton_read_set_inp'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldDropdown([["-", ""],["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"],["5", "5"]]), "Boton_num");
    this.setOutput(true, "boton_setvals1");  //tipo de salida
    this.setColour(Boton_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['boton_readbits'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldVariableModule("button"), "Name_M")
     .appendField(".readbits");
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Boton_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};
//////////////*********************************////////////////////////////

/////////******************bloques luz**********////////////////////
var Luz_input_color = 120;  ///color para input de leds

Blockly.Blocks['Luz_read'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldVariableModule("sensor1"), "Name_M")
     .appendField(".read");
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Luz_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

//////////////*********************************////////////////////////////

/////////******************bloques motor**********////////////////////

var Motor_blocks_color = 140;  ///color para bloques de led
var Motor_input_color = 140;  ///color para input de leds
var Link_help = 'http://briko.cc/modulomotor/1';

Blockly.Blocks['motor_set'] = {
  init: function() {
    this.appendValueInput("motorsetvals") //nombre de la entrada
        .setCheck("motor_setvals") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("motor"), "Name_M")
        .appendField(".set");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Motor_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};


Blockly.Blocks['set_motor_set'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Direction");
    this.appendValueInput("motorsetvals1") //nombre de la entrada
        .setCheck("motor_setvals1"); //tipo de entrada que acepta
    this.setOutput(true, "motor_setvals");  //tipo de salida
    this.setColour(Motor_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

Blockly.Blocks['set_motor_set2'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("Direction");
    this.appendValueInput("motorsetvals1") //nombre de la entrada
        .setCheck("motor_setvals1"); //tipo de entrada que acepta
    this.appendDummyInput()
    .appendField("Velocity");
    this.appendValueInput("motorsetvals2") //nombre de la entrada
        .setCheck("motor_setvals2"); //tipo de entrada que acepta
    this.setOutput(true, "motor_setvals");  //tipo de salida
    this.setColour(Motor_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_motor_set_inp'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([["LEFT","LEFT"],["RIGHT","RIGHT"],["STOP","STOP"]]), "Motor_inp");
    this.setOutput(true, "motor_setvals1");  //tipo de salida
    this.setColour(Motor_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_motor_set_inp2'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.Field255("20"), "Motor_inp2");
    this.setOutput(true, "motor_setvals2");  //tipo de salida
    this.setColour(Motor_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

//////////////*********************************////////////////////////////

/////////******************bloques perilla**********////////////////////
var Perilla_input_color = 160;  ///color para input de leds
var Link_help = 'http://briko.cc/moduloknob/1';

Blockly.Blocks['perilla_read'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldVariableModule("knob"), "Name_M")
     .appendField(".read");
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Perilla_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

//////////////*********************************////////////////////////////

/////////******************bloques distancia**********////////////////////
var Distance_blocks_color = 180;  ///color para bloques de led
var Distance_input_color = 180;  ///color para input de leds
var Link_help = 'http://briko.cc/modulodistancia/1';


Blockly.Blocks['distance_read'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldVariableModule("sensor3"), "Name_M")
     .appendField(".read");
    this.appendValueInput("distancereadvals1") //nombre de la entrada
        .setCheck("distance_readvals1"); //tipo de entrada que acepta
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Distance_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
    this.setInputsInline(true);  //los inputs en una sola linea
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['distance_read_inp'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldDropdown([["-", ""],["CM", "CM"], ["IN", "IN"]]), "Dis_num");
    this.setOutput(true, "distance_readvals1");  //tipo de salida
    this.setColour(Distance_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};
//////////////*********************************////////////////////////////

/////////******************bloques temperatura**********////////////////////
var Temperature_input_color = 200;  ///color para input de leds
var Link_help = 'http://briko.cc/modulotemperatura/1';

Blockly.Blocks['temperature_read'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldVariableModule("sensor2"), "Name_M")
     .appendField(".read");
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Temperature_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};
//////////////*********************************////////////////////////////

/////////******************bloques relevador**********////////////////////

var Relay_blocks_color = 220;  ///color para bloques de led
var Relay_input_color = 220;  ///color para input de leds
var Link_help = 'http://briko.cc/modulorelay/1';

Blockly.Blocks['relay_set'] = {
  init: function() {
    this.appendValueInput("relaysetvals") //nombre de la entrada
        .setCheck("relay_setvals1") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("relay"), "Name_M")
        .appendField(".set");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Relay_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_relay_set_inp'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([["ON","ON"],["OFF","OFF"]]), "relay_inp");
    this.setOutput(true, "relay_setvals1");  //tipo de salida
    this.setColour(Relay_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};
//////////////*********************************////////////////////////////

/////////******************bloques servo**********////////////////////

var Servo_blocks_color = 240;  ///color para bloques de led
var Servo_input_color = 240;  ///color para input de leds
var Link_help = 'http://briko.cc/moduloservo/1';

Blockly.Blocks['servo_set'] = {
  init: function() {
    this.appendValueInput("servosetvals") //nombre de la entrada
        .setCheck("servo_setvals1") //tipo de entrada que acepta
        .appendField(new Blockly.FieldVariableModule("servo"), "Name_M")
        .appendField(".set");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Servo_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  },
  //esta funcion se usa en el variablesbriko.js
  getVars2: function() {
    return [this.getFieldValue('Name_M')];
  },
  //esta funcion se usa en el variablesbriko.js
  renameVar2: function(oldName, newName) {
    if (Blockly.Names.equals(oldName, this.getFieldValue('Name_M'))) {
      this.setFieldValue(newName, 'Name_M');
    }
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_servo_set_inp'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.Field180("90"), "servo_inp");
    this.setOutput(true, "servo_setvals1");  //tipo de salida
    this.setColour(Servo_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};
//////////////*********************************////////////////////////////

/////////******************bloques maestro**********////////////////////
var Maestro_blocks_color = 260;  ///color para bloques de led
var Maestro_input_color = 260;  ///color para input de leds
var Link_help = 'http://briko.cc/modulomaestro/1';

Blockly.Blocks['maestro_led'] = {
  init: function() {
    this.appendValueInput("maestroledvals") //nombre de la entrada
        .setCheck("maestro_ledvals1") //tipo de entrada que acepta
        .appendField("bk7led");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Maestro_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

///bloque que selecciona el segmento
Blockly.Blocks['set_maestro_led_inp'] = {
  init: function() {
    this.appendDummyInput()
     .appendField(new Blockly.FieldDropdown([["ON","ON"],["OFF","OFF"]]), "maestroled_inp");
    this.setOutput(true, "maestro_ledvals1");  //tipo de salida
    this.setColour(Maestro_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['maestro_write'] = {
  init: function() {
    this.appendValueInput("maestrowritevals") //nombre de la entrada
        .setCheck("String") //tipo de entrada que acepta
        .appendField("bk7write");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Maestro_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['maestro_print'] = {
  init: function() {
    this.appendValueInput("maestroprintvals") //nombre de la entrada
        .setCheck("String") //tipo de entrada que acepta
        .appendField("bk7print");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Maestro_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['maestro_read'] = {
  init: function() {
    this.appendDummyInput()
     .appendField("bk7read");
    this.setOutput(true, "Number");  //tipo de salida
    this.setColour(Maestro_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['maestro_read2'] = {
  init: function() {
    this.appendDummyInput()
    .appendField("bk7read")
    .appendField(new Blockly.FieldTextInput("brikovar"), "Read_var")
    .appendField(new Blockly.Field25("5"), "Read_var_num");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Maestro_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['maestro_read2_ifvar'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldTextInput("brikovar"), "Read_var")
    .appendField(new Blockly.FieldDropdown([["==","=="],["!=","!="]]), "Read_varsig")
    .appendField(new Blockly.FieldTextInput("briko"), "Read_var2");
    this.setOutput(true, "Boolean");  //tipo de salida
    this.setColour(Maestro_input_color);
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};

Blockly.Blocks['maestro_clean'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("bk7clean");
    this.setPreviousStatement(true, null); //cualquier cosa se conecta arriba
    this.setNextStatement(true, null); //cualquier cosa se conecta abajo
    this.setColour(Maestro_blocks_color); //pone el color
    this.setTooltip('');
    this.setHelpUrl(Link_help);
  }
};


//////////////*********************************////////////////////////////





























