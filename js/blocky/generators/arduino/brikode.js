/**
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * http://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Arduino for control blocks.
 * @author gasolin@gmail.com  (Fred Lin)
 */
'use strict';

goog.provide('Blockly.Arduino.brikode');

goog.require('Blockly.Arduino');


//////funcion para la seguridad del nombre de los modulos//////
function Checkname(name) {
    if(name ==""){ name="name";}  //seguridad
    if(name.charAt(0) == "0" || name.charAt(0) == "1" || name.charAt(0) == "2" ||
       name.charAt(0) == "3" || name.charAt(0) == "4" || name.charAt(0) == "5" ||
       name.charAt(0) == "6" || name.charAt(0) == "7" || name.charAt(0) == "8" ||
       name.charAt(0) == "9" ){ 
    name = "_"+name.substring(1);
    }
    name = name.replace("#","_");name = name.replace(";","_");name = name.replace("+","_");name = name.replace("-","_");
    name = name.replace("*","_");name = name.replace("/","_");name = name.replace("^","_");name = name.replace("$","_");
    name = name.replace("!","_");name = name.replace("?","_");name = name.replace("~","_");name = name.replace("'","_");
    name = name.replace('"',"_");name = name.replace(".","_");name = name.replace(",","_");name = name.replace(":","_");
    name = name.replace("=","_");name = name.replace("@","_");name = name.replace("%","_");name = name.replace("[","_");
    name = name.replace("]","_");name = name.replace("{","_");name = name.replace("}","_");name = name.replace("(","_");
    name = name.replace(")","_");name = name.replace("|","_");name = name.replace("`","_");

    return name;              // The function returns the product of p1 and p2
}

////*****modulos*****/////

Blockly.Arduino.ledsbk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'ledsbk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.motorbk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'motorbk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.displaybk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'displaybk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.lightbk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'lightbk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.buzzerbk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'buzzerbk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.temperaturebk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'temperaturebk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.buttonsbk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'buttonsbk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.distancebk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'distancebk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.knobbk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'knobbk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.relaybk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'relaybk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}

Blockly.Arduino.servobk_m = function() {
  
 var name = this.getFieldValue('Name_M');
 name = Checkname(name);  //seguridad
 var port = this.getFieldValue('Port_M');
//con este lo pone arriba 
 Blockly.Arduino.setups_[name + port] = 'servobk '+name+'('+ port +');';
return (""); //no regresa nada al codigo principal
}


/////**************************************************///////


/////******funciones de led*****///////////////

//funcion de brillo
Blockly.Arduino.brightness = function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'brigvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(argument0 == "" || argument0 === null){ argument0="20";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  var code = name+'.brightness('+argument0+');\n';
  return code;
}

///bloque de input del brillo
Blockly.Arduino.set_255_val_led = function() {
  // Text value.
  var code = this.getFieldValue('Brig_c');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///funcion color
Blockly.Arduino.color = function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'colorvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="#FF0000";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///si se le pone que solo es a un led
    var led_num = "";
    var temp1 = (argument0).indexOf("lednum");
    if(temp1 >= 0){  //solo si hay numero de led
       led_num = (argument0).substring(temp1+6); //saca el numero
       led_num = led_num + ","; 
    }
    
    if((argument0).indexOf("block2")>=0){ //si es el bloque de variables
     var r = (argument0).indexOf("R_")+2;   
     var g = (argument0).indexOf("G_")+2;  
     var b = (argument0).indexOf("B_")+2;  
     var Red = (argument0).substring(r,g-2);
     var Green = (argument0).substring(g,b-2);
    if(temp1>0){
     var Blue = (argument0).substring(b,temp1);
    }else{ 
    var Blue = (argument0).substring(b);
    }
        
    }else{  //si es el bloque de seleccionar el color
    ///pasa de hex a rgb
    var Red = parseInt((argument0).substring(1,3),16);
    var Green = parseInt((argument0).substring(3,5),16);
    var Blue = parseInt((argument0).substring(5,7),16);
    }
    
  var code = name+'.color('+led_num + Red + ','+Green+','+Blue+ ');\n';
  return code;
}

///bloque de input de 1 solo color
Blockly.Arduino.set_color = function() {
  // Text value.
  var code = this.getFieldValue('Color_c');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///bloque de input color y led
Blockly.Arduino.set_color_num = function() {
  // Text value.
  var code = this.getFieldValue('Color_c');
  var arg0 = Blockly.Arduino.valueToCode(this, 'ledvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(arg0 == "" || arg0 === null){ arg0="1";}  //proteccion
  code = code + "lednum" + arg0 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///bloque selector de led
Blockly.Arduino.led_num_led = function() {
  // Text value.
  var code = this.getFieldValue('Led_num');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///bloque de input color y led
Blockly.Arduino.set_color_num2 = function() {
  var code = "block2";
    var arg1 = Blockly.Arduino.valueToCode(this, 'led_color_red', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="100";}  //proteccion
    var arg2 = Blockly.Arduino.valueToCode(this, 'led_color_green', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg2 == "" || arg2 === null){ arg2="100";}  //proteccion
    var arg3 = Blockly.Arduino.valueToCode(this, 'led_color_blue', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(arg3 == "" || arg3 === null){ arg3="100";}  //proteccion
  code = code+ "R_"+arg1+"G_"+arg2+"B_"+arg3 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///bloque de input color y led
Blockly.Arduino.set_color_num3 = function() {
  var code = "block2";
  var arg0 = Blockly.Arduino.valueToCode(this, 'ledvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="1";}  //proteccion
    var arg1 = Blockly.Arduino.valueToCode(this, 'led_color_red', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="100";}  //proteccion
    var arg2 = Blockly.Arduino.valueToCode(this, 'led_color_green', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg2 == "" || arg2 === null){ arg2="100";}  //proteccion
    var arg3 = Blockly.Arduino.valueToCode(this, 'led_color_blue', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(arg3 == "" || arg3 === null){ arg3="100";}  //proteccion
  code = code+ "R_"+arg1+"G_"+arg2+"B_"+arg3 + "lednum" + arg0 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}


//////////////*********************************////////////////////////////

/////////******************funciones display**********////////////////////

///funcion print
Blockly.Arduino.dis_print = function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'disprintvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="100";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  var code = name+'.print('+argument0+');\n';
  return code;
}

////entrada limitida de la funcion print
Blockly.Arduino.set_dis_print = function() {
  var code = this.getFieldValue('dis_print_c');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}



Blockly.Arduino.dis_printindi= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'disprintindivals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="arg11arg22";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///separa la data
     var arg1 = (argument0).indexOf("arg1")+4;   
     var arg2 = (argument0).indexOf("arg2")+4;  
    
    var val1 = (argument0).substring(arg1,arg2-4);
    var val2 = (argument0).substring(arg2);
    
  var code = name+'.printindividual('+val1+','+val2+');\n';
  return code;
}

///bloque de input color y led
Blockly.Arduino.set_dis_printindi = function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'segment_num', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="1";}  //proteccion
    var arg1 = Blockly.Arduino.valueToCode(this, 'dis_num', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="5";}  //proteccion

  var code = "arg1"+arg0+"arg2"+arg1 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.set_segment_num= function() {
  var code = this.getFieldValue('Seg_num');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el numero que mostarar el display
Blockly.Arduino.set_dis_num= function() {
  var code = this.getFieldValue('Seg_nump');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.dis_printcust= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'disprintcustvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="arg11arg22";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///separa la data
     var arg1 = (argument0).indexOf("arg1")+4;   
     var arg2 = (argument0).indexOf("arg2")+4;  
    
    var val1 = (argument0).substring(arg1,arg2-4);
    var val2 = (argument0).substring(arg2);
    
  var code = name+'.printcustom('+val1+','+val2+');\n';
  return code;
}

///bloque de input color y led
Blockly.Arduino.set_dis_printcust = function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'segmentcust_num', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="1";}  //proteccion
    var arg1 = Blockly.Arduino.valueToCode(this, 'discust_num', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="20";}  //proteccion

  var code = "arg1"+arg0+"arg2"+arg1 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.set_segmentcust_num= function() {
  var code = this.getFieldValue('Seg_num');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el numero que mostarar el display
Blockly.Arduino.set_discust_num= function() {
  var code = this.getFieldValue('Seg_nump');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.dis_erase= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'disprinterasevals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="1";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  var code = name+'.erase('+argument0+');\n';
  return code;
}

//////////////*********************************////////////////////////////

/////////******************funciones bocina**********////////////////////

Blockly.Arduino.buzzer_set= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'buzzersetvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="ON";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  var code = name+'.set('+argument0+');\n';
  return code;
}

///el que selecciona el segmento
Blockly.Arduino.set_buzzer_set_inp = function() {
  var code = this.getFieldValue('buzzer_state');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.buzzer_beep= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'buzzerbeepvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="arg11000arg21000";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///separa la data
     var arg1 = (argument0).indexOf("arg1")+4;   
     var arg2 = (argument0).indexOf("arg2")+4;  
    
    var val1 = (argument0).substring(arg1,arg2-4);
    var val2 = (argument0).substring(arg2);
    
  var code = name+'.beep('+val1+','+val2+');\n';
  return code;
}

///bloque de input color y led
Blockly.Arduino.set_buzzer_beep= function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'buzzerbeepvals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="1000";}  //proteccion
  var arg1 = Blockly.Arduino.valueToCode(this, 'buzzerbeepvals2', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="1000";}  //proteccion
  var code = "arg1"+arg0+"arg2"+arg1 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.set_buzzer_beep_inp = function() {
  var code = this.getFieldValue('buzzer_16000');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.buzzer_playtone= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'buzzerplaytonevals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="arg1NOTE_D2arg21000";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///separa la data
     var arg1 = (argument0).indexOf("arg1")+4;   
     var arg2 = (argument0).indexOf("arg2");  
    if(arg2 >=0){
        arg2 = arg2+4;
        var val1 = (argument0).substring(arg1,arg2-4);
        var val2 = (argument0).substring(arg2);
        var code = name+'.playtone('+val1+','+val2+');\n';
    }else{
        var val1 = (argument0).substring(arg1);
        var code = name+'.playtone('+val1+');\n';
    }
    
  return code;
}

///bloque de input color y led
Blockly.Arduino.set_buzzer_playtone= function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'buzzerplaytonevals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  var code = "arg1"+arg0;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///bloque de input color y led
Blockly.Arduino.set_buzzer_playtone2= function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'buzzerplaytonevals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="NOTE_D2";}  //proteccion
  var arg1 = Blockly.Arduino.valueToCode(this, 'buzzerplaytonevals2', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="1000";}  //proteccion
  var code = "arg1"+arg0+"arg2"+arg1 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.set_buzzer_playtone_inp = function() {
  var code = this.getFieldValue('buzzer_note');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.buzzer_play= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'buzzerplayvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="arg1NOTE_D2arg21000arg31000";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///separa la data
     var arg1 = (argument0).indexOf("arg1")+4;   
     var arg2 = (argument0).indexOf("arg2")+4; 
     var arg3 = (argument0).indexOf("arg3")+4; 
    
    var val1 = (argument0).substring(arg1,arg2-4);
    var val2 = (argument0).substring(arg2,arg3-4);
    var val3 = (argument0).substring(arg3);
    
  var code = name+'.play('+val1+','+val2+','+val3+');\n';
    
  return code;
}

///bloque de input color y led
Blockly.Arduino.set_buzzer_play= function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'buzzerplayvals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="NOTE_D2";}  //proteccion
  var arg1 = Blockly.Arduino.valueToCode(this, 'buzzerplayvals2', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="1000";}  //proteccion
  var arg2 = Blockly.Arduino.valueToCode(this, 'buzzerplayvals3', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg2 == "" || arg2 === null){ arg2="1000";}  //proteccion
  var code = "arg1"+arg0+"arg2"+arg1+"arg3"+arg2;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones botones**********////////////////////

 ///bloque de input color y led
Blockly.Arduino.boton_read= function() {
    
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  var arg0 = Blockly.Arduino.valueToCode(this, 'botonsetvals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0=" ";}  //proteccion
    
  var code = name+'.read('+arg0+')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.boton_read_set_inp = function() {
  var code = this.getFieldValue('Boton_num');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}   
    
Blockly.Arduino.boton_readbits = function() {
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  // Text value.
  var code = name+".readbits( )" ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones luz**********////////////////////

Blockly.Arduino.Luz_read = function() {
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
  // Text value.
  var code = name+".read( )" ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones motor**********////////////////////

Blockly.Arduino.motor_set= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'motorsetvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="arg1RIGHTarg2255";}  //proteccion
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
    ///separa la data
     var arg1 = (argument0).indexOf("arg1")+4;   
     var arg2 = (argument0).indexOf("arg2");  
    if(arg2 >=0){
        arg2 = arg2+4;
        var val1 = (argument0).substring(arg1,arg2-4);
        var val2 = (argument0).substring(arg2);
        var code = name+'.set('+val1+','+val2+');\n';
    }else{
        var val1 = (argument0).substring(arg1);
        var code = name+'.set('+val1+');\n';
    }
    
  return code;
}

///bloque de input color y led
Blockly.Arduino.set_motor_set= function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'motorsetvals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(arg0 == "" || arg0 === null){ arg0="RIGHT";}  //proteccion
  var code = "arg1"+arg0;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///bloque de input color y led
Blockly.Arduino.set_motor_set2= function() {
  var arg0 = Blockly.Arduino.valueToCode(this, 'motorsetvals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0="RIGHT";}  //proteccion
  var arg1 = Blockly.Arduino.valueToCode(this, 'motorsetvals2', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg1 == "" || arg1 === null){ arg1="255";}  //proteccion
  var code = "arg1"+arg0+"arg2"+arg1 ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.set_motor_set_inp = function() {
  var code = this.getFieldValue('Motor_inp');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.set_motor_set_inp2 = function() {
  var code = this.getFieldValue('Motor_inp2');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones perilla**********////////////////////

Blockly.Arduino.perilla_read = function() {
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  // Text value.
  var code = name+".read( )" ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones distancia**********////////////////////

 ///bloque de input color y led
Blockly.Arduino.distance_read= function() {
    
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  var arg0 = Blockly.Arduino.valueToCode(this, 'distancereadvals1', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
    if(arg0 == "" || arg0 === null){ arg0=" ";}  //proteccion
    
  var code = name+'.read('+arg0+')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

///el que selecciona el segmento
Blockly.Arduino.distance_read_inp = function() {
  var code = this.getFieldValue('Dis_num');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}   

//////////////*********************************////////////////////////////

/////////******************funciones temperatura**********////////////////////

Blockly.Arduino.temperature_read = function() {
    ///consigue el nombre
    var name = this.getFieldValue('Name_M');
    name = Checkname(name);  //seguridad
    
  // Text value.
  var code = name+".read( )" ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones relevador**********////////////////////

Blockly.Arduino.relay_set= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'relaysetvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="ON";}  //proteccion
  ///consigue el nombre
  var name = this.getFieldValue('Name_M');
  name = Checkname(name);  //seguridad
    
  var code = name+'.set('+argument0+');\n';  
    
  return code;
}

///el que selecciona el segmento
Blockly.Arduino.set_relay_set_inp = function() {
  var code = this.getFieldValue('relay_inp');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones servo**********////////////////////

Blockly.Arduino.servo_set= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'servosetvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="90";}  //proteccion
  ///consigue el nombre
  var name = this.getFieldValue('Name_M');
  name = Checkname(name);  //seguridad
    
  var code = name+'.set('+argument0+');\n';  
    
  return code;
}

///el que selecciona el segmento
Blockly.Arduino.set_servo_set_inp = function() {
  var code = this.getFieldValue('servo_inp');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

//////////////*********************************////////////////////////////

/////////******************funciones maestro**********////////////////////

Blockly.Arduino.maestro_led= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'maestroledvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0="ON";}  //proteccion
  var code = 'bk7led('+argument0+');\n';
  return code;
}

Blockly.Arduino.set_maestro_led_inp = function() {
  var code = this.getFieldValue('maestroled_inp');
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.maestro_write= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'maestrowritevals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0='"briko"';}  //proteccion
  var code = 'bk7write('+argument0+');\n';
  return code;
}

Blockly.Arduino.maestro_print= function() {
  var argument0 = Blockly.Arduino.valueToCode(this, 'maestroprintvals', Blockly.Arduino.ORDER_ATOMIC); //obtiene el codigo del bloque de input
  if(argument0 == "" || argument0 === null){ argument0='"briko"';}  //proteccion
  var code = 'bk7print('+argument0+');\n';
  return code;
}

Blockly.Arduino.maestro_read = function() {
  // Text value.
  var code = "bk7read( )" ;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}

Blockly.Arduino.maestro_read2 = function() {
  var arg0 = this.getFieldValue('Read_var');
  if(arg0 == "" || arg0 === null){ arg0="brikovar";}  //proteccion  
  var arg1 = this.getFieldValue('Read_var_num');
  if(arg1 == "" || arg1 === null){ arg1="5";}  //proteccion 

  ////pone la  variable especial stringbk
  Blockly.Arduino.brikovars_[arg0] = '  stringbk '+arg0+';'; //esta de declaro en el arduino.js
    
  var code = "bk7read(&"+arg0+","+arg1+");\n" ;
  return code ;
}

Blockly.Arduino.maestro_read2_ifvar = function() {
  var arg0 = this.getFieldValue('Read_var');
  if(arg0 == "" || arg0 === null){ arg0="brikovar";}  //proteccion  
  var arg1 = this.getFieldValue('Read_varsig');
  if(arg1 == "" || arg1 === null){ arg1="==";}  //proteccion  
  var arg2 = this.getFieldValue('Read_var2');
  if(arg2 == "" || arg2 === null){ arg2="briko";}  //proteccion 

 var code = "";
////separa en caracteres el string
var leng_arg = arg2.length;
var conter = 0;
for(var x=0; x<(leng_arg-1); x++){
code = code + arg0+".stringbuffer["+x+"] "+arg1+" "+ arg2.charAt(x)+ " && ";   
conter++;
if(conter>=3){
code = code + "\n";   //para que no deje todo en una sola linea
conter=0;
}
}
code = code + arg0+".stringbuffer["+(leng_arg-1)+"] "+arg1+" "+ arg2.charAt((leng_arg-1));     
   
  return [code, Blockly.Arduino.ORDER_ATOMIC];
}   
    

Blockly.Arduino.maestro_clean = function() {
  // Text value.
  var code = "bk7clean( );\n" ;
  return code ;
}

//////////////*********************************////////////////////////////