/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2013 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Angle input field.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';
///hay que agregar esto al blocky.js
goog.provide('Blockly.Field255');

goog.require('Blockly.FieldTextInput');
goog.require('goog.math');
goog.require('goog.userAgent');


/**
 * Class for an editable angle field.
 * @param {string} text The initial content of the field.
 * @param {Function=} opt_changeHandler An optional function that is called
 *     to validate any constraints on what the user entered.  Takes the new
 *     text as an argument and returns the accepted text or null to abort
 *     the change.
 * @extends {Blockly.FieldTextInput}
 * @constructor
 */
Blockly.Field255 = function(text, opt_changeHandler) {
  // Add degree symbol: "360°" (LTR) or "°360" (RTL)
  this.symbol_ = Blockly.createSvgElement('tspan', {}, null);
  this.symbol_.appendChild(document.createTextNode('\u00B0'));

  Blockly.Field255.superClass_.constructor.call(this, text, null);
  this.setChangeHandler(opt_changeHandler);
};
goog.inherits(Blockly.Field255, Blockly.FieldTextInput);

/**
 * Sets a new change handler for angle field.
 * @param {Function} handler New change handler, or null.
 */
Blockly.Field255.prototype.setChangeHandler = function(handler) {
  var wrappedHandler;
  if (handler) {
    // Wrap the user's change handler together with the angle validator.
    var thisObj = this;
    wrappedHandler = function(value) {
      var v1 = handler.call(thisObj, value);
      if (v1 === null) {
        var v2 = v1;
      } else {
        if (v1 === undefined) {
          v1 = value;
        }
        var v2 = Blockly.Field255.angleValidator.call(thisObj, v1);
        if (v2 !== undefined) {
          v2 = v1;
        }
      }
      return v2 === value ? undefined : v2;
    };
  } else {
    wrappedHandler = Blockly.Field255.angleValidator;
  }
  Blockly.Field255.superClass_.setChangeHandler(wrappedHandler);
};

/**
 * Clone this Field255.
 * @return {!Blockly.Field255} The result of calling the constructor again
 *   with the current values of the arguments used during construction.
 */
Blockly.Field255.prototype.clone = function() {
  return new Blockly.Field255(this.getText(), this.changeHandler_);
};

/**
 * Round angles to the nearest 15 degrees when using mouse.
 * Set to 0 to disable rounding.
 */
Blockly.Field255.ROUND = 15;

/**
 * Half the width of protractor image.
 */
Blockly.Field255.HALF = 100 / 2;

/**
 * Radius of protractor circle.  Slightly smaller than protractor size since
 * otherwise SVG crops off half the border at the edges.
 */
Blockly.Field255.RADIUS = Blockly.Field255.HALF - 1;

/**
 * Clean up this Field255, as well as the inherited FieldTextInput.
 * @return {!Function} Closure to call on destruction of the WidgetDiv.
 * @private
 */
Blockly.Field255.prototype.dispose_ = function() {
  var thisField = this;
  return function() {
    Blockly.Field255.superClass_.dispose_.call(thisField)();
    thisField.gauge_ = null;
  };
};

/**
 * Show the inline free-text editor on top of the text.
 * @private
 */
Blockly.Field255.prototype.showEditor_ = function() {
  var noFocus =
      goog.userAgent.MOBILE || goog.userAgent.ANDROID || goog.userAgent.IPAD;
  // Mobile browsers have issues with in-line textareas (focus & keyboards).
  Blockly.Field255.superClass_.showEditor_.call(this, noFocus);
  var div = Blockly.WidgetDiv.DIV;
  if (!div.firstChild) {
    // Mobile interface uses window.prompt.
    return;
  }
  // Build the SVG DOM.
  var svg = Blockly.createSvgElement('svg', {
    'xmlns': 'http://www.w3.org/2000/svg',
    'xmlns:html': 'http://www.w3.org/1999/xhtml',
    'xmlns:xlink': 'http://www.w3.org/1999/xlink',
    'version': '1.1',
    'height': (Blockly.Field255.HALF * 2) + 'px',
    'width': (Blockly.Field255.HALF * 2) + 'px'
  }, div);
  
  svg.style.marginLeft = '-35px';
  
};

/**
 * Set the angle to match the mouse's position.
 * @param {!Event} e Mouse move event.
 */
Blockly.Field255.prototype.onMouseMove = function(e) {
  var bBox = this.gauge_.ownerSVGElement.getBoundingClientRect();
  var dx = e.clientX - bBox.left - Blockly.Field255.HALF;
  var dy = e.clientY - bBox.top - Blockly.Field255.HALF;
  var angle = Math.atan(-dy / dx);
  if (isNaN(angle)) {
    // This shouldn't happen, but let's not let this error propogate further.
    return;
  }
  angle = goog.math.toDegrees(angle);
  // 0: East, 90: North, 180: West, 270: South.
  if (dx < 0) {
    angle += 180;
  } else if (dy > 0) {
    angle += 360;
  }
  if (Blockly.Field255.ROUND) {
    angle = Math.round(angle / Blockly.Field255.ROUND) *
        Blockly.Field255.ROUND;
  }
  if (angle >= 360) {
    // Rounding may have rounded up to 360.
    angle -= 360;
  }
  var angle = parseInt((angle * 255)/360);
  angle = String(angle);
  Blockly.FieldTextInput.htmlInput_.value = angle;
  this.setText(angle);
};

/**
 * Insert a degree symbol.
 * @param {?string} text New text.
 */
Blockly.Field255.prototype.setText = function(text) {
  Blockly.Field255.superClass_.setText.call(this, text);
  if (!this.textElement_) {
    // Not rendered yet.
    return;
  }
  this.updateGraph_();
  
  // Cached width is obsolete.  Clear it.
  this.size_.width = 0;
};

/**
 * Redraw the graph with the current angle.
 * @private
 */
Blockly.Field255.prototype.updateGraph_ = function() {
  if (!this.gauge_) {
    return;
  }
  var angleRadians = goog.math.toRadians(Number(this.getText()));
  if (isNaN(angleRadians)) {
    this.gauge_.setAttribute('d',
        'M ' + Blockly.Field255.HALF + ', ' + Blockly.Field255.HALF);
    this.line_.setAttribute('x2', Blockly.Field255.HALF);
    this.line_.setAttribute('y2', Blockly.Field255.HALF);
  } else {
    var x = Blockly.Field255.HALF + Math.cos(angleRadians) *
        Blockly.Field255.RADIUS;
    var y = Blockly.Field255.HALF + Math.sin(angleRadians) *
        -Blockly.Field255.RADIUS;
    var largeFlag = (angleRadians > Math.PI) ? 1 : 0;
    this.gauge_.setAttribute('d',
        'M ' + Blockly.Field255.HALF + ', ' + Blockly.Field255.HALF +
        ' h ' + Blockly.Field255.RADIUS +
        ' A ' + Blockly.Field255.RADIUS + ',' + Blockly.Field255.RADIUS +
        ' 0 ' + largeFlag + ' 0 ' + x + ',' + y + ' z');
    this.line_.setAttribute('x2', x);
    this.line_.setAttribute('y2', y);
  }
};

/**
 * Ensure that only an angle may be entered.
 * @param {string} text The user's text.
 * @return {?string} A string representing a valid angle, or null if invalid.
 */
Blockly.Field255.angleValidator = function(text) {
  var n = Blockly.FieldTextInput.numberValidator(text);
  if (n !== null) {
    
    if (n < 0) {
      n = 0;
    }
    if (n > 255) {
      n = 255;
    }
    n = String(n);
   }
  return n;
};
