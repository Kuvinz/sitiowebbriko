//detecta cambio en scroll en pantalla
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    var windowswidth = $(window).width();
    if (scroll >= 50) {
        $(".important-class").addClass("important-class2");
    }
    if (scroll < 50) {
        if(windowswidth <800){
            $(".important-class").addClass("important-class2");
        }else{
        $(".important-class").removeClass("important-class2");
        }
    }
});

//detecta cambio de ancho en pantalla
var width = $(window).width();
$(window).resize(function(){
   if($(this).width() != width){
      width = $(this).width();
      var scroll2 = $(window).scrollTop();
      if (width >= 800) {
          if(scroll2 <50){
            $(".important-class").removeClass("important-class2");
        }else{
        $(".important-class").addClass("important-class2");
        }
    }
    if (width < 800) {
        $(".important-class").addClass("important-class2");
    }
   }
});