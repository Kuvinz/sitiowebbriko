    var xml_grammar = {

      // prefix ID for regular expressions used in the grammar
    "RegExpID" : "RegExp::",
    
    //
    // Style model
    "Style" : {
        // lang token type  -> Editor (style) tag
        "comment":    "comment",
        "atom":       "constant",
        "keyword":    "keyword",
        "this":       "keyword",
        "builtin":    "support",
        "operator":   "operator",
        "identifier": "identifier",
        "property":   "constant.support",
        "number":     "operator",//"constant.numeric",
        "string":     "string",
        "regex":      "string.regexp"
    },

    
    //
    // Lexical model
    "Lex" : {
        
        // comments
        "comment" : {
            "type" : "comment",
            "tokens" : [
                // line comment
                // start, end delims  (null matches end-of-line)
                [  "//",  null ],
                // block comments
                // start,  end    delims
                [  "/*",   "*/" ]
            ]
        },
        
        // general identifiers
        "identifier" : "RegExp::/[_A-Za-z$][_A-Za-z0-9$]*/",
        
        "this" : "RegExp::/this\\b/",
        
        "property" : "RegExp::/[_A-Za-z$][_A-Za-z0-9$]*/",
        
        // numbers, in order of matching
        "number" : [
            // floats
            "RegExp::/\\d*\\.\\d+(e[\\+\\-]?\\d+)?/",
            "RegExp::/\\d+\\.\\d*/",
            "RegExp::/\\.\\d+/",
            // integers
            // hex
            "RegExp::/0x[0-9a-fA-F]+L?/",
            // binary
            "RegExp::/0b[01]+L?/",
            // octal
            "RegExp::/0o[0-7]+L?/",
            // decimal
            "RegExp::/[1-9]\\d*(e[\\+\\-]?\\d+)?L?/",
            // just zero
            "RegExp::/0(?![\\dx])/"
        ],

        // usual strings
        "string" : {
            "type" : "escaped-block",
            "escape" : "\\",
            // start, end of string (can be the matched regex group ie. 1 )
            "tokens" : [ "RegExp::/(['\"])/",   1 ]
        },
        
        // literal regular expressions
        "regex" : {
            "type" : "escaped-block",
            "escape" : "\\",
            // javascript literal regular expressions can be parsed similar to strings
            "tokens" : [ "/",    "RegExp::#/[gimy]{0,4}#" ]
        },
        
        // operators
        "operator" : {
            "combine" : true,
            "tokens" : [
                "\\", "+", "-", "*", "/", "%", "&", "|", "^", "~", "<", ">" , "!",
                "||", "&&", "==", "!=", "<=", ">=", "<>", ">>", "<<",
                "===", "!==", "<<<", ">>>" 
            ]
        },
        
        // delimiters
        "delimiter" : {
            "combine" : true,
            "tokens" : [
                "(", ")", "[", "]", "{", "}", ",", "=", ";", "?", ":",
                "+=", "-=", "*=", "/=", "%=", "&=", "|=", "^=", "++", "--",
                ">>=", "<<="
            ]
        },
            
        // atoms
        "atom" : {
            // enable autocompletion for these tokens, with their associated token ID
            "autocomplete" : true,
            "tokens" : [
                "true", "false","ON","OFF","RIGHT","LEFT","STOP",
                "PORT1","PORT2","PORT3","PORT4","PORT5","PORT6","PORT7",
                "RED","BLUE","RED","GREEN","PINK","WHITE", "ORANGE", "BLACK",
                "LIME","YELLOW","AQUA","MAGENTA","GRAY","MAROON","OLIVE","PURPLE",
                "TEAL","NAVY","BROWN","CM","IN","F","C","NOTE_B1","NOTE_D2","NOTE_DS2","NOTE_E2","NOTE_F2","NOTE_FS2","NOTE_G2","NOTE_GS2","NOTE_A2","NOTE_AS2","NOTE_B2",
                "NOTE_C3","NOTE_CS3","NOTE_D3","NOTE_DS3","NOTE_E3","NOTE_F3","NOTE_FS3","NOTE_G3","NOTE_GS3","NOTE_A3","NOTE_AS3",
                "NOTE_B3","NOTE_C4","NOTE_CS4","NOTE_D4","NOTE_DS4","NOTE_E4","NOTE_F4","NOTE_FS4","NOTE_G4","NOTE_GS4","NOTE_A4",
                "NOTE_AS4","NOTE_B4","NOTE_C5","NOTE_CS5","NOTE_D5","NOTE_DS5","NOTE_E5","NOTE_F5","NOTE_FS5","NOTE_G5","NOTE_GS5",
                "NOTE_A5","NOTE_AS5","NOTE_B5","NOTE_C6","NOTE_CS6","NOTE_D6","NOTE_DS6","NOTE_E6","NOTE_F6","NOTE_FS6","NOTE_G6",
                "NOTE_GS6","NOTE_A6","NOTE_AS6","NOTE_B6","NOTE_C7","NOTE_CS7","NOTE_D7","NOTE_DS7","NOTE_E7","NOTE_F7","NOTE_FS7",
                "NOTE_G7","NOTE_GS7","NOTE_A7","NOTE_AS7","NOTE_B7","NOTE_C8","NOTE_CS8","NOTE_D8","NOTE_DS8","NOTE_DO","NOTE_RE",
                "NOTE_MI","NOTE_FA","NOTE_SOL","NOTE_LA","NOTE_SI",
"KEY_LEFT_CTRL","KEY_LEFT_SHIFT","KEY_LEFT_ALT",        "KEY_LEFT_GUI","KEY_RIGHT_CTRL","KEY_RIGHT_SHIFT","KEY_RIGHT_ALT","KEY_RIGHT_GUI", "KEY_UP_ARROW","KEY_DOWN_ARROW","KEY_LEFT_ARROW", "KEY_RIGHT_ARROW", "KEY_BACKSPACE","KEY_TAB","KEY_RETURN","KEY_ESC","KEY_INSERT","KEY_DELETE", "KEY_PAGE_UP","KEY_PAGE_DOWN", "KEY_HOME" , "KEY_END" , "KEY_CAPS_LOCK","KEY_F1","KEY_F2", "KEY_F3", "KEY_F4", "KEY_F5", "KEY_F6", "KEY_F7",
"KEY_F8","KEY_F9","KEY_F10", "KEY_F11" ,"KEY_F12",
"MOUSE_LEFT","MOUSE_RIGHT", "MOUSE_MIDDLE"
            ]
        },

        // keywords
        "keyword" : {
            // enable autocompletion for these tokens, with their associated token ID
            "autocomplete" : true,
            "tokens" : [ 
                "if", "while", "else", "return", "break","delay",
                "for", "switch", "case", "default","code", 
                "bk7print","bk7read","bk7led","bk7write","bk7clean",
                "motorbk","lightbk","buzzerbk","distancebk",
                "buttonsbk","knobbk","temperaturebk","displaybk","servobk","relaybk",
                "ledsbk","color","brightness","read","set",
                "bk7keywrite","bk7keyprint","bk7keypress","bk7keyrelease",
                "bk7keyreleaseall","bk7keybuttons","bk7keyend",
                "bk7mouseclick","bk7mousemove","bk7mousepress","bk7mouseend",
                "bk7mouserelease"
            ]
        },
        
        // builtins
        "builtin" : {
            // enable autocompletion for these tokens, with their associated token ID
            "autocomplete" : true,
            "tokens" : [ 
                "int","float","double","byte","char","stringbk"
            ]
        }
    },
    
    //
    // Syntax model (optional)
    "Syntax" : {
        
        "dotProperty" : {
            "type" : "group",
            "match" : "all",
            "tokens" : [ ".", "property" ]
        },
        
        "builtinOrIdentifier" : {
            "type" : "group",
            "match" : "either",
            "tokens" : [ "}", ")", "this", "builtin", "identifier", "dotProperty" ]
        },
        
        "dotProperties" : {
            "type" : "group",
            "match" : "zeroOrMore",
            "tokens" : [ "dotProperty" ]
        },
        
        "builtinOrIdentifierWithProperties" : {
            "type" : "n-gram",
            "tokens" : [
                [ "builtinOrIdentifier", "dotProperties" ]
            ]
        }
    },

    // what to parse and in what order
    "Parser" : [
        "comment",
        "number",
        "string",
        "regex",
        "keyword",
        "operator",
        "atom",
        "builtinOrIdentifierWithProperties"
    ]
};
    